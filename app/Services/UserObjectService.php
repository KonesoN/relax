<?php

namespace App\Services;

use App\Models\Hotel;
use App\Models\HotelRoom;
use App\Models\HotelRoomBooking;
use App\Models\Rest;
use App\Models\RestHotelRoom;
use App\Models\RestHotelRoomBooking;
use App\Models\RestObject;
use App\Models\RestObjectBooking;
use App\Models\UserObject;
use App\Models\UserObjectBooking;
use App\Models\UserObjectFilter;
use App\Models\UserObjectGallery;

class UserObjectService
{
    static function update($item, $data)
    {
        switch ($item->type) {
            case UserObject::TYPE_COTTAGE:
                $item->cottage->update($data['cottage']);
                break;
            case UserObject::TYPE_HOTEL:
                if (empty($data['hotel']['room_ids'])) {
                    $data['hotel']['room_ids'] = [];
                }
                HotelRoom::whereNotIn('id', $data['hotel']['room_ids'])->where('hotel_id', $item->hotel->id)
                    ->delete();
                HotelRoom::whereNull('hotel_id')->whereIn('id', $data['hotel']['room_ids'])
                    ->where('hash', $data['hash_object'])
                    ->update(['hotel_id' => $item->hotel->id]);
                break;
            case UserObject::TYPE_REST:
                if (empty($data['rest']['rest_ids'])) {
                    $data['rest']['rest_ids'] = [];
                }
                RestObject::whereNotIn('id', $data['rest']['rest_ids'])->where('rest_id', $item->rest->id)
                    ->delete();
                RestObject::whereNull('rest_id')->whereIn('id', $data['rest']['rest_ids'])
                    ->where('hash', $data['hash_object'])
                    ->update(['rest_id' => $item->rest->id]);
                break;
        }
        $data['item']['is_shore'] = !empty($data['item']['is_shore']) ? 1 : 0;
        $data['item']['is_pets'] = !empty($data['item']['is_pets']) ? 1 : 0;
        $item->update($data['item']);

        UserObjectFilter::where(['object_id' => $item->id])->delete();
        foreach ($data['filters'] ?? [] as $filter) {
            UserObjectFilter::create(['object_id' => $item->id, 'value' => $filter]);
        }

        $gallery_ids = $data['gallery'];
        $new_gallery_ids = [];
        foreach ($gallery_ids as $id) {
            $uog = UserObjectGallery::where([
                'type_id' => UserObjectGallery::TYPE_PHOTO,
                'id' => $id,
            ])->first();
            if ($uog->object_id == $item->id || !$uog->object_id) {
                $new_gallery_ids[] = $uog->id;
            }
        }
        $gallery_ids = $new_gallery_ids;
        UserObjectGallery::where([
            'type_id' => UserObjectGallery::TYPE_PHOTO,
            'object_id' => $item->id,
        ])->whereNotIn('id', $gallery_ids)->delete();

        $uog = UserObjectGallery::where([
            'type_id' => UserObjectGallery::TYPE_YOUTUBE_URL,
            'object_id' => $item->id,
        ])->first();
        if ($video_url = $data['video_url']) {
            if ($uog) {
                $uog->update(['value' => $video_url]);
            } else {
                $uog = UserObjectGallery::create([
                    'type_id' => UserObjectGallery::TYPE_YOUTUBE_URL,
                    'value' => $video_url,
                    'hash' => $data['hash_object']
                ]);
                $gallery_ids[] = $uog->id;
            }
        } else {
            if ($uog) {
                $uog->delete();
            }
        }
        UserObjectGallery::whereNull('object_id')->whereIn('id', $gallery_ids)
            ->where('hash', $data['hash_object'])
            ->update(['object_id' => $item->id, 'hash' => null]);

        $gallery_is_main = !empty($data['item']['gallery_is_main']) ? $data['item']['gallery_is_main'] : [];
        $is_main = false;
        foreach ($gallery_is_main as $gim => $value) {
            if ($value) {
                if ($uog = UserObjectGallery::where(['id' => $gim, 'object_id' => $item->id])->first()) {
                    UserObjectGallery::where(['object_id' => $item->id])->update(['is_main' => 0]);
                    $uog->update(['is_main' => 1]);
                    $is_main = true;
                    break;
                }
            }
        }
        if (!$is_main) {
            $uog = UserObjectGallery::where([
                'object_id' => $item->id,
                'type_id' => UserObjectGallery::TYPE_PHOTO
            ])->first();
            if ($uog) {
                UserObjectGallery::where(['object_id' => $item->id])->update(['is_main' => 0]);
                $uog->update(['is_main' => 1]);
            }
        }
    }

    static function bookingObjectByDates(
        $item,
        $departure_at,
        $arrival_at,
        $full_name = 'Добавлено вручную',
        $phone = null,
        $email = null
    ) {
        $departure_at = Date('Y-m-d', strtotime($departure_at));
        $arrival_at = Date('Y-m-d', strtotime($arrival_at));
        $days = round((strtotime($departure_at . ' 23:59:59') - strtotime($arrival_at . ' 00:00:00')) / 86400);
        if ($days == 1) {
            UserObjectBooking::create([
                'object_id' => $item->id,
                'booking_at' => $arrival_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
        } elseif ($days <= 2) {
            UserObjectBooking::create([
                'object_id' => $item->id,
                'booking_at' => $arrival_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
            UserObjectBooking::create([
                'object_id' => $item->id,
                'booking_at' => $departure_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
        } else {
            for ($i = 0; $i < $days; $i++) {
                $d = Date('Y-m-d', strtotime($arrival_at . "+ {$i} days"));
                if (strtotime($d) >= strtotime(Date('Y-m-d'))) {
                    UserObjectBooking::create([
                        'object_id' => $item->id,
                        'booking_at' => $d,
                        'full_name' => $full_name,
                        'phone' => $phone,
                        'email' => $email,
                    ]);
                }
            }
        }
    }

    static function bookingHotelRoomByDates(
        $item,
        $departure_at,
        $arrival_at,
        $full_name = 'Добавлено вручную',
        $phone = null,
        $email = null
    ) {
        $departure_at = Date('Y-m-d', strtotime($departure_at));
        $arrival_at = Date('Y-m-d', strtotime($arrival_at));
        $days = round((strtotime($departure_at . ' 23:59:59') - strtotime($arrival_at . ' 00:00:00')) / 86400);
        if ($days == 1) {
            HotelRoomBooking::create([
                'room_id' => $item->id,
                'booking_at' => $arrival_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
        } elseif ($days <= 2) {
            HotelRoomBooking::create([
                'room_id' => $item->id,
                'booking_at' => $arrival_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
            HotelRoomBooking::create([
                'room_id' => $item->id,
                'booking_at' => $departure_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
        } else {
            for ($i = 0; $i < $days; $i++) {
                $d = Date('Y-m-d', strtotime($arrival_at . "+ {$i} days"));
                if (strtotime($d) >= strtotime(Date('Y-m-d'))) {
                    HotelRoomBooking::create([
                        'room_id' => $item->id,
                        'booking_at' => $d,
                        'full_name' => $full_name,
                        'phone' => $phone,
                        'email' => $email,
                    ]);
                }
            }
        }
        $hotel = Hotel::find($item->hotel_id);
        if ($hotel) {
            self::handlerHotel($hotel);
        }
    }

    static function handlerHotel($hotel)
    {
        if ($hotel) {
            UserObjectBooking::where('object_id', $hotel->object_id)->delete();
            $count_rooms = count($hotel->rooms);
            $dates = [];
            foreach ($hotel->rooms as $room) {
                foreach ($room->BusyDates as $db) {
                    $dates[$db] = !empty($dates[$db]) ? $dates[$db] + 1 : 1;
                }
            }
            foreach ($dates as $date => $count) {
                if ($count_rooms == $count) {
                    UserObjectBooking::create([
                        'object_id' => $hotel->object_id,
                        'booking_at' => $date,
                    ]);
                }
            }
        }
    }

    static function bookingRestHotelRoomByDates(
        $item,
        $departure_at,
        $arrival_at,
        $full_name = 'Добавлено вручную',
        $phone = null,
        $email = null
    ) {
        $departure_at = Date('Y-m-d', strtotime($departure_at));
        $arrival_at = Date('Y-m-d', strtotime($arrival_at));
        $days = round((strtotime($departure_at . ' 23:59:59') - strtotime($arrival_at . ' 00:00:00')) / 86400);
        if ($days == 1) {
            RestHotelRoomBooking::create([
                'room_id' => $item->id,
                'booking_at' => $arrival_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
        } elseif ($days <= 2) {
            RestHotelRoomBooking::create([
                'room_id' => $item->id,
                'booking_at' => $arrival_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
            RestHotelRoomBooking::create([
                'room_id' => $item->id,
                'booking_at' => $departure_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
        } else {
            for ($i = 0; $i < $days; $i++) {
                $d = Date('Y-m-d', strtotime($arrival_at . "+ {$i} days"));
                if (strtotime($d) >= strtotime(Date('Y-m-d'))) {
                    RestHotelRoomBooking::create([
                        'room_id' => $item->id,
                        'booking_at' => $d,
                        'full_name' => $full_name,
                        'phone' => $phone,
                        'email' => $email,
                    ]);
                }
            }
        }
        $rest_hotel_room = RestHotelRoom::find($item->room_id);
        if ($rest_hotel_room) {
            $rest_object = RestObject::find($rest_hotel_room->rest_object_id);
            if ($rest_object) {
                $rest = Rest::find($rest_object->rest_id);
                self::handlerRest($rest);
            }
        }
    }

    static function bookingRestObjectByDates(
        $item,
        $departure_at,
        $arrival_at,
        $full_name = 'Добавлено вручную',
        $phone = null,
        $email = null
    ) {
        $departure_at = Date('Y-m-d', strtotime($departure_at));
        $arrival_at = Date('Y-m-d', strtotime($arrival_at));
        $days = round((strtotime($departure_at . ' 23:59:59') - strtotime($arrival_at . ' 00:00:00')) / 86400);
        if ($days == 1) {
            RestObjectBooking::create([
                'rest_object_id' => $item->id,
                'booking_at' => $arrival_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
        } elseif ($days <= 2) {
            RestObjectBooking::create([
                'rest_object_id' => $item->id,
                'booking_at' => $arrival_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
            RestObjectBooking::create([
                'rest_object_id' => $item->id,
                'booking_at' => $departure_at,
                'full_name' => $full_name,
                'phone' => $phone,
                'email' => $email,
            ]);
        } else {
            for ($i = 0; $i < $days; $i++) {
                $d = Date('Y-m-d', strtotime($arrival_at . "+ {$i} days"));
                if (strtotime($d) >= strtotime(Date('Y-m-d'))) {
                    RestObjectBooking::create([
                        'rest_object_id' => $item->id,
                        'booking_at' => $d,
                        'full_name' => $full_name,
                        'phone' => $phone,
                        'email' => $email,
                    ]);
                }
            }
        }
        if ($item) {
            $rest = Rest::find($item->rest_id);
            self::handlerRest($rest);
        }
    }

    static function handlerRest($rest)
    {
        if ($rest) {
            UserObjectBooking::where('object_id', $rest->object_id)->delete();
            $count_cottages = $rest->cottages->count();
            $count_hotel_rooms = 0;
            foreach ($rest->hotels as $hotel) {
                $count_hotel_rooms += $hotel->rooms->count();
            }
            $count_total = $count_hotel_rooms + $count_cottages;
            $dates = [];
            foreach ($rest->cottages as $cottage) {
                foreach ($cottage->BusyDates as $db) {
                    $dates[$db] = !empty($dates[$db]) ? $dates[$db] + 1 : 1;
                }
            }
            foreach ($rest->hotels as $hotel) {
                foreach ($hotel->rooms as $room) {
                    foreach ($room->BusyDates as $db) {
                        $dates[$db] = !empty($dates[$db]) ? $dates[$db] + 1 : 1;
                    }
                }
            }
            foreach ($dates as $date => $count) {
                if ($count_total == $count) {
                    UserObjectBooking::create([
                        'object_id' => $rest->object_id,
                        'booking_at' => $date,
                    ]);
                }
            }
        }
    }
}
