<?php

namespace App\Mail;

use App\Models\Cottage;
use App\Models\UserObject;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingConfirm extends Mailable
{
    use Queueable, SerializesModels;

    public $cb;
    public $type;

    public function __construct($cb, $type = 'cottage')
    {
        $this->cb = $cb;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $c = Cottage::find($this->cb->cottage_id);
        $uo = UserObject::find($c->object_id);
        return $this->view('mail.bookingConfirm', ['item' => $this->cb, 'uo' => $uo, 'type' => $this->type]);
    }
}
