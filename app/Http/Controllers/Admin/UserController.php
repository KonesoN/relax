<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\TransactionUserRefill;
use App\Models\TransactionUserWithdrawal;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public $basicModel = User::class;
    public $basicView = 'user';

    public function index(Request $request)
    {
        $query = $this->basicModel::query();
        $filters = [
            'is_active' => $request->get('is_active'),
            'type_id' => $request->get('type_id'),
            'search_field' => $request->get('search_field'),
            'search_text' => $request->get('search_text'),
        ];
        if (!empty($filters['search_text']) && !empty($filters['search_field'])) {
            $query->where($filters['search_field'], 'LIKE', "%{$filters['search_text']}%");
        }
        if (!empty($filters['is_active'])) {
            $query->where('is_active', $filters['is_active'] == 'true' ? 1 : 0);
        }
        if (!empty($filters['type_id'])) {
            $query->where('type_id', $filters['type_id']);
        }

        return view("admin.$this->basicView.index",
            [
                'pagination' => $query->paginate('10', ['*'], 'page', $request->get('page')),
                'filters' => $filters
            ]
        );
    }

    public function action(Request $request, User $item)
    {
        switch ($request->get('action')) {
            case 'active':
                $item->update(['is_active' => 1]);
                break;
            case 'inactive':
                $item->update(['is_active' => 0]);
                break;
            case 'delete':
                $item->delete();
                break;
        }
        return redirect(route("admin.$this->basicView.index"));
    }

    public function form(Request $request, User $item)
    {
        if ($request->isMethod('post')) {
            $data = $request->only(['first_name', 'last_name', 'phone', 'email']);
            $data['phone'] = '+' . preg_replace('/[^0-9]+/', '', trim(($data['phone'] ?? ''), " '"));
            $item->update($data);
            $balance = intval($request->get('balance', $item->balance));
            if ($balance !== $item->balance) {
                if ($balance > $item->balance) {
                    $transaction = Transaction::create([
                        'user_id' => $item->id,
                        'type_id' => Transaction::TYPE_USER_REFILL,
                        'method_id' => Transaction::METHOD_BALANCE,
                        'amount' => $balance - $item->balance,
                        'comment' => 'Пополнение администратором №' . Auth::id()
                    ]);
                    TransactionUserRefill::create([
                        'transaction_id' => $transaction->id,
                    ]);
                } elseif ($balance < $item->balance) {
                    $transaction = Transaction::create([
                        'user_id' => $item->id,
                        'type_id' => Transaction::TYPE_USER_WITHDRAWAL,
                        'method_id' => Transaction::METHOD_BALANCE,
                        'amount' => $item->balance - $balance,
                        'comment' => 'Списание администратором №' . Auth::id()
                    ]);
                    TransactionUserWithdrawal::create([
                        'transaction_id' => $transaction->id,
                    ]);
                }
                $item->update(['balance' => $balance]);
            }
            return redirect(route("admin.$this->basicView.form", $item->id));
        }
        return view("admin.$this->basicView.form", ['item' => $item]);
    }
}
