<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserObject;
use App\Services\UserObjectService;
use Illuminate\Http\Request;

class UserObjectController extends Controller
{
    public $basicModel = UserObject::class;
    public $basicView = 'userObject';

    public function index(Request $request)
    {
        $query = $this->basicModel::query();
        $filters = [
            'status_id' => $request->get('status_id'),
            'search_text' => $request->get('search_text'),
            'search_type' => $request->get('search_type'),
            'type' => $request->get('type')
        ];
        if (!empty($filters['search_text'])) {
            $query->where($filters['search_type'], $filters['search_text']);
        }
        if (!empty($filters['status_id']) && in_array($filters['status_id'], UserObject::STATUS_IDS)) {
            $query->where('status_id', $filters['status_id']);
        }
        if (!empty($filters['type']) && in_array($filters['type'], array_keys(UserObject::TYPES))) {
            $query->where('type', $filters['type']);
        }

        return view("admin.$this->basicView.index",
            [
                'pagination' => $query->paginate('10', ['*'], 'page', $request->get('page')),
                'filters' => $filters
            ]
        );
    }

    public function form(Request $request, UserObject $item)
    {
        if ($request->isMethod('post')) {
            $data_uo = $request->only([
                'title',
                'address',
                'distance_to_city',
                'is_shore',
                'district_id',
                'direction_id',
                'is_the_city',
                'price_type_id',
                'count_view',
                'count_view_today',
                'arrival_at',
                'departure_at',
                'minimum_days',
                'insurance_deposit',
                'is_pets',
                'description',
                'lat',
                'lng',
                'gallery_is_main',
            ]);
            $data = [
                'item' => $data_uo,
                'video_url' => $request->get('video_url', ''),
                'gallery' => $request->get('gallery', []),
                'hash_object' => $request->get('hash_object'),
                'filters' => $request->get('filters', [])
            ];

            switch ($item->type) {
                case 'cottage':
                    $data_c = $request->only([
                        'living_space',
                        'count_guest',
                        'count_bedrooms',
                        'count_small_bedrooms',
                        'count_big_bedrooms',
                        'price_day',
                        'price_day_weekend',
                        'price_weekends',
                    ]);
                    $data['cottage'] = $data_c;
                    break;
                case 'hotel':
                    $data_h = $request->only([
                        'room_ids',
                    ]);
                    $data['hotel'] = $data_h;
                    break;
                case 'rest':
                    $data_r = $request->only([
                        'rest_ids',
                    ]);
                    $data['rest'] = $data_r;
                    break;
            }
            UserObjectService::update($item, $data);
            return redirect(route('admin.userObject.index'));
        }
        return view("admin.$this->basicView.form", ['item' => $item]);
    }
}
