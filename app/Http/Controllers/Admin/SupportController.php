<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SupportDialog;
use App\Models\SupportDialogAttachment;
use App\Models\SupportDialogMessage;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    public function index(Request $request)
    {
        $query = SupportDialog::orderBy('last_message_at', 'desc');
        return view('admin.support.index', [
            'pagination' => $query->paginate('20', ['*'], 'page', $request->get('page'))
        ]);
    }

    public function chat(Request $request, SupportDialog $item)
    {
        if ($request->isMethod('post')) {
            $message = trim($request->get('message'));
            $file = $request->file('file');
            if ($file && strpos($file->getMimeType(), 'image/') === false) {
                $file = null;
            }
            if ($message || $file) {
                $_message = null;
//                try {
                $_message = SupportDialogMessage::create([
                    'dialog_id' => $item->id,
                    'message' => $message,
                    'is_user' => 0
                ]);
                $item->update([
                    'last_message_at' => Date('Y-m-d H:i:s'),
                    'user_new_messages' => $item->user_new_messages + 1,
                ]);
                if ($file) {
                    $extension = $file->getClientOriginalExtension();
                    $mime = $file->getMimeType();
                    $full_name = md5(rand(10000000, 99999999) . time()) . ".{$extension}";
                    $file->move(public_path() . '/attachment', $full_name);
                    SupportDialogAttachment::create([
                        'message_id' => $_message->id,
                        'type_id' => SupportDialogAttachment::TYPE_PHOTO,
                        'extension' => $extension,
                        'mime' => $mime,
                        'path_file' => "/attachment/$full_name",
                    ]);
                }
                return [
                    'last_message_id' => $_message->id
                ];
//                } catch (\Exception $e) {
//                    if ($_message && !$_message->message) {
//                        $_message->delete();
//                    }
//                }
            }
        } else {
            $item->update([
                'last_message_at' => Date('Y-m-d H:i:s'),
                'support_new_messages' => 0,
            ]);
        }
        $query = SupportDialogMessage::where('dialog_id', $item->id);

        return view('admin.support.chat', [
            'item' => $item,
            'pagination' => $query->paginate('100', ['*'], 'page', $request->get('page'))
        ]);
    }

    public function getMessages(Request $request, SupportDialog $item)
    {
        $render = false;
        $item->update([
            'last_message_at' => Date('Y-m-d H:i:s'),
            'support_new_messages' => 0,
        ]);

        $query = SupportDialogMessage::where('dialog_id', $item->id);
        $query_check = clone $query;
        $last = $query_check->orderBy('id', 'desc')->first();
        if (strtotime($last->updated_at) !== intval($request->get('last_message_update'))) {
            $render = true;
        }

        if (!$render && $last->id == $request->get('last_message_id')) {
            return [];
        }
        return [
            'render' => view('admin.support.partials.dialogBody', [
                'pagination' => $query->paginate('100', ['*'], 'page', $request->get('page'))
            ])->render(),
            'last_message_id' => $last->id,
            'last_message_update' => strtotime($last->updated_at)
        ];
    }
}
