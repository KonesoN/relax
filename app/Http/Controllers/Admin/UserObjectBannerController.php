<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserObject;
use App\Models\UserObjectBanner;
use Illuminate\Http\Request;

class UserObjectBannerController extends Controller
{
    public $basicModel = UserObjectBanner::class;
    public $basicView = 'userObjectBanner';

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $uo = UserObject::find($request->get('object_id'));
            if ($uo) {
                $banner = UserObjectBanner::find($request->get('banner_id'));
                $start_at = Date('Y-m-d', strtotime($request->get('start_at')));
                $end_at = Date('Y-m-d', strtotime($request->get('end_at')));
                $data = [
                    'status_id' => UserObjectBanner::STATUS_ACTIVE,
                    'start_at' => $start_at,
                    'end_at' => $end_at,
                    'object_id' => $uo->id,
                    'position' => $request->get('position'),
                ];
                if ($banner) {
                    $banner->update($data);
                } else {
                    UserObjectBanner::create($data);
                }
            }
            return redirect(route("admin.{$this->basicView}.index"));
        }
        $query = $this->basicModel::where('status_id', UserObjectBanner::STATUS_ACTIVE);
        $items = [];
        foreach ($query->get() as $item) {
            $items[$item->position] = $item;
        }
        return view("admin.$this->basicView.index",
            [
                'items' => $items,
            ]
        );
    }

    public function delete(Request $request, UserObjectBanner $item)
    {
        $item->update(['status_id' => UserObjectBanner::STATUS_DELETE]);
        return redirect(route("admin.{$this->basicView}.index"));
    }
}
