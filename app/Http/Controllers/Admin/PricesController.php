<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Addition;
use App\Models\TypeAllocation;
use Illuminate\Http\Request;

class PricesController extends Controller
{
    public $basicView = 'price';

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->get('addition', []) as $id => $value) {
                Addition::find($id)->update(['value' => $value]);
            }
            foreach ($request->get('allocation', []) as $id => $value) {
                TypeAllocation::find($id)->update(['value' => $value]);
            }
            return redirect(route('admin.price.index'));
        }
        return view("admin.$this->basicView.index");
    }
}
