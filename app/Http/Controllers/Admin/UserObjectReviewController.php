<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ObjectReview;
use Illuminate\Http\Request;

class UserObjectReviewController extends Controller
{
    public $basicModel = ObjectReview::class;
    public $basicView = 'userObjectReview';

    public function index(Request $request)
    {
        $query = $this->basicModel::where('status_id', ObjectReview::STATUS_NEW);

        return view("admin.$this->basicView.index",
            [
                'pagination' => $query->paginate('10', ['*'], 'page', $request->get('page')),
            ]
        );
    }

    public function delete(Request $request, ObjectReview $item)
    {
        $item->delete();
        return redirect(route("admin.{$this->basicView}.index"));
    }

    public function approve(Request $request, ObjectReview $item)
    {
        $item->update(['status_id' => ObjectReview::STATUS_APPROVE]);
        return redirect(route("admin.{$this->basicView}.index"));
    }
}
