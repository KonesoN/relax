<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public $basicModel = Transaction::class;
    public $basicView = 'transaction';

    public function index(Request $request)
    {
        $query = $this->basicModel::orderBy('id', 'desc');
        $filters = [
            'search_field' => $request->get('search_field'),
            'search_text' => $request->get('search_text'),
            'method_id' => $request->get('method_id'),
            'end_at' => $request->get('end_at'),
            'start_at' => $request->get('start_at'),
        ];
        if (!empty($filters['search_text']) && !empty($filters['search_field'])) {
            $query->where($filters['search_field'], 'LIKE', "%{$filters['search_text']}%");
        }
        if (!empty($filters['method_id'])) {
            $query->where('method_id', $filters['method_id']);
        }
        if (!empty($filters['start_at']) && !empty($filters['end_at'])) {
            $query->whereBetween('created_at', [$filters['start_at'] . ' 00:00:00', $filters['end_at'] . ' 23:59:59']);
        } elseif (!empty($filters['start_at'])) {
            $query->where('created_at', '>=', $filters['start_at'] . ' 00:00:00');
        } elseif (!empty($filters['end_at'])) {
            $query->where('created_at', '<=', $filters['end_at'] . ' 23:59:59');
        }

        return view("admin.$this->basicView.index",
            [
                'pagination' => $query->paginate('20', ['*'], 'page', $request->get('page')),
                'filters' => $filters
            ]
        );
    }
}
