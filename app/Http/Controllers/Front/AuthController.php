<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\ObjectDialog;
use App\Models\ObjectDialogMessage;
use App\Models\SupportDialog;
use App\Models\SupportDialogMessage;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function login(Request $request)
    {
        $errors = [];
        $fields = [];
        if ($request->isMethod('post')) {
            $fields = $request->only(['email', 'password']);
            if (empty($fields['email'])) {
                $errors['email'] = 'Обязательно для заполнения';
            }
            if (empty($fields['password'])) {
                $errors['password'] = 'Обязательно для заполнения';
            }

            $user = User::where('email', $fields['email'])->first();
            if ($user) {
                if (!Hash::check($fields['password'], $user->password)) {
                    $errors['password'] = 'Не верный пароль';
                }
            } else {
                $errors['email'] = 'Почта не найдена';
            }
            if ($user && !$user->is_active) {
                $errors['email'] = 'Заблокирован';
            }
            if (!count($errors)) {
                Auth::login($user);
                return redirect(route('front.cabinet.object.index'));
            }
        }
        return view('front.auth.login', ['fields' => $fields, 'errors' => $errors]);
    }

    public function register(Request $request)
    {
        $errors = [];
        $fields = [];
        if ($request->isMethod('post')) {
            $fields = $request->only([
                'first_name',
                'last_name',
                'email',
                'password',
                'password_confirmation',
                'type_id'
            ]);

            if (empty($fields['first_name'])) {
                $errors['first_name'] = 'Обязательно для заполнения';
            }
            if (empty($fields['last_name'])) {
                $errors['last_name'] = 'Обязательно для заполнения';
            }
            if (empty($fields['email'])) {
                $errors['email'] = 'Обязательно для заполнения';
            } elseif (User::where('email', $fields['email'])->first()) {
                $errors['email'] = 'Почта занята';
            }
            if (empty($fields['password'])) {
                $errors['password'] = 'Обязательно для заполнения';
            }
            if (empty($fields['password_confirmation'])) {
                $errors['password_confirmation'] = 'Обязательно для заполнения';
            }

            if (!empty($fields['password']) && !empty($fields['password_confirmation']) &&
                $fields['password'] !== $fields['password_confirmation']) {
                $errors['password'] = 'Пароли не совпадают';
                $errors['password_confirmation'] = 'Пароли не совпадают';
            }
            if (empty($fields['type_id']) || !in_array($fields['type_id'], [User::TYPE_USER, User::TYPE_OWNER])) {
                $fields['type_id'] = 1;
            }
            if (!count($errors)) {
                unset($fields['password_confirmation']);
                $fields['password'] = Hash::make($fields['password']);
                $fields['is_active'] = 1;
                $user = User::create($fields);
                Auth::login($user);

                if ($fields['type_id'] == User::TYPE_OWNER) {
                    $dialog = SupportDialog::firstOrCreate([
                        'user_id' => $user->id,
                    ]);
                    SupportDialogMessage::create([
                        'dialog_id' => $dialog->id,
                        'message' => 'Добро пожаловать на Rusvill. Если у вас есть какие либо вопросы, вы можете задать их тут в чате',
                        'is_user' => 0,
                        'is_read' => 0
                    ]);
                    $dialog->update([
                        'last_message_at' => Date('Y-m-d H:i:s'),
                        'user_new_messages' => 1,
                        'support_new_messages' => 0
                    ]);
                }

                return redirect(route('front.cabinet.object.index'));
            }
        }
        return view('front.auth.register', ['fields' => $fields, 'errors' => $errors]);
    }

    public function logout()
    {
        try {
            Auth::logout();
        } catch (\Exception $e) {
        }
        return redirect(route('front.index'));
    }
}
