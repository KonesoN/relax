<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\UserObject;
use Illuminate\Http\Request;

class RenderController extends Controller
{
    public function objectHotelRoom(Request $request)
    {
        return [
            'render' => view('front.cabinet.object.partials.hotelRoom', [
                'hash' => $request->get('hash'),
            ])->render()
        ];
    }
    public function restHotelRoom(Request $request)
    {
        return [
            'render' => view('front.cabinet.object.partials.restHotelRoom', [
                'hash' => $request->get('hash'),
            ])->render()
        ];
    }

    public function objectRestCottage(Request $request)
    {
        $id = md5(strtotime('now') . rand(99999, 999999));
        return [
            'render' => view('front.cabinet.object.partials.restObject', [
                'hash' => $request->get('hash'),
            ])->render()
        ];
    }

    public function objectModalPhotos(Request $request)
    {
        return [
            'render' => view('front.modal.partials.vipGalleryList', [
                'item' => UserObject::find($request->get('id')),
            ])->render()
        ];
    }
}
