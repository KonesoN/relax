<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\BookingConfirm;
use App\Models\HotelRoom;
use App\Models\HotelRoomReservation;
use App\Models\RestObject;
use App\Models\RestObjectReservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class RestObjectBookingController extends Controller
{
    public function new(Request $request)
    {
        $rest_object = RestObject::find($request->get('rest_object_id'));
        if ($rest_object) {
            $data = $request->only([
                'first_name',
                'last_name',
                'phone',
                'email',
                'arrival_at',
                'departure_at',
                'rest_object_id',
            ]);
            $hash = md5(strtotime('now') . Str::random(10));
            $data['hash'] = $hash;
            $data['status_id'] = RestObjectReservation::STATUS_NEW;
            $data['arrival_at'] = Date('Y-m-d', strtotime($data['arrival_at']));
            $data['departure_at'] = Date('Y-m-d', strtotime($data['departure_at']));
            $data['phone'] = '+' . preg_replace('/[^0-9]+/', '', trim(($data['phone'] ?? ''), " '"));
            $item = RestObjectReservation::create($data);
//            try {
//                Mail::to([$data['email']])->send(new BookingConfirm($item, 'rest_cottage'));
//            } catch (\Exception $e) {
                $item->update(['status_id' => RestObjectReservation::STATUS_USER_CONFIRM]);
//            }
        }
        return [];
    }

    public function confirm(Request $request, $hash)
    {
        $hr = HotelRoomReservation::find($hash);
        if ($hr) {
            $hr->update(['status_id' => HotelRoomReservation::STATUS_USER_CONFIRM, 'hash' => null]);
            $request->session()->flash('alert_message', 'Бронь подтверждена, ожидайте звонка');
        }
        return redirect(route('front.index'));
    }
}
