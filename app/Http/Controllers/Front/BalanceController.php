<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\TransactionUserRefill;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BalanceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $amount = intval($request->get('amount'));
            $method_id = intval($request->get('method_id'));
            if ($amount >= 1 && in_array($method_id, array_keys(Transaction::METHODS))) {
                $transaction = Transaction::create([
                    'user_id' => Auth::id(),
                    'type_id' => Transaction::TYPE_USER_REFILL,
                    'method_id' => $method_id,
                    'amount' => $amount,
                ]);
                try {
                    TransactionUserRefill::create([
                        'transaction_id' => $transaction->id,
                    ]);
                    Auth::user()->update(['balance' => Auth::user()->balance + $amount]);
                } catch (Exception $e) {
                    $transaction->delete();
                }
            }
            return redirect(route('front.cabinet.balance'));
        }
        $query = Transaction::orderBy('id', 'desc');
        return view('front.cabinet.balance', [
            'pagination' => $query->paginate('10', ['*'], 'page', $request->get('page')),
            'tab' => $request->get('page') ? 'history' : 'replenish'
        ]);
    }
}
