<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\SupportDialog;
use App\Models\SupportDialogAttachment;
use App\Models\SupportDialogMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupportMessageController extends Controller
{
    public function chat(Request $request)
    {
        $item = SupportDialog::firstOrCreate([
            'user_id' => Auth::id(),
        ]);
        if ($request->isMethod('post')) {
            $message = trim($request->get('message'));
            $file = $request->file('file');
            if ($file && strpos($file->getMimeType(), 'image/') === false) {
                $file = null;
            }
            if ($message || $file) {
                $_message = null;
//                try {
                $_message = SupportDialogMessage::create([
                    'dialog_id' => $item->id,
                    'message' => $message,
                    'is_user' => 1
                ]);
                $item->update([
                    'last_message_at' => Date('Y-m-d H:i:s'),
                    'support_new_messages' => $item->support_new_messages + 1
                ]);
                if ($file) {
                    $extension = $file->getClientOriginalExtension();
                    $mime = $file->getMimeType();
                    $full_name = md5(rand(10000000, 99999999) . time()) . ".{$extension}";
                    $file->move(public_path() . '/attachment', $full_name);
                    SupportDialogAttachment::create([
                        'message_id' => $_message->id,
                        'type_id' => SupportDialogAttachment::TYPE_PHOTO,
                        'extension' => $extension,
                        'mime' => $mime,
                        'path_file' => "/attachment/$full_name",
                    ]);
                }
                return [
                    'last_message_id' => $_message->id
                ];
//                } catch (\Exception $e) {
//                    if ($_message && !$_message->message) {
//                        $_message->delete();
//                    }
//                }
            }
        } else {
            SupportDialogMessage::where(['dialog_id' => $item->id, 'is_user' => 0])->update(['is_read' => 1]);
            $item->update([
                'last_message_at' => Date('Y-m-d H:i:s'),
                'user_new_messages' => 0,
            ]);
        }
        $query = SupportDialogMessage::where('dialog_id', $item->id);

        return view('front.cabinet.support.chat', [
            'item' => $item,
            'pagination' => $query->paginate('100', ['*'], 'page', $request->get('page'))
        ]);
    }

    public function getMessages(Request $request)
    {
        $render = false;
        $item = SupportDialog::firstOrCreate([
            'user_id' => Auth::id(),
        ]);
        SupportDialogMessage::where(['dialog_id' => $item->id, 'is_user' => 0])->update(['is_read' => 1]);
        $item->update([
            'last_message_at' => Date('Y-m-d H:i:s'),
            'user_new_messages' => 0,
        ]);

        $query = SupportDialogMessage::where('dialog_id', $item->id);
        $query_check = clone $query;
        $last = $query_check->orderBy('id', 'desc')->first();
        if (strtotime($last->updated_at) !== intval($request->get('last_message_update'))) {
            $render = true;
        }

        if (!$render && $last->id == $request->get('last_message_id')) {
            return [];
        }
        return [
            'render' => view('front.cabinet.support.partials.dialogBody', [
                'pagination' => $query->paginate('100', ['*'], 'page', $request->get('page'))
            ])->render(),
            'last_message_id' => $last->id,
            'last_message_update' => strtotime($last->updated_at)
        ];
    }

    public function new(Request $request)
    {
        if ($message = trim($request->get('message'))) {
            $dialog = SupportDialog::firstOrCreate([
                'user_id' => Auth::id(),
            ]);
            SupportDialogMessage::create([
                'dialog_id' => $dialog->id,
                'message' => $message,
                'is_user' => 1,
                'is_read' => 0
            ]);
            $dialog->update([
                'last_message_at' => Date('Y-m-d H:i:s'),
                'user_new_messages' => 0,
                'support' => 1
            ]);
        }
        return [];
    }
}
