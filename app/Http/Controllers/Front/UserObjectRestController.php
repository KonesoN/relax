<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\RestHotelRoom;
use App\Models\RestHotelRoomGallery;
use App\Models\RestObject;
use App\Models\RestObjectGallery;
use Illuminate\Http\Request;

class UserObjectRestController extends Controller
{

    public function form(Request $request)
    {
        if ($request->isMethod('post')) {
            $rc = null;
//                    try {
            $hash_rest = $request->get('hash_rest');
            $hash_rest_data = $request->get($hash_rest);
            $type = $hash_rest_data['type_rest'];
            $data = $request->get($hash_rest_data['type_rest']);
            $data['type'] = $type;
            $hash_object = $request->get('hash_object');
            $data['hash'] = $hash_object;
            $data_object = $request->get('object');
            $data['description'] = !empty($data_object['description']) ? $data_object['description'] : '';
            $data['is_pets'] = !empty($data['is_pets']) ? 1 : 0;
            $rc = RestObject::find($request->get('rest_id'));
            if ($rc) {
                $rc->update($data);
            } else {
                $rc = RestObject::create($data);
            }
            $gallery_ids = $request->get('rest_gallery', []);
            RestObjectGallery::where([
                'type_id' => RestObjectGallery::TYPE_YOUTUBE_URL,
            ])->delete();
            if (!empty($data_object['video_url'])) {
                $rcg = RestObjectGallery::create([
                    'type_id' => RestObjectGallery::TYPE_YOUTUBE_URL,
                    'value' => $data_object['video_url'],
                    'hash' => $hash_object
                ]);
                $gallery_ids[] = $rcg->id;
            }
            RestObjectGallery::whereNull('rest_object_id')->whereIn('id', $gallery_ids)
                ->where('hash', $hash_object)
                ->update(['rest_object_id' => $rc->id, 'hash' => null]);

            $gallery_is_main = $request->get('gallery_is_main', []);
            $is_main = false;
            foreach ($gallery_is_main as $gim => $value) {
                if ($value) {
                    if ($uog = RestObjectGallery::where(['id' => $gim, 'rest_object_id' => $rc->id])->first()) {
                        RestObjectGallery::where(['rest_object_id' => $rc->id])->update(['is_main' => 0]);
                        $uog->update(['is_main' => 1]);
                        $is_main = true;
                        break;
                    }
                }
            }
            if (!$is_main) {
                $uog = RestObjectGallery::where([
                    'rest_object_id' => $rc->id,
                    'type_id' => RestObjectGallery::TYPE_PHOTO
                ])->first();
                if ($uog) {
                    RestObjectGallery::where(['rest_object_id' => $rc->id])->update(['is_main' => 0]);
                    $uog->update(['is_main' => 1]);
                }
            }
            if ($type == 'hotel') {
                $room_ids = $request->get('room_ids', []);
                RestHotelRoom::whereNull('rest_object_id')->whereIn('id', $room_ids)
                    ->where('hash', $hash_object)
                    ->update(['rest_object_id' => $rc->id, 'hash' => null]);
            }
//                    } catch (\Exception $e) {
//                        if ($rc) {
//                            $rc->delete();
//                        }
//                        if ($cottage) {
//                            $cottage->delete();
//                        }
//                    }

            $rc = RestObject::where('id', $rc->id)->with('photos')->first();
            return ['item' => $rc];
        }
        return [];
    }

    public function uploadPhoto(Request $request)
    {
        if ($file = $request->file('file')) {
            if (strpos($file->getMimeType(), 'image/') !== false) {
                $extension = $file->getClientOriginalExtension();
                $mime = $file->getMimeType();
                $full_name = md5(rand(10000000, 99999999) . time()) . ".{$extension}";
                $file->move(public_path() . '/gallery', $full_name);
                $item = RestObjectGallery::create([
                    'type_id' => RestObjectGallery::TYPE_PHOTO,
                    'extension' => $extension,
                    'mime' => $mime,
                    'value' => "/gallery/$full_name",
                    'hash' => $request->get('hash'),
                ]);
                return [
                    'render' => view('front.cabinet.object.partials.imageBox',
                        [
                            'item' => $item,
                            'name' => 'rest_gallery',
                        ])->render()
                ];
            }
            return ['message' => 'Не верный формат фото.'];
        }
        return ['message' => 'Не выбран файл'];
    }

    public function roomForm(Request $request)
    {
        if ($request->isMethod('post')) {
            $rc = null;
//                    try {
            $data = $request->get('room');
            $hash_object = $request->get('hash_object');
            $data['hash'] = $hash_object;
            $rc = RestHotelRoom::find($request->get('room_id'));
            if ($rc) {
                $rc->update($data);
            } else {
                $rc = RestHotelRoom::create($data);
            }
            $gallery_ids = $request->get('rest_room_gallery', []);
            RestHotelRoomGallery::where([
                'type_id' => RestHotelRoomGallery::TYPE_YOUTUBE_URL,
            ])->delete();
            if ($video_url = $request->get('room_video_url', '')) {
                $rcg = RestHotelRoomGallery::create([
                    'type_id' => RestHotelRoomGallery::TYPE_YOUTUBE_URL,
                    'value' => $video_url,
                    'hash' => $hash_object
                ]);
                $gallery_ids[] = $rcg->id;
            }
            RestHotelRoomGallery::whereNull('rest_hotel_room_id')->whereIn('id', $gallery_ids)
                ->where('hash', $hash_object)
                ->update(['rest_hotel_room_id' => $rc->id, 'hash' => null]);

            $gallery_is_main = $request->get('rest_room_gallery_is_main', []);
            $is_main = false;
            foreach ($gallery_is_main as $gim => $value) {
                if ($value) {
                    if ($uog = RestHotelRoomGallery::where(['id' => $gim, 'rest_hotel_room_id' => $rc->id])->first()) {
                        RestHotelRoomGallery::where(['rest_hotel_room_id' => $rc->id])->update(['is_main' => 0]);
                        $uog->update(['is_main' => 1]);
                        $is_main = true;
                        break;
                    }
                }
            }
            if (!$is_main) {
                $uog = RestHotelRoomGallery::where([
                    'rest_hotel_room_id' => $rc->id,
                    'type_id' => RestObjectGallery::TYPE_PHOTO
                ])->first();
                if ($uog) {
                    RestHotelRoomGallery::where(['rest_hotel_room_id' => $rc->id])->update(['is_main' => 0]);
                    $uog->update(['is_main' => 1]);
                }
            }
//                    } catch (\Exception $e) {
//                        if ($rc) {
//                            $rc->delete();
//                        }
//                        if ($cottage) {
//                            $cottage->delete();
//                        }
//                    }
            $rc = RestHotelRoom::where('id', $rc->id)->with('photos')->first();
            return ['item' => $rc];
        }
        return [];
    }

    public function roomUploadPhoto(Request $request)
    {
        if ($file = $request->file('file')) {
            if (strpos($file->getMimeType(), 'image/') !== false) {
                $extension = $file->getClientOriginalExtension();
                $mime = $file->getMimeType();
                $full_name = md5(rand(10000000, 99999999) . time()) . ".{$extension}";
                $file->move(public_path() . '/gallery', $full_name);
                $item = RestHotelRoomGallery::create([
                    'type_id' => RestHotelRoomGallery::TYPE_PHOTO,
                    'extension' => $extension,
                    'mime' => $mime,
                    'value' => "/gallery/$full_name",
                    'hash' => $request->get('hash'),
                ]);
                return [
                    'render' => view('front.cabinet.object.partials.imageBox',
                        [
                            'item' => $item,
                            'name' => 'rest_room_gallery',
                        ])->render()
                ];
            }
            return ['message' => 'Не верный формат фото.'];
        }
        return ['message' => 'Не выбран файл'];
    }
}
