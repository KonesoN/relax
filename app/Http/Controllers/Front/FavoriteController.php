<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\ObjectReview;
use App\Models\RestObject;
use App\Models\User;
use App\Models\UserFavorite;
use App\Models\UserObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    public function index(Request $request)
    {
        $query = UserFavorite::where('user_id', Auth::id())->orderBy('id', 'desc');
        return view('front.cabinet.favorite.index', [
            'pagination' => $query->paginate('20', ['*'], 'page', $request->get('page'))
        ]);
    }

    public function handler($id)
    {
        $data = ['user_id' => Auth::id(), 'object_id' => $id];
        $uf = UserFavorite::where($data)->first();
        if ($uf) {
            $uf->delete();
        } else {
            UserFavorite::create($data);
        }
        return [];
    }
}
