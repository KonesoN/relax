<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\HotelRoom;
use App\Models\HotelRoomGallery;
use Illuminate\Http\Request;

class UserObjectHotelRoomController extends Controller
{
    public function form(Request $request)
    {
        if ($request->isMethod('post')) {
            $hr = null;
//                    try {
            $data = $request->get('room');
            $hash_object = $request->get('hash_object');
            $data['hash'] = $hash_object;
            $hr = HotelRoom::find($request->get('room_id'));
            if ($hr) {
                $hr->update($data);
            } else {
                $hr = HotelRoom::create($data);
            }
            $gallery_ids = $request->get('room_gallery', []);
            HotelRoomGallery::where([
                'type_id' => HotelRoomGallery::TYPE_YOUTUBE_URL,
            ])->delete();
            if (!empty($data['room_video_url'])) {
                $hrg = HotelRoomGallery::create([
                    'type_id' => HotelRoomGallery::TYPE_YOUTUBE_URL,
                    'value' => $data['room_video_url'],
                    'hash' => $hash_object
                ]);
                $gallery_ids[] = $hrg->id;
            }
            HotelRoomGallery::whereNull('hotel_room_id')->whereIn('id', $gallery_ids)
                ->where('hash', $hash_object)
                ->update(['hotel_room_id' => $hr->id, 'hash' => null]);

            $gallery_is_main = $request->get('gallery_is_main', []);
            $is_main = false;
            foreach ($gallery_is_main as $gim => $value) {
                if ($value) {
                    if ($uog = HotelRoomGallery::where(['id' => $gim, 'hotel_room_id' => $hr->id])->first()) {
                        HotelRoomGallery::where(['hotel_room_id' => $hr->id])->update(['is_main' => 0]);
                        $uog->update(['is_main' => 1]);
                        $is_main = true;
                        break;
                    }
                }
            }
            if (!$is_main) {
                $uog = HotelRoomGallery::where([
                    'hotel_room_id' => $hr->id,
                    'type_id' => HotelRoomGallery::TYPE_PHOTO
                ])->first();
                if ($uog) {
                    HotelRoomGallery::where(['hotel_room_id' => $hr->id])->update(['is_main' => 0]);
                    $uog->update(['is_main' => 1]);
                }
            }
//                    } catch (\Exception $e) {
//                        if ($hr) {
//                            $hr->delete();
//                        }
//                        if ($cottage) {
//                            $cottage->delete();
//                        }
//                    }
            $hr = HotelRoom::where('id', $hr->id)->with('photos')->first();
            return ['item' => $hr];
        }
        return [];
    }

    public function uploadPhoto(Request $request)
    {
        if ($file = $request->file('file')) {
            if (strpos($file->getMimeType(), 'image/') !== false) {
                $extension = $file->getClientOriginalExtension();
                $mime = $file->getMimeType();
                $full_name = md5(rand(10000000, 99999999) . time()) . ".{$extension}";
                $file->move(public_path() . '/gallery', $full_name);
                $item = HotelRoomGallery::create([
                    'type_id' => HotelRoomGallery::TYPE_PHOTO,
                    'extension' => $extension,
                    'mime' => $mime,
                    'value' => "/gallery/$full_name",
                    'hash' => $request->get('hash'),
                ]);
                return [
                    'render' => view('front.cabinet.object.partials.imageBox',
                        [
                            'item' => $item,
                            'name' => 'room_gallery',
                        ])->render()
                ];
            }
            return ['message' => 'Не верный формат фото.'];
        }
        return ['message' => 'Не выбран файл'];
    }
}
