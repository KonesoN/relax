<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $user = User::find(Auth::id());
            $data = $request->only(['first_name', 'last_name', 'email', 'phone']);
            $data['phone'] =
                '+' . preg_replace('/[^0-9]+/', '', trim(($data['phone'] ?? ''), " '"));
            $user->update($data);
            return redirect(route('front.cabinet.setting.index'));
        }
        return view('front.cabinet.settings');
    }
}
