<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\ObjectReview;
use App\Models\RestObject;
use App\Models\User;
use App\Models\UserObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function index(Request $request)
    {
        if (Auth::user()->type_id == User::TYPE_OWNER) {
            $object_ids = UserObject::where('user_id', Auth::id())->get('id')->pluck('id')->toArray();
            $rest_object_ids = RestObject::whereIn('rest_id', $object_ids)->get('id')->pluck('id')->toArray();
            $query = ObjectReview::where(function ($q) use ($object_ids) {
                $q->whereIn('parent_id', $object_ids)
                    ->where('parent_type', ObjectReview::TYPE_USER_OBJECT);
            })->orWhere(function ($q) use ($rest_object_ids) {
                $q->whereIn('parent_id', $rest_object_ids)
                    ->where('parent_type', ObjectReview::TYPE_REST_OBJECT);
            });
            $review_ids = [];
            $parent_ids = [];
            foreach ($query->get() as $item) {
                if(empty($parent_ids[$item->parent_id])){
                    $review_ids[$item->id] = $item->id;
                }
                $parent_ids[$item->parent_id] = $item->parent_id;
            }
            $review_ids = array_keys($review_ids);
            return view('front.cabinet.review.index', [
                'pagination' => ObjectReview::whereIn('id', $review_ids)->orderBy('updated_at', 'desc')->paginate('20',
                    ['*'], 'page',
                    $request->get('page'))
            ]);
        } else {
            $query = ObjectReview::where('user_id', Auth::id())->orderBy('id', 'desc');
            return view('front.cabinet.review.index', [
                'pagination' => $query->paginate('20', ['*'], 'page', $request->get('page'))
            ]);
        }
    }

    public function modalRender(Request $request, ObjectReview $item)
    {
        $query = ObjectReview::where([
            'parent_id' => $item->id,
            'parent_type' => $request->get('parent_type')
        ])->orderBy('id', 'desc');
        $item->object()->update(['new_reviews' => 0]);
        return [
            'render' => view('front.cabinet.review.partials.modalListAllBody', [
                'pagination' => $query->paginate('10', ['*'], 'page', $request->get('page')),
                'uo' => $item,
            ])->render()
        ];
    }

    public function answer(Request $request, ObjectReview $item)
    {
        if ($item->userObject->user_id == Auth::id()) {
            $item->update(['answer' => $request->get('answer')]);
        }
        return [];
    }

    public function new(Request $request)
    {
        switch ($request->get('parent_type')) {
            case ObjectReview::TYPE_REST_OBJECT:
                $object = RestObject::find($request->get('parent_id'));
                break;
            default:
                $object = UserObject::find($request->get('parent_id'));
                break;
        }
        if ($object) {
            $data = $request->only(['review', 'modal_rating', 'phone', 'parent_id', 'parent_type']);
            $data['rating'] = $data['modal_rating'];
            $data['user_id'] = Auth::id();
            $data['phone'] = '+' . preg_replace('/[^0-9]+/', '', trim(($data['phone'] ?? ''), " '"));
            unset($data['modal_rating']);
            ObjectReview::create($data);
            $object->update(['new_reviews' => $object->new_reviews + 1]);
        }
        return [];
    }
}
