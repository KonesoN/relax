<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Addition;
use App\Models\Cottage;
use App\Models\CottageBooking;
use App\Models\District;
use App\Models\Hotel;
use App\Models\HotelRoom;
use App\Models\HotelRoomBooking;
use App\Models\HotelRoomReservation;
use App\Models\Rest;
use App\Models\RestHotelRoom;
use App\Models\RestHotelRoomBooking;
use App\Models\RestHotelRoomReservation;
use App\Models\RestObject;
use App\Models\RestObjectReservation;
use App\Models\Transaction;
use App\Models\TransactionObjectPayment;
use App\Models\TypeAllocation;
use App\Models\UserObject;
use App\Models\UserObjectAddition;
use App\Models\UserObjectBooking;
use App\Models\UserObjectFilter;
use App\Models\UserObjectGallery;
use App\Services\UserObjectService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserObjectController extends Controller
{
    public function index(Request $request)
    {
        return view('front.cabinet.object.index', [
            'active' => UserObject::where(['user_id' => Auth::id(), 'status_id' => UserObject::STATUS_ACTIVE])->get(),
            'inactive' => UserObject::where([
                'user_id' => Auth::id(),
                'status_id' => UserObject::STATUS_INACTIVE
            ])->get(),
            'archive' => UserObject::where(['user_id' => Auth::id(), 'status_id' => UserObject::STATUS_ARCHIVE])->get(),
        ]);
    }

    public function new(Request $request)
    {
        if ($request->isMethod('post')) {
            //            for ($i = 0; $i < 20; $i++) {

            $uo = null;
            $object = null;
            $transaction = null;
            //                    try {
            $data_uo = $request->only([
                'type_allocation_id',
                'day_allocation',
                'title',
                'address',
                'distance_to_city',
                'is_shore',
                'district_id',
                'direction_id',
                'is_the_city',
                'price_type_id',
                'count_view',
                'count_view_today',
                'arrival_at',
                'departure_at',
                'minimum_days',
                'insurance_deposit',
                'is_pets',
                'description',
                'lat',
                'lng',
                'phone',
            ]);
            //            if (!empty($data_uo['is_the_city'])) {
            //                $data_uo['direction_id'] = null;
            //            }
            $data_uo['is_shore'] = !empty($data_uo['is_shore']) ? 1 : 0;
            $data_uo['is_pets'] = !empty($data_uo['is_pets']) ? 1 : 0;
            $ta_id = intval($data_uo['type_allocation_id']);
            $data_uo['day_allocation'] = $data_uo['day_allocation'][$ta_id];
            $data_uo['district_id'] = District::find(!empty($data_uo['district_id']) ? $data_uo['district_id'] : null) ? $data_uo['district_id'] : null;
            $uo = UserObject::create(array_merge($data_uo, [
                'user_id' => Auth::id(),
                'type' => $request->get('type_object'),
                'start_at' => Date('Y-m-d H:i:s'),
                'end_at' => Date('Y-m-d H:i:s', strtotime("+{$data_uo['day_allocation']} days")),
            ]));

            $hash_object = $request->get('hash_object');
            $gallery_ids = $request->get('gallery', []);
            if ($video_url = $request->get('video_url', '')) {
                $uog = UserObjectGallery::create([
                    'type_id' => UserObjectGallery::TYPE_YOUTUBE_URL,
                    'value' => $video_url,
                    'hash' => $hash_object
                ]);
                $gallery_ids[] = $uog->id;
            }
            UserObjectGallery::whereNull('object_id')->whereIn('id', $gallery_ids)
                ->where('hash', $hash_object)
                ->update(['object_id' => $uo->id, 'hash' => null]);
            $gallery_is_main = $request->get('gallery_is_main', []);
            $is_main = false;
            foreach ($gallery_is_main as $gim => $value) {
                if ($value) {
                    if ($uog = UserObjectGallery::where(['id' => $gim, 'object_id' => $uo->id])->first()) {
                        UserObjectGallery::where(['object_id' => $uo->id])->update(['is_main' => 0]);
                        $uog->update(['is_main' => 1]);
                        $is_main = true;
                        break;
                    }
                }
            }
            if (!$is_main) {
                $uog = UserObjectGallery::where([
                    'object_id' => $uo->id,
                    'type_id' => UserObjectGallery::TYPE_PHOTO
                ])->first();
                if ($uog) {
                    UserObjectGallery::where(['object_id' => $uo->id])->update(['is_main' => 0]);
                    $uog->update(['is_main' => 1]);
                }
            }
            $filters = $request->get('filters', []);
            foreach ($filters as $filter) {
                UserObjectFilter::create([
                    'value' => $filter,
                    'object_id' => $uo->id
                ]);
            }
            $ta = TypeAllocation::where([
                'allocation_id' => $data_uo['type_allocation_id'],
                'days' => $data_uo['day_allocation'],
                'type' => $uo->type
            ])->first();
            $amount = $ta ? $ta->value : UserObject::TYPES_ALLOCATION[$data_uo['type_allocation_id']]['days'][$data_uo['day_allocation']];

            $additions = $request->get('addition', []);

            foreach ($additions as $id => $addition) {
                if (!empty($addition['check'])) {
                    $id = $addition['check'];
                    $day = $additions[$id]['day'];
                    UserObjectAddition::create([
                        'addition_id' => $id,
                        'days' => $day,
                        'start_at' => Date('Y-m-d H:i:s'),
                        'end_at' => Date('Y-m-d H:i:s', strtotime("+$day days")),
                        'object_id' => $uo->id,
                    ]);
                    $a = Addition::where(['addition_id' => $id, 'days' => $day, 'type' => $uo->type])->first();
                    $amount += $a ? $a->value : UserObjectAddition::ADDITIONS[$id]['days'][$day];
                }
            }
            switch ($request->get('type_object')) {
                case UserObject::TYPE_COTTAGE:
                    $data_c = $request->only([
                        'living_space',
                        'count_guest',
                        'count_bedrooms',
                        'count_small_bedrooms',
                        'count_big_bedrooms',
                        'price_day',
                        'price_day_weekend',
                        'price_weekends',
                    ]);
                    $object = Cottage::create(array_merge($data_c, ['object_id' => $uo->id]));
                    break;
                case UserObject::TYPE_HOTEL:
                    $object = Hotel::create(['object_id' => $uo->id]);
                    $room_ids = $request->get('room_ids', []);
                    HotelRoom::whereNull('hotel_id')->whereIn('id', $room_ids)
                        ->where('hash', $hash_object)
                        ->update(['hotel_id' => $object->id, 'hash' => null]);
                    break;
                case UserObject::TYPE_REST:
                    $object = Rest::create(['object_id' => $uo->id]);
                    $rest_ids = $request->get('rest_ids', []);
                    RestObject::whereNull('rest_id')->whereIn('id', $rest_ids)
                        ->where('hash', $hash_object)
                        ->update(['rest_id' => $object->id, 'hash' => null]);
                    break;
            }

            $method_id = $request->get('method_id');
            if (!in_array($method_id, array_keys(Transaction::METHODS))) {
                $method_id = Transaction::METHOD_BALANCE;
            }
            $status_id = Transaction::STATUS_WAIT;
            if ($method_id == Transaction::METHOD_BALANCE) {
                if (Auth::user()->balance >= $amount) {
                    Auth::user()->decrement('balance', $amount);
                    $status_id = Transaction::STATUS_DONE;
                    $uo->update(['status_id' => UserObject::STATUS_ACTIVE]);

                    foreach ($additions as $id => $addition) {
                        if (!empty($addition['check'])) {
                            UserObjectAddition::where([
                                'addition_id' => $id,
                                'object_id' => $uo->id
                            ])->update(['is_active' => 1]);
                        }
                    }
                }
            }
            $transaction = Transaction::create([
                'user_id' => Auth::id(),
                'type_id' => Transaction::TYPE_OBJECT_PAYMENT,
                'status_id' => $status_id,
                'method_id' => $method_id,
                'amount' => $amount
            ]);
            TransactionObjectPayment::create([
                'transaction_id' => $transaction->id,
                'object_id' => $uo->id,
            ]);
            //                    } catch (\Exception $e) {
            //            if ($transaction) {
            //                $transaction->delete();
            //            }
            //            if ($object) {
            //                $object->delete();
            //            }
            //            if ($uo) {
            //                $uo->delete();
            //            }
            //                    }

            return redirect(route('front.cabinet.object.index'));
        }
        return view('front.cabinet.object.new');
    }

    public function edit(Request $request, UserObject $item)
    {
        if ($item->user_id !== Auth::id()) {
            return redirect(route('front.cabinet.object.index'));
        }
        if ($request->isMethod('post')) {
            $data_uo = $request->only([
                'title',
                'address',
                'distance_to_city',
                'is_shore',
                'district_id',
                'direction_id',
                'is_the_city',
                'price_type_id',
                'count_view',
                'count_view_today',
                'arrival_at',
                'departure_at',
                'minimum_days',
                'insurance_deposit',
                'is_pets',
                'description',
                'lat',
                'lng',
                'phone',
                'gallery_is_main',
            ]);
            //            if (!empty($data_uo['is_the_city'])) {
            //                $data_uo['direction_id'] = null;
            //            }
            $data = [
                'item' => $data_uo,
                'video_url' => $request->get('video_url', ''),
                'gallery' => $request->get('gallery', []),
                'hash_object' => $request->get('hash_object'),
                'filters' => $request->get('filters', [])
            ];

            switch ($item->type) {
                case 'cottage':
                    $data_c = $request->only([
                        'living_space',
                        'count_guest',
                        'count_bedrooms',
                        'count_small_bedrooms',
                        'count_big_bedrooms',
                        'price_day',
                        'price_day_weekend',
                        'price_weekends',
                    ]);
                    $data['cottage'] = $data_c;
                    break;
                case 'hotel':
                    $data_h = $request->only([
                        'room_ids',
                    ]);
                    $data['hotel'] = $data_h;
                    break;
                case 'rest':
                    $data_r = $request->only([
                        'rest_ids',
                    ]);
                    $data['rest'] = $data_r;
                    break;
            }
            UserObjectService::update($item, $data);
            return redirect(route('front.cabinet.object.index'));
        }
        return view('front.cabinet.object.edit', ['item' => $item]);
    }

    public function payment(Request $request, UserObject $item)
    {
        if ($item->user_id !== Auth::id()) {
            return redirect(route('front.cabinet.object.index'));
        }
        if ($request->isMethod('post')) {

            $data = $request->only([
                'type_allocation_id',
                'day_allocation',
                'addition',
            ]);
            $ta_id = intval($data['type_allocation_id']);
            $data['day_allocation'] = $data['day_allocation'][$ta_id];
            $ta = TypeAllocation::where([
                'allocation_id' => $data['type_allocation_id'],
                'days' => $data['day_allocation'],
                'type' => $item->type
            ])->first();
            $amount = $ta ? $ta->value : UserObject::TYPES_ALLOCATION[$data['type_allocation_id']]['days'][$data['day_allocation']];

            foreach ($data['addition'] as $id => $addition) {
                if (!empty($addition['check'])) {
                    $id = $addition['check'];
                    $day = $data['addition'][$id]['day'];
                    UserObjectAddition::create([
                        'addition_id' => $id,
                        'days' => $day,
                        'start_at' => Date('Y-m-d H:i:s'),
                        'end_at' => Date('Y-m-d H:i:s', strtotime("+$day days")),
                        'object_id' => $item->id,
                    ]);
                    $a = Addition::where(['addition_id' => $id, 'days' => $day, 'type' => $item->type])->first();
                    $amount += $a ? $a->value : UserObjectAddition::ADDITIONS[$id]['days'][$day];
                }
            }

            $method_id = $request->get('method_id');
            if (!in_array($method_id, array_keys(Transaction::METHODS))) {
                $method_id = Transaction::METHOD_BALANCE;
            }

            $status_id = Transaction::STATUS_WAIT;
            if ($method_id == Transaction::METHOD_BALANCE) {
                if (Auth::user()->balance >= $amount) {
                    Auth::user()->decrement('balance', $amount);
                    $status_id = Transaction::STATUS_DONE;
                    if ($item->status_id == UserObject::STATUS_INACTIVE) {
                        $item->update([
                            'start_at' => Date('Y-m-d H:i:s'),
                            'end_at' => Date('Y-m-d H:i:s', strtotime("+{$data['day_allocation']} days")),
                            'status_id' => UserObject::STATUS_ACTIVE
                        ]);
                    }
                    foreach ($data['addition'] as $id => $addition) {
                        if (!empty($addition['check'])) {
                            $id = $addition['check'];
                            UserObjectAddition::where([
                                'addition_id' => $id,
                                'object_id' => $item->id
                            ])->update(['is_active' => 1]);
                        }
                    }
                }
            }
            if ($item->transactions()->orderBy('id', 'desc')->first() &&
                $item->transactions()->orderBy('id', 'desc')->first()
                    ->parent->status_id == Transaction::STATUS_WAIT) {
                $transaction = $item->transactions()->orderBy('id', 'desc')->first()->parent;
                $transaction->update([
                    'status_id' => $status_id,
                    'method_id' => $method_id,
                    'amount' => $amount,
                ]);
                $top = $transaction->data;
                if ($top) {
                } else {
                    TransactionObjectPayment::create([
                        'transaction_id' => $transaction->id,
                        'object_id' => $item->id,
                    ]);
                }
            } else {
                $transaction = Transaction::create([
                    'user_id' => Auth::id(),
                    'type_id' => Transaction::TYPE_OBJECT_PAYMENT,
                    'status_id' => $status_id,
                    'method_id' => $method_id,
                    'amount' => $amount
                ]);
                TransactionObjectPayment::create([
                    'transaction_id' => $transaction->id,
                    'object_id' => $item->id
                ]);
            }
            return redirect(route('front.cabinet.object.index'));
        }
        return view('front.cabinet.object.payment', ['item' => $item]);
    }

    public function addition(Request $request, UserObject $item)
    {
        if ($item->user_id !== Auth::id()) {
            return redirect(route('front.cabinet.object.index'));
        }
        if ($request->isMethod('post')) {

            $data = $request->only([
                'addition',
            ]);
            $amount = 0;
            foreach ($data['addition'] as $id => $addition) {
                if (!empty($addition['check'])) {
                    $id = $addition['check'];
                    $day = $data['addition'][$id]['day'];
                    UserObjectAddition::create([
                        'addition_id' => $id,
                        'days' => $day,
                        'start_at' => Date('Y-m-d H:i:s'),
                        'end_at' => Date('Y-m-d H:i:s', strtotime("+$day days")),
                        'object_id' => $item->id,
                    ]);
                    $a = Addition::where(['addition_id' => $id, 'days' => $day, 'type' => $item->type])->first();
                    $amount += $a ? $a->value : UserObjectAddition::ADDITIONS[$id]['days'][$day];
                }
            }

            $method_id = $request->get('method_id');
            if (!in_array($method_id, array_keys(Transaction::METHODS))) {
                $method_id = Transaction::METHOD_BALANCE;
            }

            $status_id = Transaction::STATUS_WAIT;
            if ($method_id == Transaction::METHOD_BALANCE) {
                if (Auth::user()->balance >= $amount) {
                    Auth::user()->decrement('balance', $amount);
                    $status_id = Transaction::STATUS_DONE;
                    if ($item->status_id == UserObject::STATUS_INACTIVE && !empty($data['day_allocation'])) {
                        $item->update([
                            'start_at' => Date('Y-m-d H:i:s'),
                            'end_at' => Date('Y-m-d H:i:s', strtotime("+{$data['day_allocation']} days")),
                            'status_id' => UserObject::STATUS_ACTIVE
                        ]);
                    }
                    foreach ($data['addition'] as $id => $addition) {
                        if (!empty($addition['check'])) {
                            $id = $addition['check'];
                            UserObjectAddition::where([
                                'addition_id' => $id,
                                'object_id' => $item->id
                            ])->update(['is_active' => 1]);
                        }
                    }
                }
            }

            $transaction = Transaction::create([
                'user_id' => Auth::id(),
                'type_id' => Transaction::TYPE_OBJECT_PAYMENT,
                'status_id' => $status_id,
                'method_id' => $method_id,
                'amount' => $amount
            ]);
            TransactionObjectPayment::create([
                'transaction_id' => $transaction->id,
                'object_id' => $item->id
            ]);
            return redirect(route('front.cabinet.object.index'));
        }
        $uoa = UserObjectAddition::where(['object_id' => $item->id, 'is_active' => 1])->get();
        $additions = [];
        foreach ($uoa as $addition) {
            $additions[$addition->addition_id] = $addition;
        }
        return view('front.cabinet.object.addition', ['item' => $item, 'additions' => $additions]);
    }

    public function booking(Request $request, UserObject $item)
    {
        if ($item->user_id !== Auth::id()) {
            return redirect(route('front.cabinet.object.index'));
        }
        if ($request->isMethod('post')) {
            if ($item->type == UserObject::TYPE_COTTAGE) {
                UserObjectService::bookingObjectByDates($item, $request->get('departure_at'),
                    $request->get('arrival_at'),
                    $request->get('full_name'), $request->get('phone'), $request->get('email'));
                return redirect(route('front.cabinet.object.booking', $item->id));
            } elseif ($item->type == UserObject::TYPE_HOTEL) {
                $room = HotelRoom::find($request->get('room_id'));
                UserObjectService::bookingHotelRoomByDates($room, $request->get('departure_at'),
                    $request->get('arrival_at'),
                    $request->get('full_name'), $request->get('phone'), $request->get('email'));
                return redirect(route('front.cabinet.object.booking', $item->id) . '?room_id=' . $room->id);
            }
        }
        if ($item->type == UserObject::TYPE_COTTAGE) {
            return view('front.cabinet.object.bookingCottage', ['item' => $item]);
        } elseif ($item->type == UserObject::TYPE_HOTEL) {
            $room = HotelRoom::find($request->get('room_id'));
            $uo = null;
            if (!$room) {
                $room = $item->object->rooms()->first();
            }
            if ($room) {
                $hotel = Hotel::find($room->hotel_id);
                if ($hotel) {
                    $uo = UserObject::find($hotel->object_id);
                }
            }
            if ($uo && $uo->user_id === Auth::id()) {
                return view('front.cabinet.object.bookingRoom', ['item' => $item, 'room' => $room]);
            }
        } elseif ($item->type == UserObject::TYPE_REST) {
            $rest_object_id = null;
            if ($rest_hotel_room = RestHotelRoom::find($request->get('rest_hotel_room_id'))) {
                $rest_object_id = $rest_hotel_room->rest_object_id;
            }
            $rest_object_id = $rest_object_id ? $rest_object_id : $request->get('rest_object_id');
            $rest_object = RestObject::find($rest_object_id);
            $uo = null;
            if (!$rest_object) {
                $rest_object = $item->object->objects()->first();
            }
            if ($rest_object->type == 'hotel' && !$rest_hotel_room) {
                $rest_hotel_room = $rest_object->rooms()->first();
            }
            if ($rest_object) {
                $rest = Rest::find($rest_object->rest_id);
                if ($rest) {
                    $uo = UserObject::find($rest->object_id);
                }
            }
            if ($uo && $uo->user_id === Auth::id()) {
                return view('front.cabinet.object.bookingRest',
                    ['item' => $item, 'rest_object' => $rest_object, 'rest_hotel_room' => $rest_hotel_room]);
            }
        }
        return [];
    }

    public function cottageBookingAction(Request $request, CottageBooking $item)
    {
        $c = Cottage::find($item->cottage_id);
        if ($c) {
            $uo = UserObject::find($c->object_id);
        } else {
            return redirect(route('front.cabinet.object.index'));
        }
        if ($uo->user_id !== Auth::id() || $uo->type !== UserObject::TYPE_COTTAGE) {
            return redirect(route('front.cabinet.object.index'));
        }
        switch ($request->get('action')) {
            case 'delete':
                $item->update(['status_id' => CottageBooking::STATUS_DELETE]);
                break;
            case 'approve':
                $item->update(['status_id' => CottageBooking::STATUS_DONE]);
                UserObjectService::bookingObjectByDates($uo, $item->departure_at, $item->arrival_at,
                    "$item->first_name $item->last_name", $item->phone, $item->email);
                break;
        }

        return redirect(route('front.cabinet.object.booking', $uo->id));
    }

    public function bookingAction(Request $request, UserObjectBooking $item)
    {
        $uo = UserObject::find($item->object_id);
        if (!$uo || $uo->user_id !== Auth::id() || $uo->type !== UserObject::TYPE_COTTAGE) {
            return redirect(route('front.cabinet.object.index'));
        }
        switch ($request->get('action')) {
            case 'delete':
                $item->delete();
                break;
        }
        return redirect(route('front.cabinet.object.booking', $uo->id));
    }

    public function action(Request $request, UserObject $item)
    {
        if ($item->user_id !== Auth::id()) {
            return redirect(route('front.cabinet.object.index'));
        }
        switch ($request->get('action')) {
            case 'active':
                $item->update(['status_id' => UserObject::STATUS_ACTIVE]);
                break;
            case 'inactive':
                $item->update(['end_at' => Date('Y-m-d H:i:s')]);
                UserObjectAddition::where('object_id', $item->id)->update(['is_active' => 0]);
                $item->update(['status_id' => UserObject::STATUS_INACTIVE]);
                break;
            case 'restore':
                $item->update(['status_id' => UserObject::STATUS_INACTIVE]);
                break;
            case 'archive':
                UserObjectAddition::where('object_id', $item->id)->update(['is_active' => 0]);
                $item->update(['status_id' => UserObject::STATUS_ARCHIVE]);
                break;
        }
        return redirect(route('front.cabinet.object.index'));
    }

    public function view(Request $request, UserObject $item)
    {
        $item->update([
            'count_view' => $item->count_view + 1,
            'count_view_today' => $item->count_view_today + 1
        ]);
        $similar = [];
        $similar_query = UserObject::where(function ($query) use ($item) {
            $query->where(function ($query) use ($item) {
                $query->where([
                    'direction_id' => $item->direction_id,
                    'status_id' => UserObject::STATUS_ACTIVE,
                    //                'type' => $item->type
                ])
                    ->whereHas('additions', function ($_q) {
                        $_q->where(['addition_id' => UserObjectAddition::DISPLAY_COMPETITOR_ONE, 'is_active' => 1]);
                    });
            })->orWhere(function ($query) use ($item) {
                $query->where([
                    'status_id' => UserObject::STATUS_ACTIVE,
                    //                'type' => $item->type
                ])
                    ->whereHas('additions', function ($_q) {
                        $_q->where(['addition_id' => UserObjectAddition::DISPLAY_COMPETITOR_ALL, 'is_active' => 1]);
                    });
            });
        })->whereNotIn('id', [$item->id])->inRandomOrder();
        $metadata = [];

        switch ($item->type) {
            case UserObject::TYPE_HOTEL:
                $metadata = [
                    'tags' => [
                        'title' => "Отель {$item->title} | телефон {$item->phone} | ID {$item->id} | в {$item->district->name} районе",
                        'description' => "Забронировать  номер в отеле {$item->title} в {$item->direction->name} направлении в {$item->district->name} районе по телефону {$item->phone}. Его ID: {$item->id}"
                    ]
                ];
                break;
            case UserObject::TYPE_REST:
                $metadata = [
                    'tags' => [
                        'title' => "База отдыха {$item->title} | телефон {$item->phone} | ID {$item->id} | в {$item->district->name} районе",
                        'description' => "Забронировать коттедж или номер на базе отдыха {$item->title} в {$item->direction->name} направлении в {$item->district->name} районе по телефону {$item->phone}. Его ID: {$item->id}"
                    ]
                ];
                break;
            case UserObject::TYPE_COTTAGE:
                $metadata = [
                    'tags' => [
                        'title' => "Коттедж {$item->title} | телефон {$item->phone} | ID {$item->id} | в {$item->district->name} районе",
                        'description' => "Забронировать коттедж {$item->title} в {$item->direction->name} направлении в {$item->district->name} районе по телефону {$item->phone}. Его ID: {$item->id}"
                    ]
                ];
                break;
        }
        $similar = $similar_query->limit(3)->get();
        return view('front.object.' . $item->type, ['item' => $item, 'similar' => $similar, 'metadata' => $metadata]);
    }

    public function viewRestHotel(Request $request, RestObject $item)
    {
        $rest = Rest::find($item->rest_id);
        $uo = UserObject::find($rest->object_id);
        return view('front.object.restHotel', ['item' => $item, 'uo' => $uo]);
    }

    public function viewRestCottage(Request $request, RestObject $item)
    {
        $rest = Rest::find($item->rest_id);
        $uo = UserObject::find($rest->object_id);
        return view('front.object.restCottage', ['item' => $item, 'uo' => $uo]);
    }

    public function uploadPhoto(Request $request)
    {
        if ($file = $request->file('file')) {
            if (strpos($file->getMimeType(), 'image/') !== false) {
                $extension = $file->getClientOriginalExtension();
                $mime = $file->getMimeType();
                $full_name = md5(rand(10000000, 99999999) . time()) . ".{$extension}";
                $file->move(public_path() . '/gallery', $full_name);
                $item = UserObjectGallery::create([
                    'type_id' => UserObjectGallery::TYPE_PHOTO,
                    'extension' => $extension,
                    'mime' => $mime,
                    'value' => "/gallery/$full_name",
                    'hash' => $request->get('hash'),
                ]);
                return [
                    'render' => view('front.cabinet.object.partials.imageBox', [
                        'item' => $item,
                    ])->render()
                ];
            }
            return ['message' => 'Не верный формат фото.'];
        }
        return ['message' => 'Не выбран файл'];
    }


    //hotel room
    public function hotelRoomReservationAction(Request $request, HotelRoomReservation $item)
    {
        $room = HotelRoom::find($item->room_id);
        if ($room) {
            $hotel = Hotel::find($room->hotel_id);
            if ($hotel) {
                $uo = UserObject::find($hotel->object_id);
            }
        } else {
            return redirect(route('front.cabinet.object.index'));
        }
        if ($uo->user_id !== Auth::id() || $uo->type !== UserObject::TYPE_HOTEL) {
            return redirect(route('front.cabinet.object.index'));
        }
        switch ($request->get('action')) {
            case 'delete':
                $item->update(['status_id' => HotelRoomReservation::STATUS_DELETE]);
                break;
            case 'approve':
                $item->update(['status_id' => HotelRoomReservation::STATUS_DONE]);
                UserObjectService::bookingHotelRoomByDates($room, $item->departure_at, $item->arrival_at,
                    "$item->first_name $item->last_name", $item->phone, $item->email);
                break;
        }

        return redirect(route('front.cabinet.object.booking', ['item' => $uo->id, 'room' => $room]));
    }

    public function hotelRoomBookingAction(Request $request, HotelRoomBooking $item)
    {
        $uo = null;
        $hotel = null;
        $room = HotelRoom::find($item->room_id);
        if ($room) {
            $hotel = Hotel::find($room->hotel_id);
            if ($hotel) {
                $uo = UserObject::find($hotel->object_id);
            }
        }
        if (!$uo || $uo->user_id !== Auth::id() || $uo->type !== UserObject::TYPE_HOTEL) {
            return redirect(route('front.cabinet.object.index'));
        }
        switch ($request->get('action')) {
            case 'delete':
                $item->delete();
                if ($hotel) {
                    UserObjectService::handlerHotel($hotel);
                }
                break;
        }
        return redirect(route('front.cabinet.object.booking', ['item' => $uo->id, 'room' => $room]));
    }

    //rest hotel
    public function restHotelReservationAction(Request $request, RestHotelRoomReservation $item)
    {
        $uo = null;
        $rest_object = null;
        $rest_hotel_room = RestHotelRoom::find($item->room_id);
        if ($rest_hotel_room) {
            $rest_object = RestObject::find($rest_hotel_room->rest_object_id);
            if ($rest_object) {
                $rest = Rest::find($rest_object->rest_id);
                if ($rest) {
                    $uo = UserObject::find($rest->object_id);
                }
            }
        }
        if ($uo && ($uo->user_id !== Auth::id() || $uo->type !== UserObject::TYPE_REST)) {
            return redirect(route('front.cabinet.object.index'));
        }
        switch ($request->get('action')) {
            case 'delete':
                $item->update(['status_id' => RestHotelRoomReservation::STATUS_DELETE]);
                break;
            case 'approve':
                $item->update(['status_id' => RestHotelRoomReservation::STATUS_DONE]);
                UserObjectService::bookingRestHotelRoomByDates($rest_hotel_room, $item->departure_at, $item->arrival_at,
                    "$item->first_name $item->last_name", $item->phone, $item->email);
                break;
        }

        return redirect(route('front.cabinet.object.booking',
            ['item' => $uo->id, 'rest_object_id' => $rest_object, 'rest_hotel_room_id' => $rest_hotel_room]));
    }

    public function restHotelBookingAction(Request $request, RestHotelRoomBooking $item)
    {
        $uo = null;
        $hotel = null;
        $rest = null;
        $rest_object = null;
        $rest_hotel_room = RestHotelRoom::find($item->room_id);
        if ($rest_hotel_room) {
            $rest_object = RestObject::find($rest_hotel_room->rest_object_id);
            if ($rest_object) {
                $rest = Rest::find($rest_object->rest_id);
                if ($rest) {
                    $uo = UserObject::find($rest->object_id);
                }
            }
        }
        if (!$uo || $uo->user_id !== Auth::id() || $uo->type !== UserObject::TYPE_REST) {
            return redirect(route('front.cabinet.object.index'));
        }
        switch ($request->get('action')) {
            case 'delete':
                $item->delete();
                if ($rest) {
                    UserObjectService::handlerRest($rest);
                }
                break;
        }
        return redirect(route('front.cabinet.object.booking',
            ['item' => $uo->id, 'rest_object_id' => $rest_object, 'rest_hotel_room_id' => $rest_hotel_room]));
    }


    //rest
    public function restReservationAction(Request $request, RestObjectReservation $item)
    {
        $uo = null;
        $rest_object = RestObject::find($item->rest_object_id);
        if ($rest_object) {
            $rest = Rest::find($rest_object->rest_id);
            if ($rest) {
                $uo = UserObject::find($rest->object_id);
            }
        }
        if ($uo && ($uo->user_id !== Auth::id() || $uo->type !== UserObject::TYPE_REST)) {
            return redirect(route('front.cabinet.object.index'));
        }
        switch ($request->get('action')) {
            case 'delete':
                $item->update(['status_id' => RestObjectReservation::STATUS_DELETE]);
                break;
            case 'approve':
                $item->update(['status_id' => RestObjectReservation::STATUS_DONE]);
                UserObjectService::bookingRestObjectByDates($rest_object, $item->departure_at, $item->arrival_at,
                    "$item->first_name $item->last_name", $item->phone, $item->email);
                break;
        }

        return redirect(route('front.cabinet.object.booking', ['item' => $uo->id, 'rest_object_id' => $rest_object]));
    }

    public function restBookingAction(Request $request, RestObjectReservation $item)
    {
        $uo = null;
        $hotel = null;
        $rest = null;
        $rest_object = RestObject::find($item->rest_object_id);
        if ($rest_object) {
            $rest = Rest::find($rest_object->rest_id);
            if ($rest) {
                $uo = UserObject::find($rest->object_id);
            }
        }
        if (!$uo || $uo->user_id !== Auth::id() || $uo->type !== UserObject::TYPE_REST) {
            return redirect(route('front.cabinet.object.index'));
        }
        switch ($request->get('action')) {
            case 'delete':
                $item->delete();
                if ($rest) {
                    UserObjectService::handlerRest($rest);
                }
                break;
        }
        return redirect(route('front.cabinet.object.booking', ['item' => $uo->id, 'rest_object_id' => $rest_object]));
    }


}
