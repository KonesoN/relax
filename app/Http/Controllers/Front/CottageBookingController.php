<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\BookingConfirm;
use App\Models\Cottage;
use App\Models\CottageBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class CottageBookingController extends Controller
{
    public function new(Request $request)
    {
        $c = Cottage::find($request->get('cottage_id'));
        if ($c) {
            $data = $request->only([
                'first_name',
                'last_name',
                'phone',
                'email',
                'arrival_at',
                'departure_at',
                'cottage_id',
            ]);
            $hash = md5(strtotime('now') . Str::random(10));
            $data['hash'] = $hash;
            $data['status_id'] = CottageBooking::STATUS_NEW;
            $data['arrival_at'] = Date('Y-m-d', strtotime($data['arrival_at']));
            $data['departure_at'] = Date('Y-m-d', strtotime($data['departure_at']));
            $data['phone'] = '+' . preg_replace('/[^0-9]+/', '', trim(($data['phone'] ?? ''), " '"));
            $item = CottageBooking::create($data);
//            try {
//                Mail::to([$data['email']])->send(new BookingConfirm($item));
//            } catch (\Exception $e) {
                $item->update(['status_id' => CottageBooking::STATUS_USER_CONFIRM]);
//            }
        }
        return [];
    }

    public function confirm(Request $request, $hash)
    {
        $cb = CottageBooking::find($hash);
        if ($cb) {
            $cb->update(['status_id' => CottageBooking::STATUS_USER_CONFIRM, 'hash' => null]);
            $request->session()->flash('alert_message', 'Бронь подтверждена, ожидайте звонка');
        }
        return redirect(route('front.index'));
    }
}
