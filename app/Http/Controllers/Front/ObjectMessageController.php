<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\ObjectDialog;
use App\Models\ObjectDialogAttachment;
use App\Models\ObjectDialogMessage;
use App\Models\ObjectReview;
use App\Models\RestObject;
use App\Models\SupportDialog;
use App\Models\SupportDialogMessage;
use App\Models\User;
use App\Models\UserObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ObjectMessageController extends Controller
{
    public function index(Request $request)
    {
        if (Auth::user()->type_id == User::TYPE_OWNER) {
            $support = SupportDialog::firstOrCreate([
                'user_id' => Auth::id(),
            ]);
            $object_ids = UserObject::where('user_id', Auth::id())->get('id')->pluck('id')->toArray();
            $rest_object_ids = RestObject::whereIn('rest_id', $object_ids)->get('id')->pluck('id')->toArray();
            $query = ObjectDialog::where(function ($q) use ($object_ids) {
                $q->whereIn('parent_id', $object_ids)
                    ->where('parent_type', ObjectDialog::TYPE_USER_OBJECT);
            })->orWhere(function ($q) use ($rest_object_ids) {
                $q->whereIn('parent_id', $rest_object_ids)
                    ->where('parent_type', ObjectDialog::TYPE_REST_OBJECT);
            })
                ->orderBy('last_message_at', 'desc');
            return view('front.cabinet.message.index', [
                'support' => $support,
                'pagination' => $query->paginate('20', ['*'], 'page', $request->get('page'))
            ]);
        } else {
            $query = ObjectDialog::where('user_id', Auth::id())->orderBy('last_message_at', 'desc');
            return view('front.cabinet.message.index', [
                'pagination' => $query->paginate('20', ['*'], 'page', $request->get('page'))
            ]);
        }
    }

    public function chat(Request $request, ObjectDialog $item)
    {
        if ($request->isMethod('post')) {
            $message = trim($request->get('message'));
            $file = $request->file('file');
            if ($file && strpos($file->getMimeType(), 'image/') === false) {
                $file = null;
            }
            if ($message || $file) {
                $_message = null;
//                try {
                $_message = ObjectDialogMessage::create([
                    'dialog_id' => $item->id,
                    'message' => $message,
                    'is_user' => Auth::user()->type_id == User::TYPE_USER ? 1 : 0
                ]);
                $user_new_messages = $item->user_new_messages;
                $object_new_messages = $item->object_new_messages;
                if (Auth::user()->type_id == User::TYPE_USER) {
                    $object_new_messages += 1;
                } else {
                    $user_new_messages += 1;
                }
                $item->update([
                    'last_message_at' => Date('Y-m-d H:i:s'),
                    'user_new_messages' => $user_new_messages,
                    'object_new_messages' => $object_new_messages
                ]);
                if ($file) {
                    $extension = $file->getClientOriginalExtension();
                    $mime = $file->getMimeType();
                    $full_name = md5(rand(10000000, 99999999) . time()) . ".{$extension}";
                    $file->move(public_path() . '/attachment', $full_name);
                    ObjectDialogAttachment::create([
                        'message_id' => $_message->id,
                        'type_id' => ObjectDialogAttachment::TYPE_PHOTO,
                        'extension' => $extension,
                        'mime' => $mime,
                        'path_file' => "/attachment/$full_name",
                    ]);
                }
                return [
                    'last_message_id' => $_message->id
                ];
//                } catch (\Exception $e) {
//                    if ($_message && !$_message->message) {
//                        $_message->delete();
//                    }
//                }
            }
        } else {
            $user_new_messages = $item->user_new_messages;
            $object_new_messages = $item->object_new_messages;
            if (Auth::user()->type_id == User::TYPE_USER) {
                ObjectDialogMessage::where(['dialog_id' => $item->id, 'is_user' => 0])->update(['is_read' => 1]);
                $user_new_messages = 0;
            } else {
                ObjectDialogMessage::where(['dialog_id' => $item->id, 'is_user' => 1])->update(['is_read' => 1]);
                $object_new_messages = 0;
            }
            $item->update([
                'last_message_at' => Date('Y-m-d H:i:s'),
                'user_new_messages' => $user_new_messages,
                'object_new_messages' => $object_new_messages
            ]);
        }
        $query = ObjectDialogMessage::where('dialog_id', $item->id);

        return view('front.cabinet.message.chat', [
            'item' => $item,
            'pagination' => $query->paginate('100', ['*'], 'page', $request->get('page'))
        ]);
    }

    public function getMessages(Request $request, ObjectDialog $item)
    {
        $render = false;
        $user_new_messages = $item->user_new_messages;
        $object_new_messages = $item->object_new_messages;
        if (Auth::user()->type_id == User::TYPE_USER) {
            ObjectDialogMessage::where(['dialog_id' => $item->id, 'is_user' => 0])->update(['is_read' => 1]);
            $user_new_messages = 0;
        } else {
            ObjectDialogMessage::where(['dialog_id' => $item->id, 'is_user' => 1])->update(['is_read' => 1]);
            $object_new_messages = 0;
        }

        $item->update([
            'last_message_at' => Date('Y-m-d H:i:s'),
            'user_new_messages' => $user_new_messages,
            'object_new_messages' => $object_new_messages
        ]);

        $query = ObjectDialogMessage::where('dialog_id', $item->id);
        $query_check = clone $query;
        $last = $query_check->orderBy('id', 'desc')->first();
        if (strtotime($last->updated_at) !== intval($request->get('last_message_update'))) {
            $render = true;
        }

        if (!$render && $last->id == $request->get('last_message_id')) {
            return [];
        }
        return [
            'render' => view('front.cabinet.message.partials.dialogBody', [
                'pagination' => $query->paginate('100', ['*'], 'page', $request->get('page'))
            ])->render(),
            'last_message_id' => $last->id,
            'last_message_update' => strtotime($last->updated_at)
        ];
    }

    public function new(Request $request)
    {
        switch ($request->get('parent_type')) {
            case ObjectReview::TYPE_REST_OBJECT:
                $object = RestObject::find($request->get('parent_id'));
                break;
            default:
                $object = UserObject::find($request->get('parent_id'));
                break;
        }
        if ($object) {
            if ($message = trim($request->get('message'))) {
                $dialog = ObjectDialog::firstOrCreate([
                    'user_id' => Auth::id(),
                    'parent_id' => $object->id,
                    'parent_type' => $request->get('parent_type')
                ]);
                ObjectDialogMessage::create([
                    'dialog_id' => $dialog->id,
                    'message' => $message,
                    'is_user' => 1,
                    'is_read' => 0
                ]);
                $dialog->update([
                    'last_message_at' => Date('Y-m-d H:i:s'),
                    'user_new_messages' => 0,
                    'object_new_messages' => 1
                ]);
            }
        }
        return [];
    }
}
