<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cottage extends Model
{
    protected $fillable = [
        'object_id',
        'living_space',
        'count_guest',
        'count_bedrooms',
        'count_small_bedrooms',
        'count_big_bedrooms',
        'price_day',
        'price_day_weekend',
        'price_weekends',
    ];

    public function bookings()
    {
        return $this->hasMany(CottageBooking::class, 'cottage_id', 'id');
    }
}
