<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserObjectAddition extends Model
{
    protected $fillable = ['addition_id', 'days', 'start_at', 'end_at', 'object_id', 'is_active'];

    const PREMIUM = 1;
    const SERVICE_SELECTION = 2;
    const DISPLAY_COMPETITOR_ONE = 3;
    const DISPLAY_COMPETITOR_ALL = 4;

    const GROUP = [
        'DISPLAY_COMPETITOR' => [self::DISPLAY_COMPETITOR_ONE, self::DISPLAY_COMPETITOR_ALL]
    ];

    const ADDITIONS = [
        self::PREMIUM => [
            'name' => 'Премиум размещение',
            'logo' => '/img/front/prem_icon.png',
            'descriptions' => [
                '- всегда показывается выше обычных объявлений по любому условию поиска',
                '- выделяется цветом',
            ],
            'days' => [
                7 => 3000,
                30 => 9000,
            ]
        ],
        self::SERVICE_SELECTION => [
            'name' => 'Выбор сервиса',
            'logo' => '/img/front/vip.png',
            'descriptions' => [
                '- крупное объявление',
                '- логотип «Выбор сервиса»',
            ],
            'days' => [
                7 => 3000,
                30 => 9000,
            ]
        ],
        self::DISPLAY_COMPETITOR_ONE => [
            'name' => 'Показ на страницах конкурентов по направлению',
            'logo' => '/img/front/competitor_icon.png',
            'descriptions' => [
                'В карточках объявлений конкурентов есть блок «похожие варианты».',
                'В этом блоке будет показываться ваш объект.'
            ],
            'days' => [
                7 => 1500,
                30 => 4000,
            ],
            'group' => 'DISPLAY_COMPETITOR',
            'group_ids' => [self::DISPLAY_COMPETITOR_ONE, self::DISPLAY_COMPETITOR_ALL]
        ],
        self::DISPLAY_COMPETITOR_ALL => [
            'name' => 'Показ на страницах конкурентов по всем направлениям',
            'logo' => '/img/front/competitor_icon.png',
            'descriptions' => [
                'В карточках объявлений конкурентов есть блок «похожие варианты».',
                'В этом блоке будет показываться ваш объект.'
            ],
            'days' => [
                7 => 2500,
                30 => 6000,
            ],
            'group' => 'DISPLAY_COMPETITOR',
            'group_ids' => [self::DISPLAY_COMPETITOR_ONE, self::DISPLAY_COMPETITOR_ALL]
        ]
    ];

    public function getLeftDaysAttribute()
    {
        $meterings = [
            0 => 'дней',
            1 => 'день',
            2 => 'дня',
        ];

        $seconds = strtotime($this->end_at) - strtotime('now');
        if ($seconds <= 0) {
            return 'Завершено';
        } elseif ($seconds >= 86400) {
            $value = ceil($seconds / 86400);
        } else {
            $meterings = [
                0 => 'часов',
                1 => 'час',
                2 => 'часа',
            ];
            $value = ceil($seconds / 3600);
            if ($value <= 0) {
                $value = 1;
            }
        }
        $lastDigits = $value % 100;
        $lastDigits = ($lastDigits >= 20) ? $lastDigits % 10 : $lastDigits;

        if ($lastDigits === 0 ||
            $lastDigits >= 5 && $lastDigits <= 20) {
            $metering = 0;
        } elseif ($lastDigits == 1) {
            $metering = 1;
        } else {
            $metering = 2;
        }

        return "Осталось: $value $meterings[$metering]";
    }
}
