<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionUserWithdrawal extends Model
{
    protected $fillable = ['transaction_id'];
}
