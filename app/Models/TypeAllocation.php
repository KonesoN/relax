<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeAllocation extends Model
{
    protected $fillable = ['allocation_id', 'type', 'days', 'value'];
}
