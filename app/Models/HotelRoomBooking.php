<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelRoomBooking extends Model
{
    protected $fillable = [
        'booking_at',
        'room_id',
        'full_name',
        'phone',
        'email',
    ];
}
