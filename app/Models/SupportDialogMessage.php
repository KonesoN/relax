<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportDialogMessage extends Model
{
    protected $fillable = ['is_user', 'message', 'dialog_id', 'is_read'];

    public function attachment()
    {
        return $this->hasOne(SupportDialogAttachment::class, 'message_id', 'id');
    }
}
