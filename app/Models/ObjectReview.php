<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObjectReview extends Model
{
    const STATUS_NEW = 1;
    const STATUS_APPROVE = 2;
    const STATUS_CANCEL = 3;

    protected $fillable = [
        'review',
        'answer',
        'rating',
        'user_id',
        'phone',
        'parent_id',
        'parent_type',
        'status_id'
    ];

    const TYPE_USER_OBJECT = 'user_object';
    const TYPE_REST_OBJECT = 'rest_object';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function userObject()
    {
        return $this->hasOne(UserObject::class, 'id', 'parent_id');
    }

    public function restObject()
    {
        return $this->hasOne(RestObject::class, 'id', 'parent_id');
    }

    public function object()
    {
        switch ($this->parent_type) {
            case self::TYPE_USER_OBJECT:
                return $this->userObject();
                break;
            case self::TYPE_REST_OBJECT:
                return $this->restObject();
                break;
        }
        return $this->userObject()->where('id', -1);
    }
}
