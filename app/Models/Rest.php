<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rest extends Model
{
    protected $fillable = ['object_id'];

    public function cottages()
    {
        return $this->hasMany(RestObject::class, 'rest_id', 'id')->where('type', 'cottage');
    }

    public function hotels()
    {
        return $this->hasMany(RestObject::class, 'rest_id', 'id')->where('type', 'hotel');
    }

    public function objects()
    {
        return $this->hasMany(RestObject::class, 'rest_id', 'id');
    }
}
