<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestHotelRoomBooking extends Model
{
    protected $fillable = [
        'booking_at',
        'room_id',
        'full_name',
        'phone',
        'email',
    ];
}
