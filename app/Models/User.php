<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    const TYPE_USER = 1;
    const TYPE_OWNER = 2;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'is_active',
        'balance',
        'type_id',
        'phone'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function objects()
    {
        return $this->hasMany(UserObject::class, 'user_id', 'id');
    }

    public function getUnreadDialogCountAttribute()
    {
        if ($this->type_id == self::TYPE_OWNER) {
            $count = ObjectDialog::whereIn('parent_id',
                $this->objects()->get('id')->pluck('id')->toArray())->where('object_new_messages', '>=',
                1)->count();
            $item = SupportDialog::firstOrCreate([
                'user_id' => Auth::id(),
            ]);
            $count_support = $item->user_new_messages;
            return $count + $count_support;
        } else {
            return ObjectDialog::where('user_new_messages', '>=', 1)->where('user_id', Auth::id())->count();
        }
    }
}
