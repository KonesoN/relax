<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportDialogAttachment extends Model
{
    protected $fillable = ['message_id', 'path_file', 'type_id', 'extension', 'mime'];

    const TYPE_PHOTO = 1;
}
