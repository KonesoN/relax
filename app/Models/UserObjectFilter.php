<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserObjectFilter extends Model
{
    protected $fillable = ['object_id', 'value'];

    public $timestamps = false;
}
