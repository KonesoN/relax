<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionObjectPayment extends Model
{

    protected $fillable = ['transaction_id', 'object_id', 'payment_url'];

    public function parent()
    {
        return $this->hasOne(Transaction::class, 'id', 'transaction_id');
    }
}
