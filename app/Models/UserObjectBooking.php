<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserObjectBooking extends Model
{
    protected $fillable = [
        'booking_at',
        'object_id',
        'full_name',
        'phone',
        'email',
    ];
}
