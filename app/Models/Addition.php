<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Addition extends Model
{
    protected $fillable = ['addition_id', 'type', 'days', 'value'];
}
