<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestObjectBooking extends Model
{
    protected $fillable = [
        'booking_at',
        'rest_object_id',
        'full_name',
        'phone',
        'email',
    ];
}
