<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['user_id', 'type_id', 'status_id', 'method_id', 'amount', 'comment'];

    const TYPE_USER_REFILL = 1;
    const TYPE_OBJECT_PAYMENT = 2;
    const TYPE_USER_WITHDRAWAL = 3;

    const STATUS_WAIT = 1;
    const STATUS_DONE = 2;
    const STATUS_CANCEL = 3;

    const STATUSES = [
        self::STATUS_WAIT => 'Ожидание',
        self::STATUS_DONE => 'Успешно',
        self::STATUS_CANCEL => 'Отменено',
    ];
    const TYPES = [
        self::TYPE_USER_REFILL => 'Пополнение',
        self::TYPE_OBJECT_PAYMENT => 'Оплата за объект',
        self::TYPE_USER_WITHDRAWAL => 'Вывод',
    ];

    const METHOD_BANK = 1;
    const METHOD_INVOICE = 2;
    const METHOD_YMONEY = 3;
    const METHOD_QIWI = 4;
    const METHOD_BALANCE = 5;

    const METHODS = [
        self::METHOD_BALANCE => [
            'name' => 'Баланс',
            'logo' => '/img/admin/wallet.png',
            'view' => [
                self::TYPE_USER_REFILL => false,
                self::TYPE_OBJECT_PAYMENT => true,
            ]
        ],
        self::METHOD_BANK => [
            'name' => 'Банковская карта',
            'logo' => '/img/admin/credit_card.png',
            'view' => [
                self::TYPE_USER_REFILL => true,
                self::TYPE_OBJECT_PAYMENT => true,
            ]
        ],
        self::METHOD_INVOICE => [
            'name' => 'Выставить счёт для юр. лица',
            'logo' => '/img/admin/score.png',
            'view' => [
                self::TYPE_USER_REFILL => true,
                self::TYPE_OBJECT_PAYMENT => true,
            ]
        ],
        self::METHOD_YMONEY => [
            'name' => 'ЮMoney',
            'logo' => '/img/admin/iomoney.png',
            'view' => [
                self::TYPE_USER_REFILL => true,
                self::TYPE_OBJECT_PAYMENT => true,
            ]
        ],
        self::METHOD_QIWI => [
            'name' => 'QIWI',
            'logo' => '/img/admin/qiwi.png',
            'view' => [
                self::TYPE_USER_REFILL => true,
                self::TYPE_OBJECT_PAYMENT => true,
            ]
        ],
    ];


    public function data()
    {
        switch ($this->type_id) {
            case self::TYPE_USER_REFILL:
                return $this->hasOne(TransactionUserRefill::class, 'transaction_id', 'id');
                break;
            case self::TYPE_OBJECT_PAYMENT:
                return $this->hasOne(TransactionObjectPayment::class, 'transaction_id', 'id');
                break;
        }
        return $this->hasOne(TransactionUserRefill::class, 'transaction_id', 'id')->where('id', -1);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
