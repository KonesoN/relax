<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelRoomGallery extends Model
{
    protected $fillable = ['hotel_room_id', 'type_id', 'value', 'extension', 'hash', 'mime', 'is_main'];

    const TYPE_PHOTO = 1;
    const TYPE_YOUTUBE_URL = 2;

    public function setValueAttribute($value)
    {
        switch ($this->type_id) {
            case self::TYPE_YOUTUBE_URL:
                if (strpos($value, 'https://www.youtube.com/embed/') === 0) {
                    $this->attributes['extension'] = 'iframe';
                    //good
                } else {
                    $new_value = substr($value, strpos($value, '?v=') + 3);
                    $new_value = trim(substr($new_value, 0, strpos($new_value, '&')));
                    if ($new_value) {
                        $value = "https://www.youtube.com/embed/{$new_value}";
                        $this->attributes['extension'] = 'iframe';
                    }
                }
                break;
        }
        $this->attributes['value'] = $value;
    }
}
