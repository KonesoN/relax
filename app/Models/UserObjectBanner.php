<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserObjectBanner extends Model
{
    protected $fillable = [
        'status_id',
        'position',
        'start_at',
        'end_at',
        'object_id',
    ];

    const STATUS_ACTIVE = 1;
    const STATUS_DELETE = 2;
    const STATUS_ENDED = 3;

    public function userObject()
    {
        return $this->hasOne(UserObject::class, 'id', 'object_id');
    }

    public function getDaysAttribute()
    {
        $meterings = [
            0 => 'дней',
            1 => 'день',
            2 => 'дня',
        ];

        $seconds = strtotime($this->end_at) - strtotime('now');
        if ($seconds <= 0) {
            return 'Завершено';
        } elseif ($seconds >= 86400) {
            $value = ceil($seconds / 86400);
        } else {
            $meterings = [
                0 => 'часов',
                1 => 'час',
                2 => 'часа',
            ];
            $value = ceil($seconds / 3600);
            if ($value <= 0) {
                $value = 1;
            }
        }
        $lastDigits = $value % 100;
        $lastDigits = ($lastDigits >= 20) ? $lastDigits % 10 : $lastDigits;

        if ($lastDigits === 0 ||
            $lastDigits >= 5 && $lastDigits <= 20) {
            $metering = 0;
        } elseif ($lastDigits == 1) {
            $metering = 1;
        } else {
            $metering = 2;
        }

        return "Осталось: $value $meterings[$metering]";
    }

}
