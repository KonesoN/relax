<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserObject extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_ARCHIVE = 3;

    const STATUS_IDS = [
        self::STATUS_ACTIVE,
        self::STATUS_INACTIVE,
        self::STATUS_ARCHIVE
    ];

    const TYPE_COTTAGE = 'cottage';
    const TYPE_HOTEL = 'hotel';
    const TYPE_REST = 'rest';

    const TYPES = [
        self::TYPE_COTTAGE => 'Коттедж',
        self::TYPE_HOTEL => 'Отель',
        self::TYPE_REST => 'База отдыха',
    ];

    const TYPE_ALLOCATION_BASIC = 1;

    const TYPES_ALLOCATION = [
        self::TYPE_ALLOCATION_BASIC => [
            'days' => [
                7 => 500,
                30 => 1500,
            ]
        ]
    ];

    protected $fillable = [
        'user_id',
        'status_id',
        'type',
        'start_at',
        'end_at',
        'type_allocation_id',
        'day_allocation',
        'title',
        'address',
        'distance_to_city',
        'is_shore',
        'district_id',
        'direction_id',
        'is_the_city',
        'price_type_id',
        'count_view',
        'count_view_today',
        'arrival_at',
        'departure_at',
        'minimum_days',
        'insurance_deposit',
        'is_pets',
        'description',
        'new_reviews',
        'lat',
        'lng',
        'phone',
    ];

    public function object()
    {
        switch ($this->type) {
            case self::TYPE_COTTAGE:
                return $this->cottage();
                break;
            case self::TYPE_HOTEL:
                return $this->hotel();
                break;
            case self::TYPE_REST:
                return $this->rest();
                break;
        }
        return $this->cottage()->where('id', -1);
    }

    public function getBookingNewRequestAttribute()
    {
        $total = 0;

        switch ($this->type) {
            case self::TYPE_COTTAGE:
                $total = $this->cottage->bookings()->where('status_id',
                    \App\Models\CottageBooking::STATUS_USER_CONFIRM)->count();
                break;
            case self::TYPE_HOTEL:
                $total = 0;
                foreach ($this->hotel->rooms as $room) {
                    $total += $room->reservations()->where('status_id',
                        \App\Models\HotelRoomReservation::STATUS_USER_CONFIRM)->count();
                }
                break;
            case self::TYPE_REST:
                foreach ($this->rest->hotels as $hotel) {
                    foreach ($hotel->rooms as $room) {
                        $total += $room->reservations()->where('status_id',
                            \App\Models\RestHotelRoomReservation::STATUS_USER_CONFIRM)->count();
                    }
                }
                foreach ($this->rest->objects as $object) {
                    $total += $object->reservations()->where('status_id',
                        \App\Models\RestObjectReservation::STATUS_USER_CONFIRM)->count();
                }
                break;
        }
        return $total;
    }

    public function cottage()
    {
        return $this->hasOne(Cottage::class, 'object_id', 'id');
    }

    public function hotel()
    {
        return $this->hasOne(Hotel::class, 'object_id', 'id');
    }

    public function rest()
    {
        return $this->hasOne(Rest::class, 'object_id', 'id');
    }

    public function direction()
    {
        return $this->hasOne(Direction::class, 'id', 'direction_id');
    }

    public function district()
    {
        return $this->hasOne(District::class, 'id', 'district_id');
    }

    public function transactions()
    {
        return $this->hasMany(TransactionObjectPayment::class, 'object_id', 'id');
    }

    public function activeReviews()
    {
        return $this->reviews()->where('status_id', ObjectReview::STATUS_APPROVE);
    }

    public function reviews()
    {
        return $this->hasMany(ObjectReview::class, 'parent_id', 'id')->where('parent_type',
            ObjectReview::TYPE_USER_OBJECT);
    }

    public function bookings()
    {
        return $this->hasMany(UserObjectBooking::class, 'object_id', 'id');
    }

    public function dialogs()
    {
        return $this->hasMany(ObjectDialog::class, 'parent_id', 'id')->where('parent_type',
            ObjectDialog::TYPE_USER_OBJECT);
    }

    public function activeBookings()
    {
        return $this->bookings()->where('booking_at', '>=', Date('Y-m-d') . ' 00:00:00')->orderBy('booking_at', 'asc');
    }

    public function getBusyDatesAttribute()
    {
        return $this->activeBookings()->get()->pluck('booking_at')->toArray();
    }

    public function gallery()
    {
        return $this->hasMany(UserObjectGallery::class, 'object_id', 'id');
    }

    public function photos()
    {
        return $this->gallery()->where('type_id', UserObjectGallery::TYPE_PHOTO);
    }

    public function getMainPhotoAttribute()
    {
        $main = $this->photos()->where('is_main', 1)->first();
        if (!$main) {
            $main = $this->photos()->first();
        }
        return $main;
    }

    public function youtube()
    {
        return $this->gallery()->where('type_id', UserObjectGallery::TYPE_YOUTUBE_URL);
    }

    public function additions()
    {
        return $this->hasMany(UserObjectAddition::class, 'object_id', 'id');
    }

    public function getDaysAttribute()
    {
        $meterings = [
            0 => 'дней',
            1 => 'день',
            2 => 'дня',
        ];

        $seconds = strtotime($this->end_at) - strtotime('now');
        if ($this->transactions()->orderBy('id', 'desc')->first() && $this->transactions()->orderBy('id',
                'desc')->first()->parent->status_id == Transaction::STATUS_WAIT) {
            return 'Ожидание оплаты';
        } elseif ($seconds <= 0) {
            return 'Завершено';
        } elseif ($seconds >= 86400) {
            $value = ceil($seconds / 86400);
        } else {
            $meterings = [
                0 => 'часов',
                1 => 'час',
                2 => 'часа',
            ];
            $value = ceil($seconds / 3600);
            if ($value <= 0) {
                $value = 1;
            }
        }
        $lastDigits = $value % 100;
        $lastDigits = ($lastDigits >= 20) ? $lastDigits % 10 : $lastDigits;

        if ($lastDigits === 0 ||
            $lastDigits >= 5 && $lastDigits <= 20) {
            $metering = 0;
        } elseif ($lastDigits == 1) {
            $metering = 1;
        } else {
            $metering = 2;
        }

        return "Осталось: $value $meterings[$metering]";
    }

    public function filters()
    {
        return $this->hasMany(UserObjectFilter::class, 'object_id', 'id');
    }

    public function getFilterValuesAttribute()
    {
        return $this->filters->pluck('value')->toArray();
    }

    public function favorites()
    {
        return $this->hasMany(UserFavorite::class, 'object_id', 'id');
    }

    public function getFavoriteAttribute()
    {
        return !!$this->favorites->where('user_id', Auth::id())->first();
    }

    const PRICE_TYPES = [
        self::TYPE_COTTAGE => [
            1 => [
                'name' => 'Эконом',
                'price' => 'до <span>7 тыс/сутки</span>',
            ],
            2 => [
                'name' => 'Комфорт',
                'price' => '<span>7-25 тыс/сутки</span>',
            ],
            3 => [
                'name' => 'Премиум',
                'price' => '<span>от 25 тыс/сутки</span>',
            ],
        ],
        self::TYPE_HOTEL => [
            1 => [
                'name' => 'Эконом',
                'price' => 'до <span>5 тыс/сутки</span>',
            ],
            2 => [
                'name' => 'Комфорт',
                'price' => '<span>5-15 тыс/сутки</span>',
            ],
            3 => [
                'name' => 'Премиум',
                'price' => '<span>от 15 тыс/сутки</span>',
            ],
        ],
        self::TYPE_REST => [
            1 => [
                'name' => 'Эконом',
                'price' => 'до <span>10 тыс/сутки</span>',
            ],
            2 => [
                'name' => 'Комфорт',
                'price' => '<span>10-20 тыс/сутки</span>',
            ],
            3 => [
                'name' => 'Премиум',
                'price' => '<span>20-30 тыс/сутки</span>',
            ],
        ],
    ];

    const CATEGORIES = [
        self::CATEGORY_1,
        self::CATEGORY_2,
        self::CATEGORY_3,
        self::CATEGORY_4,
        self::CATEGORY_5,
        self::CATEGORY_6,
        self::CATEGORY_7,
    ];

    const CATEGORY_1 = [
        'category_id' => 1,
        'name' => 'Цель отдыха',
        'values' => [
            1 => ['name' => 'Загородный отдых', 'img' => '\img\front\icons\fa-solid_home.svg'],
            2 => ['name' => 'Бизнес и конференция', 'img' => '\img\front\icons\foundation_torso-business.svg'],
            3 => ['name' => 'Корпоратив / мероприятие', 'img' => '\img\front\icons\vs_party.svg'],
            4 => ['name' => 'Длительная аренда / проживание', 'img' => '\img\front\icons\bx_bxs-calendar.svg'],
            5 => ['name' => 'Свадьба', 'img' => '\img\front\icons\vs_wedding-cake.svg'],
            6 => ['name' => 'Оздоровительный центр', 'img' => '\img\front\icons\ozdorov_center.svg'],
        ]
    ];
    const CATEGORY_2 = [
        'category_id' => 2,
        'name' => 'Развлечения',
        'values' => [
            1 => ['name' => 'Верховая езда', 'img' => '\img\front\icons\mdi_horse-human.svg'],
            2 => ['name' => 'Вейкборд', 'img' => '\img\front\icons\wakeboarding.svg'],
            3 => ['name' => 'Веревочный парк', 'img' => '\img\front\icons\rope-park.svg'],
            4 => ['name' => 'Дайвинг / Подводная охота', 'img' => '\img\front\icons\diving.svg'],
            5 => ['name' => 'Пейнтбол', 'img' => '\img\front\icons\paintball.svg'],
            6 => ['name' => 'Зоопарк', 'img' => '\img\front\icons\elephant.svg'],
            7 => ['name' => 'Рыбалка', 'img' => '\img\front\icons\fishing.svg'],
            8 => ['name' => 'Праздники / мероприятия / банкетный зал', 'img' => '\img\front\icons\champagne.svg'],
            9 => ['name' => 'Охота', 'img' => '\img\front\icons\hunting-with-arm.svg'],
            10 => ['name' => 'Романтический отдых', 'img' => '\img\front\icons\heart.svg'],
        ]
    ];
    const CATEGORY_3 = [
        'category_id' => 3,
        'name' => 'Прокат',
        'values' => [
            1 => ['name' => 'Велосипеды / ролики', 'img' => '\img\front\icons\bike.svg'],
            2 => ['name' => 'Снегоходы', 'img' => '\img\front\icons\snowmobile.svg'],
            3 => ['name' => 'Гидроциклы', 'img' => '\img\front\icons\jet-ski.svg'],
            4 => ['name' => 'Ватрушки', 'img' => '\img\front\icons\button.svg'],
            5 => ['name' => 'Квадроциклы', 'img' => '\img\front\icons\atv.svg'],
            6 => ['name' => 'Лыжи / коньки', 'img' => '\img\front\icons\ski.svg'],
            7 => ['name' => 'Сапсерфинг', 'img' => '\img\front\icons\surfing.svg'],
        ]
    ];
    const CATEGORY_4 = [
        'category_id' => 4,
        'name' => 'Спортивные площадки',
        'values' => [
            1 => ['name' => 'Футбол', 'img' => '\img\front\icons\soccer-ball.svg'],
            2 => ['name' => 'Баскетбол', 'img' => '\img\front\icons\basketball.svg'],
            3 => ['name' => 'Волейбол', 'img' => '\img\front\icons\volleyball.svg'],
            4 => ['name' => 'Теннис', 'img' => '\img\front\icons\tennis-racket.svg'],
            5 => ['name' => 'Гольф', 'img' => '\img\front\icons\golf.svg'],
            6 => ['name' => 'Сквош', 'img' => '\img\front\icons\squash.svg'],
            7 => ['name' => 'Бадминтон', 'img' => '\img\front\icons\badminton.svg'],
        ]
    ];
    const CATEGORY_5 = [
        'category_id' => 5,
        'name' => 'Удобства / опции',
        'values' => [
            1 => ['name' => 'Парковка', 'img' => '\img\front\icons\parking.svg'],
            2 => ['name' => 'Wi Fi', 'img' => '\img\front\icons\wifi.svg'],
            3 => ['name' => 'Баня / сауна', 'img' => '\img\front\icons\banya.svg'],
            4 => ['name' => 'Бассейн', 'img' => '\img\front\icons\pool.svg'],
            5 => ['name' => 'Спа отдых', 'img' => '\img\front\icons\spa.svg'],
            6 => ['name' => 'Подогреваемый бассейн на улице', 'img' => '\img\front\icons\swimming-pool.svg'],
            7 => ['name' => 'Фитнес клуб', 'img' => '\img\front\icons\fintes.svg'],
            8 => ['name' => 'Бар / лобби', 'img' => '\img\front\icons\bar.svg'],
            9 => ['name' => 'Ночной клуб', 'img' => '\img\front\icons\party-dj.svg'],
            10 => ['name' => 'Выход к пляжу', 'img' => '\img\front\icons\sun-umbrella.svg'],
            11 => ['name' => 'Площадка под барбекю', 'img' => '\img\front\icons\grill.svg'],
            12 => ['name' => 'Можно с дом. животными', 'img' => '\img\front\icons\pawprint.svg'],
            13 => ['name' => 'Ресторан', 'img' => '\img\front\icons\restaurant.svg'],
            14 => ['name' => 'Экскурсии', 'img' => '\img\front\icons\tent.svg'],
        ]
    ];

    const CATEGORY_6 = [
        'category_id' => 6,
        'name' => 'Питание',
        'values' => [
            1 => ['name' => 'Завтрак', 'img' => '\img\front\icons\coffee.svg'],
            2 => ['name' => '2/ух разовое', 'img' => '\img\front\icons\coffee2x.svg'],
            3 => ['name' => '3/ех разовое', 'img' => '\img\front\icons\coffee3x.svg'],
            4 => ['name' => 'Все включено', 'img' => '\img\front\icons\all-inclusive.svg'],
            5 => ['name' => 'Кухня в номере', 'img' => '\img\front\icons\kitchen.svg'],
        ]
    ];

    const CATEGORY_7 = [
        'category_id' => 7,
        'name' => 'Для детей',
        'values' => [
            1 => ['name' => 'Аниматоры', 'img' => '\img\front\icons\joker.svg'],
            2 => ['name' => 'Детский бассейн', 'img' => '\img\front\icons\rubber-pool.svg'],
            3 => ['name' => 'Детская площадка', 'img' => '\img\front\icons\slide.svg'],
            4 => ['name' => 'Детская комната', 'img' => '\img\front\icons\bear.svg'],
            5 => ['name' => 'Няня', 'img' => '\img\front\icons\nanny.svg'],
        ]
    ];
    //    const CATEGORY_8 = [
    //        'category_id' => 8,
    //        'name' => '',
    //        'values' => [
    //            1 => '',
    //            2 => '',
    //            3 => '',
    //            4 => '',
    //            5 => '',
    //            6 => '',
    //            7 => '',
    //            8 => '',
    //            9 => '',
    //            10 => '',
    //            11 => '',
    //            12 => '',
    //        ]
    //    ];
}
