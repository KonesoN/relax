<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelRoomReservation extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'arrival_at',
        'departure_at',
        'status_id',
        'hash',
        'room_id',
    ];

    const STATUS_NEW = 1;
    const STATUS_USER_CONFIRM = 2;
    const STATUS_DONE = 3;
    const STATUS_DELETE = 4;
}
