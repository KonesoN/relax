<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelRoom extends Model
{
    protected $fillable = [
        'title',
        'count_guest',
        'count_small_bedrooms',
        'count_big_bedrooms',
        'price_day',
        'price_day_weekend',
        'price_weekends',
        'description',
        'hotel_id',
        'hash',
    ];

    public function gallery()
    {
        return $this->hasMany(HotelRoomGallery::class, 'hotel_room_id', 'id');
    }

    public function photos()
    {
        return $this->gallery()->where('type_id', HotelRoomGallery::TYPE_PHOTO);
    }

    public function getMainPhotoAttribute()
    {
        $main = $this->photos()->where('is_main', 1)->first();
        if (!$main) {
            $main = $this->photos()->first();
        }
        return $main;
    }

    public function youtube()
    {
        return $this->gallery()->where('type_id', HotelRoomGallery::TYPE_YOUTUBE_URL);
    }

    public function reservations()
    {
        return $this->hasMany(HotelRoomReservation::class, 'room_id', 'id');
    }

    public function bookings()
    {
        return $this->hasMany(HotelRoomBooking::class, 'room_id', 'id');
    }

    public function activeBookings()
    {
        return $this->bookings()->where('booking_at', '>=', Date('Y-m-d') . ' 00:00:00')->orderBy('booking_at', 'asc');
    }

    public function getBusyDatesAttribute()
    {
        return $this->activeBookings()->get()->pluck('booking_at')->toArray();
    }
}
