<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestObject extends Model
{
    protected $fillable = [
        'title',
        'count_guest',
        'living_space',
        'count_bedrooms',
        'count_small_bedrooms',
        'count_big_bedrooms',
        'price_day',
        'price_day_weekend',
        'price_weekends',
        'description',
        'arrival_at',
        'departure_at',
        'minimum_days',
        'insurance_deposit',
        'is_pets',
        'rest_id',
        'type',
        'hash',
        'new_reviews',
    ];

    const TYPE_COTTAGE = 'cottage';
    const TYPE_HOTEL = 'hotel';

    public function gallery()
    {
        return $this->hasMany(RestObjectGallery::class, 'rest_object_id', 'id');
    }

    public function rooms()
    {
        return $this->hasMany(RestHotelRoom::class, 'rest_object_id', 'id');
    }

    public function photos()
    {
        return $this->gallery()->where('type_id', RestObjectGallery::TYPE_PHOTO);
    }

    public function getMainPhotoAttribute()
    {
        $main = $this->photos()->where('is_main', 1)->first();
        if (!$main) {
            $main = $this->photos()->first();
        }
        return $main;
    }

    public function youtube()
    {
        return $this->gallery()->where('type_id', RestObjectGallery::TYPE_YOUTUBE_URL);
    }

    public function reservations()
    {
        return $this->hasMany(RestObjectReservation::class, 'rest_object_id', 'id');
    }

    public function bookings()
    {
        return $this->hasMany(RestObjectBooking::class, 'rest_object_id', 'id');
    }

    public function activeBookings()
    {
        return $this->bookings()->where('booking_at', '>=', Date('Y-m-d') . ' 00:00:00')->orderBy('booking_at', 'asc');
    }

    public function getBusyDatesAttribute()
    {
        return $this->activeBookings()->get()->pluck('booking_at')->toArray();
    }

    public function activeReviews()
    {
        return $this->reviews()->where('status_id', ObjectReview::STATUS_APPROVE);
    }

    public function reviews()
    {
        return $this->hasMany(ObjectReview::class, 'parent_id', 'id')->where('parent_type',
            ObjectReview::TYPE_REST_OBJECT);
    }
}
