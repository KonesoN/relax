<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionUserRefill extends Model
{
    protected $fillable = ['transaction_id'];
}
