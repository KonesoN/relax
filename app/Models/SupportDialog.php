<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportDialog extends Model
{
    protected $fillable = [
        'user_id',
        'user_new_messages',
        'support_new_messages',
        'last_message_at'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function messages()
    {
        return $this->hasMany(SupportDialogMessage::class, 'dialog_id', 'id');
    }
}
