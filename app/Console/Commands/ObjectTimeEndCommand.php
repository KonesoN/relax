<?php

namespace App\Console\Commands;

use App\Models\Cottage;
use App\Models\UserObject;
use App\Models\UserObjectAddition;
use App\Models\UserObjectBanner;
use Illuminate\Console\Command;

class ObjectTimeEndCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'object:time_end';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        UserObject::where('end_at', '<=', Date('Y-m-d:H:i:s'))->where('status_id',
            UserObject::STATUS_ACTIVE)->update(['status_id' => UserObject::STATUS_INACTIVE]);
        UserObjectAddition::where('end_at', '<=',
            Date('Y-m-d:H:i:s'))->update(['is_active' => 0]);
        UserObjectBanner::where('end_at', '<=', Date('Y-m-d:H:i:s'))->where('status_id',
            UserObject::STATUS_ACTIVE)->update(['status_id' => UserObjectBanner::STATUS_ENDED]);
        return 0;
    }
}
