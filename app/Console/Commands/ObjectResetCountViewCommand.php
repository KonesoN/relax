<?php

namespace App\Console\Commands;

use App\Models\Cottage;
use App\Models\UserObject;
use Illuminate\Console\Command;

class ObjectResetCountViewCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'object:reset_count_view';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        UserObject::whereNotNull('user_id')->update(['count_view_today' => 0]);
        return 0;
    }
}
