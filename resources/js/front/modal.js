$(document).ready(function () {
    $(document).on('click', '.modal_view', function (e) {
        e.preventDefault();
        const modal_id = $(this).data('modal_id');
        $('#' + modal_id).fadeIn(500);
    });

    $(document).on('click', '.modal_hide', function (e) {
        e.preventDefault();
        hideModal($(this).data('modal_id'))
    });

    window.showGlobalModal = function (message) {
        $('#modal_global_message_text').html(message);
        $('#open-modal_global_message').fadeIn(500);
    }

    window.hideModal = function (modal_id) {
        $('#' + modal_id).fadeOut(500);
    }
});
