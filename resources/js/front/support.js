$(document).ready(function () {
    const _route_current = $('#_route_current').val();
    if (_route_current === 'front.cabinet.support.chat') {
        let getMessagesQuery = false;

        $('#select_file').click(function () {
            $('#message_file').click();
        });
        $('#message_file').change(function () {
            const file = this.files[0];
            if (file && file.type.indexOf('image/') === 0) {

            } else {
                showGlobalModal('Не верный формат фото');
                $(this).val('');
            }
        });

        $('#message_send').click(function () {
            sendMessage();
        });
        $('#message_text').keydown(function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                sendMessage();
            }
        })

        function sendMessage() {
            const file = document.getElementById('message_file').files[0];
            const message = $('#message_text').val();
            if (file || message) {
                let data = new FormData();
                data.append('file', file ? file : null);
                data.append('message', message);
                $.ajax({
                    url: '',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    type: 'POST', // For jQuery < 1.9
                    success: function (data) {
                        $('#last_message_id').val(data.last_message_id);
                        getMessages();
                        document.getElementById('message_file').value = null;
                        $('#message_text').val('');
                    },
                    error: function (data) {
                        showGlobalModal('Ошибка загрузки. Обратитесь к администрации');
                    }
                });
            }
        }

        function getMessages() {
            if (!getMessagesQuery) {
                getMessagesQuery = true;
                let url = $('#_api_render_support_messages').val()
                    + '?last_message_id=' + $('#last_message_id').val()
                    + '&last_message_update=' + $('#last_message_update').val();
                $.get(url)
                    .done(function (data) {
                        getMessagesQuery = false;
                        if (data.render) {
                            $('#dialog_body').html(data.render);
                            $('#last_message_id').val(data.last_message_id)
                            $('#last_message_update').val(data.last_message_update)
                            chatAnimate();
                            setTimeout(function () {
                                chatAnimate();
                            }, 500);
                        }
                    });
            }
        }

        setInterval(function () {
            getMessages();
        }, 10000);

        function chatAnimate() {
            $('.message-body').animate({scrollTop: $('.message-body').prop("scrollHeight")})
        }

        chatAnimate();
    }

});
