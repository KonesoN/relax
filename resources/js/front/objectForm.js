$(document).ready(function () {
    const _route_current = $('#_route_current').val();
    if (_route_current === 'front.cabinet.object.new' || _route_current === 'front.cabinet.object.edit' || _route_current === 'front.cabinet.object.payment' || _route_current === 'admin.userObject.form' || _route_current === 'front.cabinet.object.addition') {
        $('.select_tab').click(function () {
            if (!$(this).data('disabled')) {
                $('.select_tab').removeClass('select_tab-active');
                $(this).addClass('select_tab-active');
                $('.tabs').hide().removeClass('tabs-active');
                $('#tab_' + $(this).data('tab_id')).show().addClass('tabs-active');
            }
        });

        $('.tabs .subprimary-btn').click(function () {
            if (!$(this).data('submit')) {
                $(document).find('.input_error').removeClass('input_error');
                const form = $('#newObject-form .tabs-active');
                let error = validateForm(form);
                if ($(this).data('check_gallery') && !error) {
                    if (form.find('._item_gallery').length <= 0) {
                        error = true;
                        showGlobalModal('Загрузите фото');
                    }
                }

                if (!error) {
                    let tab_id = $(this).data('tab_id');
                    $('.select_tab').removeClass('select_tab-active');
                    $('.select_tab[data-tab_id="' + tab_id + '"]').addClass('select_tab-active')
                    $('.tabs').hide().removeClass('tabs-active');
                    $('#tab_' + tab_id).show().addClass('tabs-active');
                }
            }
        })

        priceCalculated();
        $('.price_calculated').click(function () {
            priceCalculated();
        }).change(function () {
            priceCalculated();
        })

        function priceCalculated() {
            let price = 0;
            let html = '';
            let type = $('[name="type_object"]:checked').val();
            type = type ? type : $('#item_type').val();
            const allocation = $('[name="type_allocation_id"]:checked');
            if (allocation[0]) {
                const allocation_day = $('[name="day_allocation[' + allocation.val() + ']"]:checked');
                let _price = allocation_day.data('price_' + type);
                price += _price;
                html += '<small>' + allocation.data('name') + ' на ' + allocation_day.val() + ' дней  = ' + _price + ' руб</small><br/>';
            }

            $.each($('._addition'), function (index, item) {
                const addition_input = $(item);
                if (addition_input.prop('checked') && !addition_input.prop('disabled')) {
                    let addition = $('[name="addition[' + addition_input.val() + '][day]"]:checked');
                    let _price = addition.data('price_' + type);
                    price += _price;
                    html += '<br/><small>' + addition_input.data('name') + ' на ' + addition.val() + ' дней  = ' + _price + ' руб</small>';
                }
            });

            $('.object_price').html('' + price);
            $('#additions_list').html(html);
            return price;
        }

        $('#order_submit').click(function (e) {
            const form = $('#newObject-form');
            let error = validateForm(form);
            if (error) {
                return false;
            }
            if ($('[name="method_id"]:checked').val() === $('#method_balance').val() && parseFloat($('#user_balance').val()) < priceCalculated()) {
                showGlobalModal('Недостаточно на балансе');
                e.preventDefault();
                return false;
            }
            if (priceCalculated() <= 0) {
                showGlobalModal('Выберите что-то');
                e.preventDefault();
                return false;
            }
        });

        function validateForm(form) {
            let elems = form.find('input,textarea');
            let error = false;
            elems.each(function (index, item) {
                let val = $(item).val();
                if ($(item).prop('required') && (val !== "0" && !val)) {
                    $(item).addClass('input_error');
                    showGlobalModal('Заполнены не все поля. Не заполненные поля обведены красным. Заполните для продолжения.');
                    error = true;
                }
            });
            return error;
        }

        $('#object_add_room').click(function (e) {
            e.preventDefault();
            $.get($('#_api_render_objectHotelRoom').val() + '?hash=' + $('#hash_object').val())
                .done(function (data) {
                    if (data.render) {
                        $('#object_list_room').append(data.render);
                    }
                });
        });

        $(document).on('click', '.rest_add_room', function (e) {
            e.preventDefault();
            $.get($('#_api_render_restHotelRoom').val() + '?hash=' + $('#hash_object').val())
                .done((data) => {
                    if (data.render) {
                        $('#rest_list_room_' + $(this).data('hash')).append(data.render);
                    }
                });
        });

        $('#object_add_rest_cottage').click(function (e) {
            e.preventDefault();
            $.get($('#_api_render_objectRestCottage').val() + '?hash=' + $('#hash_object').val())
                .done(function (data) {
                    if (data.render) {
                        $('#object_list_cottage').append(data.render);
                        handlerTypeRest();
                    }
                });
        });

        $('[name="type_object"]').change(function () {
            handlerTypeObject();
        });

        function handlerTypeObject() {
            let type = $('[name="type_object"]:checked').val();
            type = type ? type : $('#item_type').val();
            if (type) {
                $('.hide_by_type').hide().find('._required').prop('required', false);
                $('.show_by_type_' + type).show().find('._required').prop('required', true);
            }
        }

        handlerTypeObject();

        $(document).on('click', '.type_rest_radio', function () {
            let form = $('.rest_object_form_' + $(this).data('hash_rest'));
            let type = $(form).find('.type_rest_radio:checked').val();
            if (type) {
                $(form).find('.hide_by_rest_type').hide().find('._required').prop('required', false);
                $(form).find('.show_by_rest_type_' + type).show().find('._required').prop('required', true);
            }
        });

        function handlerTypeRest() {
            $.each($('.rest_object_form'), function (index, item) {
                let type = $(item).find('.type_rest_radio:checked').val();
                type = type ? type : $(item).find('.type_rest_radio').val();
                if (type) {
                    $(item).find('.hide_by_rest_type').hide().find('._required').prop('required', false);
                    $(item).find('.show_by_rest_type_' + type).show().find('._required').prop('required', true);
                }
            });
        }

        handlerTypeRest();
        $(document).on('change', '.input-file', async function () {
            let listId = $(this).data('list_id');
            let uploadUrl = $(this).data('upload_url');
            uploadUrl = uploadUrl ? uploadUrl : $('#_api_cabinet_object_uploadPhoto').val();
            $.each(this.files, (index, file) => {
                setTimeout(() => {
                    // if (file && file.type.indexOf('image/') === 0) {
// if (file.size >= 1999999) {
//     alert('Фото должно быть меньше 2Mb');
//     $(this).val('');
// } else {
                    var reader = new FileReader();
                    reader.onload = (e) => {
                        // $('#' + $(this).data('background_id')).css('background', 'url(' + e.target.result + ')');
                        let data = new FormData();
                        data.append('file', file);
                        data.append('type', 'photo');
                        data.append('hash', $('#hash_object').val());
                        $.ajax({
                            url: uploadUrl,
                            data: data,
                            cache: false,
                            contentType: false,
                            processData: false,
                            method: 'POST',
                            type: 'POST', // For jQuery < 1.9
                            success: function (data) {
                                let list = $('#' + listId);
                                if (data.message) {
                                    showGlobalModal(data.message);
                                } else if (data.render) {
                                    list.append(data.render);
                                } else {
                                    showGlobalModal('Ошибка загрузки. Обратитесь к администрации');
                                }
                                if (list.find('.upload-img_box-item').length === 1) {
                                    let first = $(list.find('.upload-img_box-item')[0]);
                                    first.find('.main-img').addClass('opacity05');
                                    first.find('._is_main').val(1);
                                }
                            }
                        });
                    };
                    reader.readAsDataURL(file);
// }
//                     } else {
//                         alert('Не верный формат фото');
//                     }
                }, 100);
            });
            $(this).val('');
        });
        $(document).on('click', '._room_view_form', function () {
            const hash = $(this).data('hash');
            $('.room_form_' + hash).show();
            $('.room_min_' + hash).hide();
        });
        $(document).on('click', '._rest_room_view_form', function () {
            const hash = $(this).data('hash');
            $('.rest_room_form_' + hash).show();
            $('.rest_room_min_' + hash).hide();
        });

        $(document).on('click', '.save_room', function () {
            const hash = $(this).data('hash');
            const form = $('.room_form_' + hash);
            let error = validateForm(form);
            if (!error) {
                if (form.find('._item_room_gallery').length > 0) {
                    $.ajax({
                        type: "POST",
                        url: form.data('action'),
                        data: form.find('select,input,textarea').serialize(),
                        success: function (data) {
                            const item = data.item;
                            $('.room_min_' + hash).show();
                            $('.room_title_' + hash).html(item.title);
                            $('.room_living_space_' + hash).html(item.living_space);
                            $('.room_count_guest_' + hash).html(item.count_guest);
                            $('.room_count_bedrooms_' + hash).html(item.count_bedrooms);
                            $('.room_price_day_' + hash).html(item.price_day);
                            $('.room_price_day_weekend_' + hash).html(item.price_day_weekend);
                            $('.room_price_weekends_' + hash).html(item.price_weekends)
                            $('.room_count_small_bedrooms_' + hash).html(item.count_small_bedrooms)
                            $('.room_count_big_bedrooms_' + hash).html(item.count_big_bedrooms)
                            $('.room_id_' + hash).val(item.id);
                            $('.room_image_src_' + hash).prop('src', item.photos[0].value);
                            form.hide();
                        }
                    });
                } else {
                    showGlobalModal('Добавьте фото');
                }
            }
        });

        $(document).on('click', '.save_rest_room', function () {
            const hash = $(this).data('hash');
            const form = $('.rest_room_form_' + hash);
            let error = validateForm(form);
            if (!error) {
                if (form.find('._item_rest_room_gallery').length > 0) {
                    $.ajax({
                        type: "POST",
                        url: form.data('action'),
                        data: form.find('select,input,textarea').serialize(),
                        success: function (data) {
                            const item = data.item;
                            $('.rest_room_min_' + hash).show();
                            $('.rest_room_title_' + hash).html(item.title);
                            $('.rest_room_living_space_' + hash).html(item.living_space);
                            $('.rest_room_count_guest_' + hash).html(item.count_guest);
                            $('.rest_room_count_bedrooms_' + hash).html(item.count_bedrooms);
                            $('.rest_room_price_day_' + hash).html(item.price_day);
                            $('.rest_room_price_day_weekend_' + hash).html(item.price_day_weekend);
                            $('.rest_room_price_weekends_' + hash).html(item.price_weekends)
                            $('.rest_room_count_small_bedrooms_' + hash).html(item.count_small_bedrooms)
                            $('.rest_room_count_big_bedrooms_' + hash).html(item.count_big_bedrooms)
                            $('.rest_room_image_src_' + hash).prop('src', item.photos[0].value);
                            $(form).find('.rest_room_id_' + hash).val(item.id)
                            form.hide();
                        }
                    });
                } else {
                    showGlobalModal('Добавьте фото');
                }
            }
        });

        $(document).on('click', '._rest_object_view_form', function () {
            const hash = $(this).data('hash');
            $('.rest_object_form_' + hash).show();
            $('.rest_object_min_' + hash).hide();
        });

        $(document).on('click', '.save_rest_object', function () {
            const hash = $(this).data('hash');
            const form = $('.rest_object_form_' + hash);
            let error = validateForm(form);
            if (!error) {
                if (form.find('._item_rest_gallery').length > 0) {
                    $.ajax({
                        type: "POST",
                        url: form.data('action'),
                        data: form.find('select,input,textarea').serialize(),
                        success: function (data) {
                            const item = data.item;
                            let formMin = $('.rest_object_min_' + hash);
                            formMin.show();
                            $('.type_rest_block_' + hash).hide();
                            $('.rest_object_title_' + hash).html(item.title);
                            $('.rest_object_arrival_at_' + hash).html(item.arrival_at);
                            $('.rest_object_departure_at_' + hash).html(item.departure_at);
                            $('.rest_object_minimum_days_' + hash).html(item.minimum_days);
                            $('.rest_object_is_pets_' + hash).html(item.is_pets ? 'Да' : 'нет');
                            $('.rest_object_living_space_' + hash).html(item.living_space);
                            $('.rest_object_count_guest_' + hash).html(item.count_guest);
                            $('.rest_object_count_bedrooms_' + hash).html(item.count_bedrooms);
                            $('.rest_object_count_small_bedrooms_' + hash).html(item.count_small_bedrooms);
                            $('.rest_object_count_big_bedrooms_' + hash).html(item.count_big_bedrooms);
                            console.log(hash);
                            console.log(item);

                            formMin.find('.hide_rest_object_data_cottage').show()
                            formMin.find('.hide_rest_object_data_hotel').show()
                            if (item.type === 'hotel') {
                                formMin.find('.hide_rest_object_data_cottage').hide();
                            } else {
                                formMin.find('.hide_rest_object_data_hotel').hide();
                            }
                            $(form).find('.rest_object_id_' + hash).val(item.id);
                            let photo = item.photos[0];
                            if (photo) {
                                $('.rest_object_image_src_' + hash).prop('src', photo.value);
                            }
                            form.hide();
                        }
                    });
                } else {
                    showGlobalModal('Добавьте фото');
                }
            }
        });

        let e_map = document.getElementById('object_form_map');
        if (e_map) {
            let object_title = $('#object_title').val();
            let markers = [];

            let map = new google.maps.Map(e_map, {
                zoom: 10,
                center: new google.maps.LatLng(59.93234612018405, 30.341958478564443),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            let input = document.getElementById('pac-input');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            let autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    showGlobalModal('Не найдено');
                    return;
                }

                map.setCenter(place.geometry.location);
                map.setZoom(17);

                let address = place.formatted_address
                removeMarkers();
                let marker = new google.maps.Marker({
                    position: place.geometry.location,
                    map: map,
                    title: object_title ? object_title : 'Объект'
                });
                markers.push(marker);
                address = address.slice(0, address.lastIndexOf(','));
                $('#object_address').val(address);
                $('#object_address_lng').val(place.geometry.location.lng());
                $('#object_address_lat').val(place.geometry.location.lat());
            });

            let geocoder = new google.maps.Geocoder();
            map.addListener('click', function (e) {
                let lat = e.latLng.lat();
                let lng = e.latLng.lng();
                geocoder.geocode({
                    'latLng': e.latLng
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            let address = results[0].formatted_address;
                            address = address.slice(0, address.indexOf('Россия') - 2)
                            $('#object_address').val(address);
                            $('#object_address_lng').val(lng);
                            $('#object_address_lat').val(lat);
                        }
                    }
                });
                removeMarkers();
                map.setCenter(e.latLng)
                let marker = new google.maps.Marker({
                    position: e.latLng,
                    map: map,
                    title: object_title ? object_title : 'Объект'
                });
                markers.push(marker);
            });


            function initializeCenter() {
                let lng = $('#object_address_lng').val();
                let lat = $('#object_address_lat').val();
                if (lng && lat) {
                    let pos = new google.maps.LatLng(lat, lng);
                    map.setCenter(pos);
                    removeMarkers();
                    let object_title = $('#object_title').val();
                    let marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        title: object_title ? object_title : 'Объект'
                    });
                    markers.push(marker);
                } else if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        let pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        map.setCenter(pos);
                    });
                }
            }

            function removeMarkers() {
                if (markers && markers.length) {
                    for (let i = 0; i < markers.length; i++) {
                        markers[i].setMap(null);
                    }
                }
            }

            initializeCenter();
        }

        $(document).on('click', '.main-img', function () {
            let list = $(this).parent().parent().parent();
            $(list).find('._is_main').val('');
            $(list).find('.main-img').removeClass('opacity05');

            let form = $(this).parent().parent();
            $(form).find('._is_main').val(1);
            $(form).find('.main-img').addClass('opacity05');
        });
    }

    /*-----limit-title-----*/
    var maxCount = 100;

    $("#counter").html(maxCount);

    $("#object_title").keyup(function () {
        var revText = this.value.length;

        if (this.value.length > maxCount) {
            this.value = this.value.substr(0, maxCount);
        }
        var cnt = (maxCount - revText);
        if (cnt <= 0) {
            $("#counter").html('0');
        } else {
            $("#counter").html(cnt);
        }

    });
    /*-----limit-title-----*/
});
