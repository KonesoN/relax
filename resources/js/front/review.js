$(document).ready(function () {
    $('.show_reviews').click(function () {
        getModalBody(this);
        $('#open-modal_review').fadeIn(500);
    });
    $(document).on('click', '.pagination_reviews', function () {
        getModalBody(this);
    });

    $(document).on('click', '._review_answer', async function () {
        let answer = $('#' + $(this).data('answer_id')).val();
        if (answer) {
            await $.get($('#_api_cabinet_review_answer').val().slice(0, -1) + $(this).data('id') + '?answer=' + answer);
            getModalBody(this);
        } else {
            showGlobalModal('Заполните комментарий');
        }
    });

    function getModalBody(_this) {
        let page = $(_this).data('page');
        page = page ? page : 1;
        $.get($('#_api_render_modal_reviews').val().slice(0, -1) + $(_this).data('parent_id') + '?page=' + page+ '&parent_type=' + $(_this).data('parent_type'))
            .done(function (data) {
                if (data.render) {
                    $('#modal_review_body').html(data.render);
                }
            });
    }
});
