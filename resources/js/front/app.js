require('./../bootstrap');

require('./modal');
require('./home');
require('./slider');
require('./cabinet');
require('./objectForm');
require('./object');
require('./review');
require('./message');
require('./support');
require('./favorite');

$(document).ready(function () {
    $(document).on('click', '._question_alert', function (e) {
        e.preventDefault();
        let message = $(this).data('message');
        message = message ? message : 'Вы уверены?';
        let result = confirm(message);
        if (result) {
            location.href = $(this).prop('href');
        }
    });

    $(document).on('click', '._route', function (e) {
        e.preventDefault();
        location.href = $(this).data('href');
    });
    $(document).on('click', '#object_filter_is_the_city', function (e) {
        if ($('#object_filter_is_the_city').prop('checked')) {
            $('.object_filter_city_id').prop('disabled', true).prop('readonly', true)
        } else {
            $('.object_filter_city_id').prop('disabled', false).prop('readonly', false)
        }
    });

    $(document).on('click', '._delete_parent', function (e) {
        e.preventDefault();
        const delete_type = $(this).data('delete');
        if (delete_type === 'parent4') {
            $(this).parent().parent().parent().parent().remove();
        } else if (delete_type === 'parent2') {
            $(this).parent().parent().remove();
        } else if (delete_type === 'parent3') {
            $(this).parent().parent().parent().remove();
        } else {
            $(this).parent().remove();
        }
    });
});
