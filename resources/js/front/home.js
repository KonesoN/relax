$(document).ready(function () {

    /*---------------*/
    if ($('.extended_filter')) {

        $('.extended_filter input:checkbox').change(function () {
            // var sum = 0;
            // $('.extended_filter input:checkbox').each(function () {
            //     if ($(this).prop('checked') == true) {
            //         sum += 1;
            //     }
            // });
        });

        $('#extended_btn').click(function () {

            if ($('.extended_filter').css('display') == 'none') {
                $('.extended_filter').css("display", "flex");
                $(this).html('Сохранить');
            } else {
                $('.extended_filter').css("display", "none");
                var sum = 0;
                $('.extended_filter input:checkbox').each(function () {
                    if ($(this).prop('checked') == true) {
                        sum += 1;
                    }
                });
                let html = '<img src="/img/front/filter.png">Расширенный фильтр';
                if (sum > 0) {
                    html += ' (+' + sum + ')';
                }
                $(this).html(html)
                $('body').animate({scrollTop: $('.main-filter_form').prop("scrollHeight") - 250});
            }
        })
    }

    /*---------------*/


    (function () {

        window.inputNumber = function (el) {

            let min = el.attr('min') || false;
            let max = el.attr('max') || false;

            let els = {};

            els.dec = el.prev();
            els.inc = el.next();

            el.each(function () {
                init($(this));
            });

            function init(el) {

                els.dec.on('click', decrement);
                els.inc.on('click', increment);

                function decrement() {
                    let value = el[0].value;
                    value--;
                    if (!min || value >= min) {
                        el[0].value = value;
                    }
                    if (value === 0) {
                        el[0].value = '-';
                    }
                }

                function increment() {
                    let value = el[0].value;
                    if (value === '-') {
                        value = 0;
                    }
                    value++;
                    if (!max || value <= max) {
                        el[0].value = value++;
                    }
                }
            }
        }
    })();

    inputNumber($('.input-number'));

    /*---------------*/
    resizeVeryGood();
    $(window).resize(function () {
        resizeVeryGood();
    });

    function resizeVeryGood() {
        $.each($(".gallery"), function (index, item) {
            $(item).find('li').removeClass('hide').removeClass('radius');
            if ($(window).width() >= 769) {
                $(item).find("li:gt(5)").addClass('hide');
            } else if ($(window).width() > 480) {
                $(item).find("li:gt(2)").addClass('hide');
            } else {
                $(item).find("li:gt(1)").addClass('hide');
            }
            notHideLastVeryGood(item);
        });
    }

    function notHideLastVeryGood(item) {
        let length = $(item).find("li").length;
        let hide = false;
        if ($(window).width() >= 769 && length > 5) {
            hide = true
        } else if ($(window).width() <= 768 && $(window).width() >= 480 && length > 2) {
            hide = true
        } else if ($(window).width() <= 479 && length > 2) {
            hide = true
        }
        if (hide) {
            $(item).find('.gallery_last-item.modal_view').remove();
            $(item).find('li:not(.hide)').last().addClass('radius').append('<a href="#" data-modal_id="open-modal_vipGallery" class="gallery_last-item modal_view _get_modal_photos">Все фото</a>');
        } else {
            $(item).find('.gallery_last-item.modal_view').remove();
        }
    }

    $(document).on('click', '._get_modal_photos', function () {
        $('#modal_gallery').html('');
        let id = $(this).parent().parent().parent().data('id')
        $.get($('#_api_render_objectModalPhotos').val() + '?id=' + id)
            .done(function (data) {
                if (data.render) {
                    $('#modal_gallery').html(data.render);
                }
            });
    })
    /*---------------*/


    let e_map = document.getElementById('map');
    let home_map = null;
    if (e_map) {
        home_map = new google.maps.Map(e_map, {
            zoom: 10,
            // center: new google.maps.LatLng(-33.92, 151.25),
            center: new google.maps.LatLng(59.93234612018405, 30.341958478564443),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        initLocations();

        function initializeMarker() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    let pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    home_map.setCenter(pos);
                });
            }
        }

        initializeMarker();
    }


    function initLocations() {
        let locations = [];
        $.each($('._location_home'), function (index, item) {
            locations.push({
                title: $(item).data('title'),
                lat: $(item).data('lat'),
                lng: $(item).data('lng'),
                image: $(item).data('image'),
                rating: $(item).data('rating'),
                price: $(item).data('price'),
                url: $(item).data('url'),
            });
        });

        let marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
                map: home_map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    let rating = '';
                    if (locations[i].rating > 0) {
                        rating = '<p><b>Рейтинг:' +
                            (parseFloat(locations[i].rating) * 2) / 10 +
                            '</b></p>';
                    }
                    console.log(locations[i]);
                    const contentString =
                        '<a href="' + locations[i].url + '" target="_blank">' +
                        '<div class="map_marker-info">' +
                        '<h1>' +
                        locations[i].title +
                        '</h1>' +
                        rating +
                        "<img src='" +
                        locations[i].image +
                        "'>" +
                        '<p>' + locations[i].price + '</p>' +
                        "</div>";
                    "</a>";
                    const infowindow = new google.maps.InfoWindow({
                        content: contentString,
                    });
                    // infowindow.setContent(locations[i].title, locations[i][6]);
                    infowindow.open(home_map, marker);
                }
            })(marker, i));
        }
    }

    /*---------------*/
    handlerTypeObject();

    $('#main_filter_select_type').change(function () {
        handlerTypeObject();
    });

    function handlerTypeObject() {
        const type = $('#main_filter_select_type').val();
        if ($('#cottage_value').val() === type) {
            $('#home_filter_count_person').slideDown(200);
        } else {
            $('#home_filter_count_person').slideUp(200);
        }

        $('.hide_by_type').hide();
        $('.show_by_type_' + type).show();
    }

    let queryPagination = false;
    let home_ids = $('#_api_home_pagination_home_ids').val()
    $(window).scroll(function () {
        loadPagination();
    });

    function loadPagination() {
        const target = $('#show_more');
        if (target.html()) {
            let targetPos = target.offset().top;
            let currentPos = targetPos - $(window).scrollTop();
            if (currentPos < 1000 && !queryPagination) {
                queryPagination = true;
                $.get($('#_api_home_pagination').val() + '?home_ids=' + home_ids + $('#_api_home_pagination_filters').val())
                    .done(function (data) {
                        if (data.render) {
                            home_ids = data.home_ids;
                            $('#home_list_object').append(data.render);
                            queryPagination = false;
                        } else {
                            clearInterval(paginationInterval);
                        }
                        initLocations();
                    });
            }
        }
    }

    let paginationInterval = setInterval(function () {
        loadPagination();
    }, 1000);
    let _alert_message = $('#_alert_message').val();
    if (_alert_message) {
        showGlobalModal(_alert_message);
    }

    $('#home_arrival_at').change(function () {
        homeDatesHandler();
    });
    $('#home_departure_at').change(function () {
        homeDatesHandler();
    });

    function homeDatesHandler() {
        let arrival_at = $('#home_arrival_at').val();
        let departure_at = $('#home_departure_at').val();

        if (moment(departure_at) < moment(arrival_at)) {
            $('#home_departure_at').val(arrival_at)
        }
    }

    let modal_object_map = document.getElementById('modal_object_map');
    let object_map = null;
    if (modal_object_map) {
        object_map = new google.maps.Map(modal_object_map, {
            zoom: 13,
            center: new google.maps.LatLng(59.93234612018405, 30.341958478564443),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
    }
    $(document).on('click', '._view_object_map', function (e) {
        e.preventDefault()
        viewObjectMap(this);
        $('#open-modal_object_map').fadeIn(500);
    })

    function viewObjectMap(_this) {
        let locationItem = $(_this);
        let lng = locationItem.data('lng');
        let lat = locationItem.data('lat');
        let title = locationItem.data('title');
        if (lng && lat) {
            let pos = new google.maps.LatLng(lat, lng);
            object_map.setCenter(pos);
            new google.maps.Marker({
                position: pos,
                map: object_map,
                title: title
            });
        }
    }

    $('.select_home_type').click(function () {
        $('#main_filter_select_type').val($(this).data('type'));
        handlerTypeObject();
        $('#home_filter_submit').click();
    });
});


