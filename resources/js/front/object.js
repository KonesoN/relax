$(document).ready(function () {
    $('#submit_review').click(function (e) {
        e.preventDefault();
        const form = $('#review_form');
        $.ajax({
            type: "POST",
            url: form.prop('action'),
            data: form.serialize(),
            success: function () {
                showGlobalModal('Ваш отзыв отправлен на модерацию');
                hideModal('open-modal_addReview');
            }
        });
    });

    $('#submit_message').click(function (e) {
        e.preventDefault();
        const form = $('#message_form');
        $.ajax({
            type: "POST",
            url: form.prop('action'),
            data: form.serialize(),
            success: function () {
                showGlobalModal('Ваше сообщение отправлено');
                hideModal('open-modal_newMessage');
            }
        });
    });

    $('#submit_booking').click(function (e) {
        e.preventDefault();
        const form = $('#booking_form');
        let error = validateForm(form);
        if (!error) {
            $.ajax({
                type: "POST",
                url: form.prop('action'),
                data: form.serialize(),
                success: function () {
                    showGlobalModal('Перейдите на почту для подтверждения бронирования');
                    hideModal('open-modal_cottageReservation');
                    hideModal('open-modal_roomReservation');
                    hideModal('open-modal_restHotelRoomReservation');
                    hideModal('open-modal_restCottageReservation');
                }
            });
        }
    });

    function validateForm(form) {
        let elems = form.find('input');
        let error = false;
        elems.each(function (index, item) {
            let val = $(item).val();
            if ($(item).prop('required') && (val !== "0" && !val)) {
                $(item).addClass('input_error');
                showGlobalModal('Заполнены не все поля. Не заполненные поля обведены красным. Заполните для продолжения.');
                error = true;
            }
        });
        return error;
    }

    let disabledDays = [];
    $.each($('._disable_days'), function (index, item) {
        disabledDays.push(item.value)
    })
    $('.datepicker-here').datepicker({
        onRenderCell: function (date, cellType) {
            if (cellType == 'day') {
                const d = getNormalDate(date);
                return {
                    disabled: disabledDays.indexOf(d) !== -1
                }
            }
        }
    });

    $('#booking_range_at').datepicker({
        range: true,
        inline: true,
        minDate: new Date(),
        'multiple-dates-separator': ' - ',
        onRenderCell: function (date, cellType) {
            if (cellType == 'day') {
                const d = getNormalDate(date);
                return {
                    disabled: disabledDays.indexOf(d) !== -1
                }
            }
        },
        onSelect(formattedDate, date, inst) {
            const date1 = getNormalDate(date[0], 'd.m.Y');
            const date2 = getNormalDate(date[1], 'd.m.Y');
            $('#booking_arrival_at').val(date1);
            $('#booking_departure_at').val(date2);
            if (date1 && date2) {
                let error = false;
                for (let current = moment(getNormalDate(date[0])); current <= moment(getNormalDate(date[1])); current.add(1, 'd')) {
                    const d = current.format("YYYY-MM-DD");
                    if (disabledDays.indexOf(d) !== -1) {
                        error = true;
                    }
                }
                if (error) {
                    showGlobalModal('Дата забронирована');
                    $('#booking_arrival_at').val('');
                    $('#booking_departure_at').val('');
                    inst.clear();
                }
            }

        }
    });

    function getNormalDate(date, format = 'Y-m-d') {
        if (date) {
            let month = date.getMonth() + 1;
            if (month.toString().length === 1) {
                month = "0" + month;
            }
            let day = date.getDate();
            if (day.toString().length === 1) {
                day = "0" + day;
            }
            if (format === 'd.m.Y') {
                return `${day}.${month}.${date.getFullYear()}`
            } else {
                return `${date.getFullYear()}-${month}-${day}`
            }
        }
        return '';
    }

    let e_map = document.getElementById('object_map');
    if (e_map) {
        let map = new google.maps.Map(e_map, {
            zoom: 13,
            center: new google.maps.LatLng(59.93234612018405, 30.341958478564443),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        let locationItem = $('#_location_item');
        let lng = locationItem.data('lng');
        let lat = locationItem.data('lat');
        let title = locationItem.data('title');
        if (lng && lat) {
            let pos = new google.maps.LatLng(lat, lng);
            map.setCenter(pos);
            new google.maps.Marker({
                position: pos,
                map: map,
                title: title
            });
        }
    }

    $('#toggle_object_map').click(function (e) {
        e.preventDefault();
        $('#object_map').slideToggle();
    })

    $('#select_room_id').change(function () {
        location.href = $(this).val();
    })

    $('#select_rest_object_id').change(function () {
        location.href = $(this).val();
    })

    $('#select_rest_hotel_room_id').change(function () {
        location.href = $(this).val();
    })

    $(document).on('click', '._save_rest_object', function () {
        $('#_rest_object_id').val($(this).data('rest_object_id'));
        handlerModalRest();
    });

    function handlerModalRest() {
        let disabledDaysRest = [];
        $.each($('._disable_days'), function (index, item) {
            disabledDaysRest.push(item.value)
        })

        $('#modal_rest_booking_range_at').datepicker({
            range: true,
            inline: true,
            minDate: new Date(),
            'multiple-dates-separator': ' - ',
            onRenderCell: function (date, cellType) {
                if (cellType == 'day') {
                    const d = getNormalDate(date);
                    return {
                        disabled: disabledDaysRest.indexOf(d) !== -1
                    }
                }
            },
            onSelect(formattedDate, date, inst) {
                const date1 = getNormalDate(date[0], 'd.m.Y');
                const date2 = getNormalDate(date[1], 'd.m.Y');
                $('#booking_arrival_at').val(date1);
                $('#booking_departure_at').val(date2);
                if (date1 && date2) {
                    let error = false;
                    for (let current = moment(getNormalDate(date[0])); current <= moment(getNormalDate(date[1])); current.add(1, 'd')) {
                        const d = current.format("YYYY-MM-DD");
                        if (disabledDaysRest.indexOf(d) !== -1) {
                            error = true;
                        }
                    }
                    if (error) {
                        showGlobalModal('Дата забронирована');
                        $('#booking_arrival_at').val('');
                        $('#booking_departure_at').val('');
                        inst.clear();
                    }
                }

            }
        });
    }

    $(document).on('click', '._save_room', function () {
        $('#_room_id').val($(this).data('room_id'));
        handlerModalHotelRoom($(this).data('room_id'));
    });

    function handlerModalHotelRoom(room_id) {
        let disabledDaysRoom = [];
        $.each($('._disable_days_' + room_id), function (index, item) {
            disabledDaysRoom.push(item.value)
        })
        console.log(disabledDaysRoom);
        $('#modal_hotel_room_booking_range_at').datepicker({
            range: true,
            inline: true,
            minDate: new Date(),
            'multiple-dates-separator': ' - ',
            onRenderCell: function (date, cellType) {
                if (cellType == 'day') {
                    const d = getNormalDate(date);
                    return {
                        disabled: disabledDaysRoom.indexOf(d) !== -1
                    }
                }
            },
            onSelect(formattedDate, date, inst) {
                const date1 = getNormalDate(date[0], 'd.m.Y');
                const date2 = getNormalDate(date[1], 'd.m.Y');
                $('#booking_arrival_at').val(date1);
                $('#booking_departure_at').val(date2);
                if (date1 && date2) {
                    let error = false;
                    for (let current = moment(getNormalDate(date[0])); current <= moment(getNormalDate(date[1])); current.add(1, 'd')) {
                        const d = current.format("YYYY-MM-DD");
                        if (disabledDaysRoom.indexOf(d) !== -1) {
                            error = true;
                        }
                    }
                    if (error) {
                        showGlobalModal('Дата забронирована');
                        $('#booking_arrival_at').val('');
                        $('#booking_departure_at').val('');
                        inst.clear();
                    }
                }

            }
        });
    }

    $(document).on('click', '._save_rest_hotel_room', function () {
        $('#_rest_hotel_room_id').val($(this).data('room_id'));
        handlerModalRestHotelRoom($(this).data('room_id'));
    });

    function handlerModalRestHotelRoom(room_id) {
        let disabledDaysRoom = [];
        $.each($('._disable_days_' + room_id), function (index, item) {
            disabledDaysRoom.push(item.value)
        })

        $('#modal_rest_hotel_room_booking_range_at').datepicker({
            range: true,
            inline: true,
            minDate: new Date(),
            'multiple-dates-separator': ' - ',
            onRenderCell: function (date, cellType) {
                if (cellType == 'day') {
                    const d = getNormalDate(date);
                    return {
                        disabled: disabledDaysRoom.indexOf(d) !== -1
                    }
                }
            },
            onSelect(formattedDate, date, inst) {
                const date1 = getNormalDate(date[0], 'd.m.Y');
                const date2 = getNormalDate(date[1], 'd.m.Y');
                $('#booking_arrival_at').val(date1);
                $('#booking_departure_at').val(date2);
                if (date1 && date2) {
                    let error = false;
                    for (let current = moment(getNormalDate(date[0])); current <= moment(getNormalDate(date[1])); current.add(1, 'd')) {
                        const d = current.format("YYYY-MM-DD");
                        if (disabledDaysRoom.indexOf(d) !== -1) {
                            error = true;
                        }
                    }
                    if (error) {
                        showGlobalModal('Дата забронирована');
                        $('#booking_arrival_at').val('');
                        $('#booking_departure_at').val('');
                        inst.clear();
                    }
                }

            }
        });
    }

})
