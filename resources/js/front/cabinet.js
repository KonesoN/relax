$(document).ready(function () {
    $('.cabinet-settings .info button').click(function (e) {
        e.preventDefault();
        $(this).parent().next().addClass('edit');
        $(this).parent().next().removeAttr("readonly");
    })

    $('.cabinet-settings-cancel').click(function () {
        $('.cabinet-settings input').removeClass('edit').attr("readonly", false);
    });

    $(document).click(function (e) {
        let item = $(e.target);
        if (!item.hasClass('toggle-menu') && !item.parent().hasClass('toggle-menu') && !item.parent().parent().hasClass('toggle-menu') && !item.parent().parent().parent().hasClass('toggle-menu')) {
            $('.toggle-btn').prop('checked', false);
        }
    })
});


