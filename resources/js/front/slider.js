const imgs = document.querySelectorAll('.img-select a');
const imgBtns = [...imgs];
let imgId = 1;

imgBtns.forEach((imgItem) => {
    imgItem.addEventListener('click', (event) => {
        event.preventDefault();
        imgId = imgItem.dataset.id;
        slideImage();
    });
});

function slideImage() {
    if (document.querySelector('.img-showcase img:first-child')) {
        const displayWidth = document.querySelector('.img-showcase img:first-child').clientWidth;
        document.querySelector('.img-showcase').style.transform = `translateX(${-(imgId - 1) * displayWidth}px)`;
    }
}

window.addEventListener('resize', slideImage);

/*---------*/

var controls = document.querySelectorAll('.controls');
for (var i = 0; i < controls.length; i++) {
    controls[i].style.display = 'flex';
}

var slides = document.querySelectorAll('#slides .slide');
var currentSlide = 0;
var slideInterval = setInterval(nextSlide, 3000);

function nextSlide() {
    goToSlide(currentSlide + 1);
}

function previousSlide() {
    goToSlide(currentSlide - 1);
}

function goToSlide(n) {
    if (slides[currentSlide]) {
        slides[currentSlide].className = 'slide';
        currentSlide = (n + slides.length) % slides.length;
        slides[currentSlide].className = 'slide showing';
    }
}


var playing = true;
var pauseButton = document.getElementById('pause');
if (pauseButton) {
    pauseButton.onclick = function () {
        if (playing) {
            pauseSlideshow();
        } else {
            playSlideshow();
        }
    };
}

function pauseSlideshow() {
    pauseButton.innerHTML = '&#9658;'; // play character
    playing = false;
    clearInterval(slideInterval);
}

function playSlideshow() {
    pauseButton.innerHTML = '&#10074;&#10074;'; // pause character
    playing = true;
    slideInterval = setInterval(nextSlide, 2000);
}


var next = document.getElementById('next');
var previous = document.getElementById('previous');

if (next) {
    next.onclick = function () {
        pauseSlideshow();
        nextSlide();
    };
}

if (previous) {
    previous.onclick = function () {
        pauseSlideshow();
        previousSlide();
    };
}

