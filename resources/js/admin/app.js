require('./../bootstrap');

require('../front/objectForm');
require('./support');

$(document).click(function (e) {
    let item = $(e.target);
    if (!item.hasClass('toggle-menu') && !item.parent().hasClass('toggle-menu') && !item.parent().parent().hasClass('toggle-menu') && !item.parent().parent().parent().hasClass('toggle-menu')) {
        $('.toggle-btn').prop('checked', false);
    }
})

$(document).on('click', '._delete_parent', function (e) {
    e.preventDefault();
    const delete_type = $(this).data('delete');
    if (delete_type === 'parent4') {
        $(this).parent().parent().parent().parent().remove();
    } else if (delete_type === 'parent2') {
        $(this).parent().parent().remove();
    } else {
        $(this).parent().remove();
    }
});

$(document).on('click', '._question_alert', function (e) {
    e.preventDefault();
    let message = $(this).data('message');
    message = message ? message : 'Вы уверены?';
    let result = confirm(message);
    if (result) {
        location.href = $(this).prop('href');
    }
});
