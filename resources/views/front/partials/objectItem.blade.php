@php
    $object = $item->object;
    $rating = $item->reviews()->avg('rating');
    if($item->type == 'cottage'){
        $price = '<li>Сутки будни <span>'.$object->price_day.'₽</span></li>
        <li>Сутки выходные <span>'.$object->price_day_weekend.'₽</span></li>
        <li>Полные выходные (ПТ-ВС) <span>'.$object->price_weekends.'₽</span></li>';
    }
    else{
        $price = '<li>Стоимость:  '.(\App\Models\UserObject::PRICE_TYPES[$item->type][$item->price_type_id]['price']).'</li>';
    }
@endphp
<input type="hidden" class="_location_home" data-lat="{{$item->lat}}" data-lng="{{$item->lng}}"
       data-title="{{$item->title}}" data-image="{{$item->MainPhoto?$item->MainPhoto->value:''}}"
       data-rating="{{$rating}}" data-price="{{$price}}"
       data-url="{{route('front.object.view',[$item->id])}}"/>
<div class="list-ad_block-item {{$item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::PREMIUM,'is_active'=>1])->first()?'premium_item':''}}">
    <div class="rec-label">
        @if($item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::SERVICE_SELECTION,'is_active'=>1])->first())
            <span class="rec-label_serv-sel">
                <img src="{{asset('/img/front/vip.png')}}">Выбор <br>сервиса
            </span>
        @endif
        @if($item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::PREMIUM,'is_active'=>1])->first())
            <span class="rec-label_prem">
                <img src="{{asset('/img/front/prem_icon.png')}}">PREMIUM
            </span>
        @endif
    </div>
    <a href="{{route('front.object.view',$item->id)}}">
        <div class="list-ad_block-item_container">
            <div class="list-ad_block-item_left">
                <img src="{{$item->MainPhoto?$item->MainPhoto->value:''}}">
                <div class="info">
                    <p>{{$item->title}}</p>
                    <p>{{$item->direction->name}}</p>
                    <p class="_view_object_map" data-lat="{{$item->lat}}" data-lng="{{$item->lng}}"
                       data-title="{{$item->title}}">{{$item->address}}</p>

                    <div class="info-rating">
                        <div class="star-rating">
                            <input type="radio" id="5-stars" name="rating"
                                   value="5" {{floor($rating) == 5?'checked':''}}/>
                            <label for="5-stars" class="star">&#9733;</label>
                            <input type="radio" id="4-stars" name="rating"
                                   value="4" {{floor($rating) == 4?'checked':''}}/>
                            <label for="4-stars" class="star">&#9733;</label>
                            <input type="radio" id="3-stars" name="rating"
                                   value="3" {{floor($rating) == 3?'checked':''}}/>
                            <label for="3-stars" class="star">&#9733;</label>
                            <input type="radio" id="2-stars" name="rating"
                                   value="2" {{floor($rating) == 2?'checked':''}}/>
                            <label for="2-stars" class="star">&#9733;</label>
                            <input type="radio" id="1-star" name="rating"
                                   value="1" {{floor($rating) == 1?'checked':''}}/>
                            <label for="1-star" class="star">&#9733;</label>
                        </div>
                        <p>({{$item->reviews()->count()}} отзыва)</p>
                    </div>
                    @if($item->type == 'cottage')
                        <ul class="advantages-list">
                            <li>Площадь коттеджа: {{$object->living_space}} м2</li>
                            <li>{{$object->count_guest}} гостя / {{$object->count_bedrooms}} спальни
                                / {{$object->count_small_bedrooms + $object->count_big_bedrooms}} кровати
                            </li>
                        </ul>
                    @endif

                    <ul class="comfort-list">
                        @include('front.partials.objectItemFilters')
                    </ul>
                </div>
            </div>
            <div class="list-ad_block-item_right">
                @if($rating>0)
                    <div class="estimate">
                        {{$rating * 2}}
                    </div>
                @endif
                <ul class="price">
                    @if($item->type == 'cottage')
                        <li>Сутки будни <span>{{$object->price_day}}₽</span></li>
                        <li>Сутки выходные <span>{{$object->price_day_weekend}}₽</span></li>
                        <li>Полные выходные (ПТ-ВС) <span>{{$object->price_weekends}}₽</span></li>
                    @else
                        <li>
                            Стоимость: {!! \App\Models\UserObject::PRICE_TYPES[$item->type][$item->price_type_id]['price'] !!}</li>
                    @endif
                    @include('front.partials.favoriteButton')
                </ul>
            </div>
        </div>
    </a>
</div>
