<ul class="sidebar-list">
    <div class="sidebar_name">
        <p>{{Auth::user()->first_name}}</p>
        <p>{{Auth::user()->last_name}}</p>
    </div>
    @include('front.partials.sidebarMenu')
</ul>
<div class="sidebar-menu">
    <input id="menu" type="checkbox"/>
    <label for="menu">
        <span></span>
        <span></span>
        <span></span>
    </label>
    <article>
        <ul>
            @include('front.partials.sidebarMenu')
        </ul>
    </article>
</div>
