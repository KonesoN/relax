@if(Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER)
    <div class="favorite-ad_block">
        <div class="form-check ">
            <input class="form-check-input _favorite_handler" type="checkbox" name="favorite" data-id="{{$item->id}}"
                   id="favorite{{$item->id}}" {{$item->favorite?'checked':''}}>

            <label class="form-check-label" for="favorite{{$item->id}}">
                <p>В избранное</p>
            </label>
        </div>
    </div>
@endif