<header>
    <div class="container">
        <div class="header-box">
            <a href="{{route('front.index')}}" class="header-box_logo">
                <img src="{{asset('/img/front/logo_header.svg')}}">
            </a>
            <div class="header-box_menu">
                @if(Auth::check())
                    <div class="profile-box">
                        @if(Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER)
                            <a href="{{route('front.cabinet.favorite.index')}}" class="favorites-item">
                                <img src="{{asset('/img/front/favorites-item.png')}}">
                            </a>
                        @endif
                        <a href="{{route('front.cabinet.message.index')}}" class="message-box">
                            <p>{{Auth::user()->UnreadDialogCount}}</p>
                        </a>
                        <a href="{{route(Auth::user()->type_id == \App\Models\User::TYPE_OWNER?'front.cabinet.object.index':'front.cabinet.review.index')}}"
                           class="profile-info">
                            <div class="profile-info_name">
                                <p>{{Auth::user()->first_name}}</p>
                                <p>{{Auth::user()->last_name}}</p>
                            </div>
                        </a>
                    </div>
                    <div class="menu-drop">
                        <div class="menu-drop_burger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <ul class="menu-drop_list">
                            <li class="menu-drop_list-item"><a
                                        href="{{route(Auth::user()->type_id == \App\Models\User::TYPE_OWNER?'front.cabinet.object.index':'front.cabinet.review.index')}}">Кабинет</a>
                            </li>
                            <li class="menu-drop_list-item"><a href="{{route('front.logout')}}">Выход</a></li>
                        </ul>
                    </div>
                @endif
                <div class="header-box_menu-btn">
                    @if(Auth::check())
                        @if(Auth::user()->type_id == \App\Models\User::TYPE_OWNER)
                            <a href="{{route('front.cabinet.object.new')}}" class="subprimary-btn">Разместить объект</a>
                        @endif
                    @else
                        <a href="{{route('front.login')}}" class="subprimary-btn">Вход</a>
                        <a href="{{route('front.register')}}" class="primary-btn">Регистрация</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>
