@if(Auth::user()->type_id == \App\Models\User::TYPE_OWNER)
    <li class="sidebar-list_item {{strpos(Route::current()->getName(),'front.cabinet.object.')!==false?'sidebar-list_item-active':''}}">
        <a href="{{route('front.cabinet.object.index')}}">Мои объекты</a></li>
@endif

@if(Auth::user()->type_id == \App\Models\User::TYPE_USER)
    <li class="sidebar-list_item {{strpos(Route::current()->getName(),'front.cabinet.favorite.index')!==false?'sidebar-list_item-active':''}}">
        <a href="{{route('front.cabinet.favorite.index')}}">Избранное</a></li>
@endif

<li class="sidebar-list_item {{strpos(Route::current()->getName(),'front.cabinet.review.index')!==false?'sidebar-list_item-active':''}}">
    <a href="{{route('front.cabinet.review.index')}}">Мои отзывы</a></li>
<li class="sidebar-list_item {{strpos(Route::current()->getName(),'front.cabinet.message.')!==false?'sidebar-list_item-active':''}}">
    <a href="{{route('front.cabinet.message.index')}}">Сообщения {{Auth::user()->UnreadDialogCount?'('.Auth::user()->UnreadDialogCount.')':''}}</a>
</li>
@if(Auth::user()->type_id == \App\Models\User::TYPE_OWNER)
    <li class="sidebar-list_item {{strpos(Route::current()->getName(),'front.cabinet.balance')!==false?'sidebar-list_item-active':''}}">
        <a href="{{route('front.cabinet.balance')}}">Баланс</a></li>
@endif
<li class="sidebar-list_item {{strpos(Route::current()->getName(),'front.cabinet.setting.index')!==false?'sidebar-list_item-active':''}}">
    <a href="{{route('front.cabinet.setting.index')}}">Настройки</a></li>
