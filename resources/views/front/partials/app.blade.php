<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@if(!empty($metadata['tags']['title'])){{$metadata['tags']['title']}}@else{{'RusVill'}}@endif</title>

    <link href="{{ asset('css/front.css') }}" rel="stylesheet">
    <link href="{{ asset('custom/css.css') }}" rel="stylesheet">
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY63MMSWBJpdwdawp4s5tRcmnl_SjWpus&libraries=places&v=weekly&region=RU&language=ru"
    ></script>

    @if(!empty($metadata['tags']) && is_array($metadata['tags']))
        @php
            unset($metadata['tags']['title']);
        @endphp
        @foreach($metadata['tags'] as $name=>$tag)
            <meta name="{{$name}}" content="{{$tag}}">
        @endforeach
    @endif

    @stack('scripts')
</head>
<body>
<div
    class="@if(Route::is('front.login') || Route::is('front.register')) auth @elseif(Route::is('front.index')) home @elseif(strpos(Route::current()->getName(),'front.cabinet.')!==false) cabinet_sec @else an_card @endif">
    @include('front.partials.header')

    @yield('content')

    @include('front.partials.footer')
    @include('front.modal.globalMessage')
    @include('front.partials.routes')
    <input type="hidden" id="_alert_message" value="@if(session('alert_message')){{session('alert_message')}}@endif">
</div>
<script src="{{ asset('js/front.js') }}" defer></script>
<script src="{{ asset('custom/js.js') }}" defer></script>
</body>
</html>
