<input type="hidden" value="{{route('front.home.pagination')}}" id="_api_home_pagination"/>
<input type="hidden" value="{{route('front.render.objectHotelRoom')}}" id="_api_render_objectHotelRoom"/>
<input type="hidden" value="{{route('front.render.restHotelRoom')}}" id="_api_render_restHotelRoom"/>
<input type="hidden" value="{{route('front.render.objectModalPhotos')}}" id="_api_render_objectModalPhotos"/>
<input type="hidden" value="{{route('front.render.objectRestCottage')}}" id="_api_render_objectRestCottage"/>
<input type="hidden" value="{{route('front.cabinet.message.messages',['item'=>1])}}" id="_api_render_dialog_messages"/>
<input type="hidden" value="{{route('front.cabinet.support.messages')}}" id="_api_render_support_messages"/>
<input type="hidden" value="{{route('front.cabinet.object.uploadPhoto')}}" id="_api_cabinet_object_uploadPhoto"/>
<input type="hidden" value="{{route('front.cabinet.review.modal.render',['item'=>1])}}" id="_api_render_modal_reviews"/>
<input type="hidden" value="{{route('front.cabinet.review.answer',['item'=>1])}}" id="_api_cabinet_review_answer"/>
<input type="hidden" value="{{route('front.cabinet.favorite.handler',['id'=>1])}}" id="_api_cabinet_favorite_handler"/>
<input type="hidden" id="_route_current" value="{{Route::current()->getName()}}"/>
