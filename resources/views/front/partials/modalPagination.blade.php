@php
    $max_pages = 7;
    $class_pagination = 'pagination_reviews';
    $button_data = !empty($button_data)?$button_data:'';
@endphp
@if($pagination->currentPage() > 1 || $pagination->hasMorePages())
    <ul class="_pagination">
        @if($pagination->currentPage() >= 2)
            <li>
                <a href="#" class="{{$class_pagination}}" data-page="{{$pagination->currentPage() - 1}}" {{$button_data}}>«</a>
            </li>
        @endif
        @php
            $left_pages = $pagination->lastPage() - $pagination->currentPage();
            if($left_pages>$max_pages){
                $prev_pages = $pagination->currentPage() > $max_pages ?  $max_pages : $pagination->currentPage() - 1;
            }else{
                $prev_pages = $pagination->currentPage() > ($max_pages + $max_pages - $left_pages) ? ($max_pages + $max_pages - $left_pages) : $pagination->currentPage() - 1;
            }

            $next_pages = $pagination->lastPage() > $pagination->currentPage() + 1 ? ($left_pages) : 2;
            $next_pages = $next_pages > ($max_pages + $max_pages - $prev_pages) ? ($max_pages + $max_pages - $prev_pages) : $next_pages;
        @endphp

        @for($i = $prev_pages; $i >= 1; $i--)
            @php
                $page = $pagination->currentPage() - $i;
            @endphp
            @if($page <= $pagination->lastPage())
                <li>
                    <a href="#" class="{{$class_pagination}}" data-page="{{$page}}" {{$button_data}}>{{$page}}</a>
                </li>
            @endif
        @endfor
        @if($pagination->currentPage() <= $pagination->lastPage())
            <li class="active">
                <a>{{$pagination->currentPage()}}</a>
            </li>
        @endif

        @for($i = 1; $i <= $next_pages; $i++)
            @php
                $page = $pagination->currentPage() + $i;
            @endphp
            @if($page <= $pagination->lastPage())
                <li>
                    <a href="#" class="{{$class_pagination}}" data-page="{{$page}}" {{$button_data}}>{{$page}}</a>
                </li>
            @endif
        @endfor


        @if($pagination->hasMorePages())
            <li>
                <a href="#" class="{{$class_pagination}}" data-page="{{$pagination->currentPage() + 1}}" {{$button_data}}>»</a>
            </li>
        @endif
    </ul>
@endif
