@foreach(\App\Models\UserObject::CATEGORIES as $category)
    @php
        $show = false;
    @endphp
    @foreach($category['values'] as $id=>$v)
        @php
            if(in_array($category['category_id'].'_'.$id,$item->FilterValues)){
                $show = true;
            }
        @endphp
    @endforeach
    @if($show)
        <li>
            {{$category['name']}}
            @foreach($category['values'] as $id=>$v)
                @php
                    $value = $category['category_id'].'_'.$id;
                @endphp
                @if(in_array($value,$item->FilterValues))
                    <img src="{{$v['img']}}" title="{{$v['name']}}">
                @endif
            @endforeach
        </li>
    @endif
@endforeach