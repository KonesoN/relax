@php
    $page = !empty($page)?$page:'other';
@endphp
<div class="extended_filter-row">
    <div class="extended_filter-title">
        <p>Расположение:</p>
    </div>
    <div class="extended_filter-options">
        <div class="extended_filter-options_row">
            <div class="extended_filter-options_col">
                <p class="subtitle" style="margin-top: 0">Расстояние от города:</p>
                <div class="custom-radio">
                    @if($page == 'home')
                        <div class="custom-radio_item">
                            <label><input type="checkbox" name="distance_to_city[]"
                                          @if((!empty($filters['distance_to_city']) && !count($filters['distance_to_city'])) || (!empty($filters['distance_to_city']) && in_array(25,$filters['distance_to_city'])))
                                          checked
                                          @endif
                                          value="25"
                                >До 25км</label>
                        </div>
                        <div class="custom-radio_item">
                            <label><input type="checkbox" name="distance_to_city[]" value="60"
                                    {{!empty($filters['distance_to_city']) && in_array(60,$filters['distance_to_city'])?'checked':''}}
                                >До 60км</label>
                        </div>
                        <div class="custom-radio_item">
                            <label><input type="checkbox" name="distance_to_city[]" value="100"
                                    {{!empty($filters['distance_to_city']) && in_array(100,$filters['distance_to_city'])?'checked':''}}
                                >До 100км</label>
                        </div>
                        <div class="custom-radio_item">
                            <label><input type="checkbox" name="distance_to_city[]" value="101"
                                    {{!empty($filters['distance_to_city']) && in_array(101,$filters['distance_to_city'])?'checked':''}}
                                >Более 100км</label>
                        </div>
                    @else
                        <div class="custom-radio_item">
                            <label><input type="radio" name="distance_to_city" value="25"
                                @if(!empty($filters['distance_to_city']))
                                    {{(!empty($filters['distance_to_city']) && ($filters['distance_to_city'] == 25 || !$filters['distance_to_city'])) || empty($filters['filters']['distance_to_city']) ?'checked':''}}
                                    @elseif($page !== 'home')
                                    {{(!empty($item) && ($item->distance_to_city == 25 || !$item->distance_to_city)) || empty($item) ?'checked':''}}
                                    @endif
                                ><span>До 25км</span></label>
                        </div>
                        <div class="custom-radio_item">
                            <label><input type="radio" value="60"
                                          @if(!empty($filters['distance_to_city']))
                                          {{!empty($filters['distance_to_city']) && $filters['distance_to_city'] == 60?'checked':''}}
                                          @else
                                          {{!empty($item) && $item->distance_to_city == 60?'checked':''}}
                                          @endif
                                          name="distance_to_city"><span>До 60км</span></label>
                        </div>
                        <div class="custom-radio_item">
                            <label><input type="radio" value="100"
                                          @if(!empty($filters['distance_to_city']))
                                          {{!empty($filters['distance_to_city']) && $filters['distance_to_city'] == 100?'checked':''}}
                                          @else
                                          {{!empty($item) && $item->distance_to_city == 100?'checked':''}}
                                          @endif
                                          name="distance_to_city"><span>До 100км</span></label>
                        </div>
                        <div class="custom-radio_item">
                            <label><input type="radio" value="101"
                                          @if(!empty($filters['distance_to_city']))
                                          {{!empty($filters['distance_to_city']) && $filters['distance_to_city'] == 101?'checked':''}}
                                          @else
                                          {{!empty($item) && $item->distance_to_city == 101?'checked':''}}
                                          @endif
                                          name="distance_to_city"><span>Более 100км</span></label>

                        </div>
                    @endif
                </div>
                <p class="subtitle">На берегу озера / залива:</p>
                <div class="custom-radio">
                    @if($page == 'home')
                        <div class="custom-radio_item">
                            <label><input type="checkbox" name="is_shore[]" value="true"
                                    {{!empty($filters['is_shore']) && in_array('true',$filters['is_shore'])?'checked':''}}
                                >Да</label>
                        </div>
                        <div class="custom-radio_item">
                            <label><input type="checkbox" name="is_shore[]" value="false"
                                    {{!empty($filters['is_shore']) && in_array('false',$filters['is_shore'])?'checked':''}}
                                >Нет</label>
                        </div>
                    @else
                        <div class="custom-radio_item">
                            <label><input type="radio" name="is_shore" value="true"
                                @if(!empty($filters['is_shore']) && $filters['is_shore'] == 'true')
                                    {{!empty($filters['is_shore'])?'checked':''}}
                                    @elseif($page !== 'home')
                                    {{(!empty($item) && $item->is_shore) || empty($item) ?'checked':''}}
                                    @endif
                                ><span>Да</span></label>
                        </div>
                        <div class="custom-radio_item">
                            <label><input type="radio" name="is_shore" value="false"
                                @if((!empty($filters['is_shore']) && $filters['is_shore'] == 'false') && empty($item))
                                    {{'checked'}}
                                    @else
                                    {{!empty($item) && !$item->is_shore?'checked':''}}
                                    @endif
                                ><span>Нет</span></label>
                        </div>
                    @endif
                </div>
                <p class="subtitle">Район:</p>
                <div class="custom-select">
                    <select name="district_id">
                        @if($page == 'home')
                            <option value="">Любой</option>
                        @else
                            <option value="">Не выбрано</option>
                        @endif
                        @foreach(\App\Models\District::orderBy('name')->get() as $d)
                            <option
                                value="{{$d->id}}"
                            @if(!empty($filters['district_id']))
                                {{$filters['district_id'] == $d->id?'selected':''}}
                                @else
                                {{!empty($item) && $item->district_id == $d->id?'selected':''}}
                                @endif
                            >{{$d->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="extended_filter-options_col">
                <p class="subtitle" style="{{$page == 'home'?'margin-top: 0':''}}; opacity: 0">Направление:</p>
                @if($page !== 'home')
                    <div class="custom-select">
                        <select name="direction_id" class="object_filter_city_id">
                            @foreach(\App\Models\Direction::where('city_id',1)->get() as $d)
                                <option
                                    value="{{$d->id}}"
                                @if(!empty($filters['direction_id']))
                                    {{$filters['direction_id'] == $d->id?'selected':''}}
                                    @else
                                    {{!empty($item) && $item->direction_id == $d->id?'selected':''}}
                                    @endif
                                >{{$d->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <label><input type="checkbox" name="is_the_city"
                              value="1" id="object_filter_is_the_city"
                    @if(!empty($filters['is_the_city']))
                        {{'checked'}}
                        @else
                        {{!empty($item) && $item->is_the_city ?'checked':''}}
                        @endif
                    >В черте
                    города</label>
            </div>
        </div>
    </div>
</div>
<div class="extended_filter-row">
    <div class="extended_filter-title">
        <p>По цене:</p>
    </div>
    <div class="extended_filter-options">
        <div class="extended_filter-options_row">
            <div class="newObj-price">
                <label>
                    @if($page !== 'home')
                        <input type="radio" name="price_type_id" value="1"
                        @if((!empty($filters['price_type_id']) || ($page !== 'home' && !empty($filters['price_type_id']))) && empty($item))
                            {{(!empty($filters['price_type_id']) && $filters['price_type_id'] == 1) || empty($filters['price_type_id']) ?'checked':''}}
                            @elseif($page !== 'home')
                            {{(!empty($item) && ($item->price_type_id == 1 || !$item->price_type_id)) || empty($item) ?'checked':''}}
                            @endif
                        >
                    @else
                        <input type="checkbox" name="price_type_id[]" value="1"
                            {{!empty($filters['price_type_id']) && in_array(1,$filters['price_type_id'])?'checked':''}}
                        >
                    @endif
                    Эконом
                    <span class="info"><svg
                            xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                            width="15" height="15"
                            viewBox="0 0 172 172"
                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero"
                                                      stroke="none" stroke-width="1"
                                                      stroke-linecap="butt"
                                                      stroke-linejoin="miter"
                                                      stroke-miterlimit="10"
                                                      stroke-dasharray=""
                                                      stroke-dashoffset="0"
                                                      font-family="none"
                                                      font-size="none"
                                                      style="mix-blend-mode: normal"><path
                                    d="M0,172v-172h172v172z" fill="none"></path><g
                                    fill="#bfbfbf"><path
                                        d="M86,7.85577c-43.15504,0 -78.14423,34.98919 -78.14423,78.14423c0,43.15505 34.98919,78.14423 78.14423,78.14423c43.15505,0 78.14423,-34.98918 78.14423,-78.14423c0,-43.15504 -34.98918,-78.14423 -78.14423,-78.14423zM102.28005,128.97416c-4.03125,1.57632 -7.23558,2.79087 -9.63883,3.61779c-2.40324,0.82692 -5.16827,1.24038 -8.34675,1.24038c-4.85817,0 -8.65685,-1.18871 -11.37019,-3.56611c-2.6875,-2.35156 -4.03125,-5.375 -4.03125,-9.04447c0,-1.42128 0.10337,-2.86839 0.3101,-4.34135c0.18088,-1.4988 0.51683,-3.15265 0.95613,-5.03906l5.03906,-17.77885c0.4393,-1.70553 0.82692,-3.30769 1.13702,-4.83233c0.3101,-1.52463 0.4393,-2.92007 0.4393,-4.18629c0,-2.2482 -0.46515,-3.85036 -1.39544,-4.72897c-0.95613,-0.90445 -2.73918,-1.34375 -5.375,-1.34375c-1.29206,0 -2.63581,0.20673 -4.0054,0.59435c-1.3696,0.41346 -2.53246,0.80108 -3.51442,1.16286l1.34375,-5.47837c3.28185,-1.34375 6.43449,-2.48077 9.45793,-3.4369c2.9976,-0.95613 5.86599,-1.44712 8.52765,-1.44712c4.83233,0 8.57933,1.1887 11.1893,3.51442c2.60997,2.32572 3.92788,5.375 3.92788,9.09615c0,0.77524 -0.07753,2.14483 -0.25842,4.08293c-0.18088,1.9381 -0.51683,3.72115 -1.00781,5.375l-5.01322,17.72717c-0.41346,1.42128 -0.77524,3.04928 -1.11118,4.85817c-0.3101,1.80889 -0.46514,3.20433 -0.46514,4.13462c0,2.35156 0.51683,3.97956 1.57632,4.83233c1.03365,0.85276 2.86839,1.26622 5.47837,1.26622c1.21454,0 2.58413,-0.20673 4.13462,-0.62019c1.52464,-0.4393 2.63582,-0.80108 3.33353,-1.13702zM101.3756,57.00601c-2.32572,2.17067 -5.14243,3.25601 -8.42428,3.25601c-3.28185,0 -6.1244,-1.08533 -8.47596,-3.25601c-2.35156,-2.17067 -3.51442,-4.80649 -3.51442,-7.88162c0,-3.07512 1.18871,-5.73678 3.51442,-7.93329c2.35156,-2.19652 5.19412,-3.28185 8.47596,-3.28185c3.28185,0 6.09856,1.08533 8.42428,3.28185c2.35156,2.19651 3.51442,4.85817 3.51442,7.93329c0,3.07512 -1.16286,5.71094 -3.51442,7.88162z"></path></g></g></svg> <span
                            class="tooltiptext">
                            @foreach(\App\Models\UserObject::PRICE_TYPES as $type=>$prices)
                                <span class="hide_by_type show_by_type_{{$type}}">
                                    {!! $prices[1]['price'] !!}
                                </span>
                            @endforeach
                        </span></span></label>
                <label>
                    @if($page !== 'home')<input type="radio" name="price_type_id" value="2"
                    @if(!empty($filters['price_type_id']))
                        {{$filters['price_type_id'] == 2 ?'checked':''}}
                        @else
                        {{!empty($item) && $item->price_type_id == 2 ?'checked':''}}
                        @endif
                    >
                    @else
                        <input type="checkbox" name="price_type_id[]" value="2"
                            {{!empty($filters['price_type_id']) && in_array(2,$filters['price_type_id'])?'checked':''}}
                        >
                    @endif
                    Комфорт
                    <span class="info"><svg
                            xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                            width="15" height="15"
                            viewBox="0 0 172 172"
                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero"
                                                      stroke="none" stroke-width="1"
                                                      stroke-linecap="butt"
                                                      stroke-linejoin="miter"
                                                      stroke-miterlimit="10"
                                                      stroke-dasharray=""
                                                      stroke-dashoffset="0"
                                                      font-family="none"
                                                      font-size="none"
                                                      style="mix-blend-mode: normal"><path
                                    d="M0,172v-172h172v172z" fill="none"></path><g
                                    fill="#bfbfbf"><path
                                        d="M86,7.85577c-43.15504,0 -78.14423,34.98919 -78.14423,78.14423c0,43.15505 34.98919,78.14423 78.14423,78.14423c43.15505,0 78.14423,-34.98918 78.14423,-78.14423c0,-43.15504 -34.98918,-78.14423 -78.14423,-78.14423zM102.28005,128.97416c-4.03125,1.57632 -7.23558,2.79087 -9.63883,3.61779c-2.40324,0.82692 -5.16827,1.24038 -8.34675,1.24038c-4.85817,0 -8.65685,-1.18871 -11.37019,-3.56611c-2.6875,-2.35156 -4.03125,-5.375 -4.03125,-9.04447c0,-1.42128 0.10337,-2.86839 0.3101,-4.34135c0.18088,-1.4988 0.51683,-3.15265 0.95613,-5.03906l5.03906,-17.77885c0.4393,-1.70553 0.82692,-3.30769 1.13702,-4.83233c0.3101,-1.52463 0.4393,-2.92007 0.4393,-4.18629c0,-2.2482 -0.46515,-3.85036 -1.39544,-4.72897c-0.95613,-0.90445 -2.73918,-1.34375 -5.375,-1.34375c-1.29206,0 -2.63581,0.20673 -4.0054,0.59435c-1.3696,0.41346 -2.53246,0.80108 -3.51442,1.16286l1.34375,-5.47837c3.28185,-1.34375 6.43449,-2.48077 9.45793,-3.4369c2.9976,-0.95613 5.86599,-1.44712 8.52765,-1.44712c4.83233,0 8.57933,1.1887 11.1893,3.51442c2.60997,2.32572 3.92788,5.375 3.92788,9.09615c0,0.77524 -0.07753,2.14483 -0.25842,4.08293c-0.18088,1.9381 -0.51683,3.72115 -1.00781,5.375l-5.01322,17.72717c-0.41346,1.42128 -0.77524,3.04928 -1.11118,4.85817c-0.3101,1.80889 -0.46514,3.20433 -0.46514,4.13462c0,2.35156 0.51683,3.97956 1.57632,4.83233c1.03365,0.85276 2.86839,1.26622 5.47837,1.26622c1.21454,0 2.58413,-0.20673 4.13462,-0.62019c1.52464,-0.4393 2.63582,-0.80108 3.33353,-1.13702zM101.3756,57.00601c-2.32572,2.17067 -5.14243,3.25601 -8.42428,3.25601c-3.28185,0 -6.1244,-1.08533 -8.47596,-3.25601c-2.35156,-2.17067 -3.51442,-4.80649 -3.51442,-7.88162c0,-3.07512 1.18871,-5.73678 3.51442,-7.93329c2.35156,-2.19652 5.19412,-3.28185 8.47596,-3.28185c3.28185,0 6.09856,1.08533 8.42428,3.28185c2.35156,2.19651 3.51442,4.85817 3.51442,7.93329c0,3.07512 -1.16286,5.71094 -3.51442,7.88162z"></path></g></g></svg> <span
                            class="tooltiptext">
                            @foreach(\App\Models\UserObject::PRICE_TYPES as $type=>$prices)
                                <span class="hide_by_type show_by_type_{{$type}}">
                                    {!! $prices[2]['price'] !!}
                                </span>
                            @endforeach
                        </span></span></label>
                <label>
                    @if($page !== 'home')
                        <input type="radio" name="price_type_id" value="3"
                        @if(!empty($filters['price_type_id']))
                            {{$filters['price_type_id'] == 3 ?'checked':''}}
                            @else
                            {{!empty($item) && $item->price_type_id == 3 ?'checked':''}}
                            @endif
                        >
                    @else
                        <input type="checkbox" name="price_type_id[]" value="3"
                            {{!empty($filters['price_type_id']) && in_array(3,$filters['price_type_id'])?'checked':''}}
                        >
                    @endif
                    Премиум
                    <span class="info"><svg
                            xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                            width="15" height="15"
                            viewBox="0 0 172 172"
                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero"
                                                      stroke="none" stroke-width="1"
                                                      stroke-linecap="butt"
                                                      stroke-linejoin="miter"
                                                      stroke-miterlimit="10"
                                                      stroke-dasharray=""
                                                      stroke-dashoffset="0"
                                                      font-family="none"
                                                      font-size="none"
                                                      style="mix-blend-mode: normal"><path
                                    d="M0,172v-172h172v172z" fill="none"></path><g
                                    fill="#bfbfbf"><path
                                        d="M86,7.85577c-43.15504,0 -78.14423,34.98919 -78.14423,78.14423c0,43.15505 34.98919,78.14423 78.14423,78.14423c43.15505,0 78.14423,-34.98918 78.14423,-78.14423c0,-43.15504 -34.98918,-78.14423 -78.14423,-78.14423zM102.28005,128.97416c-4.03125,1.57632 -7.23558,2.79087 -9.63883,3.61779c-2.40324,0.82692 -5.16827,1.24038 -8.34675,1.24038c-4.85817,0 -8.65685,-1.18871 -11.37019,-3.56611c-2.6875,-2.35156 -4.03125,-5.375 -4.03125,-9.04447c0,-1.42128 0.10337,-2.86839 0.3101,-4.34135c0.18088,-1.4988 0.51683,-3.15265 0.95613,-5.03906l5.03906,-17.77885c0.4393,-1.70553 0.82692,-3.30769 1.13702,-4.83233c0.3101,-1.52463 0.4393,-2.92007 0.4393,-4.18629c0,-2.2482 -0.46515,-3.85036 -1.39544,-4.72897c-0.95613,-0.90445 -2.73918,-1.34375 -5.375,-1.34375c-1.29206,0 -2.63581,0.20673 -4.0054,0.59435c-1.3696,0.41346 -2.53246,0.80108 -3.51442,1.16286l1.34375,-5.47837c3.28185,-1.34375 6.43449,-2.48077 9.45793,-3.4369c2.9976,-0.95613 5.86599,-1.44712 8.52765,-1.44712c4.83233,0 8.57933,1.1887 11.1893,3.51442c2.60997,2.32572 3.92788,5.375 3.92788,9.09615c0,0.77524 -0.07753,2.14483 -0.25842,4.08293c-0.18088,1.9381 -0.51683,3.72115 -1.00781,5.375l-5.01322,17.72717c-0.41346,1.42128 -0.77524,3.04928 -1.11118,4.85817c-0.3101,1.80889 -0.46514,3.20433 -0.46514,4.13462c0,2.35156 0.51683,3.97956 1.57632,4.83233c1.03365,0.85276 2.86839,1.26622 5.47837,1.26622c1.21454,0 2.58413,-0.20673 4.13462,-0.62019c1.52464,-0.4393 2.63582,-0.80108 3.33353,-1.13702zM101.3756,57.00601c-2.32572,2.17067 -5.14243,3.25601 -8.42428,3.25601c-3.28185,0 -6.1244,-1.08533 -8.47596,-3.25601c-2.35156,-2.17067 -3.51442,-4.80649 -3.51442,-7.88162c0,-3.07512 1.18871,-5.73678 3.51442,-7.93329c2.35156,-2.19652 5.19412,-3.28185 8.47596,-3.28185c3.28185,0 6.09856,1.08533 8.42428,3.28185c2.35156,2.19651 3.51442,4.85817 3.51442,7.93329c0,3.07512 -1.16286,5.71094 -3.51442,7.88162z"></path></g></g></svg> <span
                            class="tooltiptext">
                            @foreach(\App\Models\UserObject::PRICE_TYPES as $type=>$prices)
                                <span class="hide_by_type show_by_type_{{$type}}">
                                    {!! $prices[3]['price'] !!}
                                </span>
                            @endforeach
                        </span></span></label>
            </div>
        </div>
    </div>
</div>
<div class="extended_filter-row">
    <div class="extended_filter-title">
        <p>Цель отдыха:</p>
    </div>
    <div class="extended_filter-options">
        <div class="extended_filter-options_row">
            <div class="extended_filter-options_col w50">
                @foreach(\App\Models\UserObject::CATEGORY_1['values'] as $id=>$v)
                    <label><input type="checkbox" name="filters[]"
                                  @php
                                      $value = \App\Models\UserObject::CATEGORY_1['category_id'].'_'.$id;
                                  @endphp
                                  value="{{$value}}"
                        @if(!empty($filters['filters']))
                            {{in_array($value,$filters['filters'])?'checked':''}}
                            @else
                            {{!empty($item) && in_array($value,$item->FilterValues)?'checked':''}}
                            @endif
                        >{{$v['name']}}</label>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="extended_filter-row">
    <div class="extended_filter-title">
        <p>Развлечения:</p>
    </div>
    <div class="extended_filter-options">
        <div class="extended_filter-options_row">
            <div class="extended_filter-options_col w50">
                @foreach(\App\Models\UserObject::CATEGORY_2['values'] as $id=>$v)
                    <label><input type="checkbox" name="filters[]"
                                  @php
                                      $value = \App\Models\UserObject::CATEGORY_2['category_id'].'_'.$id;
                                  @endphp
                                  value="{{$value}}"
                        @if(!empty($filters['filters']))
                            {{in_array($value,$filters['filters'])?'checked':''}}
                            @else
                            {{!empty($item) && in_array($value,$item->FilterValues)?'checked':''}}
                            @endif
                        >{{$v['name']}}</label>
                @endforeach
            </div>
        </div>
        <div class="extended_filter-options_row">
            <div class="extended_filter-options_col">
                <p class="subtitle">Прокат:</p>
                @foreach(\App\Models\UserObject::CATEGORY_3['values'] as $id=>$v)
                    <label><input type="checkbox" name="filters[]"
                                  @php
                                      $value = \App\Models\UserObject::CATEGORY_3['category_id'].'_'.$id;
                                  @endphp
                                  value="{{$value}}"
                        @if(!empty($filters['filters']))
                            {{in_array($value,$filters['filters'])?'checked':''}}
                            @else
                            {{!empty($item) && in_array($value,$item->FilterValues)?'checked':''}}
                            @endif
                        >{{$v['name']}}</label>
                @endforeach
            </div>
            <div class="extended_filter-options_col">
                <p class="subtitle">Спортивные площадки:</p>
                @foreach(\App\Models\UserObject::CATEGORY_4['values'] as $id=>$v)
                    <label><input type="checkbox" name="filters[]"
                                  @php
                                      $value = \App\Models\UserObject::CATEGORY_4['category_id'].'_'.$id;
                                  @endphp
                                  value="{{$value}}"
                        @if(!empty($filters['filters']))
                            {{in_array($value,$filters['filters'])?'checked':''}}
                            @else
                            {{!empty($item) && in_array($value,$item->FilterValues)?'checked':''}}
                            @endif
                        >{{$v['name']}}</label>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="extended_filter-row">
    <div class="extended_filter-title">
        <p>Удобства / опции:</p>
    </div>
    <div class="extended_filter-options">
        <div class="extended_filter-options_row">
            <div class="extended_filter-options_col w50">
                @foreach(\App\Models\UserObject::CATEGORY_5['values'] as $id=>$v)
                    <label><input type="checkbox" name="filters[]"
                                  @php
                                      $value = \App\Models\UserObject::CATEGORY_5['category_id'].'_'.$id;
                                  @endphp
                                  value="{{$value}}"
                        @if(!empty($filters['filters']))
                            {{in_array($value,$filters['filters'])?'checked':''}}
                            @else
                            {{!empty($item) && in_array($value,$item->FilterValues)?'checked':''}}
                            @endif
                        >{{$v['name']}}</label>
                @endforeach
            </div>
        </div>
        <div class="extended_filter-options_row">
            <div class="extended_filter-options_col">
                <p class="subtitle">Питание:</p>
                @foreach(\App\Models\UserObject::CATEGORY_6['values'] as $id=>$v)
                    <label><input type="checkbox" name="filters[]"
                                  @php
                                      $value = \App\Models\UserObject::CATEGORY_6['category_id'].'_'.$id;
                                  @endphp
                                  value="{{$value}}"
                        @if(!empty($filters['filters']))
                            {{in_array($value,$filters['filters'])?'checked':''}}
                            @else
                            {{!empty($item) && in_array($value,$item->FilterValues)?'checked':''}}
                            @endif
                        >{{$v['name']}}</label>
                @endforeach
            </div>
            <div class="extended_filter-options_col">
                <p class="subtitle">Для детей:</p>
                @foreach(\App\Models\UserObject::CATEGORY_7['values'] as $id=>$v)
                    <label><input type="checkbox" name="filters[]"
                                  @php
                                      $value = \App\Models\UserObject::CATEGORY_7['category_id'].'_'.$id;
                                  @endphp
                                  value="{{$value}}"
                        @if(!empty($filters['filters']))
                            {{in_array($value,$filters['filters'])?'checked':''}}
                            @else
                            {{!empty($item) && in_array($value,$item->FilterValues)?'checked':''}}
                            @endif
                        >{{$v['name']}}</label>
                @endforeach
            </div>
        </div>
    </div>
</div>
