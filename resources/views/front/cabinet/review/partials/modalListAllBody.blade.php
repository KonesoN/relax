<div class="modal-reviews_list">
    @foreach($pagination->items() as $item)
        <div class="modal-reviews_list-item">
            <form>
                <p class="name"><b>{{$item->user->first_name}}</b> <img src="{{asset('/img/front/flag-ru.jpg')}}">
                    Россия</p>
                <p class="date">{{Date('Y-m-d H:i',strtotime($item->created_at))}}</p>
                <p class="phone">{{$item->phone}}</p>
                <p class="review-text">
                    {{$item->review}}
                </p>
                <div class="send_message">
                    <input type="text" placeholder="Введите комментарий..." value="{{$item->answer}}"
                           id="answer_review_{{$item->id}}">
                    <button class="primary-btn _review_answer" data-answer_id="answer_review_{{$item->id}}"
                            data-object_id="{{$uo->id}}" data-page="{{$pagination->currentPage()}}"
                            data-id="{{$item->id}}" type="button">
                        @if($item->answer)
                            Редактировать комментарий
                        @else
                            Оставить комментарий
                        @endif
                    </button>
                </div>
            </form>
        </div>
    @endforeach
</div>
@include('front.partials.modalPagination',['class_pagination'=>'pagination_reviews','button_data'=>"data-object_id=$uo->id"])

