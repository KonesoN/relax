@extends('front.partials.app')

@section('content')

    <div class="container">
        <div class="cabinet-wrapper">
            <div class="cabinet-sidebar">
                @include('front.partials.sidebar')
            </div>
            <div class="cabinet-content">
                <div class="reviews-list">
                    @if(Auth::user()->type_id == \App\Models\User::TYPE_OWNER)
                        @foreach($pagination->items() as $item)
                            @php
                                $object = $item->object;
                            @endphp
                            <div class="reviews-list_item">
                                <div class="reviews-list_item-info">
                                    <img src="{{$object->MainPhoto?$object->MainPhoto->value:''}}">
                                    <p class="title">{{$object->title}}</p>
                                    <p class="subtitle">{{$object->address}}</p>
                                    @php
                                        $rating = $object->reviews()->avg('rating');
                                    @endphp
                                    <div class="reviews-list_item-info_rating">
                                        <div class="star-rating">
                                            <input type="radio" id="5-stars" name="rating"
                                                   value="5" {{floor($rating) == 5?'checked':''}}/>
                                            <label for="5-stars" class="star">&#9733;</label>
                                            <input type="radio" id="4-stars" name="rating"
                                                   value="4" {{floor($rating) == 4?'checked':''}}/>
                                            <label for="4-stars" class="star">&#9733;</label>
                                            <input type="radio" id="3-stars" name="rating"
                                                   value="3" {{floor($rating) == 3?'checked':''}}/>
                                            <label for="3-stars" class="star">&#9733;</label>
                                            <input type="radio" id="2-stars" name="rating"
                                                   value="2" {{floor($rating) == 2?'checked':''}}/>
                                            <label for="2-stars" class="star">&#9733;</label>
                                            <input type="radio" id="1-star" name="rating"
                                                   value="1" {{floor($rating) == 1?'checked':''}}/>
                                            <label for="1-star" class="star">&#9733;</label>
                                        </div>
                                        <p>({{$object->reviews()->count()}} отзыва)</p>
                                    </div>
                                </div>
                                <div class="reviews-list_item-review">
                                    <p class="title">Последний отзыв
                                        @if($object->new_reviews)
                                            (Новых: {{$object->new_reviews}})
                                        @endif
                                        :</p>
                                    @if(!$object->reviews()->count())
                                        <p class="date">Отзывов нет</p>
                                    @else
                                        @php
                                            $review = $object->reviews()->orderBy('id','desc')->first();
                                        @endphp
                                        <p class="name"><b>{{$review->user->first_name}}</b> <img
                                                src="{{asset('/img/front/flag-ru.jpg')}}"> Россия
                                        </p>
                                        <p class="date">{{Date('Y-m-d H:i',strtotime($review->created_at))}}</p>
                                        <p class="review-text">
                                            {{$review->review}}
                                            <span></span>
                                        </p>
                                        <button class="show_reviews" data-parent_id="{{$object->id}}"
                                                data-parent_type="{{$item->parent_type}}">Смотреть все
                                        </button>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                        @include('front.cabinet.review.partials.modalListAll')
                    @else
                        @foreach($pagination->items() as $item)
                            @php
                                $object = $item->object;
                            @endphp
                            <div class="reviews-list_item">
                                <div class="reviews-list_item-info">
                                    <img src="{{$object->MainPhoto->value}}">
                                    <p class="title">{{$object->title}}</p>
                                    <p class="subtitle">{{$object->address}}</p>
                                    @php
                                        $rating = $object->reviews()->avg('rating');
                                    @endphp
                                    <div class="reviews-list_item-info_rating">
                                        <div class="star-rating">
                                            <input type="radio" id="5-stars" name="rating"
                                                   value="5" {{floor($rating) == 5?'checked':''}}/>
                                            <label for="5-stars" class="star">&#9733;</label>
                                            <input type="radio" id="4-stars" name="rating"
                                                   value="4" {{floor($rating) == 4?'checked':''}}/>
                                            <label for="4-stars" class="star">&#9733;</label>
                                            <input type="radio" id="3-stars" name="rating"
                                                   value="3" {{floor($rating) == 3?'checked':''}}/>
                                            <label for="3-stars" class="star">&#9733;</label>
                                            <input type="radio" id="2-stars" name="rating"
                                                   value="2" {{floor($rating) == 2?'checked':''}}/>
                                            <label for="2-stars" class="star">&#9733;</label>
                                            <input type="radio" id="1-star" name="rating"
                                                   value="1" {{floor($rating) == 1?'checked':''}}/>
                                            <label for="1-star" class="star">&#9733;</label>
                                        </div>
                                        <p>({{$object->reviews()->count()}} отзыва)</p>
                                    </div>
                                </div>
                                <div class="reviews-list_item-review">
                                    <p class="title">Ваш отзыв:</p>
                                    {{$item->review}}
                                    @if($item->answer)
                                        <p class="title" style="margin-top: 20px">Ответ владельца:</p>
                                        {{$item->answer}}
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                @include('front.partials.pagination')
            </div>
        </div>
    </div>

@endsection
