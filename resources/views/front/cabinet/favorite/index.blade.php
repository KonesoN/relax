@extends('front.partials.app')

@section('content')

    <div class="container">
        <div class="cabinet-wrapper">
            <div class="cabinet-sidebar">
                @include('front.partials.sidebar')
            </div>
            <div class="cabinet-content">
                <div class="reviews-list">
                    @foreach($pagination->items() as $item)
                        @php
                            $item = $item->object;
                            $object = $item->object;

                        @endphp
                        <a class="favorite-list_item" href="{{route('front.object.view',$item->id)}}">
                            <div class="object-block_list-item">
                                @foreach(\App\Models\UserObjectAddition::ADDITIONS as $id=>$addition)
                                    @if($item->additions()->where(['addition_id'=>$id,'is_active'=>1])->first())
                                        <div class="object-block_info-label -label_prem">
                                            <div class="status">
                                                <img src=" {{$addition['logo']}}">
                                                {{$addition['name']}}
                                            </div>
                                            <span>
                    {{$item->additions()->where(['addition_id'=>$id,'is_active'=>1])->first()->LeftDays}}
                </span>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="object-block_container">
                                    <div class="object-block_info">
                                        <div class="image">
                                            <img src="{{$item->MainPhoto?$item->MainPhoto->value:''}}">
                                        </div>
                                        <div style="max-width: 400px">
                                            <p class="object-block_info-title">{{$item->title}}</p>
                                            <p class="object-block_info-subtitle">{{$item->address}}</p>
                                            @php
                                                $rating = $item->reviews()->avg('rating');
                                            @endphp
                                            <div class="object-block_info-rating">
                                                <div class="star-rating">
                                                    <input type="radio" id="5-stars" name="rating"
                                                           value="5" {{floor($rating) == 5?'checked':''}}/>
                                                    <label for="5-stars" class="star">&#9733;</label>
                                                    <input type="radio" id="4-stars" name="rating"
                                                           value="4" {{floor($rating) == 4?'checked':''}}/>
                                                    <label for="4-stars" class="star">&#9733;</label>
                                                    <input type="radio" id="3-stars" name="rating"
                                                           value="3" {{floor($rating) == 3?'checked':''}}/>
                                                    <label for="3-stars" class="star">&#9733;</label>
                                                    <input type="radio" id="2-stars" name="rating"
                                                           value="2" {{floor($rating) == 2?'checked':''}}/>
                                                    <label for="2-stars" class="star">&#9733;</label>
                                                    <input type="radio" id="1-star" name="rating"
                                                           value="1" {{floor($rating) == 1?'checked':''}}/>
                                                    <label for="1-star" class="star">&#9733;</label>
                                                </div>
                                                <p>({{$item->reviews()->count()}} отзыва)</p>
                                            </div>

                                            <ul class="object-block_info-price">
                                                @if($item->type == 'cottage')
                                                    <li>Сутки будни <span>{{$object->price_day}}₽</span></li>
                                                    <li>Сутки выходные <span>{{$object->price_day_weekend}}₽</span></li>
                                                    <li>Полные выходные (ПТ-ВС)
                                                        <span>{{$object->price_weekends}}₽</span></li>
                                                @else
                                                    <li>
                                                        Стоимость: {!! \App\Models\UserObject::PRICE_TYPES[$item->type][$item->price_type_id]['price'] !!}</li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        {{--                        <a class="reviews-list_item" href="{{route('front.object.view',$object->id)}}">--}}
                        {{--                            <div class="reviews-list_item-info">--}}
                        {{--                                <img src="{{$object->MainPhoto->value}}">--}}
                        {{--                                <p class="title">{{$object->title}}</p>--}}
                        {{--                                <p class="subtitle">{{$object->address}}</p>--}}
                        {{--                                @php--}}
                        {{--                                    $rating = $object->reviews()->avg('rating');--}}
                        {{--                                @endphp--}}
                        {{--                                <div class="reviews-list_item-info_rating">--}}
                        {{--                                    <div class="star-rating">--}}
                        {{--                                        <input type="radio" id="5-stars" name="rating"--}}
                        {{--                                               value="5" {{floor($rating) == 5?'checked':''}}/>--}}
                        {{--                                        <label for="5-stars" class="star">&#9733;</label>--}}
                        {{--                                        <input type="radio" id="4-stars" name="rating"--}}
                        {{--                                               value="4" {{floor($rating) == 4?'checked':''}}/>--}}
                        {{--                                        <label for="4-stars" class="star">&#9733;</label>--}}
                        {{--                                        <input type="radio" id="3-stars" name="rating"--}}
                        {{--                                               value="3" {{floor($rating) == 3?'checked':''}}/>--}}
                        {{--                                        <label for="3-stars" class="star">&#9733;</label>--}}
                        {{--                                        <input type="radio" id="2-stars" name="rating"--}}
                        {{--                                               value="2" {{floor($rating) == 2?'checked':''}}/>--}}
                        {{--                                        <label for="2-stars" class="star">&#9733;</label>--}}
                        {{--                                        <input type="radio" id="1-star" name="rating"--}}
                        {{--                                               value="1" {{floor($rating) == 1?'checked':''}}/>--}}
                        {{--                                        <label for="1-star" class="star">&#9733;</label>--}}
                        {{--                                    </div>--}}
                        {{--                                    <p>({{$object->reviews()->count()}} отзыва)</p>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </a>--}}
                    @endforeach
                </div>
                @include('front.partials.pagination')
            </div>
        </div>
    </div>
@endsection
