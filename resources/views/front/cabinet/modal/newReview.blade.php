<div id="open-modal_addReview" class="modal-window" style="display: none">
    <div>
        <a href="#" title="Close" data-modal_id="open-modal_addReview" class="modal-close modal_hide">Закрыть
            <span>X</span></a>

        <div class="modal-reviews_list">
            <div class="modal-reviews_list-item addReview-item">
                <form method="post" action="{{route('front.cabinet.review.new')}}" id="review_form">
                    <input type="hidden" value="{{$parent_id}}" name="parent_id"/>
                    <input type="hidden" value="{{$parent_type}}" name="parent_type"/>
                    <textarea placeholder="Оставить отзыв" required name="review"></textarea>
                    <div class="addReview-item-control">
                        <div>
                            <input type="number" placeholder="Номер телефона" name="phone" required>
                            <div class="reviews-list_item-info_rating">
                                <span>Оцените:</span>
                                <div class="star-rating">
                                    <input type="radio" id="modal_5-stars" name="modal_rating" value="5"/>
                                    <label for="modal_5-stars" class="star">&#9733;</label>
                                    <input type="radio" id="modal_4-stars" name="modal_rating" value="4"/>
                                    <label for="modal_4-stars" class="star">&#9733;</label>
                                    <input type="radio" id="modal_3-stars" name="modal_rating" value="3" checked/>
                                    <label for="modal_3-stars" class="star">&#9733;</label>
                                    <input type="radio" id="modal_2-stars" name="modal_rating" value="2"/>
                                    <label for="modal_2-stars" class="star">&#9733;</label>
                                    <input type="radio" id="modal_1-star" name="modal_rating" value="1"/>
                                    <label for="modal_1-star" class="star">&#9733;</label>
                                </div>
                            </div>
                        </div>
                        <button class="primary-btn" type="button" id="submit_review">Оставить отзыв</button>
                    </div>
                </form>
            </div>
            @foreach($item->activeReviews as $review)
                <div class="modal-reviews_list-item">
                    <p class="name"><b>{{$review->user->first_name}}</b> <img src="{{asset('/img/front/flag-ru.jpg')}}">
                        Россия</p>
                    <p class="date">{{Date('Y-m-d H:i:s',strtotime($review->created_at))}}</p>
                    <p class="review-text">
                        {{$review->review}}
                    </p>
                </div>
            @endforeach
        </div>
    </div>
</div>
