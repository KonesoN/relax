<div id="open-modal_newMessage" class="modal-window" style="display: none">
    <div>
        <a href="#" title="Close" data-modal_id="open-modal_newMessage" class="modal-close modal_hide">Закрыть
            <span>X</span></a>
        <div class="modal-reviews_list">
            <div class="modal-reviews_list-item addReview-item">
                <form method="post" action="{{route('front.cabinet.message.new')}}" id="message_form">
                    <input type="hidden" value="{{$parent_id}}" name="parent_id"/>
                    <input type="hidden" value="{{$parent_type}}" name="parent_type"/>
                    <textarea placeholder="Сообщение" required name="message"></textarea>
                    <div class="addReview-item-control">
                        <button class="primary-btn" type="button" id="submit_message">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
