@extends('front.partials.app')

@section('content')

    <div class="container">
        <div class="cabinet-wrapper">
            <div class="cabinet-sidebar">
                @include('front.partials.sidebar')
            </div>
            <div class="cabinet-content">

                <div class="object-block">

                    <div class="tabs">

                        <!-- Tab 1 & Content -->
                        <input type="radio" name="tab" id="active" role="tab" checked>
                        <label for="active" id="active-label">Активные <span>{{$active->count()}}</span></label>
                        <section aria-labelledby="active-label">
                            <div class="object-block_list">
                                @foreach($active as $item)
                                    @include('front.cabinet.object.partials.objectItem')
                                @endforeach
                            </div>
                        </section>

                        <!-- Tab 2 & Content -->
                        <input type="radio" name="tab" id="inactive" role="tab">
                        <label for="inactive" id="inactive-label">Не
                            активные<span>{{$inactive->count()}}</span></label>
                        <section aria-labelledby="inactive-label">
                            <div class="object-block_list">
                                @foreach($inactive as $item)
                                    @include('front.cabinet.object.partials.objectItem')
                                @endforeach
                            </div>
                        </section>

                        <!-- Tab 3 & Content -->
                        <input type="radio" name="tab" id="archive" role="tab">
                        <label for="archive" id="archive-label">Архив<span>{{$archive->count()}}</span></label>
                        <section aria-labelledby="archive-label">
                            <div class="object-block_list">
                                @foreach($archive as $item)
                                    @include('front.cabinet.object.partials.objectItem')
                                @endforeach
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
