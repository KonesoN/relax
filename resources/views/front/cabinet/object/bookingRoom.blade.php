@extends('front.partials.app')

@section('content')
    @php
        $object = $item->object;
    @endphp
    <div class="container">
        <div class="cabinet-wrapper">
            <div class="cabinet-sidebar">
                @include('front.partials.sidebar')
            </div>
            <div class="cabinet-content">
                <div class="select-box_booking">
                    <div class="custom-select">
                        <select id="select_room_id">
                            @foreach($object->rooms as $r)
                                <option
                                        value="{{route('front.cabinet.object.booking',$item->id)}}?room_id={{$r->id}}"
                                        {{$room->id == $r->id?'selected':''}}>{{$r->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @foreach($room->BusyDates as $bd)
                    <input type="hidden" class="_disable_days" value="{{$bd}}"/>
                @endforeach
                <div class="booking-block">
                    <p class="booking-block_title">Бронирование</p>

                    <div class="datepicker-here booking-calendar"></div>

                    <form method="post">
                        <input type="hidden" name="room_id" value="{{$room->id}}"/>
                        <div class="booking-row">
                            <label><p>От*</p> <input type="date" name="arrival_at" required></label>
                            <label><p>До*</p> <input type="date" name="departure_at" required></label>
                            <label><p>ФИО*</p> <input type="text" name="full_name" required></label>
                            <label><p>Номер</p> <input type="text" name="phone"></label>
                            <label><p>Почта</p> <input type="email" name="email"></label>
                            <button type="submit" class="primary-btn">Бронировать</button>
                        </div>
                    </form>

                    <p class="booking-block_title">Заявки пользователей</p>

                    <div class="booking-content_list">
                        <div class="booking-content_list-header">
                            <div class="booking-content_list-row">
                                <ul>
                                    <li>Имя</li>
                                    <li>Фамилия</li>
                                    <li>E-mail</li>
                                    <li>Телефон</li>
                                    <li>Дата заезда</li>
                                    <li>Дата выезда</li>
                                    <li>Действия</li>
                                </ul>
                            </div>
                        </div>
                        <div class="booking-content_list-body">
                            <div class="booking-content_list-row">
                                @foreach($room->reservations()->where('status_id',\App\Models\HotelRoomReservation::STATUS_USER_CONFIRM)->get() as $b)
                                    <ul>
                                        <li>{{$b->first_name}}</li>
                                        <li>{{$b->last_name}}</li>
                                        <li>{{$b->email}}</li>
                                        <li>{{$b->phone}}</li>
                                        <li>{{Date('Y-m-d', strtotime($b->arrival_at))}}</li>
                                        <li>{{Date('Y-m-d', strtotime($b->departure_at))}}</li>
                                        <li>
                                            <a href="{{route('front.cabinet.object.hotel.reservation.action',$b->id)}}?action=delete"
                                               class="primary-btn">Удалить</a>
                                            <a href="{{route('front.cabinet.object.hotel.reservation.action',$b->id)}}?action=approve"
                                               class="primary-btn"
                                               style="font-size: 13px; margin-top: 5px; padding: 5px 2px 9px;">Забронировать</a>
                                        </li>
                                    </ul>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <p class="booking-block_title" style="margin-top:10px">Забронировано</p>

                    <div class="booking-content_list">
                        <div class="booking-content_list-header">
                            <div class="booking-content_list-row">
                                <ul>
                                    <li>ФИО</li>
                                    <li>E-mail</li>
                                    <li>Телефон</li>
                                    <li>Дата</li>
                                    <li>Действия</li>
                                </ul>
                            </div>
                        </div>
                        <div class="booking-content_list-body">
                            <div class="booking-content_list-row">
                                @foreach($room->activeBookings as $b)
                                    <ul>
                                        <li>{{$b->full_name}}</li>
                                        <li>{{$b->email}}</li>
                                        <li>{{$b->phone}}</li>
                                        <li>{{Date('Y-m-d', strtotime($b->booking_at))}}</li>
                                        <li>
                                            <a href="{{route('front.cabinet.object.hotel.booking.action',$b->id)}}?action=delete"
                                               class="primary-btn">Удалить</a>
                                        </li>
                                    </ul>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
