@extends('front.partials.app')

@section('content')
    <div class="container">
        <div class="cabinet-wrapper">
            <div class="cabinet-sidebar newObject-sidebar">
                <div class="newObject-nav">
                    <div class="select_tab select_tab-active" data-tab_id="1">Тип размещения</div>
                    <div class="select_tab" data-tab_id="2">Баланс</div>
                </div>
            </div>
            <div class="cabinet-content">
                <form class="add_object" id="newObject-form" method="post">
                    <div id="tab_1" class="tabs" style="display: block">
                        @include('front.cabinet.object.partials.paymentTab',['allocation'=>'hide'])
                        <button type="button" class="subprimary-btn" data-tab_id="2">Перейти к оплате</button>
                    </div>
                    <div id="tab_2" class="tabs">
                        <div class="object-balance">
                            @include('front.cabinet.object.partials.additionTab')
                            <button type="submit" class="subprimary-btn" id="order_submit" data-submit="true">Оплатить
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <input value="{{\App\Models\Transaction::METHOD_BALANCE}}" id="method_balance" type="hidden"/>
    <input value="{{Auth::user()->balance}}" id="user_balance" type="hidden"/>
    <input value="{{$item->type}}" id="item_type" type="hidden"/>
@endsection
