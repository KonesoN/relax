<p class="title">Выбранные опции:</p>
<div id="additions_list">
</div>
<div class="object_balance-row">
    <p class="title">Всего к оплате:</p>
    <span><span class="object_price">0</span> руб</span>
</div>
<div class="object_balance-row">
    <div class="object_balance-block">
        <small>На балансе</small>
        <span>{{Auth::user()->balance}} руб</span>
    </div>
    {{--                                <button class="primary-btn">Списать с баланса</button>--}}
</div>
<div class="object_balance-row">
    <p>Способ оплаты:</p>
    <div class="object_balance-box">
        @foreach(\App\Models\Transaction::METHODS as $id=>$m)
            @if($m['view'][\App\Models\Transaction::TYPE_OBJECT_PAYMENT])
                <p class="form-radio">
                    <input type="radio" id="method_{{$id}}" value="{{$id}}"
                           name="method_id" {{$loop->index == 0?'checked':''}}>
                    <label for="method_{{$id}}">
                        <img src="{{asset($m['logo'])}}" style="width: 24px;">
                        <span>{!! $m['name'] !!}</span>
                    </label>
                </p>
            @endif
        @endforeach
    </div>
</div>
