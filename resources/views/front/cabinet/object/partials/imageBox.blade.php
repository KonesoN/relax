@php
    $name = !empty($name)?$name:'gallery';
@endphp
<div class="upload-img_box-item _item_{{$name}}">
    <img src="{{$item->value}}">
    <input type="hidden" name="{{$name}}[]" value="{{$item->id}}"/>
    <input type="hidden" name="{{$name}}_is_main[{{$item->id}}]" value="{{$item->is_main?1:''}}" class="_is_main"/>
    <div>
        <button class="main-img {{$item->is_main?'opacity05':''}}" type="button" title="Сделать главным фото">
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                 width="20" height="20"
                 viewBox="0 0 172 172"
                 style=" fill:#000000;">
                <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
                   stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
                   font-family="none" font-weight="none" font-size="none" text-anchor="none"
                   style="mix-blend-mode: normal">
                    <path d="M0,172v-172h172v172z" fill="none"></path>
                    <g fill="#448d2f">
                        <path
                            d="M78.83333,117.53333l-33.68333,-33.68333l10.03333,-10.03333l23.65,23.65l60.2,-60.2c-13.61667,-13.61667 -32.25,-22.93333 -53.03333,-22.93333c-39.41667,0 -71.66667,32.25 -71.66667,71.66667c0,39.41667 32.25,71.66667 71.66667,71.66667c39.41667,0 71.66667,-32.25 71.66667,-71.66667c0,-13.61667 -3.58333,-25.8 -10.03333,-36.55z"></path>
                    </g>
                </g>
            </svg>
        </button>
        <button class="del-img _delete_parent" data-delete="parent2" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                 width="20" height="20"
                 viewBox="0 0 172 172"
                 style=" fill:#000000;">
                <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
                   stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray=""
                   stroke-dashoffset="0"
                   font-family="none" font-weight="none" font-size="none" text-anchor="none"
                   style="mix-blend-mode: normal">
                    <path d="M0,172v-172h172v172z" fill="none"></path>
                    <g fill="#ca3636">
                        <path
                            d="M71.66667,14.33333l-7.16667,7.16667h-28.66667c-3.956,0 -7.16667,3.21067 -7.16667,7.16667c0,3.956 3.21067,7.16667 7.16667,7.16667h100.33333c3.956,0 7.16667,-3.21067 7.16667,-7.16667c0,-3.956 -3.21067,-7.16667 -7.16667,-7.16667h-28.66667l-7.16667,-7.16667zM35.83333,50.16667v93.16667c0,7.88333 6.45,14.33333 14.33333,14.33333h71.66667c7.88333,0 14.33333,-6.45 14.33333,-14.33333v-93.16667zM67.43945,74.63411c1.82929,0 3.65914,0.6917 5.05306,2.08561l13.50749,13.50749l13.50749,-13.50749c2.78783,-2.78783 7.31828,-2.78783 10.10612,0c2.78783,2.78783 2.78783,7.31828 0,10.10612l-13.50749,13.50749l13.50749,13.50749c2.78783,2.78783 2.78783,7.31828 0,10.10612c-2.78783,2.78783 -7.31828,2.78783 -10.10612,0l-13.50749,-13.50749l-13.50749,13.50749c-2.78783,2.78783 -7.31828,2.78783 -10.10612,0c-2.78783,-2.78783 -2.78783,-7.31828 0,-10.10612l13.50749,-13.50749l-13.50749,-13.50749c-2.78783,-2.78783 -2.78783,-7.31828 0,-10.10612c1.39392,-1.39392 3.22377,-2.08561 5.05306,-2.08561z"></path>
                    </g>
                </g>
            </svg>
        </button>
    </div>
</div>
