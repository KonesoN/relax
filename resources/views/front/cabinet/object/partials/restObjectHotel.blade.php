<div class="hide_by_rest_type show_by_rest_type_hotel">
    <div class="object-card_row">
        <p>Правила размещения</p>
        <div class="label-box">
            <label>
                <span>Название</span>
                <textarea maxlength="190" required placeholder="" class="_required"
                          name="hotel[title]">{{!empty($restObject)?$restObject->title:''}}</textarea>
            </label>
            <label>
                <span>Время заезда:</span>
                <input type="time" placeholder="" name="hotel[arrival_at]" required class="_required"
                       value="{{!empty($restObject)?$restObject->arrival_at:'00:00'}}">
            </label>
            <label>
                <span>Время выезда:</span>
                <input type="time" placeholder="" name="hotel[departure_at]" required class="_required"
                       value="{{!empty($restObject)?$restObject->departure_at:'00:00'}}">
            </label>
            <label>
                <span>Минимальное количество суток:</span>
                <input type="number" placeholder="" name="hotel[minimum_days]" class="_required"
                       value="{{!empty($restObject)?$restObject->minimum_days:1}}">
            </label>
            <label>
                <span>Можно с животными:</span>
                <input type="checkbox" placeholder="" name="hotel[is_pets]"
                    {{!empty($restObject->is_pets)?'checked':''}}>
            </label>
        </div>
    </div>
</div>
