@php
    $hash_rest = md5(strtotime('now')+rand(99999,999999));
    $hash = !empty($hash)?$hash:$restObject->id;
@endphp
<div>
    <div class="added-objects_list rest_object_min_{{$hash_rest}}"
         style="display: {{!empty($restObject)?'block':'none'}}">
        <div class="added-objects_list-item object-block_list-item">
            <div class="object-block_info">
                <div class="image">
                    <img src="{{!empty($restObject)?$restObject->MainPhoto->value:''}}"
                         class="rest_object_image_src_{{$hash_rest}}">
                </div>
                <div>
                    <p class="object-block_info-title rest_object_title_{{$hash_rest}}">{{!empty($restObject)?$restObject->title:''}}</p>
                    @if((!empty($restObject) && $restObject->type == 'hotel') || empty($restObject))
                        <ul class="advantages-list hide_rest_object_data_hotel">
                            <li>Время заезда: <span
                                        class="rest_object_arrival_at_{{$hash_rest}}">{{!empty($restObject)?$restObject->arrival_at:''}}</span>
                            </li>
                            <li>Время выезда:<span
                                        class="rest_object_departure_at_{{$hash_rest}}">{{!empty($restObject)?$restObject->departure_at:''}}</span>
                            </li>
                            <li>Минимальное количество суток:<span
                                        class="rest_object_minimum_days_{{$hash_rest}}">{{!empty($restObject)?$restObject->minimum_days:1}}</span>
                            </li>
                            <li>Можно с животными:<span
                                        class="rest_object_is_pets_{{$hash_rest}}">{{!empty($restObject->is_pets)?'Да':'Нет'}}</span>
                            </li>
                        </ul>
                    @endif
                    @if((!empty($restObject) && $restObject->type == 'cottage') || empty($restObject))
                        <ul class="advantages-list hide_rest_object_data_cottage">
                            <li>Жилая площадь, м&sup2;:<span
                                        class="rest_object_living_space_{{$hash_rest}}">{{!empty($restObject)?$restObject->living_space:0}}</span>
                            </li>
                            <li>Вместимость гостей:<span
                                        class="rest_object_count_guest_{{$hash_rest}}">{{!empty($restObject)?$restObject->count_guest:0}}</span>
                            </li>
                            <li>Кол-во спален:<span
                                        class="rest_object_count_bedrooms_{{$hash_rest}}">{{!empty($restObject)?$restObject->count_bedrooms:0}}</span>
                            </li>
                            <li>Кол-во маленьких кроватей:<span
                                        class="rest_object_count_small_bedrooms_{{$hash_rest}}">{{!empty($restObject)?$restObject->count_small_bedrooms:0}}</span>
                            </li>
                            <li>Кол-во больших кроватей:<span
                                        class="rest_object_count_big_bedrooms_{{$hash_rest}}">{{!empty($restObject)?$restObject->count_big_bedrooms:0}}</span>
                            </li>
                        </ul>
                    @endif
                </div>
            </div>
            <div class="object-block_control">
                <button type="button" class="edit-object _rest_object_view_form" data-hash="{{$hash_rest}}">
                    Редактировать
                </button>
                <button type="button" class="delete-object _delete_parent" data-delete="parent4">Удалить</button>
            </div>
        </div>
    </div>
    <div class="addHotel-number_form rest_object_form rest_object_form_{{$hash_rest}}"
         data-action="{{route('front.cabinet.object.rest.object.form')}}"
         style="display: {{!empty($restObject)?'none':'block'}}">
        <input type="hidden" value="{{!empty($restObject)?$restObject->id:''}}" name="rest_ids[]"
               class="rest_object_id_{{$hash_rest}}"/>
        <input type="hidden" value="{{!empty($restObject)?$restObject->id:''}}" name="rest_id"
               class="rest_object_id_{{$hash_rest}}"/>
        <input type="hidden" value="{{$hash_rest}}" name="hash_rest"/>
        <input type="hidden" value="{{$hash}}" name="hash_object"/>
        @if(empty($restObject))
            <div class="newObject-row type_rest-block type_rest_block_{{$hash_rest}}">
                <p class="title">Выберите категорию объекта для размещения</p>
                <div class="newObject-row_input">
                    <p class="form-radio">
                        <input type="radio" name="{{$hash_rest}}[type_rest]" id="rest_cottage_{{$hash_rest}}"
                               class="type_rest_radio"
                               value="{{\App\Models\RestObject::TYPE_COTTAGE}}"
                               checked data-hash_rest="{{$hash_rest}}">
                        <label for="rest_cottage_{{$hash_rest}}">Коттедж</label>
                    </p>
                    <p class="form-radio">
                        <input type="radio" name="{{$hash_rest}}[type_rest]" id="rest_hotel_{{$hash_rest}}"
                               class="type_rest_radio"
                               data-hash_rest="{{$hash_rest}}"
                               value="{{\App\Models\RestObject::TYPE_HOTEL}}">
                        <label for="rest_hotel_{{$hash_rest}}">Отель / гостиница</label>
                    </p>
                </div>
            </div>
        @else
            <input class="type_rest_radio" type="hidden" name="{{$hash_rest}}[type_rest]"
                   value="{{$restObject->type}}"/>
        @endif
        @include('front.cabinet.object.partials.restObjectCottage')
        @include('front.cabinet.object.partials.restObjectHotel')
        <div class="object-card_row">
            <p>Общая информация:</p>
            <div class="label-box">
                <div class="upload-photo">
                    <div class='file-upload'>
                        <label for='input-file_{{$hash_rest}}'>
                            Добавить фото
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                 width="24" height="24"
                                 viewBox="0 0 172 172"
                                 style=" fill:#000000;">
                                <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
                                   stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray=""
                                   stroke-dashoffset="0"
                                   font-family="none" font-weight="none" font-size="none" text-anchor="none"
                                   style="mix-blend-mode: normal">
                                    <path d="M0,172v-172h172v172z" fill="none"></path>
                                    <g fill="#448d2f">
                                        <path
                                                d="M75.25,14.33333c-5.85562,0 -10.75,4.89438 -10.75,10.75v46.58333h-31.63411l53.13411,53.13411l53.13411,-53.13411h-31.63411v-46.58333c0,-5.85562 -4.89438,-10.75 -10.75,-10.75zM78.83333,28.66667h14.33333v57.33333h11.36589l-18.53256,18.53256l-18.53255,-18.53256h11.36589zM14.33333,143.33333v14.33333h143.33333v-14.33333z"></path>
                                    </g>
                                </g>
                            </svg>
                        </label>
                        <input id="input-file_{{$hash_rest}}" class='input-file' multiple type='file'
                               data-list_id="_gallery_list_{{$hash_rest}}"
                               data-upload_url="{{route('front.cabinet.object.rest.object.uploadPhoto')}}"/>
                    </div>
                    <div class="upload-img_box" id="_gallery_list_{{$hash_rest}}">
                        @if(!empty($restObject))
                            @foreach($restObject->photos as $photo)
                                @include('front.cabinet.object.partials.imageBox',['item'=>$photo,'name'=>'rest_gallery'])
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="upload-photo">
                    <p class="subtitle">Загрузить видео:</p>
                    <input type="text" name="object[video_url]"
                           value={{!empty($restObject) && !empty($restObject->youtube()->first())?$restObject->youtube()->first()->value:''}}>

                </div>
                <div class="upload-photo">
                    <p class="subtitle">Описание объекта:</p>
                    <textarea class="description-object" required class="_required"
                              name="object[description]">{{!empty($restObject)?$restObject->description:''}}</textarea>
                </div>
            </div>
        </div>
        <div class="hide_by_rest_type show_by_rest_type_hotel" id="rest_list_room_{{$hash_rest}}">
            @if(!empty($restObject) && $restObject->type == 'hotel')
                @foreach($restObject->rooms as $room)
                    @include('front.cabinet.object.partials.restHotelRoom')
                @endforeach
            @endif
        </div>
        <div class="hide_by_rest_type show_by_rest_type_hotel">
            <button class="addHotel-number_btn rest_add_room" type="button" data-hash="{{$hash_rest}}">
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                     width="24" height="24"
                     viewBox="0 0 172 172"
                     style=" fill:#000000;">
                    <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
                       stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
                       font-family="none" font-weight="none" font-size="none" text-anchor="none"
                       style="mix-blend-mode: normal">
                        <path d="M0,172v-172h172v172z" fill="none"></path>
                        <g fill="#448d2f">
                            <path
                                    d="M86,14.33333c-39.5815,0 -71.66667,32.08517 -71.66667,71.66667c0,39.5815 32.08517,71.66667 71.66667,71.66667c39.5815,0 71.66667,-32.08517 71.66667,-71.66667c0,-39.5815 -32.08517,-71.66667 -71.66667,-71.66667zM121.83333,93.16667h-28.66667v28.66667h-14.33333v-28.66667h-28.66667v-14.33333h28.66667v-28.66667h14.33333v28.66667h28.66667z"></path>
                        </g>
                    </g>
                </svg>
                Добавить номер
            </button>
        </div>
        <div style="display: flex">
            <button type="button" class="primary-btn _delete_parent" data-delete="parent3">Удалить</button>
            <button style="margin-left: 10px" type="button" class="primary-btn save_rest_object"
                    data-hash="{{$hash_rest}}">Сохранить объект
            </button>
        </div>
    </div>
</div>
