<div class="hide_by_rest_type show_by_rest_type_cottage">
    <div class="object-card_row">
        <p>Параметры объекта</p>
        <div class="label-box">
            <label>
                <span>Название</span>
                <textarea maxlength="190" required placeholder="" class="_required"
                          name="cottage[title]">{{!empty($restObject)?$restObject->title:''}}</textarea>
            </label>
            <label>
                <span>Жилая площадь,  м&sup2;</span>
                <input type="number" required placeholder="" name="cottage[living_space]" class="_required"
                       value="{{!empty($restObject)?$restObject->living_space:''}}">
            </label>
            <label>
                <span>Вместимость гостей</span>
                <input type="number" required placeholder="" name="cottage[count_guest]" class="_required"
                       value="{{!empty($restObject)?$restObject->count_guest:''}}">
            </label>
            <label>
                <span>Кол-во спален</span>
                <input type="number" placeholder="" name="cottage[count_bedrooms]" class="_required"
                       value="{{!empty($restObject)?$restObject->count_bedrooms:0}}">
            </label>
            <label>
                <span>Кол-во маленьких кроватей</span>
                <input type="number" placeholder="" name="cottage[count_small_bedrooms]" class="_required"
                       value="{{!empty($restObject)?$restObject->count_small_bedrooms:0}}">
            </label>
            <label>
                <span>Кол-во больших кроватей</span>
                <input type="number" placeholder="" name="cottage[count_big_bedrooms]" class="_required"
                       value="{{!empty($restObject)?$restObject->count_big_bedrooms:0}}">
            </label>
        </div>
    </div>
    <div class="object-card_row">
        <p>Правила размещения</p>
        <div class="label-box">
            <label>
                <span>Время заезда:</span>
                <input type="time" placeholder="" name="cottage[arrival_at]" required class="_required"
                       value="{{!empty($restObject)?$restObject->arrival_at:'00:00'}}">
            </label>
            <label>
                <span>Время выезда:</span>
                <input type="time" placeholder="" name="cottage[departure_at]" required class="_required"
                       value="{{!empty($restObject)?$restObject->departure_at:'00:00'}}">
            </label>
            <label>
                <span>Минимальное количество суток:</span>
                <input type="number" placeholder="" name="cottage[minimum_days]" class="_required"
                       value="{{!empty($restObject)?$restObject->minimum_days:1}}">
            </label>
            <label>
                <span>Страховой депозит:</span>
                <input type="number" placeholder="" name="cottage[insurance_deposit]" class="_required"
                       value="{{!empty($restObject)?$restObject->insurance_deposit:0}}">
            </label>
            <label>
                <span>Можно с животными:</span>
                <input type="checkbox" placeholder="" name="cottage[is_pets]"
                    {{!empty($restObject->is_pets)?'checked':''}}>
            </label>
        </div>
    </div>
    <div class="object-card_row">
        <p>Стоимость размещения</p>
        <div class="label-box">
            <label>
                <span>Сутки будни:</span>
                <input type="number" required placeholder="" name="cottage[price_day]" class="_required"
                       value="{{!empty($restObject)?$restObject->price_day:''}}">
            </label>
            <label>
                <span>Сутки выходные:</span>
                <input type="number" required placeholder="" name="cottage[price_day_weekend]" class="_required"
                       value="{{!empty($restObject)?$restObject->price_day_weekend:''}}">
            </label>
            <label>
                <span>Полные выходные:</span>
                <input type="number" required placeholder="" name="cottage[price_weekends]" class="_required"
                       value="{{!empty($restObject)?$restObject->price_weekends:''}}">
            </label>
        </div>
    </div>
</div>
