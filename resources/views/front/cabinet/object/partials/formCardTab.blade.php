@php
    $object = !empty($item)?$item->object:null;
@endphp
<div class="object-card">
    <div class="object-card_row">
        <p>Название объекта</p>
        <label style="align-items: flex-start; flex-direction: column;">
            <input type="text" style="max-width: 700px;" name="title" required id="object_title"
                   value="{{!empty($item)?$item->title:''}}">
            <div class="counter" style="font-size: 10px">Осталось символов: <span id="counter"></span></div>
        </label>
    </div>
    <div class="object-card_row">
        <p>Телефон</p>
        <label style="align-items: flex-start; flex-direction: column;">
            <input type="text" style="max-width: 700px;" name="phone" required id="object_phone"
                   value="{{!empty($item)?$item->phone:''}}">
        </label>
    </div>
</div>
<div class="object-card">
    <div class="object-card_row">
        <p>Адрес</p>
        <label style="color: #438b2e">
            <input type="text" placeholder="Выберите на карте" required readonly id="object_address"
                   style="max-width: 700px;" name="address" value="{{!empty($item)?$item->address:''}}">
            <input type="hidden" name="lat" id="object_address_lat" value="{{!empty($item)?$item->lat:''}}"/>
            <input type="hidden" name="lng" id="object_address_lng" value="{{!empty($item)?$item->lng:''}}"/>
            {{--            <div><img src="{{asset('/img/front/map-green.png')}}"> Выбрать на карте</div>--}}
        </label>
    </div>
    <div class="object-card_row">
        <input id="pac-input" class="controls" type="text" placeholder="Введите адрес">
        <div id="object_form_map" style="width: 100%;height: 400px"></div>
    </div>
</div>
<div class="object-card">
    <div class="object-card_row hide_by_type show_by_type_cottage">
        <p>Параметры</p>
        <div class="label-box">
            <label>
                <span>Жилая площадь,  м&sup2;</span>
                <input type="number" placeholder="" name="living_space" class="_required"
                       value="{{!empty($object->living_space)?$object->living_space:''}}">
            </label>
            <label>
                <span>Вместимость гостей</span>
                <input type="number" placeholder="" name="count_guest" class="_required"
                       value="{{!empty($object->count_guest)?$object->count_guest:''}}">
            </label>
            <label>
                <span>Кол-во спален</span>
                <input type="number" placeholder="" name="count_bedrooms" class="_required"
                       value="{{!empty($object->count_bedrooms)?$object->count_bedrooms:0}}">
            </label>
            <label>
                <span>Кол-во маленьких кроватей</span>
                <input type="number" placeholder="" name="count_small_bedrooms" class="_required"
                       value="{{!empty($object->count_small_bedrooms)?$object->count_small_bedrooms:0}}">
            </label>
            <label>
                <span>Кол-во больших кроватей</span>
                <input type="number" placeholder="" name="count_big_bedrooms" class="_required"
                       value="{{!empty($object->count_big_bedrooms)?$object->count_big_bedrooms:0}}">
            </label>
        </div>
    </div>
    <div class="object-card_row">
        <p>Правила размещения</p>
        <div class="label-box">
            <label>
                <span>Время заезда:</span>
                <input type="time" placeholder="" name="arrival_at" required
                       value="{{!empty($item)?$item->arrival_at:'00:00'}}">
            </label>
            <label>
                <span>Время выезда:</span>
                <input type="time" placeholder="" name="departure_at" required
                       value="{{!empty($item)?$item->departure_at:'00:00'}}">
            </label>
            <label>
                <span>Минимальное количество суток:</span>
                <input type="number" placeholder="" name="minimum_days"
                       value="{{!empty($item)?$item->minimum_days:1}}">
            </label>
            <label>
                <span>Страховой депозит:</span>
                <input type="number" placeholder="" name="insurance_deposit"
                       value="{{!empty($item)?$item->insurance_deposit:0}}">
            </label>
            <label>
                <span>Можно с животными:</span>
                <input type="checkbox" placeholder="" name="is_pets"
                        {{!empty($item->is_pets)?'checked':''}}>
            </label>
        </div>
    </div>
    <div class="object-card_row hide_by_type show_by_type_cottage">
        <p>Стоимость размещения</p>
        <div class="label-box">
            <label>
                <span>Сутки будни:</span>
                <input type="number" placeholder="" name="price_day" class="_required"
                       value="{{!empty($object)?$object->price_day:''}}">
            </label>
            <label>
                <span>Сутки выходные:</span>
                <input type="number" placeholder="" name="price_day_weekend" class="_required"
                       value="{{!empty($object)?$object->price_day_weekend:''}}">
            </label>
            <label>
                <span>Полные выходные:
                <span class="info">
                    <svg
                            xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                            width="15" height="15"
                            viewBox="0 0 172 172"
                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero"
                                                      stroke="none" stroke-width="1"
                                                      stroke-linecap="butt"
                                                      stroke-linejoin="miter"
                                                      stroke-miterlimit="10"
                                                      stroke-dasharray=""
                                                      stroke-dashoffset="0"
                                                      font-family="none"
                                                      font-size="none"
                                                      style="mix-blend-mode: normal"><path
                                    d="M0,172v-172h172v172z" fill="none"></path><g
                                    fill="#bfbfbf"><path
                                        d="M86,7.85577c-43.15504,0 -78.14423,34.98919 -78.14423,78.14423c0,43.15505 34.98919,78.14423 78.14423,78.14423c43.15505,0 78.14423,-34.98918 78.14423,-78.14423c0,-43.15504 -34.98918,-78.14423 -78.14423,-78.14423zM102.28005,128.97416c-4.03125,1.57632 -7.23558,2.79087 -9.63883,3.61779c-2.40324,0.82692 -5.16827,1.24038 -8.34675,1.24038c-4.85817,0 -8.65685,-1.18871 -11.37019,-3.56611c-2.6875,-2.35156 -4.03125,-5.375 -4.03125,-9.04447c0,-1.42128 0.10337,-2.86839 0.3101,-4.34135c0.18088,-1.4988 0.51683,-3.15265 0.95613,-5.03906l5.03906,-17.77885c0.4393,-1.70553 0.82692,-3.30769 1.13702,-4.83233c0.3101,-1.52463 0.4393,-2.92007 0.4393,-4.18629c0,-2.2482 -0.46515,-3.85036 -1.39544,-4.72897c-0.95613,-0.90445 -2.73918,-1.34375 -5.375,-1.34375c-1.29206,0 -2.63581,0.20673 -4.0054,0.59435c-1.3696,0.41346 -2.53246,0.80108 -3.51442,1.16286l1.34375,-5.47837c3.28185,-1.34375 6.43449,-2.48077 9.45793,-3.4369c2.9976,-0.95613 5.86599,-1.44712 8.52765,-1.44712c4.83233,0 8.57933,1.1887 11.1893,3.51442c2.60997,2.32572 3.92788,5.375 3.92788,9.09615c0,0.77524 -0.07753,2.14483 -0.25842,4.08293c-0.18088,1.9381 -0.51683,3.72115 -1.00781,5.375l-5.01322,17.72717c-0.41346,1.42128 -0.77524,3.04928 -1.11118,4.85817c-0.3101,1.80889 -0.46514,3.20433 -0.46514,4.13462c0,2.35156 0.51683,3.97956 1.57632,4.83233c1.03365,0.85276 2.86839,1.26622 5.47837,1.26622c1.21454,0 2.58413,-0.20673 4.13462,-0.62019c1.52464,-0.4393 2.63582,-0.80108 3.33353,-1.13702zM101.3756,57.00601c-2.32572,2.17067 -5.14243,3.25601 -8.42428,3.25601c-3.28185,0 -6.1244,-1.08533 -8.47596,-3.25601c-2.35156,-2.17067 -3.51442,-4.80649 -3.51442,-7.88162c0,-3.07512 1.18871,-5.73678 3.51442,-7.93329c2.35156,-2.19652 5.19412,-3.28185 8.47596,-3.28185c3.28185,0 6.09856,1.08533 8.42428,3.28185c2.35156,2.19651 3.51442,4.85817 3.51442,7.93329c0,3.07512 -1.16286,5.71094 -3.51442,7.88162z"></path></g></g></svg>
                    <span class="tooltiptext">пт-вс</span>
                </span>
                </span>
                <input type="number" placeholder="" name="price_weekends" class="_required"
                       value="{{!empty($object)?$object->price_weekends:''}}">
            </label>
        </div>
    </div>
    <div class="object-card_row">
        <p>Общая информация:</p>
        <div class="label-box" style="width: 100%">
            <div class="upload-photo">
                <div class='file-upload'>
                    <label for='input-file'>
                        Добавить фото
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                             width="24" height="24"
                             viewBox="0 0 172 172"
                             style=" fill:#000000;">
                            <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
                               stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
                               font-family="none" font-weight="none" font-size="none" text-anchor="none"
                               style="mix-blend-mode: normal">
                                <path d="M0,172v-172h172v172z" fill="none"></path>
                                <g fill="#448d2f">
                                    <path
                                            d="M75.25,14.33333c-5.85562,0 -10.75,4.89438 -10.75,10.75v46.58333h-31.63411l53.13411,53.13411l53.13411,-53.13411h-31.63411v-46.58333c0,-5.85562 -4.89438,-10.75 -10.75,-10.75zM78.83333,28.66667h14.33333v57.33333h11.36589l-18.53256,18.53256l-18.53255,-18.53256h11.36589zM14.33333,143.33333v14.33333h143.33333v-14.33333z"></path>
                                </g>
                            </g>
                        </svg>
                    </label>
                    <input id="input-file" class='input-file' type='file' multiple data-list_id="_gallery_list"/>
                </div>
                <div class="upload-img_box" id="_gallery_list">
                    @if(!empty($item))
                        @foreach($item->photos as $photo)
                            @include('front.cabinet.object.partials.imageBox',['item'=>$photo])
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="upload-photo">
                <p class="subtitle">Загрузить видео:</p>
                <input type="text" name="video_url"
                       value={{!empty($item) && !empty($item->youtube()->first())?$item->youtube()->first()->value:''}}>
            </div>
            <div class="upload-photo">
                <p class="subtitle">Описание объекта:</p>
                <textarea class="description-object"
                          name="description">{{!empty($item)?$item->description:''}}</textarea>
            </div>
        </div>
    </div>
</div>

{{---------------отели-----------------}}
<div class="object-card hide_by_type show_by_type_hotel">
    <div class="addHotel-number">
        <div id="object_list_room">
            @if(!empty($object) && !empty($object->rooms))
                @foreach($object->rooms as $room)
                    @include('front.cabinet.object.partials.hotelRoom')
                @endforeach
            @endif
        </div>
        <button class="addHotel-number_btn" id="object_add_room" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                 width="24" height="24"
                 viewBox="0 0 172 172"
                 style=" fill:#000000;">
                <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
                   stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
                   font-family="none" font-weight="none" font-size="none" text-anchor="none"
                   style="mix-blend-mode: normal">
                    <path d="M0,172v-172h172v172z" fill="none"></path>
                    <g fill="#448d2f">
                        <path
                                d="M86,14.33333c-39.5815,0 -71.66667,32.08517 -71.66667,71.66667c0,39.5815 32.08517,71.66667 71.66667,71.66667c39.5815,0 71.66667,-32.08517 71.66667,-71.66667c0,-39.5815 -32.08517,-71.66667 -71.66667,-71.66667zM121.83333,93.16667h-28.66667v28.66667h-14.33333v-28.66667h-28.66667v-14.33333h28.66667v-28.66667h14.33333v28.66667h28.66667z"></path>
                    </g>
                </g>
            </svg>
            Добавить номер
        </button>
        <hr>
    </div>
</div>
{{---------------отели-----------------}}

{{---------------база отдыха-----------------}}
<div class="object-card hide_by_type show_by_type_rest">
    <div class="addHotel-number">
        <div id="object_list_cottage">
            @if(!empty($object->objects))
                @foreach($object->objects as $restObject)
                    @include('front.cabinet.object.partials.restObject')
                @endforeach
            @endif
        </div>
        <button class="addHotel-number_btn" id="object_add_rest_cottage" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                 width="24" height="24"
                 viewBox="0 0 172 172"
                 style=" fill:#000000;">
                <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
                   stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0"
                   font-family="none" font-weight="none" font-size="none" text-anchor="none"
                   style="mix-blend-mode: normal">
                    <path d="M0,172v-172h172v172z" fill="none"></path>
                    <g fill="#448d2f">
                        <path
                                d="M86,14.33333c-39.5815,0 -71.66667,32.08517 -71.66667,71.66667c0,39.5815 32.08517,71.66667 71.66667,71.66667c39.5815,0 71.66667,-32.08517 71.66667,-71.66667c0,-39.5815 -32.08517,-71.66667 -71.66667,-71.66667zM121.83333,93.16667h-28.66667v28.66667h-14.33333v-28.66667h-28.66667v-14.33333h28.66667v-28.66667h14.33333v28.66667h28.66667z"></path>
                    </g>
                </g>
            </svg>
            Добавить объект
        </button>
        <hr>
    </div>
</div>
{{---------------база отдыха-----------------}}

