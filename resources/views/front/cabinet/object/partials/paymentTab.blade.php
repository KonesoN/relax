@php
    $hide_allocation = !empty($allocation) && $allocation == 'hide'?true:false;
@endphp
<div class="object-rate">
    @if(!$hide_allocation)
        <div class="object-rate_item">
            <div class="object-rate_item-box">
                <p class="form-radio">
                    <input type="radio" id="type_allocation_1" name="type_allocation_id"
                           class="price_calculated" data-name="Базовое размещение"
                           value="{{\App\Models\UserObject::TYPE_ALLOCATION_BASIC}}"
                           checked>
                    <label for="type_allocation_1">Базовое размещение</label>
                </p>
                <div class="custom-radio">
                    <div class="custom-radio_item">
                        <label><input type="radio" name="day_allocation[1]" value="7"
                                      class="price_calculated"
                                      @foreach(\App\Models\TypeAllocation::where(['allocation_id'=>\App\Models\UserObject::TYPE_ALLOCATION_BASIC,'days'=>7])->get() as $item)
                                      data-price_{{$item->type}}="{{$item->value}}"
                                      @endforeach
                                      checked><span>7 дней</span></label>
                    </div>
                    <div class="custom-radio_item">
                        <label><input type="radio" name="day_allocation[1]" value="30"
                                      @foreach(\App\Models\TypeAllocation::where(['allocation_id'=>\App\Models\UserObject::TYPE_ALLOCATION_BASIC,'days'=>30])->get() as $item)
                                      data-price_{{$item->type}}="{{$item->value}}"
                                      @endforeach
                                      class="price_calculated"
                            >
                            <span>30 дней</span></label>
                    </div>
                </div>
            </div>
            @foreach(\App\Models\UserObject::TYPES as $type=>$name)
                <div class="price hide_by_type show_by_type_{{$type}}">
                    @foreach(\App\Models\TypeAllocation::where(['allocation_id'=>\App\Models\UserObject::TYPE_ALLOCATION_BASIC,'type'=>$type])->get() as $item)
                        <span>{{$item->value}} руб/{{$item->days}}дней</span>
                    @endforeach
                </div>
            @endforeach
        </div>
    @endif
    @foreach(\App\Models\UserObjectAddition::ADDITIONS as $id=>$addition)
        <div class="object-rate_item">
            <div class="object-rate_item-box">
                @php
                    $disabled = false;
                    if(!empty($addition['group_ids']) && !empty($additions) && !$disabled){
                        foreach ($addition['group_ids'] as $_id){
                            if(!empty($additions[$_id])){
                                $disabled = true;
                            }
                        }
                    }
                @endphp
                <p class="form-radio form-check">
                    <input type="{{!empty($addition['group'])?'radio':'checkbox'}}" id="addition_{{$id}}"
                           class="price_calculated _addition"
                           data-name="{{$addition['name']}}"
                           name="addition[{{!empty($addition['group'])?$addition['group']:$id}}][check]"
                           value="{{$id}}"
                        {{!empty($additions[$id])?'checked disabled':''}}
                        {{$disabled?'disabled':''}}
                    >
                    <label
                        for="addition_{{$id}}">{{$addition['name']}}
                        @if(!empty($additions[$id]))
                            <br/>
                            <small>активировано
                                до {{Date('Y-m-d H:i',strtotime($additions[$id]->end_at))}}</small>
                        @endif
                    </label>
                </p>
                <div class="custom-radio">
                    <div class="custom-radio_item">
                        <label><input type="radio" value="7"
                                      class="price_calculated"
                                      @foreach(\App\Models\Addition::where(['addition_id'=>$id,'days'=>7])->get() as $item)
                                      data-price_{{$item->type}}="{{$item->value}}"
                                      @endforeach
                                      name="addition[{{$id}}][day]"
                                {{!empty($additions[$id])?'disabled':''}}
                                {{empty($additions) || empty($additions[$id])?'checked':''}}
                                {{!empty($additions[$id]) && $additions[$id]->days == 7?'checked':''}}
                            ><span>7 дней</span></label>
                    </div>
                    <div class="custom-radio_item">
                        <label><input type="radio" value="30"
                                      class="price_calculated"
                                      @foreach(\App\Models\Addition::where(['addition_id'=>$id,'days'=>30])->get() as $item)
                                      data-price_{{$item->type}}="{{$item->value}}"
                                      @endforeach
                                      {{!empty($additions[$id]) && $additions[$id]->days == 30?'checked':''}}
                                      name="addition[{{$id}}][day]"><span>30 дней</span></label>
                    </div>
                </div>
            </div>
            <ul>
                @foreach($addition['descriptions'] as $a)
                    <li>{{$a}}</li>
                @endforeach
            </ul>
            @foreach(\App\Models\UserObject::TYPES as $type=>$name)
                <div class="price hide_by_type show_by_type_{{$type}}">
                    @foreach(\App\Models\Addition::where(['addition_id'=>$id,'type'=>$type])->get() as $item)
                        <span>{{$item->value}} руб/{{$item->days}}дней</span>
                    @endforeach
                </div>
            @endforeach
        </div>
    @endforeach
    <span class="total_newObj-price">Всего к оплате: <span
            class="object_price">0</span> руб.</span>
</div>
