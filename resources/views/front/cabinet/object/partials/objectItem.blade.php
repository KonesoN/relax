@php
    $object = $item->object;
@endphp
<div class="object-block_list-item">
    @foreach(\App\Models\UserObjectAddition::ADDITIONS as $id=>$addition)
        @if($item->additions()->where(['addition_id'=>$id,'is_active'=>1])->first())
            <div class="object-block_info-label -label_prem">
                <div class="status">
                    <img src=" {{$addition['logo']}}">
                    {{$addition['name']}}
                </div>
                <span>
                    {{$item->additions()->where(['addition_id'=>$id,'is_active'=>1])->first()->LeftDays}}
                </span>
            </div>
        @endif
    @endforeach
    <div class="object-block_container">
        <div class="object-block_info">
            <a href="{{route('front.object.view',$item->id)}}" class="image">
                <img src="{{$item->MainPhoto?$item->MainPhoto->value:''}}">
            </a>
            <div style="max-width: 400px">
                <a href="{{route('front.object.view',$item->id)}}" style="color:#333333" class="object-block_info-title"
                >{{$item->title}}
                    @if($item->BookingNewRequest)
                        <span style="color: white; background: #333333; border-radius: 50%; padding: 3px; font-size: 14px; min-width: 18px; display: inline-flex; justify-content: center;"
                        >{{$item->BookingNewRequest}}</span>
                    @endif
                </a>
                <p class="object-block_info-subtitle">{{$item->address}}</p>
                @php
                    $rating = $item->reviews()->avg('rating');
                @endphp
                <div class="object-block_info-rating">
                    <div class="star-rating">
                        <input type="radio" id="5-stars" name="rating"
                               value="5" {{floor($rating) == 5?'checked':''}}/>
                        <label for="5-stars" class="star">&#9733;</label>
                        <input type="radio" id="4-stars" name="rating"
                               value="4" {{floor($rating) == 4?'checked':''}}/>
                        <label for="4-stars" class="star">&#9733;</label>
                        <input type="radio" id="3-stars" name="rating"
                               value="3" {{floor($rating) == 3?'checked':''}}/>
                        <label for="3-stars" class="star">&#9733;</label>
                        <input type="radio" id="2-stars" name="rating"
                               value="2" {{floor($rating) == 2?'checked':''}}/>
                        <label for="2-stars" class="star">&#9733;</label>
                        <input type="radio" id="1-star" name="rating"
                               value="1" {{floor($rating) == 1?'checked':''}}/>
                        <label for="1-star" class="star">&#9733;</label>
                    </div>
                    <p>({{$item->reviews()->count()}} отзыва)</p>
                </div>

                <ul class="object-block_info-price">
                    @if($item->type == 'cottage')
                        <li>Сутки будни <span>{{$object->price_day}}₽</span></li>
                        <li>Сутки выходные <span>{{$object->price_day_weekend}}₽</span></li>
                        <li>Полные выходные (ПТ-ВС) <span>{{$object->price_weekends}}₽</span></li>
                    @else
                        <li>
                            Стоимость: {!! \App\Models\UserObject::PRICE_TYPES[$item->type][$item->price_type_id]['price'] !!}</li>
                    @endif
                </ul>
            </div>
        </div>
        <div class="object-block_statistic">
            <ul>
                <li>
                    <div class="object-block_statistic-image"><img
                                src="{{asset('/img/admin/load.png')}}"></div>
                    {{$item->days}}
                </li>
                <li>
                    <div class="object-block_statistic-image"><img
                                src="{{asset('/img/front/eye.png')}}"></div>
                    {{$item->count_view??0}} <span>+ {{$item->count_view_today??0}}</span></li>
            </ul>
            <div class="menu-drop">
                <div class="menu-drop_burger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <ul class="menu-drop_list">
                    <li class="menu-drop_list-item"><a
                                href="{{route('front.cabinet.object.edit',$item->id)}}">Редактировать</a>
                    </li>
                    <li class="menu-drop_list-item"><a
                                href="{{route('front.cabinet.object.booking',$item->id)}}">Бронирование
                            ({{$item->BookingNewRequest}})</a>
                    </li>

                    @if($item->status_id == \App\Models\UserObject::STATUS_ACTIVE)
                        <li class="menu-drop_list-item"><a href="{{route('front.cabinet.object.addition',$item->id)}}">Увеличить
                                просмотры</a></li>
                        <li class="menu-drop_list-item"><a class="_question_alert"
                                                           data-message="Отменятся все оплаченные пакеты и услуги, Вы уверены?"
                                                           href="{{route('front.cabinet.object.action',$item->id)}}?action=inactive">Снять
                                с публикации</a></li>
                    @elseif($item->status_id == \App\Models\UserObject::STATUS_INACTIVE)
                        <li class="menu-drop_list-item"><a
                                    href="{{route('front.cabinet.object.payment',$item->id)}}">Возобновить показы</a>
                        </li>
                        <li class="menu-drop_list-item"><a class="_question_alert"
                                                           data-message="Переместить в архив, Вы уверены?"
                                                           href="{{route('front.cabinet.object.action',$item->id)}}?action=archive">Удалить</a>
                        </li>
                    @elseif($item->status_id == \App\Models\UserObject::STATUS_ARCHIVE)
                        <li class="menu-drop_list-item"><a class="_question_alert"
                                                           data-message="Восстановить, Вы уверены?"
                                                           href="{{route('front.cabinet.object.action',$item->id)}}?action=restore">Восстановить</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
