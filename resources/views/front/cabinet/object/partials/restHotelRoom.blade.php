@php
    $hash_room = md5(strtotime('now')+rand(99999,999999));
    $hash = !empty($hash)?$hash:$room->id;
@endphp
<div>
    <div class="added-objects_list rest_room_min_{{$hash_room}}" style="display: {{!empty($room)?'block':'none'}}">
        <div class="added-objects_list-item object-block_list-item ">
            <div class="object-block_info">
                <div class="image">
                    <img src="{{!empty($room)?$room->MainPhoto->value:''}}"
                         class="rest_room_image_src_{{$hash_room}}">
                </div>
                <div>
                    <p class="object-block_info-title rest_room_title_{{$hash_room}}">{{!empty($room)?$room->title:''}}</p>
                    <ul class="advantages-list">
                        <li>Гостей: <span
                                class="rest_room_count_guest_{{$hash_room}}">{{!empty($room)?$room->count_guest:''}}</span>
                        </li>
                        <li>Маленьких кроватей:<span
                                class="rest_room_count_small_bedrooms_{{$hash_room}}">{{!empty($room)?$room->count_small_bedrooms:0}}</span>
                        </li>
                        <li>Больших кроватей:<span
                                class="rest_room_count_big_bedrooms_{{$hash_room}}">{{!empty($room)?$room->count_big_bedrooms:0}}</span>
                        </li>
                    </ul>
                    <ul class="price">
                        <li>Сутки будни <span><span
                                    class="rest_room_price_day_{{$hash_room}}">{{!empty($room)?$room->price_day:''}}</span>₽</span>
                        </li>
                        <li>Сутки выходные <span><span
                                    class="rest_room_price_day_weekend_{{$hash_room}}">{{!empty($room)?$room->price_day_weekend:''}}</span>₽</span>
                        </li>
                        <li>Полные выходные (ПТ-ВС) <span><span
                                    class="rest_room_price_weekends_{{$hash_room}}">{{!empty($room)?$room->price_weekends:''}}</span>₽</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="object-block_control">
                <button type="button" class="edit-object _rest_room_view_form" data-hash="{{$hash_room}}">Редактировать
                </button>
                <button type="button" class="delete-object _delete_parent" data-delete="parent4">Удалить</button>
            </div>
        </div>
    </div>
    <div class="addHotel-number_form rest_room_form_{{$hash_room}}"
         data-action="{{route('front.cabinet.object.rest.room.form')}}"
         style="display: {{!empty($room)?'none':'block'}}">
        <input type="hidden" value="{{!empty($room)?$room->id:''}}" name="room_ids[]"
               class="rest_room_id_{{$hash_room}}"/>
        <input type="hidden" value="{{!empty($room)?$room->id:''}}" name="room_id" class="rest_room_id_{{$hash_room}}"/>
        <input type="hidden" value="{{$hash_room}}" name="hash_room"/>
        <input type="hidden" value="{{$hash}}" name="hash_object"/>
        <div class="object-card_row">
            <p>Параметры номера</p>
            <div class="label-box">
                <label>
                    <span>Название</span>
                    {{--                    <input type="text" required placeholder="" name="room_title"--}}
                    {{--                           value="{{!empty($room)?$room->title:''}}">--}}
                    <textarea maxlength="190" required placeholder="" class="_required"
                              name="room[title]">{{!empty($room)?$room->title:''}}</textarea>
                </label>
                <label>
                    <span>Вместимость гостей</span>
                    <input type="number" required placeholder="" name="room[count_guest]" class="_required"
                           value="{{!empty($room)?$room->count_guest:0}}">
                </label>
                <label>
                    <span>Кол-во односпальных кроватей</span>
                    <input type="number" required placeholder="" name="room[count_small_bedrooms]" class="_required"
                           value="{{!empty($room)?$room->count_small_bedrooms:0}}">
                </label>
                <label>
                    <span>Кол-во двуспальных кроватей</span>
                    <input type="number" required placeholder="" name="room[count_big_bedrooms]" class="_required"
                           value="{{!empty($room)?$room->count_big_bedrooms:0}}">
                </label>
            </div>
        </div>
        <div class="object-card_row">
            <p>Стоимость размещения</p>
            <div class="label-box">
                <label>
                    <span>Сутки будни:</span>
                    <input type="number" required placeholder="" name="room[price_day]" class="_required"
                           value="{{!empty($room)?$room->price_day:''}}">
                </label>
                <label>
                    <span>Сутки выходные:</span>
                    <input type="number" required placeholder="" name="room[price_day_weekend]" class="_required"
                           value="{{!empty($room)?$room->price_day_weekend:''}}">
                </label>
                <label>
                    <span>Полные выходные:</span>
                    <input type="number" required placeholder="" name="room[price_weekends]" class="_required"
                           value="{{!empty($room)?$room->price_weekends:''}}">
                </label>
            </div>
        </div>
        <div class="object-card_row">
            <p>Общая информация:</p>
            <div class="label-box">
                <div class="upload-photo">
                    <div class='file-upload'>
                        <label for='input-file_{{$hash_room}}'>
                            Добавить фото
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                 width="24" height="24"
                                 viewBox="0 0 172 172"
                                 style=" fill:#000000;">
                                <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt"
                                   stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray=""
                                   stroke-dashoffset="0"
                                   font-family="none" font-weight="none" font-size="none" text-anchor="none"
                                   style="mix-blend-mode: normal">
                                    <path d="M0,172v-172h172v172z" fill="none"></path>
                                    <g fill="#448d2f">
                                        <path
                                            d="M75.25,14.33333c-5.85562,0 -10.75,4.89438 -10.75,10.75v46.58333h-31.63411l53.13411,53.13411l53.13411,-53.13411h-31.63411v-46.58333c0,-5.85562 -4.89438,-10.75 -10.75,-10.75zM78.83333,28.66667h14.33333v57.33333h11.36589l-18.53256,18.53256l-18.53255,-18.53256h11.36589zM14.33333,143.33333v14.33333h143.33333v-14.33333z"></path>
                                    </g>
                                </g>
                            </svg>
                        </label>
                        <input id="input-file_{{$hash_room}}" class='input-file' multiple type='file'
                               data-list_id="_gallery_list_{{$hash_room}}"
                               data-upload_url="{{route('front.cabinet.object.rest.room.uploadPhoto')}}"/>
                    </div>
                    <div class="upload-img_box" id="_gallery_list_{{$hash_room}}">
                        @if(!empty($room))
                            @foreach($room->photos as $photo)
                                @include('front.cabinet.object.partials.imageBox',['item'=>$photo,'name'=>'rest_room_gallery'])
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="upload-photo">
                    <p class="subtitle">Загрузить видео:</p>
                    <input type="text" name="room[room_video_url]"
                           value={{!empty($room) && !empty($room->youtube()->first())?$room->youtube()->first()->value:''}}>

                </div>
                <div class="upload-photo">
                    <p class="subtitle">Описание объекта:</p>
                    <textarea class="description-object" required class="_required"
                              name="room[description]">{{!empty($room)?$room->description:''}}</textarea>
                </div>
            </div>
        </div>
        <div style="display: flex">
            <button type="button" class="primary-btn _delete_parent" data-delete="parent3">Удалить</button>
            <button style="margin-left: 10px;" type="button" class="primary-btn save_rest_room"
                    data-hash="{{$hash_room}}">
                Сохранить
            </button>
        </div>
    </div>
</div>

