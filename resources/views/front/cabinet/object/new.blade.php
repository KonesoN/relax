@extends('front.partials.app')

@section('content')

    <div class="container">
        <div class="cabinet-wrapper">
            <div class="cabinet-sidebar newObject-sidebar">
                <div class="newObject-nav">
                    <div class="select_tab select_tab-active" data-tab_id="1" data-disabled="true">Категории</div>
                    <div class="select_tab" data-tab_id="2">Параметры</div>
                    <div class="select_tab" data-tab_id="3">Карточка объекта</div>
                    <div class="select_tab" data-tab_id="4">Тип размещения</div>
                    <div class="select_tab" data-tab_id="5">Баланс</div>
                </div>
            </div>
            <div class="cabinet-content">
                <form class="add_object" id="newObject-form" method="post">
                    <div id="tab_1" class="tabs tabs-active" style="display: block">
                        <div class="newObject-row">
                            <p class="title">Выберите категорию объекта для размещения</p>
                            <div class="newObject-row_input">
                                <p class="form-radio">
                                    <input type="radio" id="object-cat_cottage" name="type_object"
                                           value="{{\App\Models\UserObject::TYPE_COTTAGE}}"
                                           checked>
                                    <label for="object-cat_cottage">Коттедж</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="object-cat_hotel" name="type_object"
                                           value="{{\App\Models\UserObject::TYPE_HOTEL}}">
                                    <label for="object-cat_hotel">Отель / гостиница</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="object-cat_rest" name="type_object"
                                           value="{{\App\Models\UserObject::TYPE_REST}}">
                                    <label for="object-cat_rest">База отдыха <small>Выбирайте эту категорию, если на
                                            локации расположено несколько коттеджей или отелей</small></label>
                                </p>
                            </div>
                            <button type="button" class="subprimary-btn" data-tab_id="2">Продолжить</button>
                        </div>
                    </div>
                    <div id="tab_2" class="tabs">
                        @include('front.partials.objectFilter')
                        <button type="button" class="subprimary-btn" data-tab_id="3">Применить</button>
                    </div>
                    <div id="tab_3" class="tabs">
                        @include('front.cabinet.object.partials.formCardTab')
                        <button type="button" class="subprimary-btn" data-tab_id="4" data-check_gallery="true">
                            Применить
                        </button>
                    </div>
                    <div id="tab_4" class="tabs">
                        @include('front.cabinet.object.partials.paymentTab')
                        <button type="button" class="subprimary-btn" data-tab_id="5">Перейти к оплате</button>
                    </div>
                    <div id="tab_5" class="tabs">
                        <div class="object-balance">
                            @include('front.cabinet.object.partials.additionTab')
                            <button type="submit" class="subprimary-btn" id="order_submit" data-submit="true">Оплатить
                            </button>
                        </div>
                    </div>
                    <input value="{{md5(strtotime('now').Auth::id())}}" id="hash_object" name="hash_object"
                           type="hidden"/>
                </form>
            </div>
        </div>
    </div>
    <input value="{{\App\Models\Transaction::METHOD_BALANCE}}" id="method_balance" type="hidden"/>
    <input value="{{Auth::user()->balance}}" id="user_balance" type="hidden"/>
@endsection
