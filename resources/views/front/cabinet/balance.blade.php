@extends('front.partials.app')

@section('content')

    <div class="container">
        <div class="cabinet-wrapper">
            <div class="cabinet-sidebar">
                @include('front.partials.sidebar')
            </div>
            <div class="cabinet-content">

                <div class="balance-block">
                    <p class="balance-block_title">Баланс {{Auth::user()->balance}}₽</p>

                    <div class="tabs">

                        <!-- Tab 1 & Content -->
                        <input type="radio" name="tab" id="replenishment"
                               role="tab" {{$tab == 'replenish'?'checked':''}}>
                        <label for="replenishment" id="replenishment-label">Пополнение</label>
                        <section aria-labelledby="replenishment-label">
                            <form method="post">
                                <div class="balance-block_replenishment">
                                    <label>
                                        <input placeholder="Введите сумму" name="amount" required> ₽
                                    </label>
                                </div>
                                <p class="balance-block_title">Способ пополнения</p>
                                <div class="balance-block_replenishment-type">
                                    @foreach(\App\Models\Transaction::METHODS as $id=>$m)
                                        @if($m['view'][\App\Models\Transaction::TYPE_USER_REFILL])
                                            <p class="form-radio">
                                                <input type="radio" id="method_{{$id}}" name="method_id" value="{{$id}}"
                                                    {{$loop->index == 1?'checked':''}}>
                                                <label for="method_{{$id}}"><img
                                                        src="{{asset($m['logo'])}}">{{$m['name']}}</label>
                                            </p>
                                        @endif
                                    @endforeach
                                </div>
                                <button class="primary-btn">Пополнить</button>
                            </form>
                        </section>

                        <!-- Tab 2 & Content -->
                        <input type="radio" name="tab" id="history" role="tab" {{$tab == 'history'?'checked':''}}>
                        <label for="history" id="history-label">История операций</label>
                        <section aria-labelledby="history-label">
                            <div class="cabinet-block_list">
                                <div class="cabinet-block_list-header">
                                    <div class="cabinet-block_list-row">
                                        <ul>
                                            <li>Дата</li>
                                            <li>Тип</li>
                                            <li>Сумма</li>
                                            <li>Способ оплаты</li>
                                            <li>Статус</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="cabinet-block_list-body">
                                    @foreach($pagination->items() as $item)
                                        <div class="cabinet-block_list-row">
                                            <ul>
                                                <li>
                                                    {{Date('Y-m-d H:i',strtotime($item->created_at))}}
                                                </li>
                                                <li>{{\App\Models\Transaction::TYPES[$item->type_id]}}</li>
                                                <li>{{$item->amount}} ₽</li>
                                                <li>{{\App\Models\Transaction::METHODS[$item->method_id]['name']}}</li>
                                                <li>{{\App\Models\Transaction::STATUSES[$item->status_id]}}</li>
                                            </ul>
                                        </div>
                                    @endforeach
                                </div>
                                @include('front.partials.pagination')
                            </div>
                        </section>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
