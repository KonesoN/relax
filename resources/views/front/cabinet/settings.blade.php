@extends('front.partials.app')

@section('content')

    <div class="container">
        <div class="cabinet-wrapper">
            <div class="cabinet-sidebar">
                @include('front.partials.sidebar')
            </div>
            <div class="cabinet-content">

                <div class="settings">
                    <form class="cabinet-settings" method="post">
                        <label>
                            <div class="info">
                                <span>Имя</span>
                                <button>
                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         width="13" height="15"
                                         viewBox="0 0 172 172"
                                         style=" fill:#000000;">
                                        <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1"
                                           stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"
                                           stroke-dasharray="" stroke-dashoffset="0" font-family="none"
                                           font-weight="none"
                                           font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                            <path d="M0,172v-172h172v172z" fill="none"></path>
                                            <g fill="#448e2f">
                                                <path
                                                    d="M28.66667,28.66667c-7.90483,0 -14.33333,6.4285 -14.33333,14.33333v86c0,7.90483 6.4285,14.33333 14.33333,14.33333h76.8877l-14.33333,-14.33333h-62.55436v-86h114.66667l-0.014,62.54036l14.34733,14.33333v-76.87369c0,-7.90483 -6.4285,-14.33333 -14.33333,-14.33333zM93.16667,93.16667v17.51074l45.08561,45.08561l17.51074,-17.52474l-45.08561,-45.07161zM161.96386,144.43913l-17.52474,17.52474l8.76237,8.74837c1.71283,1.71284 4.48118,1.71284 6.18685,0l11.32389,-11.32389c1.71283,-1.71283 1.71284,-4.48118 0,-6.18685z"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    Редактировать
                                </button>
                            </div>
                            <input type="text" required readonly value="{{Auth::user()->first_name}}" name="first_name">
                        </label>
                        <label>
                            <div class="info">
                                <span>Фамилия</span>
                                <button>
                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         width="13" height="15"
                                         viewBox="0 0 172 172"
                                         style=" fill:#000000;">
                                        <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1"
                                           stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"
                                           stroke-dasharray="" stroke-dashoffset="0" font-family="none"
                                           font-weight="none"
                                           font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                            <path d="M0,172v-172h172v172z" fill="none"></path>
                                            <g fill="#448e2f">
                                                <path
                                                    d="M28.66667,28.66667c-7.90483,0 -14.33333,6.4285 -14.33333,14.33333v86c0,7.90483 6.4285,14.33333 14.33333,14.33333h76.8877l-14.33333,-14.33333h-62.55436v-86h114.66667l-0.014,62.54036l14.34733,14.33333v-76.87369c0,-7.90483 -6.4285,-14.33333 -14.33333,-14.33333zM93.16667,93.16667v17.51074l45.08561,45.08561l17.51074,-17.52474l-45.08561,-45.07161zM161.96386,144.43913l-17.52474,17.52474l8.76237,8.74837c1.71283,1.71284 4.48118,1.71284 6.18685,0l11.32389,-11.32389c1.71283,-1.71283 1.71284,-4.48118 0,-6.18685z"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    Редактировать
                                </button>
                            </div>
                            <input type="text" readonly required value="{{Auth::user()->last_name}}" name="last_name">
                        </label>
                        <label>
                            <div class="info">
                                <span>Контактный телефон</span>
                                <button>
                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         width="13" height="15"
                                         viewBox="0 0 172 172"
                                         style=" fill:#000000;">
                                        <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1"
                                           stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"
                                           stroke-dasharray="" stroke-dashoffset="0" font-family="none"
                                           font-weight="none"
                                           font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                            <path d="M0,172v-172h172v172z" fill="none"></path>
                                            <g fill="#448e2f">
                                                <path
                                                    d="M28.66667,28.66667c-7.90483,0 -14.33333,6.4285 -14.33333,14.33333v86c0,7.90483 6.4285,14.33333 14.33333,14.33333h76.8877l-14.33333,-14.33333h-62.55436v-86h114.66667l-0.014,62.54036l14.34733,14.33333v-76.87369c0,-7.90483 -6.4285,-14.33333 -14.33333,-14.33333zM93.16667,93.16667v17.51074l45.08561,45.08561l17.51074,-17.52474l-45.08561,-45.07161zM161.96386,144.43913l-17.52474,17.52474l8.76237,8.74837c1.71283,1.71284 4.48118,1.71284 6.18685,0l11.32389,-11.32389c1.71283,-1.71283 1.71284,-4.48118 0,-6.18685z"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    Редактировать
                                </button>
                            </div>
                            <input type="text" readonly value="{{Auth::user()->phone}}" name="phone">
                        </label>
                        <label>
                            <div class="info">
                                <span>Адрес электронной почты </span>
                                <button>
                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         width="13" height="15"
                                         viewBox="0 0 172 172"
                                         style=" fill:#000000;">
                                        <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1"
                                           stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10"
                                           stroke-dasharray="" stroke-dashoffset="0" font-family="none"
                                           font-weight="none"
                                           font-size="none" text-anchor="none" style="mix-blend-mode: normal">
                                            <path d="M0,172v-172h172v172z" fill="none"></path>
                                            <g fill="#448e2f">
                                                <path
                                                    d="M28.66667,28.66667c-7.90483,0 -14.33333,6.4285 -14.33333,14.33333v86c0,7.90483 6.4285,14.33333 14.33333,14.33333h76.8877l-14.33333,-14.33333h-62.55436v-86h114.66667l-0.014,62.54036l14.34733,14.33333v-76.87369c0,-7.90483 -6.4285,-14.33333 -14.33333,-14.33333zM93.16667,93.16667v17.51074l45.08561,45.08561l17.51074,-17.52474l-45.08561,-45.07161zM161.96386,144.43913l-17.52474,17.52474l8.76237,8.74837c1.71283,1.71284 4.48118,1.71284 6.18685,0l11.32389,-11.32389c1.71283,-1.71283 1.71284,-4.48118 0,-6.18685z"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    Редактировать
                                </button>
                            </div>
                            <input type="email" readonly value="{{Auth::user()->email}}" name="email">
                        </label>
                        <div class="cabinet-settings_btns">
                            <button class="primary-btn">
                                Сохранить
                            </button>
                            <button class="primary-btn cabinet-settings-cancel" type="button">
                                Отменить
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
