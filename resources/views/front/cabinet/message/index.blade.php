@extends('front.partials.app')

@section('content')

    <div class="container">
        <div class="cabinet-wrapper">
            <div class="cabinet-sidebar">
                @include('front.partials.sidebar')
            </div>
            <div class="cabinet-content">
                <div class="messages-list">
                    @if(!empty($support))
                        @php
                            $style = '';
                            if($support->user_new_messages){
                                $style = 'background-color: #e8f5ff;';
                            }
                        @endphp
                        <a href="{{route('front.cabinet.support.chat')}}" class="messages-list_item" style="{{$style}}">
                            <div class="info">
                                <div class="info-title">
                                    @if($support->user_new_messages)
                                        <p>Новых сообщений: {{$support->user_new_messages}}</p>
                                    @endif
                                    <p>
                                        Саппорт
                                    </p>
                                </div>
                                @php
                                    $last_message = $support->messages()->orderBy('id','desc')->first();
                                @endphp
                                <div>
                                    <p class="info-messages">{{$last_message?$last_message->message:'Сообщений нет'}}</p>
                                </div>
                            </div>
                            @if($last_message)
                                <span class="date">{{Date('Y-m-d H:i',strtotime($last_message->created_at))}}</span>
                            @endif
                        </a>
                    @endif
                    @foreach($pagination->items() as $item)
                        @php
                            $style = '';
                            if(Auth::user()->type_id == \App\Models\User::TYPE_USER && $item->user_new_messages){
                                $style = 'background-color: #e8f5ff;';
                            }
                            elseif(Auth::user()->type_id == \App\Models\User::TYPE_OWNER && $item->object_new_messages)
                            {
                                $style = 'background-color: #e8f5ff;';
                            }
                        @endphp
                        <div class="messages-list_item" style="{{$style}}">
                            <a href="{{route('front.cabinet.message.chat',$item->id)}}">
                                <img src="{{$item->object->MainPhoto->value}}">
                            </a>
                            <div class="info">
                                <div class="info-title">
                                    @if(Auth::user()->type_id == \App\Models\User::TYPE_USER && $item->user_new_messages)
                                        <p>Новых сообщений: {{$item->user_new_messages}}</p>
                                    @elseif(Auth::user()->type_id == \App\Models\User::TYPE_OWNER && $item->object_new_messages)
                                        <p>Новых сообщений: {{$item->object_new_messages}}</p>
                                    @endif
                                    <p>{{$item->user->first_name}}</p>
                                    @if($item->parent_type == \App\Models\ObjectDialog::TYPE_USER_OBJECT)
                                        <p>
                                            <a href="{{route('front.object.view',$item->object->id)}}">{{$item->object->title}}</a>
                                        </p>
                                    @else
                                        @if($item->object->type == \App\Models\RestObject::TYPE_COTTAGE)
                                            <p>
                                                <a href="{{route('front.object.view.cottage',$item->object->id)}}">{{$item->object->title}}</a>
                                            </p>
                                        @else
                                            <p>
                                                <a href="{{route('front.object.view.hotel',$item->object->id)}}">{{$item->object->title}}</a>
                                            </p>
                                        @endif
                                    @endif
                                </div>
                                @php
                                    $last_message = $item->messages()->orderBy('id','desc')->first();
                                @endphp
                                <a href="{{route('front.cabinet.message.chat',$item->id)}}">
                                    <p class="info-messages">{{$last_message->message}}</p>
                                </a>
                            </div>
                            <span class="date">{{Date('Y-m-d H:i',strtotime($last_message->created_at))}}</span>
                        </div>
                    @endforeach
                </div>
                @include('front.partials.pagination')
            </div>
        </div>
    </div>

@endsection
