@php
    $date = '';
    $last_item = null;
    $_monthsList = [
    "1"=>"Январь","2"=>"Февраль","3"=>"Март",
    "4"=>"Апрель","5"=>"Май", "6"=>"Июнь",
    "7"=>"Июль","8"=>"Август","9"=>"Сентябрь",
    "10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь"];
@endphp
@foreach($pagination->items() as $item)
    @php
        $day = Date('d',strtotime($item->created_at));
        $month = $_monthsList[Date('n',strtotime($item->created_at))];
        $year = Date('Y',strtotime($item->created_at));
        $new_date = "$day $month $year";
        $last_item = $item;
    @endphp
    @if($new_date !== $date)
        @php
            $date = $new_date;
        @endphp
        <span class="date-message">{{$date}}</span>
    @endif
    @if((Auth::user()->type_id == \App\Models\User::TYPE_USER && $item->is_user) || Auth::user()->type_id == \App\Models\User::TYPE_OWNER && !$item->is_user)
        <div class="me_message">
            <div class="message-content">
                @if($item->is_read)
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" class="check" width="24" height="24"
                             viewBox="0 0 24 24">
                            <path d="M9 21.035l-9-8.638 2.791-2.87 6.156 5.874 12.21-12.436 2.843 2.817z"/>
                        </svg>
                    </div>
                @else
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M9 21.035l-9-8.638 2.791-2.87 6.156 5.874 12.21-12.436 2.843 2.817z"/>
                        </svg>
                    </div>
                @endif
                <span>{{Date('H:i',strtotime($item->created_at))}}</span>
                @if($item->message)
                    <p>{{$item->message}}</p>
                @endif
            </div>
            @if($item->attachment)
                <div class="pin_img">
                    <img src="{{$item->attachment->path_file}}">
                </div>
            @endif
        </div>
    @else
        <div class="interlocutor_message">
            <div class="message-content">
                @if($item->message)
                    <p>{{$item->message}}</p>
                @endif
                <span>{{Date('H:i',strtotime($item->created_at))}}</span>
            </div>
            @if($item->attachment)
                <div class="pin_img">
                    <img src="{{$item->attachment->path_file}}">
                </div>
            @endif
        </div>
    @endif
@endforeach
