@extends('front.partials.app')

@section('content')
    <div class="container">
        <div class="cabinet-wrapper">
            <div class="cabinet-sidebar">
                @include('front.partials.sidebar')
            </div>

            <div class="cabinet-content">
                <div class="message-container">
                    <div class="message-header">
                        <div class="message-header_info">
                            <img src="{{$item->object->MainPhoto->value}}">
                            <div class="message-header_info-name">
                                @if(Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                    <p>{{$item->object->title}} </p>
                                @else
                                    <p>{{$item->user->first_name}}</p>
                                @endif
                            </div>
                        </div>
                        <a href="{{route('front.cabinet.message.index')}}"><img src="https://img.icons8.com/external-kiranshastry-lineal-color-kiranshastry/24/000000/external-close-banking-and-finance-kiranshastry-lineal-color-kiranshastry.png"/></a>
                        {{--                <div class="toggle-menu">--}}
                        {{--                    <label for="toggle-btn_message">--}}
                        {{--                        <span></span>--}}
                        {{--                        <span></span>--}}
                        {{--                        <span></span>--}}
                        {{--                    </label>--}}
                        {{--                    <input id="toggle-btn_message" class="toggle-btn" type="checkbox">--}}
                        {{--                    <ul class="toggle-items">--}}
                        {{--                        <li class="toggle-item-1"><a href="#">Перейти в профиль</a></li>--}}
                        {{--                        <li class="toggle-item-2"><a href="#">Заблокироватье</a></li>--}}
                        {{--                        <li class="toggle-item-3"><a href="#">Удалить чат</a></li>--}}
                        {{--                    </ul>--}}
                        {{--                </div>--}}
                    </div>
                    <div class="message-body">
                        <div class="message-dialog_box" id="dialog_body">
                            @include('front.cabinet.message.partials.dialogBody')
                        </div>
                    </div>
                    <div class="message-footer">
                        <form>
                            <label>
                                <img src="{{asset('/img/front/photo.png')}}" id="select_file">
                                <input placeholder="Написать сообщение ..." id="message_text">
                            </label>
                            <button type="button" class="primary-btn" id="message_send">Отправить сообщение</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="file" id="message_file"
           style="position: absolute;left: -99999px;top: -99999px;opacity: 0"/>
    <input id="last_message_id" value="{{!empty($last_item)?$last_item->id:0}}" type="hidden"/>
    <input id="last_message_update" value="{{!empty($last_item)?strtotime($last_item->updated_at):0}}"
           type="hidden"/>
    <input type="hidden" id="dialog_id" value="{{$item->id}}"/>
@endsection
