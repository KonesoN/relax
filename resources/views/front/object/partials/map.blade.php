<input type="hidden" id="_location_item" data-lat="{{$item->lat}}" data-lng="{{$item->lng}}"
       data-title="{{$item->title}}"/>
<div class="submain-title">
    <p>{{$item->address}}</p>
    <a href="#" id="toggle_object_map"><img src="{{asset('/img/front/map-marker.png')}}"> Показать на карте</a href="#">
</div>
<div id="object_map" style="width: 100%;height: 400px;display: none;margin-top: 10px;"></div>
