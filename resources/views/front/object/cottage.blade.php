@extends('front.partials.app')

@section('content')
    @php
        $object = $item->object;
    @endphp
    @foreach($item->BusyDates as $bd)
        <input type="hidden" class="_disable_days" value="{{$bd}}"/>
    @endforeach
    <section class="rest-template cottage-template">
        <div class="container">
            <div class="main-title">
                <div>
                    <div>
                        <h2 class="title">{{$item->title}}</h2>
                        <p>{{$item->direction->name}}</p>
                        <p>Телефон: {{$item->phone}}</p>
                    </div>
                    @php
                        $rating = $item->reviews()->avg('rating');
                    @endphp
                    <div class="rating">
                        <div class="star-rating">
                            <input type="radio" id="5-stars" name="rating"
                                   value="5" {{floor($rating) == 5?'checked':''}}/>
                            <label for="5-stars" class="star">&#9733;</label>
                            <input type="radio" id="4-stars" name="rating"
                                   value="4" {{floor($rating) == 4?'checked':''}}/>
                            <label for="4-stars" class="star">&#9733;</label>
                            <input type="radio" id="3-stars" name="rating"
                                   value="3" {{floor($rating) == 3?'checked':''}}/>
                            <label for="3-stars" class="star">&#9733;</label>
                            <input type="radio" id="2-stars" name="rating"
                                   value="2" {{floor($rating) == 2?'checked':''}}/>
                            <label for="2-stars" class="star">&#9733;</label>
                            <input type="radio" id="1-star" name="rating"
                                   value="1" {{floor($rating) == 1?'checked':''}}/>
                            <label for="1-star" class="star">&#9733;</label>
                        </div>
                        <p>({{$item->reviews()->count()}} отзыва)</p>
                    </div>
                    @include('front.partials.favoriteButton')
                </div>
                @if($rating>0)
                    <div class="estimate">
                        {{$rating * 2}}
                    </div>
                @endif
            </div>
            @include('front.object.partials.map')
            <div class="info">
                <div class="info-row">
                    <div class="info-slider">
                        <div class="rec-label">
                            @if($item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::SERVICE_SELECTION,'is_active'=>1])->first())
                                <span class="rec-label_serv-sel">
                                                    <img src="{{asset('/img/front/vip.png')}}">
                                                    Выбор <br>сервиса
                            </span>
                            @endif
                            @if($item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::PREMIUM,'is_active'=>1])->first())
                                <span class="rec-label_prem">
                                            <img src="{{asset('/img/front/prem_icon.png')}}">PREMIUM
                                        </span>
                            @endif
                        </div>
                        <div class="product-imgs">
                            <div class="img-display">
                                <div class="img-showcase">
                                    @foreach($item->photos as $photo)
                                        <img src="{{$photo->value}}" alt="shoe image">
                                    @endforeach
                                </div>
                            </div>
                            <div class="img-select">
                                @foreach($item->photos as $photo)
                                    <div class="img-item">
                                        <a href="#" data-id="{{$loop->index + 1}}">
                                            <img src="{{$photo->value}}" alt="shoe image">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="info-main">
                        @include('front.object.partials.filters')
                        <div class="buttons-box">
                            @if(Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                <a href="#" data-modal_id="open-modal_addReview" class="primary-btn modal_view">Оставить
                                    отзыв</a>
                            @endif
                            @if(!Auth::check() || Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                <a href="#"
                                   data-modal_id="{{Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER?'open-modal_newMessage':'open-modal_userSmsNoRegister'}} "
                                   class="primary-btn modal_view">Написать сообщение</a>
                            @endif
                        </div>

                        @if($item->activeReviews->count())
                            <div class="slider-testimonials">
                                <ul id="slides">
                                    @foreach($item->activeReviews as $review)
                                        <li class="slide {{$loop->index == 0?'showing':''}}showing">
                                            <p class="name"><b>{{$review->user->first_name}}</b> <img
                                                        src="{{asset('/img/front/flag-ru.jpg')}}"> Россия
                                            </p>
                                            <p>{{$review->review}}</p>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="buttons">
                                    <button class="controls" id="previous"><img
                                                src="{{asset('/img/front/arr_slider.png')}}"></button>

                                    <button class="controls" id="pause">&#10074;&#10074;</button>

                                    <button class="controls" id="next"><img
                                                src="{{asset('/img/front/arr_slider.png')}}">
                                    </button>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="info-row">
                    <div class="rules-price">
                        <ul class="description">
                            <li><p>Площадь:</p><span>{{$object->living_space}} м&sup2;</span></li>
                            <li><p>Вместимость гостей:</p><span>{{$object->count_guest}}</span></li>
                            <li><p>Кол-во спален:</p><span>{{$object->count_bedrooms}}</span></li>
                            <li><p>Кол-во маленьких кроватей:</p><span>{{$object->count_small_bedrooms}}</span></li>
                            <li><p>Кол-во больших кроватей:</p><span>{{$object->count_big_bedrooms}}</span></li>
                        </ul>
                        <ul class="rules">
                            <li>Правила размещения:</li>
                            <li><p>Время заезда: </p> <span>{{$item->arrival_at}}</span></li>
                            <li><p>Время выезда: </p> <span>{{$item->departure_at}}</span></li>
                            <li><p>Минимальное количество суток: </p>&nbsp;<span>{{$item->minimum_days}}</span></li>
                            <li><p>Страховой депозит: </p> <span>{{$item->insurance_deposit}} руб.</span></li>
                            <li><p>Можно с животными: </p> <span>{{$item->is_pets?'Да':'Нет'}}</span></li>
                        </ul>
                        <ul class="price">
                            <li>Сутки будни <span>{{$object->price_day}}₽</span></li>
                            <li>Сутки выходные <span>{{$object->price_day_weekend}}₽</span></li>
                            <li>Полные выходные (ПТ-ВС) <span>{{$object->price_weekends}}₽</span></li>
                            <li><a href="#" data-modal_id="open-modal_cottageReservation"
                                   class="primary-btn modal_view">Забронировать</a></li>
                        </ul>
                    </div>
                </div>

                <div class="info-row">
                    <div class="text">
                        <p class="title">Описание</p>
                        <p style="white-space: pre-line">
                            {{$item->description}}
                        </p>
                    </div>
                    <div class="calendar info-calendar">
                        <div class="datepicker-here"></div>
                    </div>
                </div>
                @if(!empty($item->youtube()->first()->value) && $item->youtube()->first()->extension == 'iframe')
                    <div class="info-row">
                        <div class="video">
                            <div class="hs-responsive-embed-youtube">
                                <iframe width="560" height="315" src="{{$item->youtube()->first()->value}}"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            {{--            @if(count($similar))--}}
            {{--                <div class="similar-list-ad_block">--}}
            {{--                    <p class="title">Похожие варианты жилья:</p>--}}
            {{--                    <div class="list-ad_block">--}}
            {{--                        @foreach($similar as $_s)--}}
            {{--                            @php--}}
            {{--                                $_s_object = $_s->object;--}}
            {{--                            @endphp--}}
            {{--                            <div class="list-ad_block-item">--}}

            {{--                                <div class="rec-label">--}}
            {{--                                    @if($_s->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::SERVICE_SELECTION,'is_active'=>1])->first())--}}
            {{--                                        <span class="rec-label_serv-sel">--}}
            {{--                                                    <img src="{{asset('/img/front/vip.png')}}">--}}
            {{--                                                    Выбор <br>сервиса--}}
            {{--                                                </span>--}}
            {{--                                    @endif--}}
            {{--                                    @if($_s->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::PREMIUM,'is_active'=>1])->first())--}}
            {{--                                        <span class="rec-label_prem">--}}
            {{--                                            <img src="{{asset('/img/front/prem_icon.png')}}">PREMIUM--}}
            {{--                                        </span>--}}
            {{--                                    @endif--}}
            {{--                                </div>--}}
            {{--                                <div class="list-ad_block-item_container">--}}
            {{--                                    <div class="list-ad_block-item_left">--}}
            {{--                                        <img src="{{asset($_s->MainPhoto->value)}}">--}}
            {{--                                        <div class="info">--}}
            {{--                                            <p>{{$_s->title}}</p>--}}
            {{--                                            <p>{{$_s->address}}</p>--}}
            {{--                                            <div class="info-rating">--}}
            {{--                                                <div class="star-rating">--}}
            {{--                                                    @php--}}
            {{--                                                        $rating = $_s->reviews()->avg('rating');--}}
            {{--                                                    @endphp--}}
            {{--                                                    <input type="radio" id="5-stars" name="rating"--}}
            {{--                                                           value="5" {{floor($rating) == 5?'checked':''}}/>--}}
            {{--                                                    <label for="5-stars" class="star">&#9733;</label>--}}
            {{--                                                    <input type="radio" id="4-stars" name="rating"--}}
            {{--                                                           value="4" {{floor($rating) == 4?'checked':''}}/>--}}
            {{--                                                    <label for="4-stars" class="star">&#9733;</label>--}}
            {{--                                                    <input type="radio" id="3-stars" name="rating"--}}
            {{--                                                           value="3" {{floor($rating) == 3?'checked':''}}/>--}}
            {{--                                                    <label for="3-stars" class="star">&#9733;</label>--}}
            {{--                                                    <input type="radio" id="2-stars" name="rating"--}}
            {{--                                                           value="2" {{floor($rating) == 2?'checked':''}}/>--}}
            {{--                                                    <label for="2-stars" class="star">&#9733;</label>--}}
            {{--                                                    <input type="radio" id="1-star" name="rating"--}}
            {{--                                                           value="1" {{floor($rating) == 1?'checked':''}}/>--}}
            {{--                                                    <label for="1-star" class="star">&#9733;</label>--}}
            {{--                                                </div>--}}
            {{--                                                <p>({{$_s->reviews()->count()}} отзыва)</p>--}}
            {{--                                            </div>--}}
            {{--                                            <ul class="advantages-list">--}}
            {{--                                                --}}{{--                                        <li>Удалённость: 5км</li>--}}
            {{--                                                <li>Площадь коттеджа: {{$_s_object->living_space}} м2</li>--}}
            {{--                                                <li>{{$_s_object->count_guest}} гостя / {{$_s_object->count_bedrooms}}--}}
            {{--                                                    спальни--}}
            {{--                                                    / {{$_s_object->count_small_bedrooms + $_s_object->count_big_bedrooms}}--}}
            {{--                                                    кровати--}}
            {{--                                                </li>--}}
            {{--                                            </ul>--}}
            {{--                                            <ul class="comfort-list">--}}
            {{--                                                <li>Удобства <img src="{{asset('/img/front/garage.png')}}"><img--}}
            {{--                                                            src="{{asset('/img/front/tv.png')}}"><img--}}
            {{--                                                            src="{{asset('/img/front/kitchen.png')}}"></li>--}}
            {{--                                                <li>Развлечения <img src="{{asset('/img/front/music.png')}}"></li>--}}
            {{--                                            </ul>--}}
            {{--                                        </div>--}}
            {{--                                    </div>--}}
            {{--                                    <div class="list-ad_block-item_right">--}}
            {{--                                        @if($rating>0)--}}
            {{--                                            <div class="estimate">--}}
            {{--                                                {{$rating * 2}}--}}
            {{--                                            </div>--}}
            {{--                                        @endif--}}
            {{--                                        <ul class="price">--}}
            {{--                                            <li>Сутки будни <span>{{$_s_object->price_day}}₽</span></li>--}}
            {{--                                            <li>Сутки выходные <span>{{$_s_object->price_day_weekend}}₽</span></li>--}}
            {{--                                            <li>Полные выходные (ПТ-ВС) <span>{{$_s_object->price_weekends}}₽</span>--}}
            {{--                                            </li>--}}
            {{--                                        </ul>--}}

            {{--                                        <a href="{{route('front.object.view',$_s->id)}}" class="primary-btn">Подробнее</a>--}}
            {{--                                    </div>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        @endforeach--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            @endif--}}
            @if(count($similar))
                <div class="similar-offers_banner-list">
                    <p class="title">Похожие предложения:</p>
                    <div class="similar-offers_banner-list_container">
                        @foreach($similar as $_s)
                            <a href="{{route('front.object.view',$_s->id)}}" class="banner-block">
                                <img src="{{$_s->MainPhoto->value}}">
                                <p class="obyavlenie-box_info-title">{{$_s->title}}</p>
                                <p class="obyavlenie-box_info-subtitle">{{$_s->address}}</p>
                                <p class="banner-block_price">{!! \App\Models\UserObject::PRICE_TYPES[$_s->type][$_s->price_type_id]['price'] !!}</p>
                            </a>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </section>
    @include('front.cabinet.modal.newReview',['parent_id'=>$item->id,'parent_type'=>\App\Models\ObjectReview::TYPE_USER_OBJECT])
    @include('front.cabinet.modal.newMessage',['parent_id'=>$item->id,'parent_type'=>\App\Models\ObjectDialog::TYPE_USER_OBJECT])
    @include('front.modal.cottageReservation')
    @include('front.modal.userSmsNoRegister')
@endsection
