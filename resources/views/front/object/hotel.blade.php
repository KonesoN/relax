@extends('front.partials.app')

@section('content')
    @php
        $object = $item->object;

       $hash_room = md5(strtotime('now')+rand(99999,999999));
    @endphp
    <section class="rest-template">
        <div class="container">
            <div class="main-title">
                <div>
                    <div>
                        <h2 class="title">{{$item->title}}</h2>
                        <p>{{$item->direction->name}}</p>
                        <p>Телефон: {{$item->phone}}</p>
                    </div>
                    @php
                        $rating = $item->reviews()->avg('rating');
                    @endphp
                    <div class="rating">
                        <div class="star-rating">
                            <input type="radio" id="5-stars" name="rating"
                                   value="5" {{floor($rating) == 5?'checked':''}}/>
                            <label for="5-stars" class="star">&#9733;</label>
                            <input type="radio" id="4-stars" name="rating"
                                   value="4" {{floor($rating) == 4?'checked':''}}/>
                            <label for="4-stars" class="star">&#9733;</label>
                            <input type="radio" id="3-stars" name="rating"
                                   value="3" {{floor($rating) == 3?'checked':''}}/>
                            <label for="3-stars" class="star">&#9733;</label>
                            <input type="radio" id="2-stars" name="rating"
                                   value="2" {{floor($rating) == 2?'checked':''}}/>
                            <label for="2-stars" class="star">&#9733;</label>
                            <input type="radio" id="1-star" name="rating"
                                   value="1" {{floor($rating) == 1?'checked':''}}/>
                            <label for="1-star" class="star">&#9733;</label>
                        </div>
                        <p>({{$item->reviews()->count()}} отзыва)</p>
                    </div>
                    @include('front.partials.favoriteButton')
                </div>
                @if($rating>0)
                    <div class="estimate">
                        {{$rating * 2}}
                    </div>
                @endif
            </div>
            @include('front.object.partials.map')
            <div class="info">
                <div class="info-row">
                    <div class="info-slider">
                        <div class="rec-label">
                            @if($item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::SERVICE_SELECTION,'is_active'=>1])->first())
                                <span class="rec-label_serv-sel">
                                                    <img src="{{asset('/img/front/vip.png')}}">
                                                    Выбор <br>сервиса
                            </span>
                            @endif
                            @if($item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::PREMIUM,'is_active'=>1])->first())
                                <span class="rec-label_prem">
                                            <img src="{{asset('/img/front/prem_icon.png')}}">PREMIUM
                                        </span>
                            @endif
                        </div>
                        <div class="product-imgs">
                            <div class="img-display">
                                <div class="img-showcase">
                                    @foreach($item->photos as $photo)
                                        <img src="{{$photo->value}}" alt="shoe image">
                                    @endforeach
                                </div>
                            </div>
                            <div class="img-select">
                                @foreach($item->photos as $photo)
                                    <div class="img-item">
                                        <a href="#" data-id="{{$loop->index + 1}}">
                                            <img src="{{$photo->value}}" alt="shoe image">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="info-main">
                        @include('front.object.partials.filters')
                        <div class="buttons-box">
                            @if(Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                <a href="#" data-modal_id="open-modal_addReview" class="primary-btn modal_view">Оставить
                                    отзыв</a>
                            @endif
                            @if(!Auth::check() || Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                <a href="#"
                                   data-modal_id="{{Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER?'open-modal_newMessage':'open-modal_userSmsNoRegister'}} "
                                   class="primary-btn modal_view">Написать сообщение</a>
                            @endif
                        </div>
                        <div class="rules-price">
                            <ul class="rules">
                                <li>Правила размещения:</li>
                                <li><p>Время заезда: </p> <span>{{$item->arrival_at}}</span></li>
                                <li><p>Время выезда: </p> <span>{{$item->departure_at}}</span></li>
                                <li><p>Минимальное количество суток: </p>&nbsp;<span>{{$item->minimum_days}}</span></li>
                                <li><p>Страховой депозит: </p> <span>{{$item->insurance_deposit}} руб.</span></li>
                                <li><p>Можно с животными: </p> <span>{{$item->is_pets?'Да':'Нет'}}</span></li>
                            </ul>
                            <ul class="price">
                                <li>
                                    Стоимость: {!! \App\Models\UserObject::PRICE_TYPES[$item->type][$item->price_type_id]['price'] !!}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="info-row" style="justify-content: space-between">
                    <div class="testimonials-text">
                        @if($item->activeReviews->count())

                            <div class="slider-testimonials">
                                <ul id="slides">
                                    @foreach($item->activeReviews as $review)
                                        <li class="slide {{$loop->index == 0?'showing':''}}showing">
                                            <p class="name"><b>{{$review->user->first_name}}</b> <img
                                                        src="{{asset('/img/front/flag-ru.jpg')}}"> Россия
                                            </p>
                                            <p>{{$review->review}}</p>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="buttons">
                                    <button class="controls" id="previous"><img
                                                src="{{asset('/img/front/arr_slider.png')}}"></button>

                                    <button class="controls" id="pause">&#10074;&#10074;</button>

                                    <button class="controls" id="next"><img
                                                src="{{asset('/img/front/arr_slider.png')}}">
                                    </button>
                                </div>
                            </div>
                        @endif
                        <div class="text">
                            <p class="title">Описание</p>
                            <p style="white-space: pre-line">
                                {{$item->description}}
                            </p>
                        </div>
                    </div>
                    @if(!empty($item->youtube()->first()->value) && $item->youtube()->first()->extension == 'iframe')
                        <div class="video">
                            <div class="hs-responsive-embed-youtube">
                                <iframe width="560" height="315" src="{{$item->youtube()->first()->value}}"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="type-number">
                <div class="d-table type-number_desktop">
                    <div class="d-tr">
                        <div class="d-td"></div>
                        <div class="d-td title">Вмещает:</div>
                        <div class="d-td title">Тип номера:</div>
                        <div class="d-td title">Стоимость:</div>
                        <div class="d-td"></div>
                    </div>
                    @foreach($object->rooms as $room)
                        @foreach($room->BusyDates as $bd)
                            <input type="hidden" class="_disable_days_{{$room->id}}" value="{{$bd}}"/>
                        @endforeach
                        <div class="d-tr">
                            <div class="d-td photo">
                                <div>
                                    <a href="#table-photo1">
                                        <img src="{{$room->MainPhoto->value}}">
                                        <span class="zoom-in">
                                        <img src="{{asset('/img/front/zoom-in.png')}}">
                                    </span>
                                    </a>

                                    <a href="#" class="lightbox" id="table-photo1">
                                        <span
                                                style="background-image: url('{{$room->MainPhoto->value}}')"></span>
                                    </a>
                                </div>
                            </div>
                            <div class="d-td human">
                                @for($i=0;$i<$room->count_guest;$i++)
                                    <img src="{{asset('/img/front/human.png')}}">
                                @endfor
                            </div>
                            <div class="d-td text">
                                <p>{{$room->title}}</p>
                                <p>
                                    @if($room->count_big_bedrooms)
                                        Двуспальных: {{$room->count_big_bedrooms}}.
                                    @endif
                                    @if($room->count_big_bedrooms)
                                        Односпальных: {{$room->count_small_bedrooms}}
                                    @endif
                                    @if($room->count_guest)
                                        Вместимость: {{$room->count_guest}}
                                    @endif
                                </p>
                            </div>
                            <div class="d-td room-price">
                                <ul>
                                    <li>Сутки будни <span
                                                class="room_price_day_{{$hash_room}}">{{!empty($room)?$room->price_day:''}}₽</span>
                                    </li>
                                    <li>Сутки выходные <span
                                                class="room_price_day_weekend_{{$hash_room}}">{{!empty($room)?$room->price_day_weekend:''}}₽</span>
                                    </li>
                                    <li>Полные выходные (ПТ-ВС) <span
                                                class="room_price_weekends_{{$hash_room}}">{{!empty($room)?$room->price_weekends:''}}₽</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="d-td">
{{--                                <a href="#" class="primary-btn">Узнать цену</a>--}}
                                <a href="#" data-modal_id="open-modal_roomReservation" style="margin-top: 5px"
                                   class="primary-btn modal_view _save_room"
                                   data-room_id="{{$room->id}}">Забронировать</a>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="type-number_mobile">
                    @foreach($object->rooms as $room)
                        <div class="item">
                            <div class="row">
                                <div class="photo">
                                    <a href="#table-photo3">
                                        <img src="{{$room->MainPhoto->value}}">
                                        <span class="zoom-in">
                                        <img src="{{asset('/img/front/zoom-in.png')}}">
                                    </span>
                                    </a>

                                    <a href="#" class="lightbox" id="table-photo3">
                                        <span
                                                style="background-image: url('{{$room->MainPhoto->value}}')"></span>
                                    </a>
                                </div>

                                <div class="info">
                                    <p class="title">Вмещает:</p>
                                    <div class="human">
                                        @for($i=0;$i<$room->count_guest;$i++)
                                            <img src="{{asset('/img/front/human.png')}}">
                                        @endfor
                                    </div>
                                    <ul class="room-price">
                                        <li>Сутки будни <span
                                                    class="room_price_day_{{$hash_room}}">{{!empty($room)?$room->price_day:''}}₽</span>
                                        </li>
                                        <li>Сутки выходные <span
                                                    class="room_price_day_weekend_{{$hash_room}}">{{!empty($room)?$room->price_day_weekend:''}}₽</span>
                                        </li>
                                        <li>Полные выходные (ПТ-ВС) <span
                                                    class="room_price_weekends_{{$hash_room}}">{{!empty($room)?$room->price_weekends:''}}₽</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="description">
                                    <p>{{$room->title}}</p>
                                    <p>
                                        @if($room->count_big_bedrooms)
                                            Двуспальных: {{$room->count_big_bedrooms}}.
                                        @endif
                                        @if($room->count_big_bedrooms)
                                            Односпальных: {{$room->count_small_bedrooms}}
                                        @endif
                                    </p>
{{--                                    <a href="#" class="primary-btn">Узнать цену</a>--}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @if(count($similar))
                <div class="similar-offers_banner-list">
                    <p class="title">Похожие предложения:</p>
                    <div class="similar-offers_banner-list_container">
                        @foreach($similar as $_s)
                            <a href="{{route('front.object.view',$_s->id)}}" class="banner-block">
                                <img src="{{$_s->MainPhoto->value}}">
                                <p class="obyavlenie-box_info-title">{{$_s->title}}</p>
                                <p class="obyavlenie-box_info-subtitle">{{$_s->address}}</p>
                                <p class="banner-block_price">{!! \App\Models\UserObject::PRICE_TYPES[$_s->type][$_s->price_type_id]['price'] !!}</p>
                            </a>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </section>
    @include('front.cabinet.modal.newReview',['parent_id'=>$item->id,'parent_type'=>\App\Models\ObjectReview::TYPE_USER_OBJECT])
    @include('front.cabinet.modal.newMessage',['parent_id'=>$item->id,'parent_type'=>\App\Models\ObjectDialog::TYPE_USER_OBJECT])
    @include('front.modal.userSmsNoRegister')
    @include('front.modal.roomReservation')
@endsection
