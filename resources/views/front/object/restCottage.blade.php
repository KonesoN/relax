@extends('front.partials.app')

@section('content')
    @foreach($item->BusyDates as $bd)
        <input type="hidden" class="_disable_days" value="{{$bd}}"/>
    @endforeach
    <section class="rest-template cottage-template">
        <div class="container">
            <div class="main-title">
                <div>
                    <h2 class="title">{{$item->title}}</h2>
                </div>
            </div>
            <div class="info">
                <div class="info-row">
                    <div class="info-slider">
                        <div class="product-imgs">
                            <div class="img-display">
                                <div class="img-showcase">
                                    @foreach($item->photos as $photo)
                                        <img src="{{$photo->value}}" alt="shoe image">
                                    @endforeach
                                </div>
                            </div>
                            <div class="img-select">
                                @foreach($item->photos as $photo)
                                    <div class="img-item">
                                        <a href="#" data-id="{{$loop->index + 1}}">
                                            <img src="{{$photo->value}}" alt="shoe image">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="info-main">
                        @include('front.object.partials.filters',['item'=>$uo])
                        <div class="buttons-box">
                            @if(Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                <a href="#" data-modal_id="open-modal_addReview" class="primary-btn modal_view">Оставить
                                    отзыв</a>
                            @endif
                            @if(!Auth::check() || Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                <a href="#"
                                   data-modal_id="{{Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER?'open-modal_newMessage':'open-modal_userSmsNoRegister'}} "
                                   class="primary-btn modal_view">Написать сообщение</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="info-row">
                    <div class="rules-price">
                        <ul class="description">
                            <li><p>Площадь:</p><span>{{$item->living_space}} м&sup2;</span></li>
                            <li><p>Вместимость гостей:</p><span>{{$item->count_guest}}</span></li>
                            <li><p>Кол-во спален:</p><span>{{$item->count_bedrooms}}</span></li>
                            <li><p>Кол-во маленьких кроватей:</p><span>{{$item->count_small_bedrooms}}</span></li>
                            <li><p>Кол-во больших кроватей:</p><span>{{$item->count_big_bedrooms}}</span></li>
                        </ul>
                        {{--                        <ul class="rules">--}}
                        {{--                            <li>Правила размещения:</li>--}}
                        {{--                            <li><p>Время заезда:</p> <span>{{$item->arrival_at}}</span></li>--}}
                        {{--                            <li><p>Время выезда:</p> <span>{{$item->departure_at}}</span></li>--}}
                        {{--                            <li><p>Минимальное количество суток:</p> <span>{{$item->minimum_days}}</span></li>--}}
                        {{--                            <li><p>Страховой депозит:</p> <span>{{$item->insurance_deposit}} руб.</span></li>--}}
                        {{--                            <li><p>Можно с животными:</p> <span>{{$item->is_pets?'Да':'Нет'}}</span></li>--}}
                        {{--                        </ul>--}}
                        <ul class="price">
                            <li>Сутки будни <span>{{$item->price_day}}₽</span></li>
                            <li>Сутки выходные <span>{{$item->price_day_weekend}}₽</span></li>
                            <li>Полные выходные (ПТ-ВС) <span>{{$item->price_weekends}}₽</span></li>
                            <li><a href="#" data-modal_id="open-modal_restCottageReservation"
                                   class="primary-btn modal_view _save_rest_object" data-rest_object_id="{{$item->id}}">Забронировать</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="info-row">
                    <div class="text">
                        <p class="title">Описание</p>
                        <p style="white-space: pre-line">
                            {{$item->description}}
                        </p>
                    </div>
                    <div class="calendar info-calendar">
                        <div class="datepicker-here"></div>
                    </div>
                </div>
                @if(!empty($item->youtube()->first()->value) && $item->youtube()->first()->extension == 'iframe')
                    <div class="info-row">
                        <div class="video">
                            <div class="hs-responsive-embed-youtube">
                                <iframe width="560" height="315" src="{{$item->youtube()->first()->value}}"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    @include('front.cabinet.modal.newReview',['parent_id'=>$item->id,'parent_type'=>\App\Models\ObjectReview::TYPE_REST_OBJECT])
    @include('front.cabinet.modal.newMessage',['parent_id'=>$item->id,'parent_type'=>\App\Models\ObjectDialog::TYPE_REST_OBJECT])
    @include('front.modal.userSmsNoRegister')
    @include('front.modal.restCottageReservation')
@endsection
