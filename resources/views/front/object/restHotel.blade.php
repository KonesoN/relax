@extends('front.partials.app')
@php
    $hash_room = md5(strtotime('now')+rand(99999,999999));
@endphp
@section('content')
    <section class="rest-template">
        <div class="container">
            <div class="main-title">
                <div>
                    <h2 class="title">{{$item->title}}</h2>
                </div>
            </div>
            <div class="info">
                <div class="info-row">
                    <div class="info-slider">
                        <div class="product-imgs">
                            <div class="img-display">
                                <div class="img-showcase">
                                    @foreach($item->photos as $photo)
                                        <img src="{{$photo->value}}" alt="shoe image">
                                    @endforeach
                                </div>
                            </div>
                            <div class="img-select">
                                @foreach($item->photos as $photo)
                                    <div class="img-item">
                                        <a href="#" data-id="{{$loop->index + 1}}">
                                            <img src="{{$photo->value}}" alt="shoe image">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="info-main">
                        @include('front.object.partials.filters',['item'=>$uo])
                        <div class="buttons-box">
                            @if(Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                <a href="#" data-modal_id="open-modal_addReview" class="primary-btn modal_view">Оставить
                                    отзыв</a>
                            @endif
                            @if(!Auth::check() || Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                <a href="#"
                                   data-modal_id="{{Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER?'open-modal_newMessage':'open-modal_userSmsNoRegister'}} "
                                   class="primary-btn modal_view">Написать сообщение</a>
                            @endif
                        </div>
                        <div class="rules-price">
                            <ul class="rules">
                                <li>Правила размещения:</li>
                                <li><p>Время заезда: </p> <span>{{$item->arrival_at}}</span></li>
                                <li><p>Время выезда: </p> <span>{{$item->departure_at}}</span></li>
                                <li><p>Минимальное количество суток: </p>&nbsp;<span>{{$item->minimum_days}}</span></li>
                                <li><p>Можно с животными: </p> <span>{{$item->is_pets?'Да':'Нет'}}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="info-row">
                    <div class="testimonials-text">
                        <div class="text">
                            <p class="title">Описание</p>
                            <p style="white-space: pre-line">
                                {{$item->description}}
                            </p>
                        </div>
                    </div>
                    @if(!empty($item->youtube()->first()->value) && $item->youtube()->first()->extension == 'iframe')
                        <div class="video">
                            <div class="hs-responsive-embed-youtube">
                                <iframe width="560" height="315" src="{{$item->youtube()->first()->value}}"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="type-number">
                <div class="d-table type-number_desktop">
                    <div class="d-tr">
                        <div class="d-td"></div>
                        <div class="d-td title">Вмещает:</div>
                        <div class="d-td title">Тип номера:</div>
                        <div class="d-td title">Стоимость:</div>
                        <div class="d-td"></div>
                    </div>
                    @foreach($item->rooms as $room)
                        @foreach($room->BusyDates as $bd)
                            <input type="hidden" class="_disable_days_{{$room->id}}" value="{{$bd}}"/>
                        @endforeach
                        <div class="d-tr">
                            <div class="d-td photo">
                                <div>
                                    <a href="#table-photo1">
                                        <img src="{{$room->MainPhoto->value}}">
                                        <span class="zoom-in">
                                        <img src="{{asset('/img/front/zoom-in.png')}}">
                                    </span>
                                    </a>

                                    <a href="#" class="lightbox" id="table-photo1">
                                        <span
                                                style="background-image: url('{{$room->MainPhoto->value}}')"></span>
                                    </a>
                                </div>
                            </div>
                            <div class="d-td human">
                                @for($i=0;$i<$room->count_guest;$i++)
                                    <img src="{{asset('/img/front/human.png')}}">
                                @endfor
                            </div>
                            <div class="d-td text">
                                <p>{{$room->title}}</p>
                                <p>
                                    @if($room->count_big_bedrooms)
                                        Двуспальных: {{$room->count_big_bedrooms}}.
                                    @endif
                                    @if($room->count_big_bedrooms)
                                        Односпальных: {{$room->count_small_bedrooms}}
                                    @endif
                                </p>
                            </div>
                            <div class="d-td room-price">
                                <ul>
                                    <li>Сутки будни <span
                                                class="room_price_day_{{$hash_room}}">{{!empty($room)?$room->price_day:''}}₽</span>
                                    </li>
                                    <li>Сутки выходные <span
                                                class="room_price_day_weekend_{{$hash_room}}">{{!empty($room)?$room->price_day_weekend:''}}₽</span>
                                    </li>
                                    <li>Полные выходные (ПТ-ВС) <span
                                                class="room_price_weekends_{{$hash_room}}">{{!empty($room)?$room->price_weekends:''}}₽</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="d-td">
{{--                                <a href="#" class="primary-btn">Узнать цену</a>--}}
                                <a href="#" data-modal_id="open-modal_restHotelRoomReservation" style="margin-top: 5px"
                                   class="primary-btn modal_view _save_rest_hotel_room"
                                   data-room_id="{{$room->id}}">Забронировать</a>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="type-number_mobile">
                    @foreach($item->rooms as $room)
                        <div class="item">
                            <div class="row">
                                <div class="photo">
                                    <a href="#table-photo3">
                                        <img src="{{$room->MainPhoto->value}}">
                                        <span class="zoom-in">
                                        <img src="{{asset('/img/front/zoom-in.png')}}">
                                    </span>
                                    </a>

                                    <a href="#" class="lightbox" id="table-photo3">
                                        <span
                                                style="background-image: url('{{$room->MainPhoto->value}}')"></span>
                                    </a>
                                </div>

                                <div class="info">
                                    <p class="title">Вмещает:</p>
                                    <div class="human">
                                        @for($i=0;$i<$room->count_guest;$i++)
                                            <img src="{{asset('/img/front/human.png')}}">
                                        @endfor
                                    </div>
                                    <ul class="room-price">
                                        <li>Сутки будни <span
                                                    class="room_price_day_{{$hash_room}}">{{!empty($room)?$room->price_day:''}}₽</span>
                                        </li>
                                        <li>Сутки выходные <span
                                                    class="room_price_day_weekend_{{$hash_room}}">{{!empty($room)?$room->price_day_weekend:''}}₽</span>
                                        </li>
                                        <li>Полные выходные (ПТ-ВС) <span
                                                    class="room_price_weekends_{{$hash_room}}">{{!empty($room)?$room->price_weekends:''}}₽</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="description">
                                    <p>{{$room->title}}</p>
                                    <p>
                                        @if($room->count_big_bedrooms)
                                            Двуспальных: {{$room->count_big_bedrooms}}.
                                        @endif
                                        @if($room->count_big_bedrooms)
                                            Односпальных: {{$room->count_small_bedrooms}}
                                        @endif
                                    </p>
{{--                                    <a href="#" class="primary-btn">Узнать цену</a>--}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @include('front.cabinet.modal.newReview',['parent_id'=>$item->id,'parent_type'=>\App\Models\ObjectReview::TYPE_REST_OBJECT])
    @include('front.cabinet.modal.newMessage',['parent_id'=>$item->id,'parent_type'=>\App\Models\ObjectDialog::TYPE_REST_OBJECT])
    @include('front.modal.userSmsNoRegister')
    @include('front.modal.restHotelRoomReservation')
@endsection
