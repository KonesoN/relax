@extends('front.partials.app')

@section('content')
    @php
        $object = $item->object;
    @endphp
    <section class="rest-template">
        <div class="container">
            <div class="main-title">
                <div>
                    <div>
                        <h2 class="title">{{$item->title}}</h2>
                        <p>{{$item->direction->name}}</p>
                        <p>Телефон: {{$item->phone}}</p>
                        @include('front.partials.favoriteButton')
                    </div>
                </div>
            </div>
            @include('front.object.partials.map')
            <div class="info">
                <div class="info-row">
                    <div class="info-slider">
                        <div class="rec-label">
                            @if($item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::SERVICE_SELECTION,'is_active'=>1])->first())
                                <span class="rec-label_serv-sel">
                                                    <img src="{{asset('/img/front/vip.png')}}">
                                                    Выбор <br>сервиса
                            </span>
                            @endif
                            @if($item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::PREMIUM,'is_active'=>1])->first())
                                <span class="rec-label_prem">
                                            <img src="{{asset('/img/front/prem_icon.png')}}">PREMIUM
                                        </span>
                            @endif
                        </div>
                        <div class="product-imgs">
                            <div class="img-display">
                                <div class="img-showcase">
                                    @foreach($item->photos as $photo)
                                        <img src="{{$photo->value}}" alt="shoe image">
                                    @endforeach
                                </div>
                            </div>
                            <div class="img-select">
                                @foreach($item->photos as $photo)
                                    <div class="img-item">
                                        <a href="#" data-id="{{$loop->index + 1}}">
                                            <img src="{{$photo->value}}" alt="shoe image">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="info-main">
                        @include('front.object.partials.filters')
                        <div class="buttons-box">
                            @if(Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                <a href="#" data-modal_id="open-modal_addReview" class="primary-btn modal_view">Оставить
                                    отзыв</a>
                            @endif
                            @if(!Auth::check() || Auth::user()->type_id == \App\Models\User::TYPE_USER)
                                <a href="#"
                                   data-modal_id="{{Auth::check() && Auth::user()->type_id == \App\Models\User::TYPE_USER?'open-modal_newMessage':'open-modal_userSmsNoRegister'}} "
                                   class="primary-btn modal_view">Написать сообщение</a>
                            @endif
                        </div>
                        <div class="rules-price">
                            <ul class="rules">
                                <li>Правила размещения:</li>
                                <li><p>Время заезда: </p> <span>{{$item->arrival_at}}</span></li>
                                <li><p>Время выезда: </p> <span>{{$item->departure_at}}</span></li>
                                <li><p>Минимальное количество суток:</p>&nbsp;<span>{{$item->minimum_days}}</span></li>
                                <li><p>Страховой депозит: </p> <span>{{$item->insurance_deposit}} руб.</span></li>
                                <li><p>Можно с животными: </p> <span>{{$item->is_pets?'Да':'Нет'}}</span></li>
                            </ul>
                            <ul class="price">
                                <li>
                                    Стоимость: {!! \App\Models\UserObject::PRICE_TYPES[$item->type][$item->price_type_id]['price'] !!}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="info-row">
                    <div class="text">
                        <p class="title">Описание</p>
                        <p style="white-space: pre-line">
                            {{$item->description}}
                        </p>
                    </div>
                    @if(!empty($item->youtube()->first()->value) && $item->youtube()->first()->extension == 'iframe')
                        <div class="video">
                            <div class="hs-responsive-embed-youtube">
                                <iframe width="560" height="315" src="{{$item->youtube()->first()->value}}"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="list-ad_block">
                @foreach($object->cottages as $cottage)
                    @php
                        $rating = $cottage->reviews()->avg('rating');
                    @endphp
                    @foreach($cottage->BusyDates as $bd)
                        <input type="hidden" class="_disable_days_{{$cottage->id}}" value="{{$bd}}"/>
                    @endforeach
                    <div class="list-ad_block-item">
                        <div class="list-ad_block-item_container">
                            <div class="list-ad_block-item_left">
                                <img src="{{$cottage->MainPhoto->value}}">
                                <div class="info">
                                    <p>{{$cottage->title}}</p>
                                    {{--                                    <p>Киевское шоссе, 33-й км</p>--}}
                                    <div class="info-rating">
                                        <div class="star-rating">
                                            <input type="radio" id="5-stars" name="rating"
                                                   value="5" {{floor($rating) == 5?'checked':''}}/>
                                            <label for="5-stars" class="star">&#9733;</label>
                                            <input type="radio" id="4-stars" name="rating"
                                                   value="4" {{floor($rating) == 4?'checked':''}}/>
                                            <label for="4-stars" class="star">&#9733;</label>
                                            <input type="radio" id="3-stars" name="rating"
                                                   value="3" {{floor($rating) == 3?'checked':''}}/>
                                            <label for="3-stars" class="star">&#9733;</label>
                                            <input type="radio" id="2-stars" name="rating"
                                                   value="2" {{floor($rating) == 2?'checked':''}}/>
                                            <label for="2-stars" class="star">&#9733;</label>
                                            <input type="radio" id="1-star" name="rating"
                                                   value="1" {{floor($rating) == 1?'checked':''}}/>
                                            <label for="1-star" class="star">&#9733;</label>
                                        </div>
                                        <p>({{$cottage->reviews()->count()}} отзыва)</p>
                                    </div>
                                    <ul class="advantages-list">
                                        <li>Вместимость гостей: {{$cottage->count_guest}}</li>
                                        <li>Кол-во односпальных кроватей: {{$cottage->count_small_bedrooms}}</li>
                                        <li>Кол-во двуспальных кроватей: {{$cottage->count_big_bedrooms}}</li>
                                        {{--                                        <li>Описание: {{$cottage->description}}</li>--}}
                                    </ul>
                                    {{--                                    <ul class="comfort-list">--}}
                                    {{--                                        @include('front.partials.objectItemFilters')--}}
                                    {{--                                    </ul>--}}
                                </div>
                            </div>
                            <div class="list-ad_block-item_right">
                                <div>
                                    @if($rating>0)
                                        <div class="estimate">
                                            {{$rating * 2}}
                                        </div>
                                    @endif
                                    <ul class="price">
                                        <li>Сутки будни <span>{{$cottage->price_day}}₽</span></li>
                                        <li>Сутки выходные <span>{{$cottage->price_day_weekend}}₽</span></li>
                                        <li>Полные выходные (ПТ-ВС) <span>{{$cottage->price_weekends}}₽</span></li>
                                    </ul>
                                </div>
                                <a class="primary-btn"
                                   href="{{route('front.object.view.cottage',['item'=>$cottage->id])}}">Подробнее</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="list-ad_block">
                @foreach($object->hotels as $hotel)
                    @php
                        $rating = $hotel->reviews()->avg('rating');
                    @endphp
                    <div class="list-ad_block-item">
                        <div class="list-ad_block-item_container">
                            <div class="list-ad_block-item_left">
                                <img src="{{$hotel->MainPhoto->value}}">
                                <div class="info">
                                    <p>{{$hotel->title}}</p>
                                    {{--                                    <p>Киевское шоссе, 33-й км</p>--}}
                                    <div class="info-rating">
                                        <div class="star-rating">
                                            <input type="radio" id="5-stars" name="rating"
                                                   value="5" {{floor($rating) == 5?'checked':''}}/>
                                            <label for="5-stars" class="star">&#9733;</label>
                                            <input type="radio" id="4-stars" name="rating"
                                                   value="4" {{floor($rating) == 4?'checked':''}}/>
                                            <label for="4-stars" class="star">&#9733;</label>
                                            <input type="radio" id="3-stars" name="rating"
                                                   value="3" {{floor($rating) == 3?'checked':''}}/>
                                            <label for="3-stars" class="star">&#9733;</label>
                                            <input type="radio" id="2-stars" name="rating"
                                                   value="2" {{floor($rating) == 2?'checked':''}}/>
                                            <label for="2-stars" class="star">&#9733;</label>
                                            <input type="radio" id="1-star" name="rating"
                                                   value="1" {{floor($rating) == 1?'checked':''}}/>
                                            <label for="1-star" class="star">&#9733;</label>
                                        </div>
                                        <p>({{$hotel->reviews()->count()}} отзыва)</p>
                                    </div>
                                    <ul class="advantages-list">
                                        <li>Вместимость гостей: {{$hotel->count_guest}}</li>
                                        <li>Время заезда: {{$hotel->arrival_at}}</li>
                                        <li>Время выезда: {{$hotel->departure_at}}</li>
                                        <li>Минимальное количество суток: {{$hotel->minimum_days}}</li>
                                        <li>Можно с животными: {{$item->is_pets?'Да':'Нет'}}</li>
                                        {{--                                        <li>Описание: {{$hotel->description}}</li>--}}
                                    </ul>
                                    {{--                                    <ul class="comfort-list">--}}
                                    {{--                                        @include('front.partials.objectItemFilters')--}}
                                    {{--                                    </ul>--}}
                                </div>
                            </div>
                            <div class="list-ad_block-item_right">
                                @if($rating>0)
                                    <div class="estimate">
                                        {{$rating * 2}}
                                    </div>
                                @endif
                                {{--                                <ul class="price">--}}
                                {{--                                    <li>Сутки будни <span>{{$hotel->price_day}}₽</span></li>--}}
                                {{--                                    <li>Сутки выходные <span>{{$hotel->price_day_weekend}}₽</span></li>--}}
                                {{--                                    <li>Полные выходные (ПТ-ВС) <span>{{$hotel->price_weekends}}₽</span></li>--}}
                                {{--                                </ul>--}}
                                <a class="primary-btn" href="{{route('front.object.view.hotel',['item'=>$hotel->id])}}">Подробнее</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @if(count($similar))
                <div class="similar-offers_banner-list">
                    <p class="title">Похожие предложения:</p>
                    <div class="similar-offers_banner-list_container">
                        @foreach($similar as $_s)
                            <a href="{{route('front.object.view',$_s->id)}}" class="banner-block">
                                <img src="{{$_s->MainPhoto->value}}">
                                <p class="obyavlenie-box_info-title">{{$_s->title}}</p>
                                <p class="obyavlenie-box_info-subtitle">{{$_s->address}}</p>
                                <p class="banner-block_price">{!! \App\Models\UserObject::PRICE_TYPES[$_s->type][$_s->price_type_id]['price'] !!}</p>
                            </a>
                        @endforeach
                    </div>
                </div>
        @endif
    </section>
    @include('front.cabinet.modal.newReview',['parent_id'=>$item->id,'parent_type'=>\App\Models\ObjectReview::TYPE_USER_OBJECT])
    @include('front.cabinet.modal.newMessage',['parent_id'=>$item->id,'parent_type'=>\App\Models\ObjectDialog::TYPE_USER_OBJECT])
    @include('front.modal.userSmsNoRegister')
    @include('front.modal.restCottageReservation')
@endsection
