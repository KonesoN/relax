@extends('front.partials.app')

@section('content')
    <section class="login-section">
        <div class="container">
            <div class="auth-box">
                <div class="auth-box_header">Укажите актуальные данные</div>
                <div class="auth-box_subheader">для авторизации на сервисе</div>

                <div class="auth-box_body">
                    <form method="POST" action="{{ route('front.login') }}">
                        <div class="form-group">
                            <label for="email">Адрес электронной почты</label>

                            <div class="form-group_input-box">
                                <input id="email" type="email"
                                       class="form-control" name="email"
                                       value="{{!empty($fields['email'])?$fields['email']:''}}" required
                                       autocomplete="email" autofocus>

                                <span class="invalid-feedback" role="alert"
                                      style="{{!empty($errors['email'])?'display:block':''}}">
                                        <strong>{{!empty($errors['email'])?$errors['email']:''}}</strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password">Пароль</label>

                            <div class="form-group_input-box">
                                <input id="password" type="password" class="form-control" name="password" required
                                       autocomplete="current-password"
                                       value="{{!empty($fields['password'])?$fields['password']:''}}"
                                >

                                <span class="invalid-feedback" role="alert"
                                      style="{{!empty($errors['password'])?'display:block':''}}">
                                        <strong>{{!empty($errors['password'])?$errors['password']:''}}</strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember"
                                       id="remember">

                                <label class="form-check-label" for="remember">
                                    Запомнить меня
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="primary-btn">
                                Войти
                            </button>
                        </div>

                        <div class="form-group">
                            <a href="{{route('front.register')}}" class="primary-btn">Регистрация</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
