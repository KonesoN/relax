@extends('front.partials.app')

@section('content')
    <div class="container">
        <div class="auth-box">
            <div class="auth-box_header">Укажите актуальные данные</div>
            <div class="auth-box_subheader">для регистрации на сервисе</div>

            <div class="auth-box_body">
                <form method="POST" action="{{ route('front.register') }}">
                    <div class="form-group">
                        <label for="name">Имя*</label>

                        <div class="form-group_input-box">
                            <input id="name" type="text" class="form-control"
                                   name="first_name" value="{{!empty($fields['first_name'])?$fields['first_name']:''}}"
                                   required autocomplete="first_name" autofocus>

                            <span class="invalid-feedback" role="alert"
                                  style="{{!empty($errors['first_name'])?'display:block':''}}">
                                        <strong>{{!empty($errors['first_name'])?$errors['first_name']:''}}</strong>
                                    </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Фамилия*</label>

                        <div class="form-group_input-box">
                            <input id="lastname" type="text" class="form-control" name="last_name" required
                                   autocomplete="last_name" autofocus
                                   value="{{!empty($fields['last_name'])?$fields['last_name']:''}}">

                            <span class="invalid-feedback" role="alert"
                                  style="{{!empty($errors['last_name'])?'display:block':''}}">
                                        <strong>{{!empty($errors['last_name'])?$errors['last_name']:''}}</strong>
                                    </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email">Адрес электронной
                            почты*</label>

                        <div class="form-group_input-box">
                            <input id="email" type="email" class="form-control"
                                   value="{{!empty($fields['email'])?$fields['email']:''}}"
                                   name="email" required autocomplete="email">

                            <span class="invalid-feedback" role="alert"
                                  style="{{!empty($errors['email'])?'display:block':''}}">
                                        <strong>{{!empty($errors['email'])?$errors['email']:''}}</strong>
                                    </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password">Пароль</label>

                        <div class="form-group_input-box">
                            <input id="password" type="password"
                                   value="{{!empty($fields['password'])?$fields['password']:''}}"
                                   class="form-control" name="password" required
                                   autocomplete="new-password">

                            <span class="invalid-feedback" role="alert"
                                  style="{{!empty($errors['password'])?'display:block':''}}">
                                        <strong>{{!empty($errors['password'])?$errors['password']:''}}</strong>
                                    </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm">Подтвердите пароль</label>

                        <div class="form-group_input-box">
                            <input id="password-confirm" type="password" class="form-control"
                                   value="{{!empty($fields['password_confirmation'])?$fields['password_confirmation']:''}}"
                                   name="password_confirmation" required autocomplete="new-password">

                            <span class="invalid-feedback" role="alert"
                                  style="{{!empty($errors['password_confirmation'])?'display:block':''}}">
                                        <strong>{{!empty($errors['password_confirmation'])?$errors['password_confirmation']:''}}</strong>
                                    </span>
                        </div>
                    </div>
                    <div class="form-group" style="display: flex; justify-content: space-between">
                        <div>
                            <input type="radio" name="type_id" checked value="{{\App\Models\User::TYPE_USER}}"/>
                            <label>Пользователь</label>
                        </div>
                        <div>
                            <input type="radio" name="type_id" value="{{\App\Models\User::TYPE_OWNER}}"/>
                            <label>Владелец объекта</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="primary-btn">
                            Зарегистрироваться
                        </button>
                    </div>

                    <p class="confirmation_text">Нажимая “Зарегистрироваться” Вы соглашаетесь
                        с <a href="#">Политикой конфиденциальности</a>
                    </p>
                </form>
            </div>
        </div>
    </div>
@endsection
