<div id="open-modal_userSmsNoRegister" class="modal-window" style="display: none">
    <div style="max-width: 350px;">
        <a href="#" title="Close" data-modal_id="open-modal_userSmsNoRegister" class="modal-close modal_hide">Закрыть
            <span>X</span></a>

        <p style="margin-bottom: 25px">Для того что бы написать владельцу объекта - зарегистрируйтесь</p>

        <a href="{{route('front.register')}}" class="subprimary-btn">Регистрация</a>
    </div>
</div>
