<div id="open-modal_cottageReservation" class="modal-window" style="display: none">
    <div style="max-width: 710px;">
        <a href="#" title="Close" data-modal_id="open-modal_cottageReservation" class="modal-close modal_hide">Закрыть
            <span>X</span></a>

        <form class="cottageReservation-container" action="{{route('front.object.booking.new')}}" id="booking_form">
            <input type="hidden" name="cottage_id" value="{{$object->id}}"/>
            <div class="cottageReservation-form">
                <div class="cottageReservation-form_row">
                    <label>
                        Дата заезда:
                        <input type='text' placeholder="--.--.----" required name="arrival_at" readonly
                               id="booking_arrival_at"/>
                    </label>
                    <label>
                        Дата выезда:
                        <input type='text' placeholder="--.--.----" required name="departure_at" readonly
                               id="booking_departure_at"/>
                    </label>
                </div>
                <div class="cottageReservation-form_row">
                    <label>
                        Имя (латиницей)*
                        <input type='text' required name="first_name"/>
                    </label>
                </div>
                <div class="cottageReservation-form_row">
                    <label>
                        Фамилия (латиницей)*
                        <input type='text' required name="last_name"/>
                    </label>
                </div>
                <div class="cottageReservation-form_row">
                    <label>
                        Контактный телефон*
                        <input type='text' required name="phone"/>
                    </label>
                </div>
                <div class="cottageReservation-form_row">
                    <label>
                        Адрес электронной почты*
                        <input type="email" required name="email"/>
                        <small>
                            На этот адрес будет отправлено подтверждение бронирования
                        </small>
                    </label>
                </div>
            </div>
            <div class="calendar">
                <div id="booking_range_at"></div>
                <button class="primary-btn" id="submit_booking">Забронировать</button>
            </div>
        </form>
    </div>
</div>
