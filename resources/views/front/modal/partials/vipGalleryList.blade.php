@foreach(!empty($item->photos)?$item->photos:[] as $photo)
    <div class="popup-gallery-item">
        <a href="#gallery{{$photo->id}}">
            <img src="{{$photo->value}}">
        </a>
        <a href="#" class="lightbox" id="gallery{{$photo->id}}">
            <span style="background-image: url('{{$photo->value}}')"></span>
        </a>
    </div>
@endforeach
