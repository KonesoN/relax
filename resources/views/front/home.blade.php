@extends('front.partials.app')

@section('content')
    <section class="main-display">
        <div class="container">
            <ul class="main-display_menu">
                <li><a class="select_home_type" data-type="cottage"><img src="{{asset('/img/front/cottage.png')}}">
                        <p>Коттеджи</p></a></li>
                <li><a class="select_home_type" data-type="rest"><img src="{{asset('/img/front/cottage.png')}}">
                        <p>Базы отдыха</p></a></li>
                <li><a class="select_home_type" data-type="hotel"><img src="{{asset('/img/front/cottage.png')}}">
                        <p>Загородные отели / гостинницы</p></a></li>
                {{--                <li><a href="#"><img src="{{asset('/img/front/cottage.png')}}">--}}
                {{--                        <p>Оздоровительные центры / пансионаты</p></a></li>--}}
            </ul>
            @if(!empty($metadata['title']) || !empty($metadata['subtitle']))
                <div class="main-display_title">
                    @if(!empty($metadata['title']))
                        <h1>{!! $metadata['title'] !!}</h1>
                    @endif
                    @if(!empty($metadata['subtitle']))
                        <p>
                            {!! $metadata['subtitle'] !!}
                        </p>
                    @endif

                </div>
            @endif

            <div class="main-display_banner-list">
                @foreach($banners as $banner)
                    @php
                        $uo = $banner->userObject;
                    @endphp
                    <a class="banner-block" href="{{route('front.object.view',$uo->id)}}">
                        <img src="{{$uo->MainPhoto->value}}">
                        <p class="obyavlenie-box_info-title">{{$uo->title}}</p>
                        <p class="obyavlenie-box_info-subtitle">{{$uo->address}}</p>
                        <p class="banner-block_price">
                            @if($uo->type == 'cottage')
                                <span>{{$uo->object->price_day}}</span> ₽/сутки
                            @else
                                {!! \App\Models\UserObject::PRICE_TYPES[$uo->type][$uo->price_type_id]['price'] !!}
                            @endif
                        </p>
                    </a>
                @endforeach
            </div>
        </div>
    </section>

    <section class="main-filter">
        <div class="container">
            <div class="main-filter_form">
                <form method="post">
                    <div>
                        <div class="main-filter_form-row">
                            <div class="main-filter_form-col">
                                <div class="name">
                                    <img src="{{asset('/img/front/lupa.png')}}">Поиск
                                </div>
                                <input type="text" class="search" name="search_text"
                                       value="{{!empty($filters['search_text'])?$filters['search_text']:''}}">
                            </div>
                            <div class="main-filter_form-col">
                                <div class="name">
                                    <img src="{{asset('/img/front/map.png')}}">Направление
                                </div>
                                <div class="custom-select">
                                    <select name="direction_id" class="object_filter_city_id">
                                        <option value="">Любое</option>
                                        @foreach(\App\Models\Direction::where('city_id',1)->get() as $d)
                                            <option
                                                    value="{{$d->id}}" {{!empty($filters['direction_id']) && $filters['direction_id'] == $d->id?'selected':''}}
                                            >{{$d->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="main-filter_form-col">
                                <div class="name">
                                    <img src="{{asset('/img/front/cot.png')}}">Тип объекта
                                </div>
                                <div class="custom-select">
                                    <select name="type" id="main_filter_select_type">
                                        <option value="">Любой</option>
                                        @foreach(\App\Models\UserObject::TYPES as $v=>$n)
                                            <option
                                                    value="{{$v}}" {{!empty($filters['type']) && $filters['type'] == $v?'selected':''}}>{{$n}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="main-filter_form-row" id="main_filter_cottage">
                            <div class="main-filter_form-col">
                                <div class="name">
                                    <img src="{{asset('/img/front/calendar.png')}}">Дата заезда
                                </div>
                                <input type="date" name="arrival_at" id="home_arrival_at"
                                       value="{{!empty($filters['arrival_at'])?$filters['arrival_at']:''}}">
                            </div>
                            <div class="main-filter_form-col">
                                <div class="name">
                                    <img src="{{asset('/img/front/calendar.png')}}">Дата выезда
                                </div>
                                <input type="date" name="departure_at" id="home_departure_at"
                                       value="{{!empty($filters['departure_at'])?$filters['departure_at']:''}}">
                            </div>
                            <div class="main-filter_form-col" id="home_filter_count_person">
                                <div class="name">
                                    <img src="{{asset('/img/front/team.png')}}">Количество персон
                                </div>
                                <div class="custom-number">
                                    <span class="input-number-decrement"></span>
                                    <input class="input-number" type="text" min="0" max="10"
                                           name="count_guest"
                                           value="{{!empty($filters['count_guest'])?$filters['count_guest']:'-'}}">
                                    <span class="input-number-increment"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-filter_form-btns">
                        <button class="subprimary-btn" id="home_filter_submit">
                            Применить
                        </button>
                        <a href="{{route('front.index')}}" class="primary-btn" type="reset">
                            Сбросить все
                        </a>
                    </div>
                    <div class="extended_filter">
                        @include('front.partials.objectFilter',['page'=>'home'])
                    </div>
                </form>
                <input type="hidden" value="{{\App\Models\UserObject::TYPE_COTTAGE}}" id="cottage_value">
                @php
                    $count = !empty($filters['filters'])?count($filters['filters']):0;
                @endphp
                <button class="subprimary-btn" id="extended_btn"><img src="{{asset('/img/front/filter.png')}}">Расширенный
                    фильтр {{$count?"(+ $count)":''}}
                </button>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div>
                <div id="map" style="width: 100%; height: 230px; border-radius: 25px; margin-bottom: 60px;"></div>
                <!--<input type="button" value="get location" onclick="getUserLocation()" />
                <div id="locationData">Location data here</div>
                <script src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="list-ad_block">
                @foreach($items['vip'] as $item)
                    @if($item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::SERVICE_SELECTION,'is_active'=>1])->first())
                        @include('front.partials.objectItemVip')
                    @else
                        @include('front.partials.objectItem')
                    @endif
                @endforeach
            </div>
        </div>
        <div class="container">
            <div class="list-ad_block" id="home_list_object">
                @foreach($items['basic'] as $item)
                    @if($item->additions()->where(['addition_id'=>\App\Models\UserObjectAddition::SERVICE_SELECTION,'is_active'=>1])->first())
                        @include('front.partials.objectItemVip')
                    @else
                        @include('front.partials.objectItem')
                    @endif
                @endforeach
            </div>
        </div>
        <div id="show_more" style="opacity: 0;"></div>
        @php
            $query_search = '';
            $count = 0;
            foreach (!empty($filters)?$filters:[] as $id => $value) {
                if (is_array($value)) {
                    foreach ($value as $v){
                        $query_search .= '&'.$id . '[]=' . $v;
                        $count++;
                    }
                } else {
                    $query_search .= '&'.$id . '=' . $value;
                    $count++;
                }
            }
        @endphp
        <input type="hidden" value="{{$query_search}}" id="_api_home_pagination_filters"/>
        <input type="hidden" value="{{$home_ids}}" id="_api_home_pagination_home_ids"/>
    </section>
    @if(!empty($metadata['bottom_text']))
        <section style="margin-top: 75px;">
            <div class="container">
                <div>
                    {!! $metadata['bottom_text'] !!}
                </div>
            </div>
        </section>
    @endif
@endsection
@include('front.modal.vipGallery')
@include('front.modal.objectMap')
