<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mail</title>
    <style type="text/css">
        html {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        .table-mailConfirm {
            width: 690px;
            padding: 0 15px;
            box-sizing: border-box;
        }

        .header {
            margin: 16px 0;
            padding: 0 20px;
        }

        .header .header-logo img {
            width: 215px;
        }

        .header .header-title p {
            font-size: 12px;
            color: #a3a3a3;
            font-family: 'Montserrat';
            text-align: right;
        }

        .body {
            background: url('https://demo.u-host.in/mail-bg.png');
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            padding: 0 10px;
        }

        .body .body-title {
            margin: 50px 0 35px;
            font-family: 'Montserrat';
            font-size: 20px;
            color: #448d2f;
            font-weight: bold;
            display: block;
        }

        .body .body-info {
            border-top: 1px solid #a3a3a3;
            border-bottom: 1px solid #a3a3a3;
            padding: 40px 0;
        }


        .body .body-info .body-info_image {
            display: block;
            padding-right: 20px;
            width: 215px;
            margin: 25px 0;
        }

        .body .body-info .body-info_image img {
            width: 100%;
        }

        .body .body-info .body-info_name {
            margin: 25px 0;
        }

        .body .body-info .body-info_name a {
            display: block;
        }

        .body .body-info_title {
            font-family: 'Montserrat';
            font-size: 19px;
            color: #394e33;
            font-weight: bold;
            text-decoration: none;
        }

        .body .body-info_subtitle {
            font-family: 'Montserrat';
            font-size: 17px;
            color: #a3a3a3;
        }

        .confirmBtn {
            display: block;
            width: 100%;
            height: 70px;
            font-family: 'Montserrat';
            color: #fff;
            font-size: 17px;
            line-height: 70px;
            text-decoration: none;
            white-space: nowrap;
            background: #448d2f;
            border-radius: 5px;
            text-align: center;
            margin: 25px 0;
        }

        .footer span {
            font-family: 'Montserrat';
            font-size: 16px;
            color: #394e33;
        }

        .footer td span:first-of-type {
            font-weight: bold;
        }

        .footer td span.total-price {
            color: #448d2f;
            font-size: 24px;
            font-weight: bold;
        }

        .footer {
            padding-top: 25px;
            padding-bottom: 25px;
        }

        @media screen and (max-width: 575px) {

            .header td,
            .body .body-info td {
                display: block;
            }

            .header .header-logo img {
                margin: 0 auto;
                display: block;
            }

            .header .header-title p {
                text-align: center;
            }

            .body .body-info .body-info_image {
                width: 100%;
            }

            .footer span {
                font-size: 14px;
            }

            .footer td span.total-price {
                font-size: 16px;
            }
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">

<table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-size: 1px; line-height: normal;">
    <tr>
        <td align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table cellpadding="0" border="0" width="660">
                <tr>
                    <td>
            <![endif]-->
            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="table-mailConfirm"
                   style="max-width: 690px; min-width: 320px; width: 100%;">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="header">
                            <tr>
                                <td>
                                    <div class="header-logo">
                                        <a href="" target="_blank">
                                            <img src="https://demo.u-host.in/rusvill_logo.png">
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <div class="header-title">
                                        <p>Служба бронирования Rusvill.ru</p>
                                    </div>
                                </td>
                            </tr>
                        </table>

                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="body">
                            <tr>
                                <td>
                                    <span
                                        class="body-title">Спасибо, {{$item->first_name}}! Подтвердите бронировние!</span>
                                </td>
                            </tr>
                        </table>

                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="body"
                               style="border-top-width: 1px; border-top-style: solid; border-top-color: #a3a3a3; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #a3a3a3;">
                            <tr valign="top" class="body-info">
                                <td>
                                    <a href="" class="body-info_image" target="_blank">
                                        <img src="{{config('app.url') . $uo->photos()->first()->value}}">
                                    </a>
                                </td>
                                <td>
                                    <div class="body-info_name">
                                        <a href="" class="body-info_title" target="_blank">{{$uo->title}}</a>
                                        <span class="body-info_subtitle">{{$uo->address}}</span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        @php
                            $_monthsList = [
                            "1"=>"Январь","2"=>"Февраль","3"=>"Март",
                            "4"=>"Апрель","5"=>"Май", "6"=>"Июнь",
                            "7"=>"Июль","8"=>"Август","9"=>"Сентябрь",
                            "10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь"];
                            $day = Date('d',strtotime($item->arrival_at));
                            $month = $_monthsList[Date('n',strtotime($item->arrival_at))];
                            $year = Date('Y',strtotime($item->arrival_at));
                            $arrival_at = "$day $month $year";

                            $day = Date('d',strtotime($item->departure_at));
                            $month = $_monthsList[Date('n',strtotime($item->departure_at))];
                            $year = Date('Y',strtotime($item->departure_at));
                            $departure_at = "$day $month $year";
                        @endphp
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center"
                               class="body footer">
                            <tr>
                                <td valign="top"><span>Дата заезда:</span></td>
                                <td valign="top"><span>{{$arrival_at}}</span></td>
                            </tr>
                            <tr>
                                <td valign="top"><span>Дата выезда:</span></td>
                                <td valign="top" style="padding-bottom: 25px">
                                    <span>{{$departure_at}}</span></td>
                            </tr>
                            <tr>
                                <td valign="top"><span>Забронировано пользователем:</span></td>
                                <td valign="top" style="padding-bottom: 45px">
                                    <span>{{$item->last_name}} {{$item->first_name}}<br> тел: {{$item->phone}}<br> e-mail: {{$item->email}}</span>
                                </td>
                            </tr>
                            {{--                            <tr>--}}
                            {{--                                <td valign="top"><span>Стоимость проживания:</span></td>--}}
                            {{--                                <td valign="top"><span class="total-price">24 000 руб.</span></td>--}}
                            {{--                            </tr>--}}
                        </table>

                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="body">
                            <tr>
                                <td>
                                    @if($type == 'room')
                                        <a href="{{route('front.hotel.booking.confirm',['hash'=>$item->hash])}}"
                                           target="_blank" class="confirmBtn">Подтвердить
                                            бронь</a>
                                    @elseif($type == 'rest_cottage')
                                        <a href="{{route('front.rest.cottage.booking.confirm',['hash'=>$item->hash])}}"
                                           target="_blank" class="confirmBtn">Подтвердить
                                            бронь</a>
                                    @elseif($type == 'rest_hotel_room')
                                        <a href="{{route('front.rest.hotel.booking.confirm',['hash'=>$item->hash])}}"
                                           target="_blank" class="confirmBtn">Подтвердить
                                            бронь</a>
                                    @else
                                        <a href="{{route('front.object.booking.confirm',['hash'=>$item->hash])}}"
                                           target="_blank" class="confirmBtn">Подтвердить
                                            бронь</a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
