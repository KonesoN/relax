@extends('admin.partials.app')

@section('content')

    <div class="container">
        <div class="admin-wrapper">
            <div class="sidebar">
                @include('admin.partials.sidebar')
            </div>
            <div class="admin-content">

                <div class="admin-content_filter admin-content_filter-replenishment">
                    <form action="#">
                        <div class="admin-content_filter-row">
                            <p>Поиск транзакции по:</p>
                            <div class="admin-content_filter-container">
                                <select name="search_field">
                                    <option value="id">
                                        ID транзакции
                                    </option>
                                    <option value="user_id"
                                        {{!empty($filters['search_field']) && $filters['search_field'] == 'user_id'?'selected':''}}
                                    >ID пользователя
                                    </option>
                                </select>
                                <input placeholder="Введите значение" name="search_text"
                                       value="{{!empty($filters['search_text'])?$filters['search_text']:''}}">
                                <button class="subprimary-btn">Найти</button>
                            </div>
                        </div>
                        <div class="admin-content_filter-row">
                            <p>Способ оплаты:</p>
                            <div class="admin-content_filter-container">
                                <p class="form-radio">
                                    <input type="radio" id="method_all" name="method_id" value="0"
                                        {{empty($filters['method_id'])?'checked':''}}>
                                    <label for="method_all"><img style="width: 24px;"
                                                                 src="{{asset('/img/admin/wallet.png')}}">Все
                                    </label>
                                </p>
                                @foreach(\App\Models\Transaction::METHODS as $id=>$m)
                                    <p class="form-radio">
                                        <input type="radio" id="method_{{$id}}" name="method_id" value="{{$id}}"
                                            {{!empty($filters['method_id']) && $filters['method_id'] == $id?'checked':''}}>
                                        <label for="method_{{$id}}"><img style="width: 24px;"
                                                                         src="{{asset($m['logo'])}}">{{$m['name']}}
                                        </label>
                                    </p>
                                @endforeach
                            </div>
                        </div>
                        <div class="admin-content_filter-row">
                            <p>Дата оплаты:</p>
                            <div class="admin-content_filter-container_date">
                                <input type="date" name="start_at"
                                       value="{{!empty($filters['start_at'])?$filters['start_at']:''}}">
                                <span>-</span>
                                <input type="date" name="end_at"
                                       value="{{!empty($filters['end_at'])?$filters['end_at']:''}}">
                            </div>
                        </div>
                    </form>
                </div>

                <div class="admin-content_list-container">
                    <div class="admin-content_list">
                        <div class="admin-content_list-header">
                            <div class="admin-content_list-row">
                                <ul>
                                    <li>ID</li>
                                    <li>Пользователь</li>
                                    <li>Дата</li>
                                    <li>Тип</li>
                                    <li>Сумма</li>
                                    <li>Способ оплаты</li>
                                    <li>Статус</li>
                                </ul>
                            </div>
                        </div>
                        <div class="admin-content_list-body">
                            @php
                                $total = 0;
                            @endphp
                            @foreach($pagination->items() as $item)
                                @php
                                    $total += $item->amount;
                                @endphp
                                <div class="admin-content_list-row">
                                    <ul>
                                        <li>{{$item->id}}</li>
                                        <li>ID{{$item->user_id}}<br/>{{$item->user->email}}</li>
                                        <li>{{Date('Y-m-d H:i:s',strtotime($item->created_at))}}</li>
                                        <li>
                                            @if($item->comment)
                                                {{$item->comment}}
                                            @else
                                                {{\App\Models\Transaction::TYPES[$item->type_id]}}
                                            @endif
                                        </li>
                                        <li>{{$item->amount}} ₽</li>
                                        <li class="replenishment-list_item">
                                            <div>
                                                <img style="width: 24px"
                                                     src="{{asset(\App\Models\Transaction::METHODS[$item->method_id]['logo'])}}"
                                                >
                                                {{\App\Models\Transaction::METHODS[$item->method_id]['name']}}
                                            </div>
                                        </li>
                                        <li>{{\App\Models\Transaction::STATUSES[$item->status_id]}}</li>
                                    </ul>
                                </div>
                            @endforeach
                            <div class="admin-content_list-row">
                                <ul>
                                    <li></li>
                                    <li class="replenishment-list_item-total">
                                        Всего: {{$total}} ₽
                                    </li>
                                    <li></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @include('admin.partials.pagination')
            </div>
        </div>
    </div>

@endsection
