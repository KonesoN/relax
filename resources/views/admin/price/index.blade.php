@extends('admin.partials.app')

@section('content')

    <div class="container">
        <div class="admin-wrapper">
            <div class="sidebar">
                @include('admin.partials.sidebar')
            </div>
            <div class="admin-content">

                <div class="costPlacement">
                    <p class="title">Базовое размещение :</p>
                    <div class="costPlacement-box">
                        <form method="post">
                            <div class="price">
                                @foreach(\App\Models\UserObject::TYPES as $type=>$name)
                                    <ul>
                                        <li>
                                            <p>{{$name}}</p>
                                        </li>
                                        @foreach(\App\Models\TypeAllocation::where(['allocation_id'=>\App\Models\UserObject::TYPE_ALLOCATION_BASIC,'type'=>$type])->get() as $item)
                                            <li><input value="{{$item->value}}" placeholder="3000"
                                                       name="allocation[{{$item->id}}]"> руб/{{$item->days}}дней
                                            </li>
                                        @endforeach
                                    </ul>
                                @endforeach
                            </div>
                            <button class="subprimary-btn">Сохранить</button>
                        </form>
                    </div>

                    <p class="title">Премиум размещение :</p>
                    <div class="costPlacement-box">
                        <ul class="description">
                            <li>- всегда показывается выше обычных объявлений по любому условию поиска</li>
                            <li>- выделяется цветом</li>
                        </ul>
                        <div class="img-container">
                            <img src="{{asset('/img/admin/item1.png')}}">
                        </div>
                        <form method="post">
                            <div class="price">
                                @foreach(\App\Models\UserObject::TYPES as $type=>$name)
                                    <ul>
                                        <li>
                                            <p>{{$name}}</p>
                                        </li>
                                        @foreach(\App\Models\Addition::where(['addition_id'=>\App\Models\UserObjectAddition::PREMIUM,'type'=>$type])->get() as $item)
                                            <li><input value="{{$item->value}}" placeholder="3000"
                                                       name="addition[{{$item->id}}]"> руб/{{$item->days}}дней
                                            </li>
                                        @endforeach
                                    </ul>
                                @endforeach
                            </div>
                            <button class="subprimary-btn">Сохранить</button>
                        </form>
                    </div>

                    <p class="title">Статус “Выбор сервиса”</p>
                    <div class="costPlacement-box">
                        <ul class="description">
                            <li>- крупное объявление</li>
                            <li>- логотип «Выбор сервиса»</li>
                        </ul>
                        <div class="img-container serviceChoice">
                            <img src="{{asset('/img/admin/item2.png')}}">
                        </div>
                        <form method="post">
                            <div class="price">
                                @foreach(\App\Models\UserObject::TYPES as $type=>$name)
                                    <ul>
                                        <li>
                                            <p>{{$name}}</p>
                                        </li>
                                        @foreach(\App\Models\Addition::where(['addition_id'=>\App\Models\UserObjectAddition::SERVICE_SELECTION,'type'=>$type])->get() as $item)
                                            <li><input value="{{$item->value}}" placeholder="3000"
                                                       name="addition[{{$item->id}}]"> руб/{{$item->days}}дней
                                            </li>
                                        @endforeach
                                    </ul>
                                @endforeach
                            </div>
                            <button class="subprimary-btn">Сохранить</button>
                        </form>
                    </div>
                    <p class="title">Показ на страницах конкурентов по направлению</p>
                    <div class="costPlacement-box">
                        <ul class="description">
                            <li>В карточках объявлений конкурентов есть блок «похожие варианты».</li>
                            <li>В этом блоке будет показываться ваш объект.</li>
                        </ul>
                        <form method="post">
                            <div class="price">

                                @foreach(\App\Models\UserObject::TYPES as $type=>$name)
                                    <ul>
                                        <li>
                                            <p>{{$name}}</p>
                                        </li>
                                        @foreach(\App\Models\Addition::where(['addition_id'=>\App\Models\UserObjectAddition::DISPLAY_COMPETITOR_ONE,'type'=>$type])->get() as $item)
                                            <li><input value="{{$item->value}}" placeholder="3000"
                                                       name="addition[{{$item->id}}]"> руб/{{$item->days}}дней
                                            </li>
                                        @endforeach
                                    </ul>
                                @endforeach
                            </div>
                            <button class="subprimary-btn">Сохранить</button>
                        </form>
                    </div>

                    <p class="title">Показ на страницах конкурентов по всем направлениям</p>
                    <div class="costPlacement-box">
                        <ul class="description">
                            <li>В карточках объявлений конкурентов есть блок «похожие варианты».</li>
                            <li>В этом блоке будет показываться ваш объект.</li>
                        </ul>
                        <form method="post">
                            <div class="price">
                                @foreach(\App\Models\UserObject::TYPES as $type=>$name)
                                    <ul>
                                        <li>
                                            <p>{{$name}}</p>
                                        </li>
                                        @foreach(\App\Models\Addition::where(['addition_id'=>\App\Models\UserObjectAddition::DISPLAY_COMPETITOR_ALL,'type'=>$type])->get() as $item)
                                            <li><input value="{{$item->value}}" placeholder="3000"
                                                       name="addition[{{$item->id}}]"> руб/{{$item->days}}дней
                                            </li>
                                        @endforeach
                                    </ul>
                                @endforeach
                            </div>
                            <button class="subprimary-btn">Сохранить</button>
                        </form>
                    </div>

                    {{--                    <p class="title">Показываться на страницах конкурентов </p>--}}
                    {{--                    <div class="costPlacement-box">--}}
                    {{--                        <p class="description">--}}
                    {{--                            В карточках объявлений конкурентов есть блок «похожие варианты».--}}
                    {{--                            В этом блоке будет показываться ваш объект.--}}
                    {{--                        </p>--}}
                    {{--                        <p class="description">Существует два варианта показов:</p>--}}
                    {{--                        <form>--}}
                    {{--                            <div class="price">--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <p>В одном направлении</p>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li><input value="" placeholder="1500">руб/7дней</li>--}}
                    {{--                                    <li><input value="" placeholder="4000">руб/месяц</li>--}}
                    {{--                                </ul>--}}
                    {{--                                <ul>--}}
                    {{--                                    <li>--}}
                    {{--                                        <p>Во всех направлениях</p>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li><input value="" placeholder="2500">руб/7дней</li>--}}
                    {{--                                    <li><input value="" placeholder="6000">руб/месяц</li>--}}
                    {{--                                </ul>--}}
                    {{--                                <span>--}}
                    {{--( один из параметров фильтров – направление за город.--}}
                    {{--Например, если ваш объект находится в направлении--}}
                    {{--Приозерского шоссе, то при подключении данной опции,--}}
                    {{--он будет показываться в карточках объектов, которые так же--}}
                    {{--расположены в этом направлении)--}}
                    {{--                            </span>--}}
                    {{--                            </div>--}}
                    {{--                            <button  class="subprimary-btn">Сохранить</button>--}}
                    {{--                        </form>--}}
                    {{--                    </div>--}}
                </div>

            </div>
        </div>
    </div>

@endsection
