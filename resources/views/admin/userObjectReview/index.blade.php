@extends('admin.partials.app')

@section('content')

    <div class="container">
        <div class="admin-wrapper">
            <div class="sidebar">
                @include('admin.partials.sidebar')
            </div>
            <div class="admin-content">
                <div class="admin-content_list-container">
                    <div class="admin-content_list">
                        <div class="admin-content_list-header">
                            <div class="admin-content_list-row admin-content_list-rewModer_row">
                                <ul>
                                    <li>User ID</li>
                                    <li>Отзыв</li>
                                    <li>Телефон</li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                        <div class="admin-content_list-body">
                            <div class="admin-content_list-row admin-content_list-rewModer_row">
                                @foreach($pagination->items() as $item)
                                    <form action="{{route('admin.userObjectReview.approve',$item->id)}}">
                                        <ul>
                                            <li>{{$item->user_id}}</li>
                                            <li>
                                                <textarea>{{$item->review}}</textarea>
                                            </li>
                                            <li>{{$item->phone}}</li>
                                            <li>
                                                <button type="submit" class="primary-btn">Одобрить</button>
                                                <a href="{{route('admin.userObjectReview.delete',$item->id)}}"
                                                   type="button" class="subprimary-btn _question_alert">Удалить</a>
                                            </li>
                                        </ul>
                                    </form>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @include('admin.partials.pagination')
            </div>
        </div>
    </div>

@endsection
