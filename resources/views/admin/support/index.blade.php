@extends('admin.partials.app')

@section('content')

    <div class="container">
        <div class="admin-wrapper">
            <div class="sidebar">
                @include('admin.partials.sidebar')
            </div>
            <div class="cabinet-content" style="padding-top: 50px">
                <div class="messages-list">
                    @foreach($pagination->items() as $item)
                        @php
                            $style = '';
                            if($item->support_new_messages){
                                $style = 'background-color: #e8f5ff;';
                            }
                        @endphp
                        <div class="messages-list_item" style="{{$style}}">
                            <div class="info">
                                <div class="info-title">
                                    @if($item->support_new_messages)
                                        <p>Новых сообщений: {{$item->support_new_messages}}</p>
                                    @endif
                                    <p>{{$item->user->first_name}}</p>
                                </div>
                                @php
                                    $last_message = $item->messages()->orderBy('id','desc')->first();
                                @endphp
                                <a href="{{route('admin.support.chat',$item->id)}}">
                                    <p class="info-messages">{{$last_message?$last_message->message:'Сообщений нет'}}</p>
                                </a>
                            </div>
                            @if($last_message)
                                <span class="date">{{Date('Y-m-d H:i',strtotime($last_message->created_at))}}</span>
                            @endif
                        </div>
                    @endforeach
                </div>
                @include('admin.partials.pagination')
            </div>
        </div>
    </div>

@endsection
