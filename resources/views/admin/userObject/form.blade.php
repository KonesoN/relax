@extends('admin.partials.app')

@section('content')
    @php
        $object = !empty($item)?$item->object:null;
    @endphp
    <div class="container">
        <div class="cabinet-wrapper admin-wrapper">
            <div class="sidebar">
                @include('admin.partials.sidebar')
            </div>
            <div class="admin-content">
                <div class="newObject-sidebar">
                    <div class="newObject-nav">
                        <div class="select_tab select_tab-active" data-tab_id="1">Параметры</div>
                        <div class="select_tab" data-tab_id="2">Карточка объекта</div>
                    </div>
                </div>
                <div class="cabinet-content">
                    <form class="add_object" id="newObject-form" method="post">
                        <div id="tab_1" class="tabs tabs-active" style="display: block">
                            @include('front.partials.objectFilter')
                            <button type="submit" class="subprimary-btn">Применить</button>
                        </div>
                        <div id="tab_2" class="tabs">
                            @include('front.cabinet.object.partials.formCardTab')
                            <button type="submit" class="subprimary-btn" data-check_gallery="true">Применить</button>
                        </div>
                        <input value="{{md5(strtotime('now').Auth::id())}}" id="hash_object" name="hash_object"
                               type="hidden"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{$item->type}}" id="item_type"/>
@endsection
