@php
    $object = $item->object;
    $rating = $item->reviews()->avg('rating');
@endphp
<div class="obyavlenie-list_row">
    <div class="obyavlenie-box">
        <div class="obyavlenie-box_img">
            <img src="{{$item->MainPhoto?$item->MainPhoto->value:''}}">
        </div>
        <div class="obyavlenie-box_info">
            <p class="obyavlenie-box_info-user">Пользователь, id: <b>{{$item->user_id}}</b></p>
            <p class="obyavlenie-box_info-user">Объект, id: <b>{{$item->id}}</b></p>
            <p class="obyavlenie-box_info-title">{{$item->title}}</p>
            <p class="obyavlenie-box_info-subtitle">{{$item->address}}</p>

            <div class="obyavlenie-box_info-rating">
                <div class="star-rating">
                    <input type="radio" id="5-stars" name="rating"
                           value="5" {{floor($rating) == 5?'checked':''}}/>
                    <label for="5-stars" class="star">&#9733;</label>
                    <input type="radio" id="4-stars" name="rating"
                           value="4" {{floor($rating) == 4?'checked':''}}/>
                    <label for="4-stars" class="star">&#9733;</label>
                    <input type="radio" id="3-stars" name="rating"
                           value="3" {{floor($rating) == 3?'checked':''}}/>
                    <label for="3-stars" class="star">&#9733;</label>
                    <input type="radio" id="2-stars" name="rating"
                           value="2" {{floor($rating) == 2?'checked':''}}/>
                    <label for="2-stars" class="star">&#9733;</label>
                    <input type="radio" id="1-star" name="rating"
                           value="1" {{floor($rating) == 1?'checked':''}}/>
                    <label for="1-star" class="star">&#9733;</label>
                </div>
                <p>({{$item->reviews()->count()}} отзыва)</p>
            </div>
            <ul class="obyavlenie-box_info-price">

                @if($item->type == 'cottage')
                    <li>Сутки будни <span>{{$object->price_day}}₽</span></li>
                    <li>Сутки выходные <span>{{$object->price_day_weekend}}₽</span></li>
                    <li>Полные выходные (ПТ-ВС) <span>{{$object->price_weekends}}₽</span></li>
                @else
                    <li>
                        Стоимость: {!! \App\Models\UserObject::PRICE_TYPES[$item->type][$item->price_type_id]['price'] !!}</li>
                @endif
            </ul>
        </div>
    </div>
    <div class="obyavlenie-filter">
        <form>
            <div class="obyavlenie-filter_container">
                <p class="form-radio">
                    <input type="checkbox" id="ad-active" name="ad-obyavlenie"
                           disabled {{$item->type_allocation_id == \App\Models\UserObject::TYPE_ALLOCATION_BASIC?'checked':''}}>
                    <label for="ad-active">Базовый</label>
                    @if($item->type_allocation_id == \App\Models\UserObject::TYPE_ALLOCATION_BASIC)
                        <small>{{$item->days}}</small>
                    @endif
                </p>
                @foreach(\App\Models\UserObjectAddition::ADDITIONS as $id=>$addition)
                    <p class="form-radio">
                        <input type="checkbox" id="ad-addition_{{$id}}" name="ad-addition_{{$id}}"
                               disabled {{$item->additions()->where(['addition_id'=>$id,'is_active'=>1])->first()?'checked':''}}>
                        <label for="ad-addition_{{$id}}">{{$addition['name']}}</label>
                        @if($item->additions()->where(['addition_id'=>$id,'is_active'=>1])->first())
                            <small>{{$item->additions()->where(['addition_id'=>$id,'is_active'=>1])->first()->LeftDays}}</small>
                        @endif
                    </p>
                @endforeach
            </div>
            <a href="{{route('admin.userObject.form',$item->id)}}" class="subprimary-btn">Редактировать</a>
        </form>
    </div>
</div>

