@extends('admin.partials.app')

@section('content')

    <div class="container">
        <div class="admin-wrapper">
            <div class="sidebar">
                @include('admin.partials.sidebar')
            </div>
            <div class="admin-content">

                <div class="admin-content_filter">
                    <form>
                        <div class="admin-content_filter-row">
                            <p>Поиск ПО:</p>
                            <div class="admin-content_filter-container">
                                <select name="search_type">
                                    <option value="id">ID объекта</option>
                                    <option
                                        value="user_id" {{!empty($filters['search_type']) && $filters['search_type'] == 'user_id'?'selected':''}}>
                                        ID пользователя
                                    </option>
                                </select>
                                <input placeholder="Введите значение" name="search_text"
                                       value="{{!empty($filters['search_text'])?$filters['search_text']:''}}">
                                <button class="subprimary-btn">Найти</button>
                            </div>
                        </div>
                        <div class="admin-content_filter-row">
                            <p>Статус объявления:</p>
                            <div class="admin-content_filter-container">
                                <p class="form-radio">
                                    <input type="radio" id="all-status" name="status_id" checked value="0"
                                        {{(!empty($filters['status_id']) && !in_array($filters['status_id'], \App\Models\UserObject::STATUS_IDS)) || empty($filters['status_id'])?'checked':''}}
                                    >
                                    <label for="all-status">Все</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="active-status" name="status_id"
                                           {{!empty($filters['status_id']) && $filters['status_id'] == \App\Models\UserObject::STATUS_ACTIVE?'checked':''}}
                                           value="{{\App\Models\UserObject::STATUS_ACTIVE}}">
                                    <label for="active-status">Активный</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="inactive-status" name="status_id"
                                           {{!empty($filters['status_id']) && $filters['status_id'] == \App\Models\UserObject::STATUS_INACTIVE?'checked':''}}
                                           value="{{\App\Models\UserObject::STATUS_INACTIVE}}">
                                    <label for="inactive-status">Не активный</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="archive-status" name="status_id"
                                           {{!empty($filters['status_id']) && $filters['status_id'] == \App\Models\UserObject::STATUS_ARCHIVE?'checked':''}}
                                           value="{{\App\Models\UserObject::STATUS_ARCHIVE}}">
                                    <label for="archive-status">Архив</label>
                                </p>
                            </div>
                        </div>
                        <div class="admin-content_filter-row">
                            <p>Тип объявлений:</p>
                            <div class="admin-content_filter-container">
                                <p class="form-radio">
                                    <input type="radio" id="all-type" name="type" value="all"
                                        {{(!empty($filters['type']) && !in_array($filters['type'], \App\Models\UserObject::TYPES)) || empty($filters['type'])?'checked':''}}
                                    >
                                    <label for="all-type">Все</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="type-cottage" name="type" value="{{\App\Models\UserObject::TYPE_COTTAGE}}"
                                        {{!empty($filters['type']) && $filters['type'] == \App\Models\UserObject::TYPE_COTTAGE?'checked':''}}
                                    >
                                    <label for="type-cottage">Коттеджи</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="type-hotel" name="type" value="{{\App\Models\UserObject::TYPE_HOTEL}}"
                                        {{!empty($filters['type']) && $filters['type'] == \App\Models\UserObject::TYPE_HOTEL?'checked':''}}>
                                    <label for="type-hotel">Отели</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="type-rest" name="type" value="{{\App\Models\UserObject::TYPE_REST}}"
                                        {{!empty($filters['type']) && $filters['type'] == \App\Models\UserObject::TYPE_REST?'checked':''}}>
                                    <label for="type-rest">Базы отдыха</label>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="obyavlenie-list">
                    @foreach($pagination->items() as $item)
                        @include('admin.userObject.partials.objectItem')
                    @endforeach
                </div>
                @include('admin.partials.pagination')
            </div>
        </div>
    </div>

@endsection
