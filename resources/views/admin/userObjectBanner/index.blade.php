@extends('admin.partials.app')

@section('content')

    <div class="container">
        <div class="admin-wrapper">
            <div class="sidebar">
                @include('admin.partials.sidebar')
            </div>
            <div class="admin-content">

                <div class="admin-content_banners">
                    <p class="admin-content_title">Рекламные размещения: </p>

                    <div class="admin-content_banners-list">
                        @include('admin.userObjectBanner.partials.item',['item'=>!empty($items[1])?$items[1]:null,'position'=>1])
                        @include('admin.userObjectBanner.partials.item',['item'=>!empty($items[2])?$items[2]:null,'position'=>2])
                        @include('admin.userObjectBanner.partials.item',['item'=>!empty($items[3])?$items[3]:null,'position'=>3])
                        @include('admin.userObjectBanner.partials.item',['item'=>!empty($items[4])?$items[4]:null,'position'=>4])
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
