@php
    $uo = $item?$item->userObject:null;
@endphp
<div class="admin-content_banners-list_item">
    <p class="admin-content_title">Размещение №{{$position}}: </p>
    <div class="admin-content_banners-list_container">
        @if(!empty($uo))
            <div class="banner-block">
                <img src="{{$uo->MainPhoto?$uo->MainPhoto->value:''}}">
                <p class="obyavlenie-box_info-title">{{$uo->title}}</p>
                <p class="obyavlenie-box_info-subtitle">{{$uo->address}}</p>
                <p class="banner-block_price">
                    @if($uo->type == 'cottage')
                        <span>{{$uo->object->price_day}}</span> ₽/сутки
                    @else
                        {!! \App\Models\UserObject::PRICE_TYPES[$uo->type][$uo->price_type_id]['price'] !!}
                    @endif
                </p>
            </div>
        @endif
        <div class="banner-form">
            <form method="post">
                <input type="hidden" value="{{$position}}" name="position"/>
                <input type="hidden" value="{{!empty($item)?$item->id:''}}" name="banner_id"/>
                <div class="banner-form_row">
                    <input placeholder="Введите id" name="object_id" value="{{!empty($item)?$item->object_id:''}}">
                    <div class="banner-form_btn">
                        <button class="subprimary-btn">Сохранить</button>
                        @if(!empty($item))
                            <a href="{{route('admin.userObjectBanner.delete',$item->id)}}"
                               class="subprimary-btn _question_alert">Удалить</a>
                        @endif
                    </div>
                </div>
                <div class="banner-form_row">
                    <div class="admin-content_filter-container_date">
                        {{--                        <input type="datetime-local" name="start_at"--}}
                        {{--                               value="{{!empty($item)?str_replace(' ','T',Date('Y-m-d H:i:s',strtotime($item->start_at))):''}}">--}}
                        {{--                        <span>-</span>--}}
                        {{--                        <input type="datetime-local" name="end_at"--}}
                        {{--                               value="{{!empty($item)?str_replace(' ','T',Date('Y-m-d H:i:s',strtotime($item->end_at))):''}}">--}}
                        <input class="datepicker-here" data-timepicker="true" data-time-format='hh:ii' name="start_at"
                               value="{{!empty($item)?Date('d.m.Y H:i',strtotime($item->start_at)):''}}"
                        >
                        <span>-</span>
                        <input class="datepicker-here" data-timepicker="true" data-time-format='hh:ii' name="end_at"
                               value="{{!empty($item)?Date('d.m.Y H:i',strtotime($item->end_at)):''}}"
                        >
                    </div>
                </div>
                @if(!empty($item))
                    <div class="banner-form_row">
                        <div class="banner-form_timeLeft">
                            <img src="{{asset('/img/admin/load.png')}}">
                            <p>{{$item->days}}</p>
                        </div>
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>
