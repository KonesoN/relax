<ul class="sidebar-list">
    <li class="sidebar-list_item {{strpos(Route::current()->getName(),'admin.user.')!==false?'sidebar-list_item-active':''}}">
        <a href="{{route('admin.user.index')}}">Пользователи</a></li>
    <li class="sidebar-list_item {{strpos(Route::current()->getName(),'admin.userObject.index')!==false?'sidebar-list_item-active':''}}">
        <a href="{{route('admin.userObject.index')}}">Объявления</a></li>
    <li class="sidebar-list_item {{strpos(Route::current()->getName(),'admin.costPlacement')!==false?'sidebar-list_item-active':''}}">
        <a href="{{route('admin.price.index')}}">Стоимость размещения</a></li>
    <li class="sidebar-list_item {{strpos(Route::current()->getName(),'admin.price.')!==false?'sidebar-list_item-active':''}}">
        <a href="{{route('admin.transaction.index')}}">Транзакции</a></li>
    <li class="sidebar-list_item {{strpos(Route::current()->getName(),'admin.userObjectBanner.index')!==false?'sidebar-list_item-active':''}}">
        <a href="{{route('admin.userObjectBanner.index')}}">Рекламные баннеры</a></li>
    <li class="sidebar-list_item {{strpos(Route::current()->getName(),'admin.userObjectReview.index')!==false?'sidebar-list_item-active':''}}">
        <a href="{{route('admin.userObjectReview.index')}}">Модерация отзывов</a></li>
    <li class="sidebar-list_item {{strpos(Route::current()->getName(),'admin.support.index')!==false?'sidebar-list_item-active':''}}">
        <a href="{{route('admin.support.index')}}">Сообщения</a></li>
</ul>
