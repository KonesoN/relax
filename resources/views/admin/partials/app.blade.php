<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/admin.js') }}" defer></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY63MMSWBJpdwdawp4s5tRcmnl_SjWpus&libraries=places&v=weekly&region=RU&language=ru"
    ></script>
    <!-- Styles -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">

</head>
<body>
<div class="admin_sec cabinet_sec">
    @include('admin.partials.header')
    @yield('content')
    @include('admin.partials.routes')
</div>
</body>
</html>
