<header>
    <div class="container">
        <div class="header-box">
            <div class="header-box_logo">
                <a href="{{route('front.index')}}" class="header-box_logo">
                    <img src="{{asset('/img/front/logo_header.svg')}}">
                </a>
            </div>
            <div class="header-box_menu">
                <div class="header-box_menu-btn">
                    <a href="{{route('front.index')}}" class="primary-btn">На сайт</a>
                </div>
            </div>
            <div class="menu-drop">
                <div class="menu-drop_burger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <ul class="menu-drop_list">
                    <li class="menu-drop_list-item"><a href="{{route('front.index')}}">На сайт</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
