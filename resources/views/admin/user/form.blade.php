@extends('admin.partials.app')

@section('content')

    <div class="container">
        <div class="admin-wrapper">
            <div class="sidebar">
                @include('admin.partials.sidebar')
            </div>
            <div class="admin-content">

                <div class="settings">
                    <form class="user-edit" method="post">
                        <label>
                            <div class="info">
                                <span>Имя</span>
                            </div>
                            <input type="text" required value="{{$item->first_name}}" name="first_name">
                        </label>
                        <label>
                            <div class="info">
                                <span>Фамилия</span>
                            </div>
                            <input type="text" required value="{{$item->last_name}}" name="last_name">
                        </label>
                        <label>
                            <div class="info">
                                <span>Телефон</span>
                            </div>
                            <input type="text" value="{{$item->phone}}" name="phone">
                        </label>
                        <label>
                            <div class="info">
                                <span>Адрес электронной почты </span>
                            </div>
                            <input type="email" required value="{{$item->email}}" name="email">
                        </label>
                        <label>
                            <div class="info">
                                <span>Баланс</span>
                            </div>
                            <input type="number" value="{{$item->balance}}" name="balance">
                        </label>
                        <div class="user-edit_btns">
                            <button class="primary-btn">
                                Сохранить
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
