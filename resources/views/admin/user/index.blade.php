@extends('admin.partials.app')

@section('content')

    <div class="container">
        <div class="admin-wrapper">
            <div class="sidebar">
                @include('admin.partials.sidebar')
            </div>
            <div class="admin-content">

                <div class="admin-content_filter">
                    <form action="#">
                        <div class="admin-content_filter-row">
                            <p>Поиск пользователя по:</p>
                            <div class="admin-content_filter-container">
                                <select name="search_field">
                                    <option value="id">ID пользователя</option>
                                    <option
                                            value="first_name" {{!empty($filters['search_field']) && $filters['search_field'] == 'first_name'?'selected':''}}>
                                        Имя
                                    </option>
                                    <option
                                            value="last_name" {{!empty($filters['search_field']) && $filters['search_field'] == 'last_name'?'selected':''}}>
                                        Фамилия
                                    </option>
                                    <option
                                            value="email" {{!empty($filters['search_field']) && $filters['search_field'] == 'email'?'selected':''}}>
                                        Почта
                                    </option>
                                </select>
                                <input placeholder="Введите значение" name="search_text"
                                       value="{{!empty($filters['search_text'])?$filters['search_text']:''}}">
                                <button class="subprimary-btn">Найти</button>
                            </div>
                        </div>
                        <div class="admin-content_filter-row">
                            <p>Статус пользователя:</p>
                            <div class="admin-content_filter-container">
                                <p class="form-radio">
                                    <input type="radio" id="all-user" name="is_active"
                                           value="" {{empty($filters['is_active'])?'checked':''}}>
                                    <label for="all-user">Все</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="active-user" name="is_active"
                                           value="true" {{!empty($filters) && $filters['is_active'] == 'true'?'checked':''}}>
                                    <label for="active-user">Активный</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="inactive-user" name="is_active"
                                           value="false" {{!empty($filters) && $filters['is_active'] == 'false'?'checked':''}}>
                                    <label for="inactive-user">Заблокированный</label>
                                </p>
                            </div>
                        </div>
                        <div class="admin-content_filter-row">
                            <p>Тип пользователя:</p>
                            <div class="admin-content_filter-container">
                                <p class="form-radio">
                                    <input type="radio" id="all-type" name="type_id"
                                           value="" {{empty($filters['type_id'])?'checked':''}}>
                                    <label for="all-type">Все</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="type_1-user" name="type_id"
                                           value="{{\App\Models\User::TYPE_USER}}" {{!empty($filters) && $filters['type_id'] == 1?'checked':''}}>
                                    <label for="type_1-user">Пользователь</label>
                                </p>
                                <p class="form-radio">
                                    <input type="radio" id="type_2-user" name="type_id"
                                           value="{{\App\Models\User::TYPE_OWNER}}" {{!empty($filters) && $filters['type_id'] == 2?'checked':''}}>
                                    <label for="type_2-user">Владелец</label>
                                </p>
                            </div>
                        </div>
                        {{--                        <div class="admin-content_filter-row">--}}
                        {{--                            <p>Тип объявлений:</p>--}}
                        {{--                            <div class="admin-content_filter-container">--}}
                        {{--                                <p class="form-radio">--}}
                        {{--                                    <input type="radio" id="cottage" name="users-ad_type">--}}
                        {{--                                    <label for="cottage">Коттеджи</label>--}}
                        {{--                                </p>--}}
                        {{--                                <p class="form-radio">--}}
                        {{--                                    <input type="radio" id="hotel" name="users-ad_type">--}}
                        {{--                                    <label for="hotel">Отели</label>--}}
                        {{--                                </p>--}}
                        {{--                                <p class="form-radio">--}}
                        {{--                                    <input type="radio" id="rest" name="users-ad_type">--}}
                        {{--                                    <label for="rest">Базы отдыха</label>--}}
                        {{--                                </p>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </form>
                </div>

                <div class="admin-content_list-container">
                    <div class="admin-content_list">
                        <div class="admin-content_list-header">
                            <div class="admin-content_list-row">
                                <ul>
                                    <li>ID</li>
                                    <li>Имя</li>
                                    <li>Фамилия</li>
                                    <li>E-mail</li>
                                    <li>Телефон</li>
                                    <li>Баланс</li>
                                    <li>Статус</li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                        <div class="admin-content_list-body">
                            @foreach($pagination->items() as $item)
                                <div class="admin-content_list-row">
                                    <ul>
                                        <li>{{$item->id}}</li>
                                        <li>{{$item->first_name}}</li>
                                        <li>{{$item->last_name}}</li>
                                        <li>{{$item->email}}</li>
                                        <li>{{$item->phone}}</li>
                                        <li>{{$item->balance > 0 ? $item->balance : 0}} ₽</li>
                                        <li>{{$item->is_active ? 'Активный' : 'Заблокированный'}}</li>
                                        <li>
                                            <div class="menu-drop">
                                                <div class="menu-drop_burger">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>

                                                <ul class="menu-drop_list">
                                                    @if($item->is_active)
                                                        <li class="menu-drop_list-item"><a
                                                                    href="{{route('admin.user.action',$item->id)}}?action=inactive">Заблокировать</a>
                                                        </li>
                                                    @else
                                                        <li class="menu-drop_list-item"><a
                                                                    href="{{route('admin.user.action',$item->id)}}?action=active">Разблокировать</a>
                                                        </li>
                                                    @endif
                                                        <li class="menu-drop_list-item"><a class="_question_alert"
                                                                href="{{route('admin.user.action',$item->id)}}?action=delete">Удалить</a>
                                                        </li>
                                                    <li class="menu-drop_list-item"><a
                                                                href="{{route('admin.userObject.index')}}?search_type=user_id&search_text={{$item->id}}">Перейти
                                                            к объектам</a></li>
                                                    <li class="menu-drop_list-item"><a href="{{route('admin.user.form',$item->id)}}">Редактировать</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @include('admin.partials.pagination')
            </div>
        </div>
    </div>
@endsection
