<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Direction;
use Illuminate\Database\Seeder;

class DirectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = City::where('short_name', 'СПБ')->first();
        $items = [
            'Выборгское шоссе',
            'Скандинавия',
            'Ленинградское шоссе (Токсово)',
            'Рябовское шоссе (Дорога жизни)',
            'Мурманское шоссе',
            'Петразоводское шоссе (Металлстрой)',
            'Московское шоссе',
            'Пулковское шоссе',
            'Таллинское шоссе',
            'Петергофское шоссе',
            'Ропшинское шоссе',
            'Гостилицкое шоссе',
            'Приморское шоссе (Сосновый Бор)',
            'Кронштадт',
            'Приморское Шоссе (Сестрорецк, Зеленогорск)',
            'Приозерское шосссе',
        ];
        foreach ($items as $item) {
            Direction::firstOrCreate(['city_id' => $city->id, 'name' => $item]);
        }
    }
}
