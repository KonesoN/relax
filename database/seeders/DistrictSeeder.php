<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Direction;
use App\Models\District;
use Illuminate\Database\Seeder;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = City::where('short_name', 'СПБ')->first();
        $items = [
            'Адмиралтейский',
            'Василеостровский',
            'Выборгский',
            'Калининский',
            'Кировский',
            'Колпинский',
            'Красногвардейский',
            'Красносельский',
            'Кронштадтский',
            'Курортный',
            'Московский',
            'Невский',
            'Петроградский',
            'Петродворцовый',
            'Приморский',
            'Пушкинский',
            'Фрунзенский',
            'Центральный',
        ];

        foreach ($items as $item) {
            District::firstOrCreate(['city_id' => $city->id, 'name' => $item]);
        }

        $city = City::where('short_name', 'ЛО')->first();
        $items = [
            'Бокситогорский',
            'Волосовский',
            'Волховский',
            'Всеволожский',
            'Выборгский район (Ленинградской области)',
            'Гатчинский',
            'Кингисепский',
            'Киришский',
            'Кировский район (Ленинградской области)',
            'Лодейнопольский',
            'Ломоносовский ',
            'Лужский',
            'Подпорожский',
            'Приозерский',
            'Сланцевский',
            'Тихвинский',
            'Тосненский',

        ];

        foreach ($items as $item) {
            District::firstOrCreate(['city_id' => $city->id, 'name' => $item]);
        }
    }
}
