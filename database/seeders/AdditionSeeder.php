<?php

namespace Database\Seeders;

use App\Models\Addition;
use App\Models\UserObject;
use App\Models\UserObjectAddition;
use Illuminate\Database\Seeder;

class AdditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            UserObjectAddition::PREMIUM => [
                ['days' => 7, 'value' => 3000],
                ['days' => 30, 'value' => 9000]
            ],
            UserObjectAddition::SERVICE_SELECTION => [
                ['days' => 7, 'value' => 3000],
                ['days' => 30, 'value' => 9000]
            ],
            UserObjectAddition::DISPLAY_COMPETITOR_ONE => [
                ['days' => 7, 'value' => 3000],
                ['days' => 30, 'value' => 9000]
            ],
            UserObjectAddition::DISPLAY_COMPETITOR_ALL => [
                ['days' => 7, 'value' => 3000],
                ['days' => 30, 'value' => 9000]
            ],
        ];
        foreach (UserObject::TYPES as $type => $name) {
            foreach ($items as $id => $values) {
                foreach ($values as $value) {
                    Addition::firstOrCreate([
                        'addition_id' => $id,
                        'days' => $value['days'],
                        'value' => $value['value'],
                        'type' => $type
                    ]);
                }
            }
        }
    }
}
