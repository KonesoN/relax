<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'Санкт-Петербург',
                'short_name' => 'СПБ'
            ],
            [
                'name' => 'Ленинград',
                'short_name' => 'ЛО'
            ]
        ];

        foreach ($items as $item) {
            City::firstOrCreate($item);
        }
    }
}
