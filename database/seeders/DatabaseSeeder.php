<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(CitySeeder::class);
        $this->call(DirectionSeeder::class);
        $this->call(DistrictSeeder::class);
        $this->call(AllocationSeeder::class);
        $this->call(AdditionSeeder::class);
    }
}
