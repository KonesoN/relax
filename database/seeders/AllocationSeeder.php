<?php

namespace Database\Seeders;

use App\Models\TypeAllocation;
use App\Models\UserObject;
use Illuminate\Database\Seeder;

class AllocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            UserObject::TYPE_ALLOCATION_BASIC => [
                ['days' => 7, 'value' => 500],
                ['days' => 30, 'value' => 1500]
            ],
        ];
        foreach (UserObject::TYPES as $type => $name) {
            foreach ($items as $id => $values) {
                foreach ($values as $value) {
                    TypeAllocation::firstOrCreate([
                        'allocation_id' => $id,
                        'days' => $value['days'],
                        'value' => $value['value'],
                        'type' => $type
                    ]);
                }
            }
        }
    }
}
