<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_rooms', function (Blueprint $table) {
            $table->id();
            $table->string('title')->index();
            $table->integer('count_guest')->nullable()->index();
            $table->integer('count_small_bedrooms')->nullable()->index();
            $table->integer('count_big_bedrooms')->nullable()->index();
            $table->integer('price_day')->nullable()->index();
            $table->integer('price_day_weekend')->nullable()->index();
            $table->integer('price_weekends')->nullable()->index();
            $table->text('description')->nullable();
            $table->string('hash')->nullable()->index();
            $table->unsignedBigInteger('hotel_id')->nullable();
            $table->timestamps();

            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_rooms');
    }
}
