<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCottageBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cottage_bookings', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->index();
            $table->string('last_name')->index();
            $table->string('phone')->index();
            $table->string('email')->index();
            $table->string('hash')->nullable()->index();
            $table->date('arrival_at')->index();
            $table->date('departure_at')->index();
            $table->integer('status_id')->default(\App\Models\CottageBooking::STATUS_NEW)->index();
            $table->unsignedBigInteger('cottage_id');
            $table->timestamps();

            $table->foreign('cottage_id')->references('id')->on('cottages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cottage_bookings');
    }
}
