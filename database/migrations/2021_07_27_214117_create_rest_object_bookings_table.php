<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestObjectBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rest_object_bookings', function (Blueprint $table) {
            $table->id();
            $table->date('booking_at')->index();
            $table->string('full_name')->index()->nullable();
            $table->string('phone')->index()->nullable();
            $table->string('email')->index()->nullable();
            $table->unsignedBigInteger('rest_object_id');
            $table->timestamps();

            $table->foreign('rest_object_id')->references('id')->on('rest_objects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rest_object_bookings');
    }
}
