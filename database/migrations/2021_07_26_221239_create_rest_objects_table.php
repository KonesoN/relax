<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rest_objects', function (Blueprint $table) {
            $table->id();
            $table->string('title')->index();
            $table->string('type')->index();
            $table->integer('count_guest')->nullable()->index();
            $table->integer('living_space')->nullable()->index();
            $table->integer('count_bedrooms')->nullable()->index();
            $table->integer('count_small_bedrooms')->nullable()->index();
            $table->integer('count_big_bedrooms')->nullable()->index();
            $table->integer('price_day')->nullable()->index();
            $table->integer('price_day_weekend')->nullable()->index();
            $table->integer('price_weekends')->nullable()->index();
            $table->text('description')->nullable();
            $table->time('arrival_at')->nullable()->index();
            $table->time('departure_at')->nullable()->index();
            $table->integer('minimum_days')->nullable()->index();
            $table->integer('insurance_deposit')->nullable()->index();
            $table->integer('new_reviews')->nullable()->index();
            $table->boolean('is_pets')->default(0)->index();
            $table->string('hash')->nullable()->index();
            $table->unsignedBigInteger('rest_id')->nullable();

            $table->timestamps();

            $table->foreign('rest_id')->references('id')->on('rests')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rest_objects');
    }
}
