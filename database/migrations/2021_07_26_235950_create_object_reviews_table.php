<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjectReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('object_reviews', function (Blueprint $table) {
            $table->id();
            $table->text('review');
            $table->text('answer')->nullable();
            $table->integer('rating')->index();
            $table->string('phone')->index();
            $table->string('status_id')->default(\App\Models\ObjectReview::STATUS_NEW)->index();
            $table->string('parent_type');
            $table->unsignedBigInteger('parent_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('object_reviews');
    }
}
