<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelRoomGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_room_galleries', function (Blueprint $table) {
            $table->id();
            $table->integer('type_id')->default(\App\Models\HotelRoomGallery::TYPE_PHOTO)->nullable();
            $table->text('value');
            $table->string('extension')->nullable()->index();
            $table->string('mime')->nullable()->index();
            $table->string('hash')->nullable()->index();
            $table->string('is_main')->default(0)->index();
            $table->unsignedBigInteger('hotel_room_id')->nullable();
            $table->timestamps();

            $table->foreign('hotel_room_id')->references('id')->on('hotel_rooms')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_room_galleries');
    }
}
