<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjectDialogMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('object_dialog_messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('dialog_id');
            $table->boolean('is_user')->default(1)->index();
            $table->boolean('is_read')->default(0)->index();
            $table->text('message')->nullable();
            $table->timestamps();

            $table->foreign('dialog_id')->references('id')->on('object_dialogs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('object_dialog_messages');
    }
}
