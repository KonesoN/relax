<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCottagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cottages', function (Blueprint $table) {
            $table->id();
            $table->integer('living_space')->nullable()->index();
            $table->integer('count_guest')->nullable()->index();
            $table->integer('count_bedrooms')->nullable()->index();
            $table->integer('count_small_bedrooms')->nullable()->index();
            $table->integer('count_big_bedrooms')->nullable()->index();
            $table->integer('price_day')->nullable()->index();
            $table->integer('price_day_weekend')->nullable()->index();
            $table->integer('price_weekends')->nullable()->index();
            $table->unsignedBigInteger('object_id');

            $table->timestamps();
            $table->foreign('object_id')->references('id')->on('user_objects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cottages');
    }
}
