<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestHotelRoomBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rest_hotel_room_bookings', function (Blueprint $table) {
            $table->id();
            $table->date('booking_at')->index();
            $table->string('full_name')->index()->nullable();
            $table->string('phone')->index()->nullable();
            $table->string('email')->index()->nullable();
            $table->unsignedBigInteger('room_id');
            $table->timestamps();

            $table->foreign('room_id')->references('id')->on('rest_hotel_rooms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rest_hotel_room_bookings');
    }
}
