<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportDialogAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_dialog_attachments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('message_id');
            $table->text('path_file');
            $table->integer('type_id')->default(\App\Models\SupportDialogAttachment::TYPE_PHOTO)->nullable();
            $table->string('extension')->nullable()->index();
            $table->string('mime')->nullable()->index();
            $table->timestamps();

            $table->foreign('message_id')->references('id')->on('support_dialog_messages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_dialog_attachments');
    }
}
