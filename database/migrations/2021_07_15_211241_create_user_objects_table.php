<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_objects', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->integer('status_id')->default(\App\Models\UserObject::STATUS_INACTIVE)->index();
            $table->string('type')->index();
            $table->dateTime('start_at')->nullable()->index();
            $table->dateTime('end_at')->nullable()->index();
            $table->integer('type_allocation_id')->nullable()->index();
            $table->integer('day_allocation')->nullable()->index();
            $table->unsignedBigInteger('direction_id')->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
            $table->string('title')->index();
            $table->text('address');
            $table->integer('distance_to_city')->nullable()->index();
            $table->boolean('is_shore')->default(0)->nullable()->index();
            $table->boolean('is_the_city')->default(0)->index();
            $table->integer('price_type_id')->nullable()->index();
            $table->bigInteger('count_view')->nullable()->index();
            $table->bigInteger('count_view_today')->nullable()->index();
            $table->time('arrival_at')->nullable()->index();
            $table->time('departure_at')->nullable()->index();
            $table->integer('minimum_days')->nullable()->index();
            $table->integer('insurance_deposit')->nullable()->index();
            $table->boolean('is_pets')->default(0)->index();
            $table->integer('new_reviews')->nullable()->index();
            $table->text('description')->nullable();
            $table->string('lat')->nullable()->index();
            $table->string('lng')->nullable()->index();
            $table->timestamps();

            $table->foreign('district_id')->references('id')->on('districts')->onDelete('set null');
            $table->foreign('direction_id')->references('id')->on('directions')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_objects');
    }
}
