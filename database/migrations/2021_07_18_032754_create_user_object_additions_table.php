<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserObjectAdditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_object_additions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('object_id');
            $table->integer('addition_id')->index();
            $table->integer('days')->index();
            $table->boolean('is_active')->default(0)->index();
            $table->dateTime('start_at')->nullable()->index();
            $table->dateTime('end_at')->nullable()->index();
            $table->timestamps();

            $table->foreign('object_id')->references('id')->on('user_objects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_object_additions');
    }
}
