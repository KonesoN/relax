<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestObjectGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rest_object_galleries', function (Blueprint $table) {
            $table->id();
            $table->integer('type_id')->default(\App\Models\RestObjectGallery::TYPE_PHOTO)->nullable();
            $table->text('value');
            $table->string('extension')->nullable()->index();
            $table->string('mime')->nullable()->index();
            $table->string('hash')->nullable()->index();
            $table->string('is_main')->default(0)->index();
            $table->unsignedBigInteger('rest_object_id')->nullable();
            $table->timestamps();

            $table->foreign('rest_object_id')->references('id')->on('rest_objects')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rest_object_galleries');
    }
}
