<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestObjectReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rest_object_reservations', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->index();
            $table->string('last_name')->index();
            $table->string('phone')->index();
            $table->string('email')->index();
            $table->string('hash')->nullable()->index();
            $table->date('arrival_at')->index();
            $table->date('departure_at')->index();
            $table->integer('status_id')->default(\App\Models\RestObjectReservation::STATUS_NEW)->index();
            $table->unsignedBigInteger('rest_object_id');
            $table->timestamps();

            $table->foreign('rest_object_id')->references('id')->on('rest_objects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rest_object_reservations');
    }
}
