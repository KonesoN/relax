<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();


Route::group(['as' => 'admin.', 'prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', function () {
        return redirect(route('admin.user.index'));
    });
    Route::group(['as' => 'user.', 'prefix' => 'user'], function () {
        Route::get('/', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('index');
        Route::get('/{item}/action',
            [App\Http\Controllers\Admin\UserController::class, 'action'])->name('action');
        Route::match(['get', 'post'], '/{item}/form',
            [App\Http\Controllers\Admin\UserController::class, 'form'])->name('form');
    });
    Route::group(['as' => 'transaction.', 'prefix' => 'transaction'], function () {
        Route::get('/', [App\Http\Controllers\Admin\TransactionController::class, 'index'])->name('index');
    });

    Route::group(['as' => 'userObject.', 'prefix' => 'object'], function () {
        Route::get('/', [App\Http\Controllers\Admin\UserObjectController::class, 'index'])->name('index');
        Route::match(['get', 'post'], '/{item}',
            [App\Http\Controllers\Admin\UserObjectController::class, 'form'])->name('form');
    });
    Route::group(['as' => 'userObjectReview.', 'prefix' => 'review'], function () {
        Route::get('/', [App\Http\Controllers\Admin\UserObjectReviewController::class, 'index'])->name('index');
        Route::get('/{item}/approve',
            [App\Http\Controllers\Admin\UserObjectReviewController::class, 'approve'])->name('approve');
        Route::get('/{item}/delete',
            [App\Http\Controllers\Admin\UserObjectReviewController::class, 'delete'])->name('delete');
    });
    Route::group(['as' => 'userObjectBanner.', 'prefix' => 'banner'], function () {
        Route::match(['get', 'post'], '/',
            [App\Http\Controllers\Admin\UserObjectBannerController::class, 'index'])->name('index');
        Route::get('/{item}/delete',
            [App\Http\Controllers\Admin\UserObjectBannerController::class, 'delete'])->name('delete');
    });
    Route::group(['as' => 'price.', 'prefix' => 'price'], function () {
        Route::match(['get', 'post'], '/',
            [App\Http\Controllers\Admin\PricesController::class, 'index'])->name('index');
    });

    Route::group(['as' => 'support.', 'prefix' => 'support'], function () {
        Route::get('/',
            [\App\Http\Controllers\Admin\SupportController::class, 'index'])->name('index');
        Route::get('/messages/{item}',
            [\App\Http\Controllers\Admin\SupportController::class, 'getMessages'])->name('messages');
        Route::match(['get', 'post'], '/chat/{item}',
            [\App\Http\Controllers\Admin\SupportController::class, 'chat'])->name('chat');
        Route::post('/new',
            [\App\Http\Controllers\Admin\SupportController::class, 'new'])->name('new');
    });
});

Route::group(['as' => 'front.'], function () {
    Route::match(['get','post'],'/', [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index');
    Route::get('/snjat_kottedzh_dlja_vecherinki',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_dlja_vecherinki');
    Route::get('/snjat_kottedzh_dlja_svadby',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_dlja_svadby');
    Route::get('/snjat_kottedzh_v_leningradskoj_oblasti_s_banej', [
        App\Http\Controllers\Front\HomeController::class,
        'index'
    ])->name('index.snjat_kottedzh_v_leningradskoj_oblasti_s_banej');
    Route::get('/snjat_kottedzh_v_vyborgskom_rajone',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_v_vyborgskom_rajone');
    Route::get('/snjat_kottedzh_v_vyborge',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_v_vyborge');
    Route::get('/snjat_kottedzh_v_priozerskom_rajone',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_v_priozerskom_rajone');
    Route::get('/snjat_kottedzh_v_pushkine',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_v_pushkine');
    Route::get('/snjat_kottedzh_v_gatchine',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_v_gatchine');
    Route::get('/snjat_kottedzh_v_petergofe',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_v_petergofe');
    Route::get('/snjat_kottedzh_v_tosnenskom_rajone',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_v_tosnenskom_rajone');
    Route::get('/snjat_kottedzh_v_tihvine',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_v_tihvine');
    Route::get('/snjat_kottedzh_v_sosnovo',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_v_sosnovo');
    Route::get('/snjat_kottedzh_na_novyj_god',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snjat_kottedzh_na_novyj_god');
    Route::get('/baza_otdyha_v_leningradskoj_oblasti_u_ozera', [
        App\Http\Controllers\Front\HomeController::class,
        'index'
    ])->name('index.baza_otdyha_v_leningradskoj_oblasti_u_ozera');
    Route::get('/baza_otdyha_s_detmi',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_s_detmi');
    Route::get('/baza_otdyha_v_gatchine',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_v_gatchine');
    Route::get('/baza_baza_otdyha_vyborg',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_baza_otdyha_vyborg');
    Route::get('/baza_otdyha_sosnovyj_bor',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_sosnovyj_bor');
    Route::get('/baza_otdyha_tihvin',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_tihvin');
    Route::get('/baza_otdyha_kingisepp',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_kingisepp');
    Route::get('/baza_otdyha_volhov',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_volhov');
    Route::get('/baza_otdyha_tosno',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_tosno');
    Route::get('/baza_otdyha_lodejnoe_pole',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_lodejnoe_pole');
    Route::get('/baza_otdyha_boksitogorsk',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_boksitogorsk');
    Route::get('/baza_otdyha_shlissel_burg',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_shlissel_burg');

    Route::get('/snyat_kottedzh_posutochno',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snyat_kottedzh_posutochno');
    Route::get('/snyat_kottedzh_s_banej',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snyat_kottedzh_s_banej');
    Route::get('/snyat_kottedzh_s_bassejnom',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snyat_kottedzh_s_bassejnom');
    Route::get('/snyat_kottedzh_v_leningradskoj_oblasti_posutochno',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snyat_kottedzh_v_leningradskoj_oblasti_posutochno');
    Route::get('/snyat_kottedzh_v_leningradskoj_oblasti',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snyat_kottedzh_v_leningradskoj_oblasti');
    Route::get('/snyat_kottedzh_s_bassejnom_v_leningradskoj_oblasti',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.snyat_kottedzh_s_bassejnom_v_leningradskoj_oblasti');


    Route::get('/baza_otdyha_v_leningradkoj_oblasti',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_v_leningradkoj_oblasti');
    Route::get('/baza_otdyha_s_bassejnom',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_s_bassejnom');
    Route::get('/luchshie_bazy_otdyha_v_leningradskoj_oblasti',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.luchshie_bazy_otdyha_v_leningradskoj_oblasti');
    Route::get('/baza_otdyha_spb',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_spb');
    Route::get('/baza_otdyha_v_luge',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_v_luge');
    Route::get('/baza_otdyha_otradnoe',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_otradnoe');
    Route::get('/baza_otdyha_priozersk',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.baza_otdyha_priozersk');

    Route::get('/zagorodnye_oteli_v_leningradskoj_oblasti',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.zagorodnye_oteli_v_leningradskoj_oblasti');
    Route::get('/zagorodnye_oteli_v_spb',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.zagorodnye_oteli_v_spb');
    Route::get('/spa_otdyh_v_leningradskoj_oblasti',
        [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index.spa_otdyh_v_leningradskoj_oblasti');


    Route::get('/home/pagination',
        [App\Http\Controllers\Front\HomeController::class, 'homePagination'])->name('home.pagination');
    Route::match(['get', 'post'], '/login', [App\Http\Controllers\Front\AuthController::class, 'login'])
        ->name('login');
    Route::match(['get', 'post'], '/logout', [App\Http\Controllers\Front\AuthController::class, 'logout'])
        ->name('logout');
    Route::match(['get', 'post'], '/register', [App\Http\Controllers\Front\AuthController::class, 'register'])
        ->name('register');

    Route::group(['as' => 'object.', 'prefix' => 'object'], function () {
        Route::group(['as' => 'booking.', 'prefix' => 'booking'], function () {
            Route::post('/new',
                [\App\Http\Controllers\Front\CottageBookingController::class, 'new'])->name('new');
            Route::get('/confirm/{hash}',
                [\App\Http\Controllers\Front\CottageBookingController::class, 'confirm'])->name('confirm');
        });

        Route::match(['get', 'post'], '/{item}',
            [\App\Http\Controllers\Front\UserObjectController::class, 'view'])->name('view');
        Route::match(['get', 'post'], '/hotel/{item}',
            [\App\Http\Controllers\Front\UserObjectController::class, 'viewRestHotel'])->name('view.hotel');
        Route::match(['get', 'post'], '/cottage/{item}',
            [\App\Http\Controllers\Front\UserObjectController::class, 'viewRestCottage'])->name('view.cottage');
    });

    Route::group(['as' => 'hotel.', 'prefix' => 'hotel'], function () {
        Route::group(['as' => 'booking.', 'prefix' => 'booking'], function () {
            Route::post('/new',
                [\App\Http\Controllers\Front\HotelBookingController::class, 'new'])->name('new');
            Route::get('/confirm/{hash}',
                [\App\Http\Controllers\Front\HotelBookingController::class, 'confirm'])->name('confirm');
        });
    });

    Route::group(['as' => 'rest.', 'prefix' => 'rest'], function () {
        Route::group(['as' => 'hotel.', 'prefix' => 'hotel'], function () {
            Route::group(['as' => 'booking.', 'prefix' => 'booking'], function () {
                Route::post('/new',
                    [\App\Http\Controllers\Front\RestHotelBookingController::class, 'new'])->name('new');
                Route::get('/confirm/{hash}',
                    [\App\Http\Controllers\Front\RestHotelBookingController::class, 'confirm'])->name('confirm');
            });
        });
        Route::group(['as' => 'cottage.', 'prefix' => 'cottage'], function () {
            Route::group(['as' => 'booking.', 'prefix' => 'booking'], function () {
                Route::post('/new',
                    [\App\Http\Controllers\Front\RestObjectBookingController::class, 'new'])->name('new');
                Route::get('/confirm/{hash}',
                    [\App\Http\Controllers\Front\RestObjectBookingController::class, 'confirm'])->name('confirm');
            });
        });
    });

    Route::group(['as' => 'render.', 'prefix' => 'render'], function () {
        Route::match(['get', 'post'], '/object/hotelRoom',
            [\App\Http\Controllers\Front\RenderController::class, 'objectHotelRoom'])->name('objectHotelRoom');
        Route::match(['get', 'post'], '/rest/hotelRoom',
            [\App\Http\Controllers\Front\RenderController::class, 'restHotelRoom'])->name('restHotelRoom');
        Route::match(['get', 'post'], '/object/modalPhotos',
            [\App\Http\Controllers\Front\RenderController::class, 'objectModalPhotos'])->name('objectModalPhotos');
        Route::match(['get', 'post'], '/object/restCottage',
            [
                \App\Http\Controllers\Front\RenderController::class,
                'objectRestCottage'
            ])->name('objectRestCottage');
    });
    Route::group(['as' => 'cabinet.', 'prefix' => 'cabinet', 'middleware' => 'auth'], function () {
        Route::match(['get', 'post'], '/balance',
            [\App\Http\Controllers\Front\BalanceController::class, 'index'])->name('balance');

        Route::group(['as' => 'message.', 'prefix' => 'message'], function () {
            Route::get('/',
                [\App\Http\Controllers\Front\ObjectMessageController::class, 'index'])->name('index');
            Route::get('/messages/{item}',
                [\App\Http\Controllers\Front\ObjectMessageController::class, 'getMessages'])->name('messages');
            Route::match(['get', 'post'], '/chat/{item}',
                [\App\Http\Controllers\Front\ObjectMessageController::class, 'chat'])->name('chat');
            Route::post('/new',
                [\App\Http\Controllers\Front\ObjectMessageController::class, 'new'])->name('new');
        });

        Route::group(['as' => 'support.', 'prefix' => 'support'], function () {
            Route::get('/messages',
                [\App\Http\Controllers\Front\SupportMessageController::class, 'getMessages'])->name('messages');
            Route::match(['get', 'post'], '/chat',
                [\App\Http\Controllers\Front\SupportMessageController::class, 'chat'])->name('chat');
            Route::post('/new',
                [\App\Http\Controllers\Front\SupportMessageController::class, 'new'])->name('new');
        });

        Route::group(['as' => 'setting.', 'prefix' => 'setting'], function () {
            Route::match(['get', 'post'], '/',
                [\App\Http\Controllers\Front\SettingController::class, 'index'])->name('index');
        });

        Route::group(['as' => 'review.', 'prefix' => 'review'], function () {
            Route::get('/',
                [\App\Http\Controllers\Front\ReviewController::class, 'index'])->name('index');
            Route::get('/modal/render/{item}',
                [\App\Http\Controllers\Front\ReviewController::class, 'modalRender'])->name('modal.render');
            Route::get('/answer/{item}',
                [\App\Http\Controllers\Front\ReviewController::class, 'answer'])->name('answer');
            Route::post('/new',
                [\App\Http\Controllers\Front\ReviewController::class, 'new'])->name('new');
        });

        Route::group(['as' => 'favorite.', 'prefix' => 'favorite'], function () {
            Route::get('/',
                [\App\Http\Controllers\Front\FavoriteController::class, 'index'])->name('index');
            Route::get('/handler/{id}',
                [\App\Http\Controllers\Front\FavoriteController::class, 'handler'])->name('handler');
        });

        Route::group(['as' => 'object.', 'prefix' => 'object', 'middleware' => ['isOwner']], function () {
            Route::get('/',
                [\App\Http\Controllers\Front\UserObjectController::class, 'index'])->name('index');
            Route::match(['get', 'post'], '/new',
                [\App\Http\Controllers\Front\UserObjectController::class, 'new'])->name('new');
            Route::post('/uploadPhoto',
                [\App\Http\Controllers\Front\UserObjectController::class, 'uploadPhoto'])->name('uploadPhoto');
            Route::get('/{item}/action',
                [\App\Http\Controllers\Front\UserObjectController::class, 'action'])->name('action');
            Route::match(['get', 'post'], '/{item}/payment',
                [\App\Http\Controllers\Front\UserObjectController::class, 'payment'])->name('payment');
            Route::match(['get', 'post'], '/{item}/addition',
                [\App\Http\Controllers\Front\UserObjectController::class, 'addition'])->name('addition');
            Route::match(['get', 'post'], '/{item}/booking',
                [\App\Http\Controllers\Front\UserObjectController::class, 'booking'])->name('booking');
            Route::match(['get', 'post'], '/{item}/cottage/booking/action',
                [
                    \App\Http\Controllers\Front\UserObjectController::class,
                    'cottageBookingAction'
                ])->name('cottage.booking.action');
            Route::match(['get', 'post'], '/{item}/booking/action',
                [\App\Http\Controllers\Front\UserObjectController::class, 'bookingAction'])->name('booking.action');
            Route::match(['get', 'post'], '/{item}',
                [\App\Http\Controllers\Front\UserObjectController::class, 'edit'])->name('edit');


            Route::match(['get', 'post'], '/{item}/hotel/reservation/action',
                [
                    \App\Http\Controllers\Front\UserObjectController::class,
                    'hotelRoomReservationAction'
                ])->name('hotel.reservation.action');

            Route::match(['get', 'post'], '/{item}/rest/hotel/reservation/action',
                [
                    \App\Http\Controllers\Front\UserObjectController::class,
                    'restHotelReservationAction'
                ])->name('rest.hotel.reservation.action');

            Route::match(['get', 'post'], '/{item}/rest/reservation/action',
                [
                    \App\Http\Controllers\Front\UserObjectController::class,
                    'restReservationAction'
                ])->name('rest.reservation.action');

            Route::match(['get', 'post'], '/{item}/hotel/booking/action',
                [\App\Http\Controllers\Front\UserObjectController::class, 'hotelRoomBookingAction'])
                ->name('hotel.booking.action');

            Route::match(['get', 'post'], '/{item}/rest/booking/action',
                [\App\Http\Controllers\Front\UserObjectController::class, 'restBookingAction'])
                ->name('rest.booking.action');

            Route::match(['get', 'post'], '/{item}/rest/hotel/booking/action',
                [\App\Http\Controllers\Front\UserObjectController::class, 'restHotelBookingAction'])
                ->name('rest.hotel.booking.action');

            Route::get('/hotel/booking/view/{item}',
                [\App\Http\Controllers\Front\UserObjectController::class, 'hotelRoomBookingBody'])
                ->name('hotel.booking.view');

            Route::group(['as' => 'hotel.', 'prefix' => 'hotel'], function () {
                Route::group(['as' => 'room.', 'prefix' => 'room'], function () {
                    Route::post('/form',
                        [\App\Http\Controllers\Front\UserObjectHotelRoomController::class, 'form'])->name('form');
                    Route::post('/uploadPhoto',
                        [
                            \App\Http\Controllers\Front\UserObjectHotelRoomController::class,
                            'uploadPhoto'
                        ])->name('uploadPhoto');
                });
            });

            Route::group(['as' => 'rest.', 'prefix' => 'rest'], function () {
                Route::group(['as' => 'object.', 'prefix' => 'object'], function () {
                    Route::post('/form',
                        [\App\Http\Controllers\Front\UserObjectRestController::class, 'form'])->name('form');
                    Route::post('/uploadPhoto',
                        [
                            \App\Http\Controllers\Front\UserObjectRestController::class,
                            'uploadPhoto'
                        ])->name('uploadPhoto');
                });
                Route::group(['as' => 'room.', 'prefix' => 'room'], function () {
                    Route::post('/form',
                        [\App\Http\Controllers\Front\UserObjectRestController::class, 'roomForm'])->name('form');
                    Route::post('/uploadPhoto',
                        [
                            \App\Http\Controllers\Front\UserObjectRestController::class,
                            'roomUploadPhoto'
                        ])->name('uploadPhoto');
                });
            });
        });
    });
});
